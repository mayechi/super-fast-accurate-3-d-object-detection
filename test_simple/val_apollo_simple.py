import numpy as np
import sys
import random
import os
import torch
import warnings
import pdb
from numba.core.errors import (NumbaDeprecationWarning, 
                               NumbaPendingDeprecationWarning,
                               NumbaPerformanceWarning)
warnings.simplefilter('ignore', category=NumbaDeprecationWarning)
warnings.simplefilter('ignore', category=NumbaPendingDeprecationWarning)
warnings.simplefilter('ignore', category=NumbaPerformanceWarning)
warnings.filterwarnings("ignore", category=UserWarning)
warnings.filterwarnings("ignore", category=UserWarning)
from torch.utils.tensorboard import SummaryWriter
import torch.distributed as dist
import torch.multiprocessing as mp
import torch.utils.data.distributed

from data_process.apollo_dataloader import create_test_dataloader
from utils.model_utils import create_model
from utils.torch_utils import _sigmoid
from utils.evaluation_utils import decode, post_processing
from utils.simple_utils import map_calculate
from utils.bev_map_val import get_map
from config.train_config_apollo import parse_train_configs

def main():
    configs = parse_train_configs()
    main_worker(configs)

def main_worker(configs):
    for i in range(1):
        # model_all_path = './checkpoints_zhijiangyihao_50_no_i/fpn_resnet_50/Model_fpn_resnet_50_epoch_' + str(20+i) + '.pth'
        # print('Model_fpn_resnet_50_epoch_' + str(20+i) + '.pth')
        # model_all_path = './checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_28_0.3.9.pth'
        # model_all_path = './checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_74.pth'
        model_all_path = './checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_105_0.3.15.pth'
        model_all = create_model(configs)
        model_all.load_state_dict(torch.load(model_all_path, map_location=lambda storage, loc: storage))
        model_all.cuda()

        test_dataloader = create_test_dataloader(configs)
        val_loss = validate_all(test_dataloader, model_all, configs)          
        print('val_loss: {:.4e}'.format(val_loss))

def validate_all(test_dataloader, model_all, configs):

    result_dir = './result/zhijiangyihao/'
    # result_dir = './result/apollo/'
    result_1_f = open(result_dir+'1.txt', 'w')
    result_2_f = open(result_dir+'2.txt', 'w')
    result_3_f = open(result_dir+'3.txt', 'w')
    result_4_f = open(result_dir+'4.txt', 'w')
    result_5_f = open(result_dir+'5.txt', 'w')     
    # switch mode
    model_all.eval()

    with torch.no_grad():
        for batch_idx, batch_data in enumerate(test_dataloader):
            metadatas, bev_maps, bev_maps_index = batch_data
            input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float() 
            input_bev_maps_index = bev_maps_index.to(configs.device, non_blocking=True).int()
            # pdb.set_trace()          
            outputs = model_all(input_bev_maps)

            outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
            outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
            # detections size (batch_size, K, 10)
            detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
                                            outputs['dim'], K=50)
            detections = detections.cpu().numpy().astype(np.float32)
            detections_post = post_processing(detections, configs.num_classes, configs.down_ratio, 0.0)

            # 整理reuslt
            # map_calculate(metadatas, detections_post, result_1_f, result_3_f, result_5_f)
            map_calculate(metadatas, detections_post, result_1_f, result_2_f, result_3_f, result_4_f, result_5_f)
            # map_calculate(metadatas, detections_post, result_1_f, result_3_f, None)
    
    result_1_f.close()
    result_2_f.close()
    result_3_f.close()
    result_4_f.close()
    result_5_f.close()
    detpath  = result_dir+'{:s}.txt'
    # annopath = './dataset/zhijiangyihao/training2/pointcloud/test_map/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/training2/pointcloud/ImageSets/val_id.txt' 
    annopath = './dataset/zhijiangyihao/training_fusion/test_map/{:s}.txt'
    imagesetfile = './dataset/zhijiangyihao/training_fusion/test_9+10_shuffle.list' 
    map = get_map(detpath, annopath, imagesetfile)
    return map

if __name__ == '__main__':
    main()

