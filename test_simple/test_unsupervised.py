import numpy as np
import sys
import cv2
import torch
import numba
import math
import pdb
import torch.nn.functional as F

sys.path.append('./')
import fpn_resnet
from torch_utils import _sigmoid

colors = [[0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [255, 120, 120], [0, 120, 0], [120, 255, 255], [120, 0, 255],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],]
data_from = 'apollo'
boundary = {
    "minX": -50,
    "maxX": 50,
    "minY": -25, 
    "maxY": 25,
    "minZ": -2.4,
    "maxZ": 0.6
}
bound_size_x = boundary['maxX'] - boundary['minX']
bound_size_y = boundary['maxY'] - boundary['minY']
bound_size_z = boundary['maxZ'] - boundary['minZ']
BEV_WIDTH = 608
BEV_HEIGHT = 1216
DISCRETIZATION = (boundary["maxX"] - boundary["minX"]) / BEV_HEIGHT
DISCRETIZATION_Y = (boundary["maxX"] - boundary["minX"]) / BEV_HEIGHT
DISCRETIZATION_X = (boundary["maxY"] - boundary["minY"]) / BEV_WIDTH
id_path = '/media/myc/Data21/zhijiangyihao/test/test.list'
model_all_path = './test_simple/Model_fpn_resnet_50_epoch_28_0.3.9.pth'

wirte_path = '/media/myc/Data21/zhijiangyihao/test/unsupervised_test/re/'


def get_lidar(lidar_file):
  return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)

def get_filtered_lidar(sample_path, lidar, boundary, labels=None, rgb=None):
    minX = boundary['minX']
    maxX = boundary['maxX']
    minY = boundary['minY']
    maxY = boundary['maxY']
    minZ = boundary['minZ']
    maxZ = boundary['maxZ']

    # filter master
    minX_my = -3.7
    maxX_my = 0.2
    minY_my = -0.8
    maxY_my = 0.8

    # Remove master
    # if 'S2_1' in sample_path:
    if 1:
        mask = np.where(((lidar[:, 0] <= minX_my) | (lidar[:, 0] >= maxX_my)) |
                        ((lidar[:, 1] <= minY_my) | (lidar[:, 1] >= maxY_my)))
        lidar = lidar[mask]

    # Remove the point out of range x,y,z
    mask = np.where((lidar[:, 0] >= minX) & (lidar[:, 0] <= maxX) &
                    (lidar[:, 1] >= minY) & (lidar[:, 1] <= maxY) &
                    (lidar[:, 2] >= minZ) & (lidar[:, 2] <= maxZ))
    lidar = lidar[mask]
    lidar[:, 2] = lidar[:, 2] - minZ

    
    if labels is not None:
        label_x = (labels[:, 1] >= minX) & (labels[:, 1] < maxX)
        label_y = (labels[:, 2] >= minY) & (labels[:, 2] < maxY)
        label_z = (labels[:, 3] >= minZ) & (labels[:, 3] < maxZ)
        mask_label = label_x & label_y & label_z
        labels = labels[mask_label]
        if rgb is not None:
            rgb = rgb[mask]        
            return lidar, labels, rgb
        else:
            return lidar, labels, None
    else:
        if rgb is not None:
            rgb = rgb[mask] 
            return lidar, rgb     
        else:
            return lidar, None

def makeBEVMap(PointCloud_, boundary, rgb=None):
    Height = BEV_HEIGHT + 1
    Width = BEV_WIDTH + 1

    # Discretize Feature Map
    PointCloud = np.copy(PointCloud_)
    # PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / DISCRETIZATION))
    if data_from == 'kitti':
        # 针对Kitti数据集，只检测正前方
        PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / DISCRETIZATION))
        PointCloud[:, 1] = np.int_(np.floor(PointCloud[:, 1] / DISCRETIZATION) + Width / 2)
    else:
        # 针对Apollo数据集，检测360°
        PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / DISCRETIZATION_Y) + Height / 2)
        PointCloud[:, 1] = np.int_(np.floor(PointCloud[:, 1] / DISCRETIZATION_X) + Width / 2)


    # sort-3times
    indices = np.lexsort((-PointCloud[:, 2], PointCloud[:, 1], PointCloud[:, 0]))
    PointCloud = PointCloud[indices]
    if rgb is not None:
        rgb = rgb[indices]

    # Height Map
    heightMap = np.zeros((Height, Width))

    _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
    PointCloud_frac = PointCloud[indices]
    # some important problem is image coordinate is (y,x), not (x,y)
    max_height = float(np.abs(boundary['maxZ'] - boundary['minZ']))

    heightMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = PointCloud_frac[:, 2] / max_height

    # Intensity Map & DensityMap
    intensityMap = np.zeros((Height, Width))
    densityMap = np.zeros((Height, Width))

    _, indices, counts = np.unique(PointCloud[:, 0:2], axis=0, return_index=True, return_counts=True)
    PointCloud_top = PointCloud[indices]

    normalizedCounts = np.minimum(1.0, np.log(counts + 1) / np.log(64))

    intensityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = PointCloud_top[:, 3] / 255.0 # hesai40p的反射强度0~255
    densityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = normalizedCounts

    if rgb is not None:
        # Height Map
        # ImgMap = np.zeros((Height, Width, 1))
        ImgMap = np.zeros((Height, Width, 3))
        _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
        PointCloud_frac = PointCloud[indices]        
        # rgb_frac = rgb[indices].reshape(-1, 1)
        rgb_frac = rgb[indices].reshape(-1, 3)
        ImgMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = rgb_frac / 255.0
        ImgMap = np.transpose(ImgMap, (2, 0, 1))
        # RGB_Map = np.zeros((3, Height - 1, Width - 1))
        RGB_Map = np.zeros((3, Height - 1, Width - 1))
        
        RGB_Map[0:3, :, :] = ImgMap[:, :BEV_HEIGHT, :BEV_WIDTH]
        # RGB_Map[1, :, :] = ImgMap[:, :BEV_HEIGHT, :BEV_WIDTH]

        # RGB_Map[4, :, :] = densityMap[:BEV_HEIGHT, :BEV_WIDTH]  # r_map
        # RGB_Map[3, :, :] = heightMap[:BEV_HEIGHT, :BEV_WIDTH]  # g_map
        # RGB_Map[0, :, :] = intensityMap[:BEV_HEIGHT, :BEV_WIDTH]  # b_map     
    else:
        if data_from == 'kitti':        
            RGB_Map = np.zeros((3, Height - 1, Width - 1))
            RGB_Map[2, :, :] = densityMap[:BEV_HEIGHT, :BEV_WIDTH]  # r_map
            RGB_Map[1, :, :] = heightMap[:BEV_HEIGHT, :BEV_WIDTH]  # g_map
            RGB_Map[0, :, :] = intensityMap[:BEV_HEIGHT, :BEV_WIDTH]  # b_map
        else:
            RGB_Map = np.zeros((3, Height - 1, Width - 1))
            RGB_Map[2, :, :] = densityMap[:BEV_HEIGHT, :BEV_WIDTH]  # r_map
            RGB_Map[1, :, :] = heightMap[:BEV_HEIGHT, :BEV_WIDTH]  # g_map
            # RGB_Map[0, :, :] = intensityMap[:BEV_HEIGHT, :BEV_WIDTH]  # b_map            

    return RGB_Map

@numba.jit(nopython=True)
def make_bev_voxel_no_i(PointCloud_, rgb=None):
    minX = -50
    maxX = 50
    minY = -25
    maxY = 25
    minZ = -2.28
    maxZ = 0.72

    Height = 1216
    Width = 608
    Deep = 10  
    CLOUD_R = PointCloud_.shape[0]
    bev_map = np.zeros((4, 10, Height, Width))
    
    bev_map_per_index = np.zeros((Height, Width), dtype=np.int32)

    # voxel
    voxel_num = np.zeros((10, Height, Width), dtype=np.int32)
    voxel_all_feature = np.zeros((3, 10, Height, Width))
    voxel_ave_feature = np.zeros((4, 10, Height, Width))

    # 将点云按照z从高到低排序
    indices = np.argsort(-PointCloud_[:, 2])
    PointCloud_ = PointCloud_[indices]  
    range_list = list(range(CLOUD_R)) 
    random_shuffle_list = range_list

    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]
        z_ori = z + minZ

        row = Height * (x - minX) / (maxX - minX)
        col = Width * (y - minY) / (maxY - minY)
        r = int(math.floor(row))
        c = int(math.floor(col))

        deep = Deep * (z_ori - minZ) / (maxZ - minZ)
        d = int(math.floor(deep))

        if r >= Height or c >= Width or d >= Deep:
            continue
        bev_map_per_index[r, c] = 1

        voxel_num[d, r, c] += 1
        voxel_all_feature[0, d, r, c] += x
        voxel_all_feature[1, d, r, c] += y
        voxel_all_feature[2, d, r, c] += z

        voxel_ave_feature[0, d, r, c] = voxel_all_feature[0, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[1, d, r, c] = voxel_all_feature[1, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[2, d, r, c] = voxel_all_feature[2, d, r, c] / voxel_num[d, r, c]

        # add density feature
        voxel_ave_feature[3, d, r, c] = np.minimum(1.0, np.log(voxel_num[d, r, c] + 1) / np.log(64))
    
    # 三个维度，可以翻转
    bev_map = voxel_ave_feature.reshape(-1, Height, Width)
    return bev_map, bev_map_per_index 

def _nms(heat, kernel=3):
    pad = (kernel - 1) // 2
    hmax = F.max_pool2d(heat, (kernel, kernel), stride=1, padding=pad)
    keep = (hmax == heat).float()

    return heat * keep

def _gather_feat(feat, ind, mask=None):
    dim = feat.size(2)
    ind = ind.unsqueeze(2).expand(ind.size(0), ind.size(1), dim)
    feat = feat.gather(1, ind)
    if mask is not None:
        mask = mask.unsqueeze(2).expand_as(feat)
        feat = feat[mask]
        feat = feat.view(-1, dim)
    return feat

def _transpose_and_gather_feat(feat, ind):
    feat = feat.permute(0, 2, 3, 1).contiguous()
    feat = feat.view(feat.size(0), -1, feat.size(3))
    feat = _gather_feat(feat, ind)
    return feat

def _topk(scores, K=40):
    batch, cat, height, width = scores.size()

    topk_scores, topk_inds = torch.topk(scores.view(batch, cat, -1), K)

    topk_inds = topk_inds % (height * width)
    topk_ys = (topk_inds / width).int().float()
    topk_xs = (topk_inds % width).int().float()

    topk_score, topk_ind = torch.topk(topk_scores.view(batch, -1), K)
    topk_clses = (topk_ind / K).int()
    topk_inds = _gather_feat(topk_inds.view(batch, -1, 1), topk_ind).view(batch, K)
    topk_ys = _gather_feat(topk_ys.view(batch, -1, 1), topk_ind).view(batch, K)
    topk_xs = _gather_feat(topk_xs.view(batch, -1, 1), topk_ind).view(batch, K)

    all_scores = scores[:, :, topk_ys.squeeze().long(), topk_xs.squeeze().long()]

    return topk_score, topk_inds, topk_clses, topk_ys, topk_xs, all_scores

def decode(hm_cen, cen_offset, direction, z_coor, dim, K=40):
    batch_size, num_classes, height, width = hm_cen.size()

    hm_cen = _nms(hm_cen)
    scores, inds, clses, ys, xs, all_scores = _topk(hm_cen, K=K)
    if cen_offset is not None:
        cen_offset = _transpose_and_gather_feat(cen_offset, inds)
        cen_offset = cen_offset.view(batch_size, K, 2)
        xs = xs.view(batch_size, K, 1) + cen_offset[:, :, 0:1]
        ys = ys.view(batch_size, K, 1) + cen_offset[:, :, 1:2]
    else:
        xs = xs.view(batch_size, K, 1) + 0.5
        ys = ys.view(batch_size, K, 1) + 0.5

    direction = _transpose_and_gather_feat(direction, inds)
    direction = direction.view(batch_size, K, 2)
    z_coor = _transpose_and_gather_feat(z_coor, inds)
    z_coor = z_coor.view(batch_size, K, 1)
    dim = _transpose_and_gather_feat(dim, inds)
    dim = dim.view(batch_size, K, 3)
    clses = clses.view(batch_size, K, 1).float()
    scores = scores.view(batch_size, K, 1)
    all_scores = all_scores.permute(0, 2, 1)

    detections = torch.cat([scores, xs, ys, z_coor, dim, direction, clses], dim=2)

    return detections, all_scores

def get_yaw(direction):
    return np.arctan2(direction[:, 0:1], direction[:, 1:2])

def post_processing(detections, num_classes=3, down_ratio=4, peak_thresh=0.2):
    """
    :param detections: [batch_size, K, 10]
    # (scores x 1, xs x 1, ys x 1, z_coor x 1, dim x 3, direction x 2, clses x 1)
    # (scores-0:1, xs-1:2, ys-2:3, z_coor-3:4, dim-4:7, direction-7:9, clses-9:10)
    :return:
    """
    # TODO: Need to consider rescale to the original scale: x, y

    # 去除不同类别下，完全重复的检测结果
    # xyz_dim_list = []
    # for i in range(detections[0].shape[0]):
    #     xyz_dim = detections[0, i, 1:-1].tolist()
    #     if xyz_dim in xyz_dim_list:
    #         detections[0, i, 0] = -2
    #     else:
    #         xyz_dim_list.append(xyz_dim)

    ret = []
    for i in range(detections.shape[0]):
        top_preds = {}
        classes = detections[i, :, -1]
        for j in range(num_classes):
            inds = (classes == j)
            # x, y, z, h, w, l, yaw
            top_preds[j] = np.concatenate([
                detections[i, inds, 0:1],
                detections[i, inds, 1:2] * down_ratio,
                detections[i, inds, 2:3] * down_ratio,
                detections[i, inds, 3:4],
                detections[i, inds, 4:5],
                detections[i, inds, 5:6] / bound_size_y * BEV_WIDTH,
                detections[i, inds, 6:7] / bound_size_x * BEV_HEIGHT,
                get_yaw(detections[i, inds, 7:9]).astype(np.float32)], axis=1)
            # Filter by peak_thresh
            if len(top_preds[j]) > 0:
                keep_inds = (top_preds[j][:, 0] > peak_thresh)
                top_preds[j] = top_preds[j][keep_inds]
        ret.append(top_preds)
    
    return ret

# bev image coordinates format
def get_corners(x, y, w, l, yaw):
    bev_corners = np.zeros((4, 2), dtype=np.float32)
    cos_yaw = np.cos(yaw)
    sin_yaw = np.sin(yaw)
    # front left
    bev_corners[0, 0] = x - w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[0, 1] = y - w / 2 * sin_yaw + l / 2 * cos_yaw

    # rear left
    bev_corners[1, 0] = x - w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[1, 1] = y - w / 2 * sin_yaw - l / 2 * cos_yaw

    # rear right
    bev_corners[2, 0] = x + w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[2, 1] = y + w / 2 * sin_yaw - l / 2 * cos_yaw

    # front right
    bev_corners[3, 0] = x + w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[3, 1] = y + w / 2 * sin_yaw + l / 2 * cos_yaw

    return bev_corners

def drawRotatedBox(img, x, y, w, l, yaw, color, c, rect=None, score=1.0):
    font = cv2.FONT_HERSHEY_SIMPLEX
    bev_corners = get_corners(x, y, w, l, yaw)
    corners_int = bev_corners.reshape(-1, 1, 2).astype(int)

    cv2.polylines(img, [corners_int], True, color, 1)
    corners_int = bev_corners.reshape(-1, 2).astype(int)
    cv2.line(img, (corners_int[0, 0], corners_int[0, 1]), (corners_int[3, 0], corners_int[3, 1]), (255, 255, 0), 1)
    center = (int((corners_int[0, 0]+corners_int[2, 0])/2), int((corners_int[0, 1]+corners_int[2, 1])/2))
    cv2.putText(img, str(score)[0:4], (corners_int[0, 0], corners_int[0, 1]), font, 1, (0, 255, 0), 1)
    cv2.putText(img, str(int(c))[0:2], (corners_int[3, 0], corners_int[3, 1]), font, 1, (0, 0, 255), 1)
    if rect is not None:
        pt1_x, pt1_y = int(rect[0]), int(rect[1])
        pt2_x, pt2_y = int(rect[2]), int(rect[3])
        cv2.rectangle(img, (pt1_x, pt1_y), (pt2_x, pt2_y), (255, 255, 0), 1)

def draw_predictions(img, detections, num_classes=3):
    for j in range(num_classes):
        # if j not in [2]:
        #     continue
        if len(detections[j] > 0):
            for det in detections[j]:
                # (scores-0:1, x-1:2, y-2:3, z-3:4, dim-4:7, yaw-7:8)
                _score, _x, _y, _z, _h, _w, _l, _yaw = det
                drawRotatedBox(img, _x, _y, _w, _l, _yaw, colors[int(j)], j, None, _score)

    return img

def convert_det_to_real_values(detections, num_classes=6):
    kitti_dets = []
    for cls_id in range(num_classes):
        if len(detections[cls_id] > 0):
            for det in detections[cls_id]:
                # (scores-0:1, x-1:2, y-2:3, z-3:4, dim-4:7, yaw-7:8)
                _score, _x, _y, _z, _h, _w, _l, _yaw = det
                _yaw = -_yaw
                x = _y / BEV_HEIGHT * bound_size_x + boundary['minX']
                y = _x / BEV_WIDTH * bound_size_y + boundary['minY']
                z = _z + boundary['minZ']
                w = _w / BEV_WIDTH * bound_size_y
                l = _l / BEV_HEIGHT * bound_size_x

                # kitti_dets.append([cls_id, x, y, z, _h, w, l, _yaw])
                kitti_dets.append([cls_id, x, y, z, _h, w, l, _yaw, _score])

    return np.array(kitti_dets)

def show_result_all(model_all):
  	# switch mode
    model_all.eval()
    feature_list = list()
    sample_path_list = [x[0:-1] for x in open(id_path).readlines()][:51]
    for sample_path in sample_path_list:
        # print('sample_path:', sample_path)
        lidarData = get_lidar(sample_path)
        lidarData, _ = get_filtered_lidar(sample_path, lidarData, boundary)
        bev_maps_ori = makeBEVMap(lidarData, boundary)
        bev_maps_ori = torch.from_numpy(bev_maps_ori)
        bev_maps_ori = (bev_maps_ori.permute(1, 2, 0).numpy() * 255).astype(np.uint8)
        # cv2.imshow('bev_maps_ori', bev_maps_ori)
        # cv2.waitKey(0)

        bev_maps, bev_map_per_index = make_bev_voxel_no_i(lidarData)
        bev_maps = torch.from_numpy(bev_maps)  
        bev_maps = bev_maps.view(4, 10, BEV_HEIGHT*BEV_WIDTH)
        bev_map_per_index = bev_map_per_index.reshape(-1)
        with torch.no_grad():
            input_bev_maps = bev_maps.to(0, non_blocking=True).float()
            input_bev_maps = torch.unsqueeze(input_bev_maps, 0)
            feature = model_all(input_bev_maps).squeeze().permute(1, 2, 0).reshape(-1, 64)
            feature_list.append(feature)
    
    for i in range(0, 51, 2):
        rs = np.zeros((608, 304))
        feature_1 = feature_list[i]
        feature_2 = feature_list[i+1]
        # feature_3 = feature_list[i+2]
        cos_12 = F.cosine_similarity(feature_1, feature_2).reshape(608, 304).cpu().numpy()
        # cos_23 = F.cosine_similarity(feature_2, feature_3).reshape(608, 304).cpu().numpy()
        # cos_13 = F.cosine_similarity(feature_1, feature_3).reshape(608, 304).cpu().numpy()
        cos_12_mask = cos_12 < 0.3
        # cos_23_mask = cos_23 < 0.3
        # cos_13_mask = cos_13 < 0.3
        # rs_mask = ~(cos_12_mask & cos_23_mask) & cos_23_mask
        # rs_mask = cos_12_mask & cos_23_mask
        # cos_12 = (1 - cos_12) / 2

        rs_mask = cos_12_mask
        rs[rs_mask] = 1
        # rs = cos_12
        rs = cv2.resize(rs, (608, 1216))
        bev_show = np.zeros((3, 608*2, 304*2))
        bev_show[2, :, :] = (rs[:608*2, :304*2] * 255).astype(np.uint8)
        # bev_show[2, :, :] = rs[:608*2, :304*2] 
        bev_show = bev_show.transpose(1, 2, 0)
        bev_show = cv2.rotate(bev_show, cv2.ROTATE_180)
        # cv2.imshow('bev_show', bev_show)
        # cv2.waitKey(0)
        # cv2.imwrite(wirte_path+str(i+2)+".jpg", bev_show)
        cv2.imwrite(wirte_path+sample_path_list[i+1].split('/')[-1].split('.bin')[0]+".jpg", bev_show)
        

    # # test lidar base
    # sample_path_list = [x[0:-1] for x in open(id_path).readlines()]
    # sample_path = sample_path_list[0]
    # lidarData_base = get_lidar(sample_path)
    # lidarData_base, _ = get_filtered_lidar(sample_path, lidarData_base, boundary)
    # bev_maps, bev_map_per_index = make_bev_voxel_no_i(lidarData_base)
    # bev_maps = torch.from_numpy(bev_maps)
    # bev_maps = bev_maps.view(4, 10, BEV_HEIGHT*BEV_WIDTH)    
    # with torch.no_grad():
    #     input_bev_maps = bev_maps.to(0, non_blocking=True).float()
    #     input_bev_maps = torch.unsqueeze(input_bev_maps, 0)      
    #     feature_base = model_all(input_bev_maps).squeeze().permute(1, 2, 0).reshape(-1, 64)

  
    # # sample_path_list = sample_path_list[1:]
    # # for sample_path in sample_path_list:
    # det_mask_list = []
    # for i in range(len(sample_path_list)-1):
    #     lidarData_1 = get_lidar(sample_path_list[i])
    #     lidarData_2 = get_lidar(sample_path_list[i+1])
    #     lidarData_1, _ = get_filtered_lidar(sample_path_list[i], lidarData_1, boundary)
    #     lidarData_2, _ = get_filtered_lidar(sample_path_list[i+1], lidarData_2, boundary)
    #     bev_maps_1, bev_map_per_index_1 = make_bev_voxel_no_i(lidarData_1)
    #     bev_maps_2, bev_map_per_index_2 = make_bev_voxel_no_i(lidarData_2)
    #     bev_maps_1 = torch.from_numpy(bev_maps_1)
    #     bev_maps_2 = torch.from_numpy(bev_maps_2)
    #     bev_maps_1 = bev_maps_1.view(4, 10, BEV_HEIGHT*BEV_WIDTH)
    #     bev_maps_2 = bev_maps_2.view(4, 10, BEV_HEIGHT*BEV_WIDTH)
    #     bev_map_per_index_1 = bev_map_per_index_1.reshape(-1)
    #     bev_map_per_index_2 = bev_map_per_index_2.reshape(-1)

    #     with torch.no_grad():
    #         input_bev_maps_1 = bev_maps_1.to(0, non_blocking=True).float()
    #         input_bev_maps_2 = bev_maps_2.to(0, non_blocking=True).float()
    #         input_bev_maps_1 = torch.unsqueeze(input_bev_maps_1, 0)
    #         input_bev_maps_2 = torch.unsqueeze(input_bev_maps_2, 0)       
    #         feature_1 = model_all(input_bev_maps_1).squeeze().permute(1, 2, 0).reshape(-1, 64)
    #         feature_2 = model_all(input_bev_maps_2).squeeze().permute(1, 2, 0).reshape(-1, 64)

    #     bev_cos = F.cosine_similarity(feature_1, feature_2).reshape(608, 304).cpu().numpy()
    #     bev_diff = np.zeros((608, 304))
    #     bev_cos_mask = bev_cos < 0.4
    #     if len(det_mask_list) > 0:
    #         det_mask = bev_cos_mask & det_mask_list[i-1]
    #         bev_diff[det_mask] = 1

    #         bev_show = np.zeros((3, 608, 304))
    #         bev_show[2, :, :] = (bev_diff[:608, :304] * 255).astype(np.uint8)
    #         bev_show = bev_show.transpose(1, 2, 0)
    #         bev_show = cv2.rotate(bev_show, cv2.ROTATE_180)
    #         cv2.imwrite(wirte_path+str(i)+".jpg", bev_show)
        
    #     det_mask_list.append(bev_cos_mask)
            # cv2.imshow('result', bev_show)
            # cv2.waitKey(0)    

      # outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
      # outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
      # detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
      # 								outputs['dim'], K=50)
      # detections = detections.cpu().numpy().astype(np.float32)
      # detections_post = post_processing(detections, 6, 2, 0.4)
      # lidar_dets = convert_det_to_real_values(detections_post[0], num_classes=6)

      #       # Draw prediction in the image
      # bev_map = (bev_maps_ori.permute(1, 2, 0).numpy() * 255).astype(np.uint8)
      # bev_map = cv2.resize(bev_map, (608, 1216))
      # bev_map_copy = bev_map.copy()
      # bev_map = draw_predictions(bev_map, detections_post[0], 6)
      # # Rotate the bev_map
      # bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
      # bev_map_copy = cv2.rotate(bev_map_copy, cv2.ROTATE_180)
      # bev_map_copy = cv2.resize(bev_map_copy, (500, 1000))

      # cv2.imshow('oir', bev_map_copy)
      # cv2.imshow('result', bev_map)
      # cv2.waitKey(0)            

def main():
    num_layers = 50
    heads = {
        'hm_cen': 6,
        'cen_offset': 2,
        'direction': 2,
        'z_coor': 1,
        'dim': 3
    }    
    model_all = fpn_resnet.get_pose_net(num_layers=num_layers, heads=heads, head_conv=64,
                                        imagenet_pretrained=True)
    model_all.load_state_dict(torch.load(model_all_path, map_location=lambda storage, loc: storage))
    model_all.cuda(0)
    show_result_all(model_all)

if __name__ == '__main__':
    main()
 