import numpy as np
import sys
import cv2
import os
import torch
import numba
import math
import torch.nn.functional as F

sys.path.append('./')
import fpn_resnet
from utils.torch_utils import _sigmoid

colors = [[0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [255, 120, 120], [0, 120, 0], [120, 255, 255], [120, 0, 255],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],]
data_from = 'apollo'
boundary = {
    "minX": -50,
    "maxX": 50,
    "minY": -25, 
    "maxY": 25,
    "minZ": -2.4-3,
    "maxZ": 0.6+3
}
# boundary = {
#     "minX": -50,
#     "maxX": 50,
#     "minY": -25, 
#     "maxY": 25,
#     "minZ": -2.4,
#     "maxZ": 0.6
# }
# boundary = {
#     "minX": -50,
#     "maxX": 50,
#     "minY": -25, 
#     "maxY": 25,
#     "minZ": -1000,
#     "maxZ": 1000
# }
# # boundary = {
#     "minX": -10,
#     "maxX": 10,
#     "minY": -5, 
#     "maxY": 5,
#     "minZ": -5,
#     "maxZ": 5
# }
bound_size_x = boundary['maxX'] - boundary['minX']
bound_size_y = boundary['maxY'] - boundary['minY']
bound_size_z = boundary['maxZ'] - boundary['minZ']
BEV_WIDTH = 608
BEV_HEIGHT = 1216
DISCRETIZATION = (boundary["maxX"] - boundary["minX"]) / BEV_HEIGHT
DISCRETIZATION_Y = (boundary["maxX"] - boundary["minX"]) / BEV_HEIGHT
DISCRETIZATION_X = (boundary["maxY"] - boundary["minY"]) / BEV_WIDTH
id_path = '/media/myc/Data21/zhijiangyihao/test/test.list'
# model_all_path = './checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_28_0.3.9.pth'
# model_all_path = './checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_80.pth'
# model_all_path = './checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_50.pth'
# model_all_path = './checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_15_0.3.13.pth'
model_all_path = './checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_105_0.3.15.pth'

write_flag = 0
result_dir = "/home/myc/桌面/0916_r/"

# use rear axle coordinate system
# def trans_to_rear(lidar):

def get_lidar(lidar_file):
  return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)

def cal_points_img(lidar_file):
    lidar_time = int(lidar_file.split('.')[0])
    temp = 570 * 1000
    temp = 0
    lidar_time = lidar_time - temp
    diff_min = lidar_time
    diff_min_index = 0
    for i, image_file in enumerate(image_path_list):
        image_time = int(image_file.split('/')[-1].split('.')[0])
        diff = abs(lidar_time - image_time)
        if diff < diff_min:
            diff_min = diff
            diff_min_index = i
    print('diff_min:', diff_min/1000)
    print('lidar_file:', lidar_file)
    if diff_min < 100*1000: 
        return diff_min_index
    else:
        return -1

def get_filtered_lidar(sample_path, lidar, boundary, labels=None, rgb=None):
    minX = boundary['minX']
    maxX = boundary['maxX']
    minY = boundary['minY']
    maxY = boundary['maxY']
    minZ = boundary['minZ']
    maxZ = boundary['maxZ']

    # C3 C4
    # minX_my = -2.5
    # maxX_my = 0.5

    # minY_my = -0.8
    # maxY_my = 0.8

    # filter master
    minX_my = -4.5
    maxX_my = 0.5

    minY_my = -1.0
    maxY_my = 1.3

    # E1 filter master
    # minX_my = 1.0
    # maxX_my = 2.5

    # minY_my = -1.0
    # maxY_my = 1.3

    # Remove master 
    # if 'S2_1' in sample_path:
    if 0:
        mask = np.where(((lidar[:, 0] <= minX_my) | (lidar[:, 0] >= maxX_my)) |
                        ((lidar[:, 1] <= minY_my) | (lidar[:, 1] >= maxY_my)))
        lidar = lidar[mask]

    # Remove the point out of range x,y,z
    mask = np.where((lidar[:, 0] >= minX) & (lidar[:, 0] <= maxX) &
                    (lidar[:, 1] >= minY) & (lidar[:, 1] <= maxY) &
                    (lidar[:, 2] >= minZ) & (lidar[:, 2] <= maxZ))
    lidar = lidar[mask]
    lidar[:, 2] = lidar[:, 2] - minZ

    
    if labels is not None:
        label_x = (labels[:, 1] >= minX) & (labels[:, 1] < maxX)
        label_y = (labels[:, 2] >= minY) & (labels[:, 2] < maxY)
        label_z = (labels[:, 3] >= minZ) & (labels[:, 3] < maxZ)
        mask_label = label_x & label_y & label_z
        labels = labels[mask_label] 
        if rgb is not None:
            rgb = rgb[mask]        
            return lidar, labels, rgb
        else:
            return lidar, labels, None
    else:
        if rgb is not None:
            rgb = rgb[mask] 
            return lidar, rgb     
        else:
            return lidar, None

def makeBEVMap(PointCloud_, boundary, rgb=None):
    Height = BEV_HEIGHT + 1
    Width = BEV_WIDTH + 1

    # Discretize Feature Map
    PointCloud = np.copy(PointCloud_)
    # PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / DISCRETIZATION))
    if data_from == 'kitti':
        # 针对Kitti数据集，只检测正前方
        PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / DISCRETIZATION))
        PointCloud[:, 1] = np.int_(np.floor(PointCloud[:, 1] / DISCRETIZATION) + Width / 2)
    else:
        # 针对Apollo数据集，检测360°
        PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / DISCRETIZATION_Y) + Height / 2)
        PointCloud[:, 1] = np.int_(np.floor(PointCloud[:, 1] / DISCRETIZATION_X) + Width / 2)


    # sort-3times
    indices = np.lexsort((-PointCloud[:, 2], PointCloud[:, 1], PointCloud[:, 0]))
    PointCloud = PointCloud[indices]
    if rgb is not None:
        rgb = rgb[indices]

    # Height Map
    heightMap = np.zeros((Height, Width))

    _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
    PointCloud_frac = PointCloud[indices]
    # some important problem is image coordinate is (y,x), not (x,y)
    max_height = float(np.abs(boundary['maxZ'] - boundary['minZ']))

    heightMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = PointCloud_frac[:, 2] / max_height

    # Intensity Map & DensityMap
    intensityMap = np.zeros((Height, Width))
    densityMap = np.zeros((Height, Width))

    _, indices, counts = np.unique(PointCloud[:, 0:2], axis=0, return_index=True, return_counts=True)
    PointCloud_top = PointCloud[indices]

    normalizedCounts = np.minimum(1.0, np.log(counts + 1) / np.log(64))

    intensityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = PointCloud_top[:, 3] / 255.0 # hesai40p的反射强度0~255
    densityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = normalizedCounts

    if rgb is not None:
        # Height Map
        # ImgMap = np.zeros((Height, Width, 1))
        ImgMap = np.zeros((Height, Width, 3))
        _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
        PointCloud_frac = PointCloud[indices]        
        # rgb_frac = rgb[indices].reshape(-1, 1)
        rgb_frac = rgb[indices].reshape(-1, 3)
        ImgMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = rgb_frac / 255.0
        ImgMap = np.transpose(ImgMap, (2, 0, 1))
        # RGB_Map = np.zeros((3, Height - 1, Width - 1))
        RGB_Map = np.zeros((3, Height - 1, Width - 1))
        
        RGB_Map[0:3, :, :] = ImgMap[:, :BEV_HEIGHT, :BEV_WIDTH]
        # RGB_Map[1, :, :] = ImgMap[:, :BEV_HEIGHT, :BEV_WIDTH]

        # RGB_Map[4, :, :] = densityMap[:BEV_HEIGHT, :BEV_WIDTH]  # r_map
        # RGB_Map[3, :, :] = heightMap[:BEV_HEIGHT, :BEV_WIDTH]  # g_map
        # RGB_Map[0, :, :] = intensityMap[:BEV_HEIGHT, :BEV_WIDTH]  # b_map     
    else:
        if data_from == 'kitti':        
            RGB_Map = np.zeros((3, Height - 1, Width - 1))
            RGB_Map[2, :, :] = densityMap[:BEV_HEIGHT, :BEV_WIDTH]  # r_map
            RGB_Map[1, :, :] = heightMap[:BEV_HEIGHT, :BEV_WIDTH]  # g_map
            RGB_Map[0, :, :] = intensityMap[:BEV_HEIGHT, :BEV_WIDTH]  # b_map
        else:
            RGB_Map = np.zeros((3, Height - 1, Width - 1))
            RGB_Map[2, :, :] = densityMap[:BEV_HEIGHT, :BEV_WIDTH]  # r_map
            RGB_Map[1, :, :] = heightMap[:BEV_HEIGHT, :BEV_WIDTH]  # g_map
            # RGB_Map[0, :, :] = intensityMap[:BEV_HEIGHT, :BEV_WIDTH]  # b_map            

    return RGB_Map

@numba.jit(nopython=True)
def make_bev_voxel_no_i(PointCloud_, rgb=None):
    minX = -50
    maxX = 50
    minY = -25
    maxY = 25
    minZ = -2.4-3
    maxZ = 0.6+3

    # minX = -50
    # maxX = 50
    # minY = -25
    # maxY = 25
    # minZ = -2.4
    # maxZ = 0.6

    Height = 1216
    Width = 608
    Deep = 10  
    CLOUD_R = PointCloud_.shape[0]
    bev_map = np.zeros((4, 10, Height, Width))
    
    bev_map_per_index = np.zeros((Height, Width), dtype=np.int32)

    # voxel
    voxel_num = np.zeros((10, Height, Width), dtype=np.int32)
    voxel_all_feature = np.zeros((3, 10, Height, Width))
    voxel_ave_feature = np.zeros((4, 10, Height, Width))

    # 将点云按照z从高到低排序
    indices = np.argsort(-PointCloud_[:, 2])
    PointCloud_ = PointCloud_[indices]  
    range_list = list(range(CLOUD_R)) 
    random_shuffle_list = range_list

    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]
        z_ori = z + minZ

        row = Height * (x - minX) / (maxX - minX)
        col = Width * (y - minY) / (maxY - minY)
        r = int(math.floor(row))
        c = int(math.floor(col))

        deep = Deep * (z_ori - minZ) / (maxZ - minZ)
        d = int(math.floor(deep))

        if r >= Height or c >= Width or d >= Deep:
            continue
        bev_map_per_index[r, c] = 1

        voxel_num[d, r, c] += 1
        voxel_all_feature[0, d, r, c] += x
        voxel_all_feature[1, d, r, c] += y
        # voxel_all_feature[2, d, r, c] += z
        voxel_all_feature[2, d, r, c] += z_ori

        voxel_ave_feature[0, d, r, c] = voxel_all_feature[0, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[1, d, r, c] = voxel_all_feature[1, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[2, d, r, c] = voxel_all_feature[2, d, r, c] / voxel_num[d, r, c]

        # add density feature
        voxel_ave_feature[3, d, r, c] = np.minimum(1.0, np.log(voxel_num[d, r, c] + 1) / np.log(64))
    
    # 三个维度，可以翻转
    bev_map = voxel_ave_feature.reshape(-1, Height, Width)
    return bev_map, bev_map_per_index 

# @numba.jit(nopython=True)
def make_bev_voxel_relative_height_no_i(PointCloud_, rgb=None):
    minX = -50
    maxX = 50
    minY = -25
    maxY = 25

    Height = 1216
    Width = 608
    Deep = 10  
    CLOUD_R = PointCloud_.shape[0]
    bev_map = np.zeros((4, 10, Height, Width))
    
    bev_map_per_index = np.zeros((Height, Width), dtype=np.int32)

    # voxel
    voxel_num = np.zeros((10, Height, Width), dtype=np.int32)
    voxel_all_feature = np.zeros((3, 10, Height, Width))
    voxel_ave_feature = np.zeros((4, 10, Height, Width))

    # 将点云按照z从高到低排序
    indices = np.argsort(-PointCloud_[:, 2])
    PointCloud_ = PointCloud_[indices]  
    range_list = list(range(CLOUD_R)) 
    random_shuffle_list = range_list

    bev_map_min_z = np.ones((Height, Width), dtype=np.float) * 1000
    # bev_map_max_z = np.ones((Height, Width), dtype=np.float) * (-1000)
    # # Calculate the smallest z for each pillar
    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]

        row = Height * (x - minX) / (maxX - minX)
        col = Width * (y - minY) / (maxY - minY)
        r = int(math.floor(row))
        c = int(math.floor(col))
        if r >= Height or c >= Width:
            continue
        
        if z < bev_map_min_z[r, c]:
            bev_map_min_z[r, c] = z
        # if z > bev_map_max_z[r, c]:
        #     bev_map_max_z[r, c] = z

    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]

        row = Height * (x - minX) / (maxX - minX)
        col = Width * (y - minY) / (maxY - minY)
        r = int(math.floor(row))
        c = int(math.floor(col))

        if r >= Height or c >= Width:
            continue
        if bev_map_min_z[r, c] >= 1000 or z - bev_map_min_z[r, c] > 4.0:
            continue

        z_relative = z - bev_map_min_z[r, c]
        deep = Deep * (z - bev_map_min_z[r, c]) / 4.0
        d = int(math.floor(deep))

        if d >= Deep:
            continue

        bev_map_per_index[r, c] = 1

        voxel_num[d, r, c] += 1
        voxel_all_feature[0, d, r, c] += x
        voxel_all_feature[1, d, r, c] += y
        voxel_all_feature[2, d, r, c] += z_relative

        voxel_ave_feature[0, d, r, c] = voxel_all_feature[0, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[1, d, r, c] = voxel_all_feature[1, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[2, d, r, c] = voxel_all_feature[2, d, r, c] / voxel_num[d, r, c]

        # add density feature
        voxel_ave_feature[3, d, r, c] = np.minimum(1.0, np.log(voxel_num[d, r, c] + 1) / np.log(64))
    
    # 三个维度，可以翻转
    bev_map = voxel_ave_feature.reshape(-1, Height, Width)
    return bev_map, bev_map_per_index 

def _nms(heat, kernel=3):
    pad = (kernel - 1) // 2
    hmax = F.max_pool2d(heat, (kernel, kernel), stride=1, padding=pad)
    keep = (hmax == heat).float()

    return heat * keep

def _gather_feat(feat, ind, mask=None):
    dim = feat.size(2)
    ind = ind.unsqueeze(2).expand(ind.size(0), ind.size(1), dim)
    feat = feat.gather(1, ind)
    if mask is not None:
        mask = mask.unsqueeze(2).expand_as(feat)
        feat = feat[mask]
        feat = feat.view(-1, dim)
    return feat

def _transpose_and_gather_feat(feat, ind):
    feat = feat.permute(0, 2, 3, 1).contiguous()
    feat = feat.view(feat.size(0), -1, feat.size(3))
    feat = _gather_feat(feat, ind)
    return feat

def _topk(scores, K=40):
    batch, cat, height, width = scores.size()

    topk_scores, topk_inds = torch.topk(scores.view(batch, cat, -1), K)

    topk_inds = topk_inds % (height * width)
    topk_ys = (topk_inds / width).int().float()
    topk_xs = (topk_inds % width).int().float()

    topk_score, topk_ind = torch.topk(topk_scores.view(batch, -1), K)
    topk_clses = (topk_ind / K).int()
    topk_inds = _gather_feat(topk_inds.view(batch, -1, 1), topk_ind).view(batch, K)
    topk_ys = _gather_feat(topk_ys.view(batch, -1, 1), topk_ind).view(batch, K)
    topk_xs = _gather_feat(topk_xs.view(batch, -1, 1), topk_ind).view(batch, K)

    all_scores = scores[:, :, topk_ys.squeeze().long(), topk_xs.squeeze().long()]

    return topk_score, topk_inds, topk_clses, topk_ys, topk_xs, all_scores

def decode(hm_cen, cen_offset, direction, z_coor, dim, K=40):
    batch_size, num_classes, height, width = hm_cen.size()

    hm_cen = _nms(hm_cen)
    scores, inds, clses, ys, xs, all_scores = _topk(hm_cen, K=K)
    if cen_offset is not None:
        cen_offset = _transpose_and_gather_feat(cen_offset, inds)
        cen_offset = cen_offset.view(batch_size, K, 2)
        xs = xs.view(batch_size, K, 1) + cen_offset[:, :, 0:1]
        ys = ys.view(batch_size, K, 1) + cen_offset[:, :, 1:2]
    else:
        xs = xs.view(batch_size, K, 1) + 0.5
        ys = ys.view(batch_size, K, 1) + 0.5

    direction = _transpose_and_gather_feat(direction, inds)
    direction = direction.view(batch_size, K, 2)
    z_coor = _transpose_and_gather_feat(z_coor, inds)
    z_coor = z_coor.view(batch_size, K, 1)
    dim = _transpose_and_gather_feat(dim, inds)
    dim = dim.view(batch_size, K, 3)
    clses = clses.view(batch_size, K, 1).float()
    scores = scores.view(batch_size, K, 1)
    all_scores = all_scores.permute(0, 2, 1)

    detections = torch.cat([scores, xs, ys, z_coor, dim, direction, clses], dim=2)

    return detections, all_scores

def get_yaw(direction):
    return np.arctan2(direction[:, 0:1], direction[:, 1:2])

def post_processing(detections, num_classes=3, down_ratio=4, peak_thresh=0.2):
    """
    :param detections: [batch_size, K, 10]
    # (scores x 1, xs x 1, ys x 1, z_coor x 1, dim x 3, direction x 2, clses x 1)
    # (scores-0:1, xs-1:2, ys-2:3, z_coor-3:4, dim-4:7, direction-7:9, clses-9:10)
    :return:
    """
    # TODO: Need to consider rescale to the original scale: x, y

    # 去除不同类别下，完全重复的检测结果
    # xyz_dim_list = []
    # for i in range(detections[0].shape[0]):
    #     xyz_dim = detections[0, i, 1:-1].tolist()
    #     if xyz_dim in xyz_dim_list:
    #         detections[0, i, 0] = -2
    #     else:
    #         xyz_dim_list.append(xyz_dim)

    ret = []
    for i in range(detections.shape[0]):
        top_preds = {}
        classes = detections[i, :, -1]
        for j in range(num_classes):
            inds = (classes == j)
            # x, y, z, h, w, l, yaw
            top_preds[j] = np.concatenate([
                detections[i, inds, 0:1],
                detections[i, inds, 1:2] * down_ratio,
                detections[i, inds, 2:3] * down_ratio,
                detections[i, inds, 3:4],
                detections[i, inds, 4:5],
                detections[i, inds, 5:6] / bound_size_y * BEV_WIDTH,
                detections[i, inds, 6:7] / bound_size_x * BEV_HEIGHT,
                get_yaw(detections[i, inds, 7:9]).astype(np.float32)], axis=1)
            # Filter by peak_thresh
            if len(top_preds[j]) > 0:
                keep_inds = (top_preds[j][:, 0] > peak_thresh)
                top_preds[j] = top_preds[j][keep_inds]
        ret.append(top_preds)
    
    return ret

# bev image coordinates format
def get_corners(x, y, w, l, yaw):
    bev_corners = np.zeros((4, 2), dtype=np.float32)
    cos_yaw = np.cos(yaw)
    sin_yaw = np.sin(yaw)
    # front left
    bev_corners[0, 0] = x - w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[0, 1] = y - w / 2 * sin_yaw + l / 2 * cos_yaw

    # rear left
    bev_corners[1, 0] = x - w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[1, 1] = y - w / 2 * sin_yaw - l / 2 * cos_yaw

    # rear right
    bev_corners[2, 0] = x + w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[2, 1] = y + w / 2 * sin_yaw - l / 2 * cos_yaw

    # front right
    bev_corners[3, 0] = x + w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[3, 1] = y + w / 2 * sin_yaw + l / 2 * cos_yaw

    return bev_corners

def drawRotatedBox(img, x, y, w, l, yaw, color, c, rect=None, score=1.0):
    font = cv2.FONT_HERSHEY_SIMPLEX
    bev_corners = get_corners(x, y, w, l, yaw)
    corners_int = bev_corners.reshape(-1, 1, 2).astype(int)

    cv2.polylines(img, [corners_int], True, color, 1)
    corners_int = bev_corners.reshape(-1, 2).astype(int)
    cv2.line(img, (corners_int[0, 0], corners_int[0, 1]), (corners_int[3, 0], corners_int[3, 1]), (255, 255, 0), 1)
    center = (int((corners_int[0, 0]+corners_int[2, 0])/2), int((corners_int[0, 1]+corners_int[2, 1])/2))
    cv2.putText(img, str(score)[0:4], (corners_int[0, 0], corners_int[0, 1]), font, 1, (0, 255, 0), 1)
    cv2.putText(img, str(int(c))[0:2], (corners_int[3, 0], corners_int[3, 1]), font, 1, (0, 0, 255), 1)
    if rect is not None:
        pt1_x, pt1_y = int(rect[0]), int(rect[1])
        pt2_x, pt2_y = int(rect[2]), int(rect[3])
        cv2.rectangle(img, (pt1_x, pt1_y), (pt2_x, pt2_y), (255, 255, 0), 1)

def draw_predictions(img, detections, num_classes=3):
    for j in range(num_classes):
        # if j not in [2]:
        #     continue       
        if len(detections[j] > 0):
            for det in detections[j]:
                # (scores-0:1, x-1:2, y-2:3, z-3:4, dim-4:7, yaw-7:8)
                _score, _x, _y, _z, _h, _w, _l, _yaw = det
                drawRotatedBox(img, _x, _y, _w, _l, _yaw, colors[int(j)], j, None, _score)

    return img

def convert_det_to_real_values(detections, num_classes=6):
    kitti_dets = []
    for cls_id in range(num_classes):
        if len(detections[cls_id] > 0):
            for det in detections[cls_id]:
                # (scores-0:1, x-1:2, y-2:3, z-3:4, dim-4:7, yaw-7:8)
                _score, _x, _y, _z, _h, _w, _l, _yaw = det
                _yaw = -_yaw
                x = _y / BEV_HEIGHT * bound_size_x + boundary['minX']
                y = _x / BEV_WIDTH * bound_size_y + boundary['minY']
                z = _z + boundary['minZ']
                w = _w / BEV_WIDTH * bound_size_y
                l = _l / BEV_HEIGHT * bound_size_x

                # kitti_dets.append([cls_id, x, y, z, _h, w, l, _yaw])
                kitti_dets.append([cls_id, x, y, z, _h, w, l, _yaw, _score])

    return np.array(kitti_dets)

def show_result_all(model_all):
    write_dir_path = "/media/myc/Data21/zhijiangyihao/test/unsupervised_test/det/"
    # write_dir_path = "/media/myc/Data21/zhijiangyihao/test/write/"
    # switch mode
    model_all.eval()

    # sample_path_list = [x[0:-1] for x in open(id_path).readlines()]
    # sample_path_list = sample_path_list[0:]
    sample_path_list = data_path_list
    for i, sample_path in enumerate(sample_path_list):
        print(sample_path)
        lidarData = get_lidar(sample_path)
        # 针对于几何C, 需要把x=y, y=-x
        # vector_x = np.copy(lidarData[:, 0])
        # vector_y =  np.copy(lidarData[:, 1])
        # lidarData[:, 0] = vector_y
        # lidarData[:, 1] = -vector_x       
        # lidarData[:, 0] = lidarData[:, 1]
        # lidarData[:, 1] = -lidarData[:, 0]
        lidarData, _ = get_filtered_lidar(sample_path, lidarData, boundary)
        # if lidarData.shape[0] < 30000:
        #     continue
        bev_maps_ori = makeBEVMap(lidarData, boundary)
        bev_maps_ori = torch.from_numpy(bev_maps_ori)

        bev_maps, bev_map_per_index = make_bev_voxel_no_i(lidarData)
        # bev_maps, bev_map_per_index = make_bev_voxel_relative_height_no_i(lidarData)

        # print("bev_map_per_index.sum(): ", bev_map_per_index.sum())
        bev_maps = torch.from_numpy(bev_maps)

        bev_maps = bev_maps.view(4, 10, BEV_HEIGHT*BEV_WIDTH)
        bev_map_per_index = bev_map_per_index.reshape(-1)

        # if 'apollo' in sample_path:
        # 	frame_id = sample_path.split('/')[-2] + '_' + sample_path.split('/')[-1].split('.bin')[0]
        # else:
        # 	frame_id = sample_path.split('/')[-1].split('.bin')[0]

        frame_id = sample_path.split('/')[-1].split('.bin')[0]
        
        metadatas = {
            'frame_id': frame_id
        }

        with torch.no_grad():
            frame_id = metadatas['frame_id']
            # flag = frame_id.find('_1')''
            # if flag == -1:
            # 	continue

            input_bev_maps = bev_maps.to(0, non_blocking=True).float()
            input_bev_maps = torch.unsqueeze(input_bev_maps, 0)      
            outputs = model_all(input_bev_maps)

            outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
            outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
            detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
                                            outputs['dim'], K=50)
            detections = detections.cpu().numpy().astype(np.float32) 
            detections_post = post_processing(detections, 6, 2, 0.2)
            lidar_dets = convert_det_to_real_values(detections_post[0], num_classes=6)
            
            if write_flag:
                with open(result_dir+metadatas['frame_id']+'.txt', 'w') as f:         
                    for i in range(lidar_dets.shape[0]):
                        cls = int(lidar_dets[i][0])+1
                        x, y, z = lidar_dets[i][1], lidar_dets[i][2], lidar_dets[i][3]
                        h, w, l = lidar_dets[i][4], lidar_dets[i][5], lidar_dets[i][6]
                        yaw = lidar_dets[i][7]
                        score = lidar_dets[i][-1]
                        f.write(str(cls)+' '+str(x)+' '+str(y)+' '+str(z)+' '+str(h)+' '+\
                                str(w)+' '+str(l)+' '+str(yaw)+' '+str(score)+'\n')
            

            # print("detec num: ", lidar_dets.shape[0])
            
            # Draw prediction in the image
            bev_map = (bev_maps_ori.permute(1, 2, 0).numpy() * 255).astype(np.uint8)
            bev_map = cv2.resize(bev_map, (608, 1216))
            bev_map_copy = bev_map.copy()
            bev_map = draw_predictions(bev_map, detections_post[0], 6)
            # Rotate the bev_map
            bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
            # bev_map = cv2.resize(bev_map, (500, 800))
            bev_map_copy = cv2.rotate(bev_map_copy, cv2.ROTATE_180)
            bev_map_copy = cv2.resize(bev_map_copy, (500, 800))


            # bev_map_608 = cv2.resize(bev_map, (304, 608))
            
            # cv2.imwrite(write_dir_path+frame_id+".jpg", bev_map)
            cv2.imshow('oir', bev_map_copy)
            cv2.imshow('result', bev_map)
            cv2.waitKey(0)            
    # f.close()
def main():
    num_layers = 50
    heads = {
        'hm_cen': 6,
        'cen_offset': 2,
        'direction': 2,
        'z_coor': 1,
        'dim': 3
    }    
    model_all = fpn_resnet.get_pose_net(num_layers=num_layers, heads=heads, head_conv=64,
                                        imagenet_pretrained=True)
    model_all.load_state_dict(torch.load(model_all_path, map_location=lambda storage, loc: storage))
    model_all.cuda(0)
    show_result_all(model_all)
                
if __name__ == '__main__':
    main()
 