import numpy as np
import sys
import random
import os
import cv2
import torch
import warnings
import pdb
import numba
import math
from numba.core.errors import (NumbaDeprecationWarning, 
                               NumbaPendingDeprecationWarning,
                               NumbaPerformanceWarning)
warnings.simplefilter('ignore', category=NumbaDeprecationWarning)
warnings.simplefilter('ignore', category=NumbaPendingDeprecationWarning)
warnings.simplefilter('ignore', category=NumbaPerformanceWarning)
warnings.filterwarnings("ignore", category=UserWarning)
warnings.filterwarnings("ignore", category=UserWarning)
from torch.utils.tensorboard import SummaryWriter
import torch.distributed as dist
import torch.multiprocessing as mp
import torch.utils.data.distributed

from data_process.apollo_dataloader import create_new_framework_test_dataloader
from utils.model_utils import create_model
from utils.torch_utils import _sigmoid
from utils.evaluation_utils import decode, post_processing
from utils.simple_utils import map_calculate_new_framework
from utils.bev_map_val import get_map
from config.train_config_apollo import parse_train_configs

from config.config import cfg, cfg_from_yaml_file
from utils_liga import common_utils
from models.sfa_zj import SFA_ZJ
from data_process.apollo_dataset_new_framework import ApolloDataset

cnf_dict = {}
cnf_dict['minX'] = -50
cnf_dict['maxX'] = 50
cnf_dict['minY'] = -25
cnf_dict['maxY'] = 25
cnf_dict['minZ'] = -2.4-3
cnf_dict['maxZ'] = 0.6+3   
cnf_dict['BEV_HEIGHT'] = 1216
cnf_dict['BEV_WIDTH'] = 608
cnf_dict['DISCRETIZATION'] = (cnf_dict['maxX'] - cnf_dict['minX']) / (cnf_dict['BEV_HEIGHT'] * 1.0)
cnf_dict['peak_thresh'] = 0.2
cnf_dict['bound_size_x'] = cnf_dict['maxX'] - cnf_dict['minX']
cnf_dict['bound_size_y'] = cnf_dict['maxY'] - cnf_dict['minY']
cnf_dict['bound_size_z'] = cnf_dict['maxZ'] - cnf_dict['minZ']

# test_lidar_9_dir = './dataset/zhijiangyihao/training_fusion/bin9/'
# test_lidar_10_dir = './dataset/zhijiangyihao/training_fusion/bin10_filter/'
# test_9_path = './dataset/zhijiangyihao/training_fusion/test_9_shuffle.list'
# test_10_path = './dataset/zhijiangyihao/training_fusion/test_10_shuffle.list'
# sample_9_path_list = [test_lidar_9_dir+x[0:-1]+'.bin' for x in open(test_9_path).readlines()]
# sample_10_path_list = [test_lidar_10_dir+x[0:-1]+'.bin' for x in open(test_10_path).readlines()]
# sample_path_list = sample_9_path_list + sample_10_path_list

test_lidar_dir = './dataset/zhijiangyihao/training2/pointcloud/outdoor_0_filter_bin/'
test_path = './dataset/zhijiangyihao/training2/pointcloud/ImageSets/train_id_outdoor.txt'
sample_path_list = [test_lidar_dir+x[0:-1]+'.bin' for x in open(test_path).readlines()][0:10]

colors = [[0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [255, 120, 120], [0, 120, 0], [120, 255, 255], [120, 0, 255],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],]

write_dir_path = "./test_simple/results/"

def get_lidar(lidar_file):
  return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)

def get_filtered_lidar(sample_path, lidar, boundary, labels=None, rgb=None):
    minX = boundary['minX']
    maxX = boundary['maxX']
    minY = boundary['minY']
    maxY = boundary['maxY']
    minZ = boundary['minZ']
    maxZ = boundary['maxZ']

    # filter master
    minX_my = -4.5
    maxX_my = 0.5
    minY_my = -1.0
    maxY_my = 1.0

    # Remove master
    if 1:
        mask = np.where(((lidar[:, 0] <= minX_my) | (lidar[:, 0] >= maxX_my)) |
                        ((lidar[:, 1] <= minY_my) | (lidar[:, 1] >= maxY_my)))
        lidar = lidar[mask]

    # Remove the point out of range x,y,z
    mask = np.where((lidar[:, 0] >= minX) & (lidar[:, 0] <= maxX) &
                    (lidar[:, 1] >= minY) & (lidar[:, 1] <= maxY) &
                    (lidar[:, 2] >= minZ) & (lidar[:, 2] <= maxZ))
    lidar = lidar[mask]
    lidar[:, 2] = lidar[:, 2] - minZ

    
    if labels is not None:
        label_x = (labels[:, 1] >= minX) & (labels[:, 1] < maxX)
        label_y = (labels[:, 2] >= minY) & (labels[:, 2] < maxY)
        label_z = (labels[:, 3] >= minZ) & (labels[:, 3] < maxZ)
        mask_label = label_x & label_y & label_z
        labels = labels[mask_label]
        if rgb is not None:
            rgb = rgb[mask]        
            return lidar, labels, rgb
        else:
            return lidar, labels, None
    else:
        if rgb is not None:
            rgb = rgb[mask] 
            return lidar, rgb     
        else:
            return lidar, None

def makeBEVMap(PointCloud_, boundary, rgb=None):
    BEV_HEIGHT = boundary['BEV_HEIGHT']
    BEV_WIDTH = boundary['BEV_WIDTH']
    DISCRETIZATION_Y = boundary['DISCRETIZATION']
    DISCRETIZATION_X = boundary['DISCRETIZATION']
    Height = BEV_HEIGHT + 1
    Width = BEV_WIDTH + 1

    # Discretize Feature Map
    PointCloud = np.copy(PointCloud_)
    # PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / DISCRETIZATION))

    # 针对Apollo数据集，检测360°
    PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / DISCRETIZATION_Y) + Height / 2)
    PointCloud[:, 1] = np.int_(np.floor(PointCloud[:, 1] / DISCRETIZATION_X) + Width / 2)

    # sort-3times
    indices = np.lexsort((-PointCloud[:, 2], PointCloud[:, 1], PointCloud[:, 0]))
    PointCloud = PointCloud[indices]
    if rgb is not None:
        rgb = rgb[indices]

    # Height Map
    heightMap = np.zeros((Height, Width))

    _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
    PointCloud_frac = PointCloud[indices]
    # some important problem is image coordinate is (y,x), not (x,y)
    max_height = float(np.abs(boundary['maxZ'] - boundary['minZ']))

    heightMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = PointCloud_frac[:, 2] / max_height

    # Intensity Map & DensityMap
    intensityMap = np.zeros((Height, Width))
    densityMap = np.zeros((Height, Width))

    _, indices, counts = np.unique(PointCloud[:, 0:2], axis=0, return_index=True, return_counts=True)
    PointCloud_top = PointCloud[indices]

    normalizedCounts = np.minimum(1.0, np.log(counts + 1) / np.log(64))

    intensityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = PointCloud_top[:, 3] / 255.0 # hesai40p的反射强度0~255
    densityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = normalizedCounts

    if rgb is not None:
        # Height Map
        # ImgMap = np.zeros((Height, Width, 1))
        ImgMap = np.zeros((Height, Width, 3))
        _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
        PointCloud_frac = PointCloud[indices]        
        # rgb_frac = rgb[indices].reshape(-1, 1)
        rgb_frac = rgb[indices].reshape(-1, 3)
        ImgMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = rgb_frac / 255.0
        ImgMap = np.transpose(ImgMap, (2, 0, 1))
        # RGB_Map = np.zeros((3, Height - 1, Width - 1))
        RGB_Map = np.zeros((3, Height - 1, Width - 1))
        
        RGB_Map[0:3, :, :] = ImgMap[:, :BEV_HEIGHT, :BEV_WIDTH]  
    else:
        RGB_Map = np.zeros((3, Height - 1, Width - 1))
        RGB_Map[2, :, :] = densityMap[:BEV_HEIGHT, :BEV_WIDTH]  # r_map
        RGB_Map[1, :, :] = heightMap[:BEV_HEIGHT, :BEV_WIDTH]  # g_map
        # RGB_Map[0, :, :] = intensityMap[:BEV_HEIGHT, :BEV_WIDTH]  # b_map            

    return RGB_Map

@numba.jit(nopython=True)
def make_bev_voxel_no_i_new_framework(PointCloud_, rgb=None):
    minX = -50
    maxX = 50
    minY = -25
    maxY = 25
    minZ = -2.4-3
    maxZ = 0.6+3

    Height = 1216
    Width = 608
    Deep = 10
 
    CLOUD_R = PointCloud_.shape[0]
    bev_map = np.zeros((4, 10, Width, Height), dtype=np.float32)
    
    bev_map_per_index = np.zeros((Width, Height), dtype=np.int32)

    # voxel
    voxel_num = np.zeros((10, Width, Height), dtype=np.int32)
    voxel_all_feature = np.zeros((3, 10, Width, Height), dtype=np.float32)
    voxel_ave_feature = np.zeros((4, 10, Width, Height), dtype=np.float32)

    indices = np.argsort(-PointCloud_[:, 2])
    PointCloud_ = PointCloud_[indices]  
    range_list = list(range(CLOUD_R)) 
    random_shuffle_list = range_list

    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]
        z_ori = z + minZ
        # if x < -30:
        #     aa = 0
        row = Height * (x - minX) / (maxX - minX)
        col = Width * (y - minY) / (maxY - minY)
        r = int(math.floor(row))
        c = int(math.floor(col))

        deep = Deep * (z_ori - minZ) / (maxZ - minZ)
        d = int(math.floor(deep))

        if r >= Height or c >= Width or d >= Deep:
            continue

        bev_map_per_index[c, r] = 1

        voxel_num[d, c, r] += 1
        voxel_all_feature[0, d, c, r] += x
        voxel_all_feature[1, d, c, r] += y
        # voxel_all_feature[2, d, r, c] += z
        voxel_all_feature[2, d, c, r] += z_ori

        voxel_ave_feature[0, d, c, r] = voxel_all_feature[0, d, c, r] / voxel_num[d, c, r]
        voxel_ave_feature[1, d, c, r] = voxel_all_feature[1, d, c, r] / voxel_num[d, c, r]
        voxel_ave_feature[2, d, c, r] = voxel_all_feature[2, d, c, r] / voxel_num[d, c, r]

        # add density feature
        voxel_ave_feature[3, d, c, r] = np.minimum(1.0, np.log(voxel_num[d, c, r] + 1) / np.log(64))

    bev_map = voxel_ave_feature.reshape(-1, Width, Height)    

    return bev_map, bev_map_per_index

def main():
    configs = parse_train_configs()
    main_worker(configs)

def load_data_to_gpu(configs, batch_dict):
    for key, val in batch_dict.items():
        if isinstance(val, torch.FloatTensor):
            batch_dict[key] = val.type(torch.float32).to(configs.device, non_blocking=True)
            continue        
        if isinstance(val, torch.DoubleTensor): 
            batch_dict[key] = val.type(torch.float32).to(configs.device, non_blocking=True)
            continue  
        if not isinstance(val, np.ndarray):
            continue
        if key in ['frame_id', 'gt_names']:
            continue
        if val.dtype in [np.float32, np.float64]:
            batch_dict[key] = torch.from_numpy(val).float().to(configs.device, non_blocking=True)
        elif val.dtype in [np.uint8, np.int32, np.int64]:
            batch_dict[key] = torch.from_numpy(val).long().to(configs.device, non_blocking=True)
        elif val.dtype in [bool]:
            pass
        else:
            raise ValueError(f"invalid data type {key}: {type(val)}")

def main_worker(configs):
    for i in range(1):
        # model_all_path = './checkpoints_zhijiangyihao_50_no_i/fpn_resnet_50/Model_fpn_resnet_50_epoch_' + str(30+i) + '.pth'
        model_all_path = './checkpoints_zhijiangyihao_50_new_framework/fpn_resnet_50/Model_fpn_resnet_50_epoch_300.pth'
        # print(model_all_path)

        cfg_from_yaml_file(configs.cfg_file, cfg)
        test_dataset = ApolloDataset(configs, cfg.DATA_CONFIG, mode='test', lidar_aug=None, hflip_prob=configs.hflip_prob,
                                    num_samples=configs.num_samples)
        model_all = SFA_ZJ(model_cfg=cfg.MODEL, num_class=len(cfg.CLASS_NAMES), dataset=test_dataset)
        model_all.load_state_dict(torch.load(model_all_path, map_location=lambda storage, loc: storage))

        model_all.cuda()

        # Create dataloader
        _, test_dataloader, _ = create_new_framework_test_dataloader(configs, test_dataset)
        show_result_all(model_all)

def get_corners(x, y, w, l, yaw):
    bev_corners = np.zeros((4, 2), dtype=np.float32)
    cos_yaw = np.cos(yaw)
    sin_yaw = np.sin(yaw)
    # front left
    bev_corners[0, 0] = x - w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[0, 1] = y - w / 2 * sin_yaw + l / 2 * cos_yaw

    # rear left
    bev_corners[1, 0] = x - w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[1, 1] = y - w / 2 * sin_yaw - l / 2 * cos_yaw

    # rear right
    bev_corners[2, 0] = x + w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[2, 1] = y + w / 2 * sin_yaw - l / 2 * cos_yaw

    # front right
    bev_corners[3, 0] = x + w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[3, 1] = y + w / 2 * sin_yaw + l / 2 * cos_yaw

    return bev_corners

def drawRotatedBox(img, x, y, w, l, yaw, color, c, rect=None, score=1.0):
    font = cv2.FONT_HERSHEY_SIMPLEX
    bev_corners = get_corners(x, y, w, l, yaw)
    corners_int = bev_corners.reshape(-1, 1, 2).astype(int)

    cv2.polylines(img, [corners_int], True, color, 1)
    corners_int = bev_corners.reshape(-1, 2).astype(int)
    cv2.line(img, (corners_int[0, 0], corners_int[0, 1]), (corners_int[3, 0], corners_int[3, 1]), (255, 255, 0), 1)
    center = (int((corners_int[0, 0]+corners_int[2, 0])/2), int((corners_int[0, 1]+corners_int[2, 1])/2))
    cv2.putText(img, str(score)[0:4], (corners_int[0, 0], corners_int[0, 1]), font, 1, (0, 255, 0), 1)
    cv2.putText(img, str(int(c))[0:2], (corners_int[3, 0], corners_int[3, 1]), font, 1, (0, 0, 255), 1)
    if rect is not None:
        pt1_x, pt1_y = int(rect[0]), int(rect[1])
        pt2_x, pt2_y = int(rect[2]), int(rect[3])
        cv2.rectangle(img, (pt1_x, pt1_y), (pt2_x, pt2_y), (255, 255, 0), 1)

def show_result_all(model_all):

    model_all.eval()

    for i, sample_path in enumerate(sample_path_list):

        lidarData = get_lidar(sample_path)
        lidarData, _ = get_filtered_lidar(sample_path, lidarData, cnf_dict)

        bev_maps_ori = makeBEVMap(lidarData, cnf_dict)
        bev_maps_ori = torch.from_numpy(bev_maps_ori)
        bev_map = (bev_maps_ori.permute(1, 2, 0).numpy() * 255).astype(np.uint8)
        bev_map = cv2.resize(bev_map, (608, 1216))
        bev_map_copy = bev_map.copy()

        voxel_features, _ = make_bev_voxel_no_i_new_framework(lidarData)

        voxel_features = torch.from_numpy(voxel_features)

        voxel_features = voxel_features.view(4, 10, cnf_dict['BEV_HEIGHT']*cnf_dict['BEV_WIDTH'])

        frame_id = sample_path.split('/')[-1].split('.bin')[0]
        
        metadatas = {
            'frame_id': frame_id
        }

        with torch.no_grad():
            frame_id = metadatas['frame_id']

            voxel_features = voxel_features.to(0, non_blocking=True).float()
            voxel_features = torch.unsqueeze(voxel_features, 0)
            batch_dict = {}
            batch_dict['voxel_features'] = voxel_features
            batch_dict['batch_size'] = 1
            pred_dicts, ret_dicts = model_all(batch_dict)
            pred_dicts = pred_dicts[0]
            for i in range(pred_dicts['pred_boxes'].shape[0]):
                x, y, z, l, w, h, yaw = pred_dicts['pred_boxes'][i].cpu().numpy()
                cls_id = int(pred_dicts['pred_labels'][i].item())
                score = pred_dicts['pred_scores'][i].item()
                if score < 0.2:
                    continue
                if x < cnf_dict['minX'] or x > cnf_dict['maxX']:
                    continue
                if y < cnf_dict['minY'] or y > cnf_dict['maxY']:
                    continue
                if z < cnf_dict['minZ'] or z > cnf_dict['maxZ']:
                    continue   
                l = l / cnf_dict['bound_size_x'] * cnf_dict['BEV_HEIGHT']
                w = w / cnf_dict['bound_size_y'] * cnf_dict['BEV_WIDTH']
                center_input_y = (x - cnf_dict['minX']) / cnf_dict['bound_size_x'] * cnf_dict['BEV_HEIGHT']  # x --> y (invert to 2D image space)
                center_input_x = (y - cnf_dict['minY']) / cnf_dict['bound_size_y'] * cnf_dict['BEV_WIDTH']  # y --> x
                yaw = -yaw
                # Draw prediction in the image
                drawRotatedBox(bev_map, center_input_x, center_input_y, w, l, yaw, colors[cls_id], cls_id, None, score)

            # Rotate the bev_map
            bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
            # bev_map = cv2.resize(bev_map, (500, 800))
            bev_map_copy = cv2.rotate(bev_map_copy, cv2.ROTATE_180)
            bev_map_copy = cv2.resize(bev_map_copy, (500, 800))

            cv2.imwrite(write_dir_path+frame_id+".jpg", bev_map)
            # cv2.imshow('oir', bev_map_copy)
            # cv2.imshow('result', bev_map)
            # cv2.waitKey(0)            
    # f.close()
if __name__ == '__main__':
    main()

