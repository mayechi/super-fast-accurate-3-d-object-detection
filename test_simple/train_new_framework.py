import time
import numpy as np
import sys
import random
import os
import warnings
warnings.filterwarnings("ignore")
from numba.core.errors import (NumbaDeprecationWarning, 
                               NumbaPendingDeprecationWarning,
                               NumbaPerformanceWarning)
warnings.simplefilter('ignore', category=NumbaDeprecationWarning)
warnings.simplefilter('ignore', category=NumbaPendingDeprecationWarning)
warnings.simplefilter('ignore', category=NumbaPerformanceWarning)
warnings.filterwarnings("ignore", category=UserWarning)
warnings.filterwarnings("ignore", category=UserWarning)
import torch
import torch.nn as nn
from torch.utils.tensorboard import SummaryWriter
import torch.distributed as dist
import torch.multiprocessing as mp
import torch.utils.data.distributed
from tqdm import tqdm

sys.path.append('./')

from data_process.apollo_dataloader import create_new_framework_train_dataloader
from utils.model_utils import create_model, make_data_parallel, get_num_parameters
from utils.train_utils import create_optimizer, create_lr_scheduler, get_saved_state, save_checkpoint
from utils.torch_utils import reduce_tensor, to_python_float
from utils.misc import AverageMeter, ProgressMeter
from utils.logger import Logger
from config.train_config_apollo import parse_train_configs
from losses.losses_apollo import Compute_Loss

from config.config import cfg, cfg_from_yaml_file
from utils_liga import common_utils
from models.sfa_zj import SFA_ZJ
from data_process.apollo_dataset_new_framework import ApolloDataset

def main():
    configs = parse_train_configs()

    if configs.seed is not None:
        random.seed(configs.seed)
        np.random.seed(configs.seed)
        torch.manual_seed(configs.seed)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False

    if configs.gpu_idx is not None:
        configs.gpu_idx = configs.local_rank

    if "WORLD_SIZE" in os.environ:
        configs.world_size = int(os.environ["WORLD_SIZE"])
    else:
        configs.world_size = 1
    configs.distributed = configs.world_size > 1
    print("gpu_idx:", configs.gpu_idx)
    print("world_size:", configs.world_size)
    main_worker(configs.gpu_idx, configs)


def main_worker(gpu_idx, configs):
    configs.gpu_idx = gpu_idx
    configs.device = torch.device('cpu' if configs.gpu_idx is None else 'cuda:{}'.format(configs.gpu_idx))
    if configs.distributed:
        configs.local_rank = getattr(common_utils, 'init_dist_pytorch')(
            configs.tcp_port, configs.local_rank, configs.world_size, backend='nccl'
        )
    else:
        configs.subdivisions = int(64 / configs.batch_size)

    configs.is_master_node = (not configs.distributed) or (
            configs.distributed and (configs.local_rank % configs.world_size == 0))

    if configs.is_master_node:
        logger = Logger(configs.logs_dir, configs.saved_fn)
        logger.info('>>> Created a new logger')
        logger.info('>>> configs: {}'.format(configs))
        tb_writer = SummaryWriter(log_dir=os.path.join(configs.logs_dir, 'tensorboard'))
    else:
        logger = None
        tb_writer = None

    cfg_from_yaml_file(configs.cfg_file, cfg)
    train_dataset = ApolloDataset(configs, cfg.DATA_CONFIG, mode='train', lidar_aug=None, hflip_prob=configs.hflip_prob,
                                  num_samples=configs.num_samples)
    model = SFA_ZJ(model_cfg=cfg.MODEL, num_class=len(cfg.CLASS_NAMES), dataset=train_dataset)

    if configs.pretrained_path is not None:
        assert os.path.isfile(configs.pretrained_path), "=> no checkpoint found at '{}'".format(configs.pretrained_path)
        model.load_state_dict(torch.load(configs.pretrained_path, map_location=lambda storage, loc: storage))
        if logger is not None:
            logger.info('loaded pretrained model at {}'.format(configs.pretrained_path))

    if configs.resume_path is not None:
        assert os.path.isfile(configs.resume_path), "=> no checkpoint found at '{}'".format(configs.resume_path)
        model.load_state_dict(torch.load(configs.resume_path))
        if logger is not None:
            logger.info('resume training model from checkpoint {}'.format(configs.resume_path))

    optimizer = create_optimizer(configs, model)
    lr_scheduler = create_lr_scheduler(optimizer, configs)
    configs.step_lr_in_epoch = False if configs.lr_type in ['multi_step', 'cosin', 'one_cycle'] else True

    if configs.resume_path is not None:
        utils_path = configs.resume_path.replace('Model_', 'Utils_')
        assert os.path.isfile(utils_path), "=> no checkpoint found at '{}'".format(utils_path)
        utils_state_dict = torch.load(utils_path, map_location='cuda:{}'.format(configs.gpu_idx))
        optimizer.load_state_dict(utils_state_dict['optimizer'])
        lr_scheduler.load_state_dict(utils_state_dict['lr_scheduler'])
        configs.start_epoch = utils_state_dict['epoch'] + 1

    if configs.is_master_node:
        num_parameters = get_num_parameters(model)
        logger.info('number of trained parameters of the model: {}'.format(num_parameters))

    if logger is not None:
        logger.info(">>> Loading dataset & getting dataloader...")

    # Create dataloader
    _, train_dataloader, train_sampler = create_new_framework_train_dataloader(configs, train_dataset)
    if logger is not None:
        logger.info('number of batches in training set: {}'.format(len(train_dataloader)))
   
    model.cuda()
    model.train()
    if configs.distributed:
        model = nn.parallel.DistributedDataParallel(
            model, device_ids=[configs.local_rank % configs.world_size], find_unused_parameters=True)

    for epoch in range(configs.start_epoch, configs.num_epochs + 1):      
        if logger is not None:
            logger.info('{}'.format('*-' * 40))
            logger.info('{} {}/{} {}'.format('=' * 35, epoch, configs.num_epochs, '=' * 35))
            logger.info('{}'.format('*-' * 40))
            logger.info('>>> Epoch: [{}/{}]'.format(epoch, configs.num_epochs))

        if configs.distributed:
            train_sampler.set_epoch(epoch)
        
        train_one_epoch(train_dataloader, model, optimizer, lr_scheduler, epoch, configs, logger, tb_writer)

        if configs.is_master_node and ((epoch % configs.checkpoint_freq) == 0):
            model_state_dict, utils_state_dict = get_saved_state(model, optimizer, lr_scheduler, epoch, configs)
            save_checkpoint(configs.checkpoints_dir, configs.saved_fn, model_state_dict, utils_state_dict, epoch)

        if not configs.step_lr_in_epoch:
            lr_scheduler.step()
            if tb_writer is not None:
                tb_writer.add_scalar('LR', lr_scheduler.get_lr()[0], epoch)

    if tb_writer is not None:
        tb_writer.close()
    if configs.distributed:
        cleanup()


def cleanup():
    dist.destroy_process_group()

def load_data_to_gpu(configs, batch_dict):
    for key, val in batch_dict.items():
        if isinstance(val, torch.FloatTensor):
            batch_dict[key] = val.type(torch.float32).to(configs.device, non_blocking=True)
            continue        
        if isinstance(val, torch.DoubleTensor): 
            batch_dict[key] = val.type(torch.float32).to(configs.device, non_blocking=True)
            continue  
        if not isinstance(val, np.ndarray):
            continue
        if key in ['metadata', 'gt_names']:
            continue
        if val.dtype in [np.float32, np.float64]:
            batch_dict[key] = torch.from_numpy(val).float().to(configs.device, non_blocking=True)
        elif val.dtype in [np.uint8, np.int32, np.int64]:
            batch_dict[key] = torch.from_numpy(val).long().to(configs.device, non_blocking=True)
        elif val.dtype in [bool]:
            pass
        else:
            raise ValueError(f"invalid data type {key}: {type(val)}")

def train_one_epoch(train_dataloader, model, optimizer, lr_scheduler, epoch, configs, logger, tb_writer):
    batch_time = AverageMeter('Time', ':6.3f')
    data_time = AverageMeter('Data', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')

    progress = ProgressMeter(len(train_dataloader), [batch_time, data_time, losses],
                             prefix="Train - Epoch: [{}/{}]".format(epoch, configs.num_epochs))
    criterion = Compute_Loss(device=configs.device)
    num_iters_per_epoch = len(train_dataloader)
    model.train()      
    start_time = time.time()
    for batch_idx, batch_data in enumerate(tqdm(train_dataloader)):
        data_time.update(time.time() - start_time)
        voxel_features = batch_data['voxel_features']
        batch_size = voxel_features.shape[0]
        global_step = num_iters_per_epoch * (epoch - 1) + batch_idx + 1
        load_data_to_gpu(configs, batch_data)
        ret_dict, tb_dict, disp_dict = model(batch_data)
        loss = ret_dict['loss']
        if (not configs.distributed) and (configs.gpu_idx is None):
            loss = torch.mean(loss)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()
        
        if configs.step_lr_in_epoch:
            lr_scheduler.step()
            if tb_writer is not None:
                tb_writer.add_scalar('LR', lr_scheduler.get_lr()[0], global_step)

        if configs.distributed:
            reduced_loss = reduce_tensor(torch.tensor(tb_dict['loss_rpn']).cuda(), configs.world_size)
        else:
            reduced_loss = torch.tensor(tb_dict['loss_rpn']).cuda()
        losses.update(to_python_float(reduced_loss), batch_size)
        batch_time.update(time.time() - start_time)

        if logger is not None:
            if (global_step % configs.print_freq) == 0:
                logger.info(progress.get_message(batch_idx))

        start_time = time.time()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        try:
            cleanup()
            sys.exit(0)
        except SystemExit:
            os._exit(0)
