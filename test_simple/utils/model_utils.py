import torch
import fpn_resnet

def create_model(configs):
    arch_parts = configs.arch.split('_')
    num_layers = int(arch_parts[-1])
    print('using ResNet architecture with feature pyramid')
    model = fpn_resnet.get_pose_net(num_layers=num_layers, heads=configs.heads, head_conv=configs.head_conv,
                                    imagenet_pretrained=configs.imagenet_pretrained)

    return model

def get_num_parameters(model):
    """Count number of trained parameters of the model"""
    if hasattr(model, 'module'):
        num_parameters = sum(p.numel() for p in model.module.parameters() if p.requires_grad)
    else:
        num_parameters = sum(p.numel() for p in model.parameters() if p.requires_grad)

    return num_parameters


def make_data_parallel(model, configs):
    if configs.distributed:
        # For multiprocessing distributed, DistributedDataParallel constructor
        # should always set the single device scope, otherwise,
        # DistributedDataParallel will use all available devices.
        if configs.gpu_idx is not None:
            torch.cuda.set_device(configs.gpu_idx)
            model.cuda(configs.gpu_idx)
            # When using a single GPU per process and per
            # DistributedDataParallel, we need to divide the batch size
            # ourselves based on the total number of GPUs we have
            configs.batch_size = int(configs.batch_size / configs.ngpus_per_node)
            configs.num_workers = int((configs.num_workers + configs.ngpus_per_node - 1) / configs.ngpus_per_node)
            model = torch.nn.parallel.DistributedDataParallel(model, device_ids=[configs.gpu_idx])
        else:
            model.cuda()
            # DistributedDataParallel will divide and allocate batch_size to all
            # available GPUs if device_ids are not set
            model = torch.nn.parallel.DistributedDataParallel(model)
    elif configs.gpu_idx is not None:
        torch.cuda.set_device(configs.gpu_idx)
        model = model.cuda(configs.gpu_idx)
    else:
        # DataParallel will divide and allocate batch_size to all available GPUs
        model = torch.nn.DataParallel(model).cuda()

    return model

def update_model_from_centernet2_backbone(configs):
    from detectron2.config import get_cfg
    from config.centernet_config.config import add_centernet_config
    from models.modeling.backbone.dlafpn import dla34
    from models.modeling.backbone.bifpn_fcos_backbone import BiFPN

    config_file = "./src/config/model_configs/CenterNet2_DLA-BiFPN-P5_640_24x_ST.yaml"
    cfg = get_cfg()
    add_centernet_config(cfg)
    cfg.merge_from_file(config_file)

    bottom_up = dla34(cfg)
    in_features = cfg.MODEL.FPN.IN_FEATURES
    out_channels = cfg.MODEL.BIFPN.OUT_CHANNELS
    num_repeats = cfg.MODEL.BIFPN.NUM_BIFPN
    top_levels = 0

    backbone = BiFPN(
        bottom_up=bottom_up,
        in_features=in_features,
        out_channels=out_channels,
        num_top_levels=top_levels,
        num_repeats=num_repeats,
        configs=configs,
        norm=cfg.MODEL.BIFPN.NORM
    )

    return backbone


def update_model_from_centernet2(configs):
    from detectron2.config import get_cfg
    from config.centernet_config.config import add_centernet_config
    from models.modeling.backbone.dlafpn import dla34
    from models.modeling.backbone.bifpn_fcos import BiFPN
    from models.modeling.dense_heads.centernet import CenterNet
    from models.modeling.roi_heads.custom_roi_heads import CustomCascadeROIHeads
    from models.centernet2 import centernet2_model


    config_file = "./src/config/model_configs/CenterNet2_DLA-BiFPN-P5_640_24x_ST.yaml"
    cfg = get_cfg()
    add_centernet_config(cfg)
    cfg.merge_from_file(config_file)

    bottom_up = dla34(cfg)
    in_features = cfg.MODEL.FPN.IN_FEATURES
    out_channels = cfg.MODEL.BIFPN.OUT_CHANNELS
    num_repeats = cfg.MODEL.BIFPN.NUM_BIFPN
    top_levels = 2

    backbone = BiFPN(
        bottom_up=bottom_up,
        in_features=in_features,
        out_channels=out_channels,
        num_top_levels=top_levels,
        num_repeats=num_repeats,
        configs=configs,
        norm=cfg.MODEL.BIFPN.NORM
    )
    proposal_generator = CenterNet(cfg, backbone.output_shape())
    roi_heads = CustomCascadeROIHeads(cfg, backbone.output_shape()),

    model = centernet2_model(backbone, proposal_generator, roi_heads)

    return model

if __name__ == '__main__':
    import argparse

    from torchsummary import summary
    from easydict import EasyDict as edict

    parser = argparse.ArgumentParser(description='RTM3D Implementation')
    parser.add_argument('-a', '--arch', type=str, default='resnet_18', metavar='ARCH',
                        help='The name of the model architecture')
    parser.add_argument('--head_conv', type=int, default=-1,
                        help='conv layer channels for output head'
                             '0 for no conv layer'
                             '-1 for default setting: '
                             '64 for resnets and 256 for dla.')

    configs = edict(vars(parser.parse_args()))
    if configs.head_conv == -1:  # init default head_conv
        configs.head_conv = 256 if 'dla' in configs.arch else 64

    configs.num_classes = 3
    configs.num_vertexes = 8
    configs.num_center_offset = 2
    configs.num_vertexes_offset = 2
    configs.num_dimension = 3
    configs.num_rot = 8
    configs.num_depth = 1
    configs.num_wh = 2
    configs.heads = {
        'hm_mc': configs.num_classes,
        'hm_ver': configs.num_vertexes,
        'vercoor': configs.num_vertexes * 2,
        'cenoff': configs.num_center_offset,
        'veroff': configs.num_vertexes_offset,
        'dim': configs.num_dimension,
        'rot': configs.num_rot,
        'depth': configs.num_depth,
        'wh': configs.num_wh
    }

    configs.device = torch.device('cuda:1')
    # configs.device = torch.device('cpu')

    model = create_model(configs).to(device=configs.device)
    sample_input = torch.randn((1, 3, 224, 224)).to(device=configs.device)
    # summary(model.cuda(1), (3, 224, 224))
    output = model(sample_input)
    for hm_name, hm_out in output.items():
        print('hm_name: {}, hm_out size: {}'.format(hm_name, hm_out.size()))

    print('number of parameters: {}'.format(get_num_parameters(model)))
