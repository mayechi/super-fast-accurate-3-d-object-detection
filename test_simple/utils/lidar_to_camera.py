import cv2
import numpy as np
import os

# from centernet.modeling.backbone.dla import Tree

def boxes3d_to_corners3d(boxes3d):
    """
    :param boxes3d: (N, 7) [x, y, z, l, w, h, yaw] in lidar coords
    :return: corners3d: (N, 8, 3)
        7 -------- 4
       /|         /|
      6 -------- 5 . h
      | |        | |
      . 3 -------- 0
      |/         |/ w     ----> x
      2 -------- 1
          l
    """
    boxes_num = boxes3d.shape[0]
    l, w, h, yaw = boxes3d[:, 3], boxes3d[:, 4], boxes3d[:, 5], boxes3d[:, 6]
    x_corners_body = np.array(
        [l / 2., l / 2., -l / 2., -l / 2., l / 2., l / 2., -l / 2., -l / 2.],
        dtype=np.float32).T  # N*8
    y_corners_body = np.array(
        [w / 2., -w / 2., -w / 2., w / 2., w / 2., -w / 2., -w / 2., w / 2.],
        dtype=np.float32).T  # N*8
    z_corners_body = np.array(
        [-h / 2., -h / 2., -h / 2., -h / 2., h / 2., h / 2., h / 2., h / 2.],
        dtype=np.float32).T  # N*8

    body_corners = np.concatenate(
        (x_corners_body.reshape(-1, 8, 1), y_corners_body.reshape(
            -1, 8, 1), z_corners_body.reshape(-1, 8, 1)),
        axis=2)  # (N, 8, 3)

    zeros, ones = np.zeros(yaw.size,
                           dtype=np.float32), np.ones(yaw.size,
                                                      dtype=np.float32)
    rot_list = np.array([
        [np.cos(-yaw), -np.sin(-yaw), zeros],
        [np.sin(-yaw), np.cos(-yaw), zeros],
        [zeros, zeros, ones],
    ])  # (3, 3, N)
    R_list = np.transpose(rot_list, (2, 0, 1))  # (N, 3, 3)

    rotated_corners = np.matmul(body_corners, R_list)  # (N, 8, 3)
    x_corners, y_corners, z_corners = rotated_corners[:, :,
                                                      0], rotated_corners[:, :,
                                                                          1], rotated_corners[:, :,
                                                                                              2]

    x_loc, y_loc, z_loc = boxes3d[:, 0], boxes3d[:, 1], boxes3d[:, 2]

    x = x_loc.reshape(-1, 1) + x_corners.reshape(-1, 8)
    y = y_loc.reshape(-1, 1) + y_corners.reshape(-1, 8)
    z = z_loc.reshape(-1, 1) + z_corners.reshape(-1, 8)

    corners = np.concatenate(
        (x.reshape(-1, 8, 1), y.reshape(-1, 8, 1), z.reshape(-1, 8, 1)),
        axis=2)

    return corners.astype(np.float32)

def corners3d_to_camera3d(corners3d):
    """
    :params corners: (N, 8, 3)
    :return pts_image: (N, 8, 3)
    """
    # calibration
    T_lidar_to_camera = np.array(
        [[0.0277387, -0.999607, -0.00404495, 0.108141],
         [0.00308998, 0.00413226, -0.999987, -0.123988],
         [0.99961, 0.0277258, 0.00320342, -0.197486]])

    sample_num = corners3d.shape[0]
    corners3d_hom = np.concatenate((corners3d, np.ones((sample_num, 8, 1))),
                                   axis=2)  # (N, 8, 4)
    pts_image = np.matmul(corners3d_hom, T_lidar_to_camera.T)

    return pts_image

def camera3d_to_img_boxes(camera3d):
    """
    :param camera3d: (N, 8, 3) corners in camera coordinate
    :return: (N, 4) 4:(xmin,ymin,xmax,ymax) pts in rgb coordinate
    """

    image_width = 1280
    image_height = 720

    camera_matrix = np.array(
        [[1.312364254819129e+03, 0., 5.880519199337333e+02],
         [0., 1.312927278233663e+03, 3.316051861963202e+02], [0., 0., 1.]])
    dist_coef = np.array([[
        -0.487604444954244, 0.294488520444290, 0.001201495313292,
        7.242191805280423e-04, -0.090999234677858
    ]])

    # check
    new_camera_matrix, _ = cv2.getOptimalNewCameraMatrix(
        camera_matrix, dist_coef, (image_width, image_height), 1,
        (image_width, image_height))

    zeros = np.array([[0., 0., 0.]])
    ones = np.array([[1., 0., 0.], [0., 1., 0.], [0., 0., 1.]])

    used_index, boxes_pts = [], []
    for i in range(camera3d.shape[0]):
        # z check
        camera_pt = camera3d[i]
        if camera_pt.min(axis=0)[2] < 2.:
            continue

        # range check
        cols = new_camera_matrix[
            0, 2] + new_camera_matrix[0, 0] * camera_pt[:, 0] / camera_pt[:, 2]
        rows = new_camera_matrix[
            1, 2] + new_camera_matrix[1, 1] * camera_pt[:, 1] / camera_pt[:, 2]
        min_col = cols.min()
        max_col = cols.max()
        min_row = rows.min()
        max_row = rows.max()
        if max_row < 0.:
            continue
        if max_col < 0.:
            continue
        if min_col > image_width:
            continue
        if min_row > image_height:
            continue
        if min_col < 0:
            continue
        if min_row < 0:
            continue
        if max_row > image_height:
            continue
        if max_col > image_width:
            continue

        # project
        box_pts, _ = cv2.projectPoints(camera_pt, ones, zeros, camera_matrix,
                                       dist_coef)

        min_pt = box_pts.min(axis=0)
        max_pt = box_pts.max(axis=0)

        # pixel 20
        max_pt[0, 1] += 20

        if max_pt[0, 0] < 0.:
            continue
        if max_pt[0, 1] < 0.:
            continue
        if min_pt[0, 0] > image_width:
            continue
        if min_pt[0, 1] > image_height:
            continue

        if min_pt[0, 0] < 0.:
            min_pt[0, 0] = 0.
        if min_pt[0, 1] < 0.:
            min_pt[0, 1] = 0.
        if max_pt[0, 0] > image_width:
            max_pt[0, 0] = image_width - 1.
        if max_pt[0, 1] > image_height:
            max_pt[0, 1] = image_height - 1

        used_index.append(i)
        boxes_pts.append(
            [min_pt[0, 0], min_pt[0, 1], max_pt[0, 0], max_pt[0, 1]])
    
    return used_index, boxes_pts

def find_index_in_image(camera3d):
    """
    :param camera3d: (N, 8, 3) corners in camera coordinate
    :return: index in projection in image
    """
    used_index = []

    image_width = 1280
    image_height = 720

    camera_matrix = np.array(
        [[1.312364254819129e+03, 0., 5.880519199337333e+02],
         [0., 1.312927278233663e+03, 3.316051861963202e+02], [0., 0., 1.]])
    dist_coef = np.array([[
        -0.487604444954244, 0.294488520444290, 0.001201495313292,
        7.242191805280423e-04, -0.090999234677858
    ]])

    # check
    new_camera_matrix, _ = cv2.getOptimalNewCameraMatrix(
        camera_matrix, dist_coef, (image_width, image_height), 1,
        (image_width, image_height))

    zeros = np.array([[0., 0., 0.]])
    ones = np.array([[1., 0., 0.], [0., 1., 0.], [0., 0., 1.]])

    boxes_pts = []
    for i in range(camera3d.shape[0]):
        # z check
        camera_pt = camera3d[i]
        if camera_pt.min(axis=0)[2] < 2.:
            continue

        # range check
        cols = new_camera_matrix[
            0, 2] + new_camera_matrix[0, 0] * camera_pt[:, 0] / camera_pt[:, 2]
        rows = new_camera_matrix[
            1, 2] + new_camera_matrix[1, 1] * camera_pt[:, 1] / camera_pt[:, 2]
        min_col = cols.min()
        max_col = cols.max()
        min_row = rows.min()
        max_row = rows.max()
        if max_row < 0.:
            continue
        if max_col < 0.:
            continue
        if min_col > image_width:
            continue
        if min_row > image_height:
            continue
        if min_col < 0:
            continue
        if min_row < 0:
            continue
        if max_row > image_height:
            continue
        if max_col > image_width:
            continue

        # project
        box_pts, _ = cv2.projectPoints(camera_pt, ones, zeros, camera_matrix,
                                       dist_coef)

        min_pt = box_pts.min(axis=0)
        max_pt = box_pts.max(axis=0)

        # pixel 20
        max_pt[0, 1] += 20

        if max_pt[0, 0] < 0.:
            continue
        if max_pt[0, 1] < 0.:
            continue
        if min_pt[0, 0] > image_width:
            continue
        if min_pt[0, 1] > image_height:
            continue

        used_index.append(i)
    return used_index

def filter_boxes(boxes_lidar_3d):
    """
    :param boxes3d: (N, 7) [x, y, z, l, w, h, yaw] in lidar coords
    :return:  stay index in image
    """

    corners3d = boxes3d_to_corners3d(boxes_lidar_3d)
    camera3d = corners3d_to_camera3d(corners3d)
    used_index = find_index_in_image(camera3d)
    # boxes_lidar_3d = boxes_lidar_3d[used_index]
    
    return used_index

def whether_in_image(boxes_lidar_3d_list):
    """
    :param boxes3d: (7) [x, y, z, l, w, h, yaw] in lidar coords
    :return:  whether stay in image
    """
    boxes_lidar_3d = np.array(boxes_lidar_3d_list).reshape(1, 7)
    corners3d = boxes3d_to_corners3d(boxes_lidar_3d)
    camera3d = corners3d_to_camera3d(corners3d)
    used_index = find_index_in_image(camera3d)
    if used_index == []:
        return False
    else:
        return True


if __name__ == '__main__':
    IMAGE_DIR = '/home/zhangshun/zs_work/datasets/lidar_camera/0831/test/image'
    LABEL_DIR = '/home/zhangshun/zs_work/datasets/lidar_camera/0831/test/lidar_label'
    RESULT_DIR = '/home/zhangshun/zs_work/datasets/lidar_camera/0831/test/result'
    CORNER_DIR = '/home/zhangshun/zs_work/datasets/lidar_camera/0831/test/corners'

    for label_file in os.listdir(LABEL_DIR):
        # print(label_file)
        labels = np.loadtxt(LABEL_DIR + '/' + label_file)

        # test boxes3d_to_corners3d
        corners3d = boxes3d_to_corners3d(labels[:, 1:])
        corners = []
        for i in range(corners3d.shape[0]):
            for j in range(corners3d.shape[1]):
                corners.append(corners3d[i, j, :])

        np.savetxt(CORNER_DIR + '/' + label_file.split('.')[0] + '.txt',
                   corners)
        for i in range(corners3d.shape[0]):
            np.savetxt(
                '/home/zhangshun/zs_work/datasets/lidar_camera/0831/test/boxes/'
                + label_file.split('.')[0] + "_" + str(i) + '.txt',
                corners3d[i, :, :])

        # test corners3d_to_camera3d
        camera3d = corners3d_to_camera3d(corners3d)

        # test camera3d to img boxes
        img_boxes = camera3d_to_img_boxes(camera3d)

        image = cv2.imread(IMAGE_DIR + '/' + label_file.split('.')[0] + '.jpg')

        for i in range(len(img_boxes)):
            box = img_boxes[i]
            cv2.rectangle(image, (int(box[0]), int(box[1])),
                          (int(box[2]), int(box[3])), (0, 0, 255), 1)

        cv2.imwrite(RESULT_DIR + '/' + label_file.split('.')[0] + '.jpg',
                    image)
        # np.savetxt('/home/zhangshun/zs_work/datasets/lidar_camera/0831/test/r.txt',
        #           img_boxes)
