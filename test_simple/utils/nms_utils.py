import torch

from ops.iou3d_nms import iou3d_nms_utils


def class_agnostic_nms(box_scores, box_preds, nms_config, score_thresh=None):
    src_box_scores = box_scores
    if score_thresh is not None:
        scores_mask = (box_scores >= score_thresh)
        box_scores = box_scores[scores_mask]
        box_preds = box_preds[scores_mask]

    selected = []
    if box_scores.shape[0] > 0:
        box_scores_nms, indices = torch.topk(box_scores, k=min(nms_config.NMS_PRE_MAXSIZE, box_scores.shape[0]))
        boxes_for_nms = box_preds[indices]
        keep_idx, selected_scores = getattr(iou3d_nms_utils, nms_config.NMS_TYPE)(
            boxes_for_nms[:, 0:7], box_scores_nms, nms_config.NMS_THRESH, **nms_config
        )
        selected = indices[keep_idx[:nms_config.NMS_POST_MAXSIZE]]

    if score_thresh is not None:
        original_idxs = scores_mask.nonzero().view(-1)
        selected = original_idxs[selected]
    return selected, src_box_scores[selected]


def multi_classes_nms(cls_scores, box_preds, nms_config, score_thresh=None, label_preds=None, toonnx=False):
    """
    Args:
        cls_scores: (N, num_class)
        box_preds: (N, 7 + C)
        nms_config:
        score_thresh:

    Returns:

    """
    pred_scores, pred_labels, pred_boxes = [], [], []
    if toonnx:
        cls_scores = torch.where(cls_scores > 0.1, cls_scores.clone().detach(), torch.tensor(0., device = cls_scores.device))
        box_scores_nms, indices = torch.topk(cls_scores, k=100, dim=0)
        pred_boxes = box_preds[indices]
        pred_labels = torch.ones_like(box_scores_nms)
        return box_scores_nms.transpose(0,1), pred_labels.transpose(0,1), pred_boxes.transpose(0,1)
    for k in range(cls_scores.shape[1] if label_preds is None else label_preds.max().item()):
        '''add by me   to onnx needed'''
        if toonnx:
            scores_mask = (cls_scores[:, k] >= 0.1)
            # box_scores = cls_scores[scores_mask, k]
            # cur_box_preds = box_preds[scores_mask]
            # box_scores = torch.masked_select(cls_scores[:, k],scores_mask)
            # cur_box_preds = torch.masked_select(box_preds,scores_mask.reshape(-1,1)).reshape(-1,7)
            box_scores = cls_scores[:, k]
            cur_box_preds = box_preds[:]
            box_scores_nms, indices = torch.topk(box_scores, k=min(50, box_scores.shape[0]))
            boxes_for_nms = cur_box_preds[indices]
            pred_scores.append(box_scores_nms)
            pred_labels.append(torch.ones_like(box_scores_nms)+k)
            pred_boxes.append(boxes_for_nms)
            continue

        if score_thresh is not None:
            if label_preds is None:
                scores_mask = (cls_scores[:, k] >= score_thresh)
                box_scores = cls_scores[scores_mask, k]
                cur_box_preds = box_preds[scores_mask]
            else:
                scores_mask = (cls_scores[:, 0] >= score_thresh) & (label_preds == k + 1)
                box_scores = cls_scores[scores_mask, 0]
                cur_box_preds = box_preds[scores_mask]
        else:
            raise NotImplementedError
            box_scores = cls_scores[:, k]
        
        '''add by me   to onnx needed'''
        # if toonnx:
        #     pred_scores.append(box_scores)
        #     pred_labels.append(torch.ones_like(box_scores))
        #     pred_boxes.append(cur_box_preds)
        #     continue

        selected = []
        if box_scores.shape[0] > 0:
            box_scores_nms, indices = torch.topk(box_scores, k=min(nms_config.NMS_PRE_MAXSIZE, box_scores.shape[0]))
            boxes_for_nms = cur_box_preds[indices]
            keep_idx, selected_scores = getattr(iou3d_nms_utils, nms_config.NMS_TYPE)(
                boxes_for_nms[:, 0:7], box_scores_nms, nms_config.NMS_THRESH, **nms_config
            )
            selected = indices[keep_idx[:nms_config.NMS_POST_MAXSIZE]]
        
        pred_scores.append(box_scores[selected])
        pred_labels.append(box_scores.new_ones(len(selected)).long() * k)
        pred_boxes.append(cur_box_preds[selected])
    '''add by me   to onnx needed'''
    # if toonnx:
    #     return pred_scores, pred_labels, pred_boxes

    pred_scores = torch.cat(pred_scores, dim=0)
    pred_labels = torch.cat(pred_labels, dim=0)
    pred_boxes = torch.cat(pred_boxes, dim=0)

    return pred_scores, pred_labels, pred_boxes