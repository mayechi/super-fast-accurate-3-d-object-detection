import numpy as np
import torch
from torch.utils.data import DataLoader
from collections import defaultdict

from data_process.apollo_dataset import ApolloDataset
# from data_process.apollo_dataset_new_framework import ApolloDataset

def create_train_dataloader(configs):
    train_lidar_aug = None
    train_dataset = ApolloDataset(configs, mode='train', lidar_aug=train_lidar_aug, hflip_prob=configs.hflip_prob,
                                 num_samples=configs.num_samples)
    train_sampler = None
    if configs.distributed:
        train_sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)
    train_dataloader = DataLoader(train_dataset, batch_size=configs.batch_size, shuffle=(train_sampler is None),
                                  pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=train_sampler)

    return train_dataset, train_dataloader, train_sampler

def create_new_framework_train_dataloader(configs, train_dataset):
    train_sampler = None
    if configs.distributed:
        train_sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)
    train_dataloader = DataLoader(train_dataset, batch_size=configs.batch_size, shuffle=(train_sampler is None), collate_fn=collate_batch,
                                  pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=train_sampler)

    return train_dataset, train_dataloader, train_sampler

def create_test_dataloader(configs):
    test_dataset = ApolloDataset(configs, mode='test', lidar_aug=None, hflip_prob=0., num_samples=configs.num_samples)
    test_sampler = None
    test_dataloader = DataLoader(test_dataset, batch_size=1, shuffle=False,
                                 pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=test_sampler)

    return test_dataloader

def create_new_framework_test_dataloader(configs, test_dataset):
    test_sampler = None
    test_dataloader = DataLoader(test_dataset, batch_size=configs.batch_size, shuffle=(test_sampler is None), collate_fn=collate_batch,
                                 pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=test_sampler)

    return test_sampler, test_dataloader, test_sampler

def collate_batch(batch_list, _unused=False):
    data_dict = defaultdict(list)
    batch_size = len(batch_list)
    for cur_sample in batch_list:
        for key, val in cur_sample.items():
            data_dict[key].append(val)    
    ret = {}

    for key, val in data_dict.items():
        if key in ['voxel_features']:
            voxel_features_list = []
            for i, voxel_features in enumerate(val):
                voxel_features_list.append(voxel_features)
            ret[key] = np.stack(voxel_features_list, axis=0)
        elif key in ['gt_boxes']:
            max_gt = max([len(x) for x in val])
            batch_gt_boxes3d = np.zeros(
                (batch_size, max_gt, val[0].shape[-1]), dtype=np.float32)
            for k in range(batch_size):
                batch_gt_boxes3d[k, :val[k].__len__(), :] = val[k]
            ret[key] = batch_gt_boxes3d
        elif key in ['gt_names']:
            ret[key] = [np.array(x) for x in val]                   
        elif key in ['targets']:
            ret[key] = defaultdict(list)
            for elem in val:
                for idx, ele in enumerate(elem):
                    ret[key][ele].append(torch.tensor(elem[ele]))                   
            for kk, vv in ret[key].items():
                ret[key][kk] = torch.stack(vv)
        elif key in ['frame_id']:
            ret[key] = val
        else:
            print(key)
            raise NotImplementedError

    ret['batch_size'] = batch_size
    return ret
