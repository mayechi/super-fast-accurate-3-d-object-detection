import math
import pickle
import logging
from builtins import int
from pathlib import Path

import numpy as np
from torch.utils.data import Dataset
import cv2
import torch
from det3d.core.sampler import preprocess as prep
from det3d.core.sampler.sample_ops_v2 import DataBaseSamplerV2
from det3d.core.bbox import box_np_ops
from det3d.datasets.utils import sa_da_v2
from det3d.builder import build_db_preprocess, DataBasePreprocessor

from utils.simple_utils import gen_hm_radius, compute_radius, get_filtered_lidar, drawRotatedBox
from utils.simple_utils import makeBEVMap, make_bev_voxel_no_i, make_bev_voxel_no_i_new_framework
import config.apollo_config as cnf

class ApolloDataset(Dataset):
    def __init__(self, configs, dataset_cfg=None, mode='train',lidar_aug=None, hflip_prob=None, num_samples=None):
        self.dataset_cfg = dataset_cfg
        self.input_size = configs.input_size
        self.hm_size = configs.hm_size

        self.num_classes = configs.num_classes
        self.max_objects = configs.max_objects

        self.class_names = self.dataset_cfg.CLASS_NAMES
        self.point_cloud_range = np.array(
            self.dataset_cfg.POINT_CLOUD_RANGE, dtype=np.float32)
        self.voxel_size = self.dataset_cfg.VOXEL_SIZE
        grid_size = (
            self.point_cloud_range[3:6] - self.point_cloud_range[0:3]) / np.array(self.voxel_size)
        self.grid_size = np.round(grid_size).astype(np.int64)

        assert mode in ['train', 'val', 'test'], 'Invalid mode: {}'.format(mode)
        self.mode = mode
        self.is_test = (self.mode == 'test')

        # For data_aug
        if mode == 'train':
            self.lidar_aug = lidar_aug
            self.hflip_prob = hflip_prob
            self.gt_loc_noise_std = [1.0, 1.0, 0.5]
            self.gt_rotation_noise = [-0.785, 0.785]
            self.global_rotation_noise = [-0.785, 0.785]
            self.global_scaling_noise = [0.95, 1.05]
            self.global_random_rot_range = [0, 0]
            self.global_translate_noise_std = [0.0, 0.0, 0.0]

            # self.db_sampler = self.build_data_aug_sample()
            self.remove_points_after_sample = True

        self.lidar_dir = './dataset/zhijiangyihao/training2/pointcloud/bin/'
        self.label_dir = './dataset/zhijiangyihao/training2/pointcloud/label/'       
        if mode == 'train':
            self.lidar_apollo_path = './dataset/apollo/ImageSets/train.txt' 
            self.label_apollo_path = './dataset/apollo/ImageSets/train_label.txt'
            # self.id_zhijiangyihao_path = './dataset/zhijiangyihao/training2/pointcloud/ImageSets/train_id.txt'
            self.id_zhijiangyihao_path = './dataset/zhijiangyihao/training2/pointcloud/ImageSets/train_id_outdoor.txt'
        elif mode == 'val':
            self.lidar_apollo_path = './dataset/apollo/ImageSets/val.txt'
            self.id_zhijiangyihao_path = './dataset/zhijiangyihao/training2/pointcloud/ImageSets/val_id.txt'
        elif mode == 'test':
            self.lidar_apollo_path = './dataset/apollo/ImageSets/val.txt'
            self.id_zhijiangyihao_path = './dataset/zhijiangyihao/training2/pointcloud/ImageSets/val_id.txt'

        if mode == 'train' or mode == 'val':
            self.sample_apollo_path_list = [x[0:-1] for x in open(self.lidar_apollo_path).readlines()]
            self.sample_apollo_label_list = [x[0:-1] for x in open(self.label_apollo_path).readlines()]
            self.sample_zhijiangyihao_path_list = [self.lidar_dir+x[0:-1]+'.bin' for x in open(self.id_zhijiangyihao_path).readlines()]
            self.sample_zhijiangyihao_label_list = [self.label_dir+x[0:-1]+'.txt' for x in open(self.id_zhijiangyihao_path).readlines()]
            # self.sample_path_list = self.sample_apollo_path_list + self.sample_zhijiangyihao_path_list
            # self.sample_label_list = self.sample_apollo_label_list + self.sample_zhijiangyihao_label_list
            self.sample_path_list = self.sample_zhijiangyihao_path_list
            self.sample_label_list = self.sample_zhijiangyihao_label_list            
        elif mode == 'test':
            self.sample_apollo_path_list = [x[0:-1] for x in open(self.lidar_apollo_path).readlines()]     
            self.sample_zhijiangyihao_path_list = [self.lidar_dir+x[0:-1]+'.bin' for x in open(self.id_zhijiangyihao_path).readlines()]
            self.sample_path_list = self.sample_apollo_path_list + self.sample_zhijiangyihao_path_list     
            self.sample_path_list = self.sample_zhijiangyihao_path_list

            # test path
            test_lidar_9_dir = './dataset/zhijiangyihao/training_fusion/bin9/'
            test_lidar_10_dir = './dataset/zhijiangyihao/training_fusion/bin10_filter/'
            self.test_9_path = './dataset/zhijiangyihao/training_fusion/test_9_shuffle.list'
            self.test_10_path = './dataset/zhijiangyihao/training_fusion/test_10_shuffle.list'
            # # self.id_zhijiangyihao_path = './dataset/apollo/lidar_rcnn/data_processer/ImageSets/id.txt'
            # self.sample_path_list = [test_lidar_9_dir+x[0:-1]+'.bin' for x in open(self.test_9_path).readlines()]
            self.sample_9_path_list = [test_lidar_9_dir+x[0:-1]+'.bin' for x in open(self.test_9_path).readlines()]
            self.sample_10_path_list = [test_lidar_10_dir+x[0:-1]+'.bin' for x in open(self.test_10_path).readlines()]
            # self.sample_10_path_list = []
            self.sample_path_list = self.sample_9_path_list + self.sample_10_path_list 
        self.sample_path_list = self.sample_path_list[0:10]
        # self.sample_label_list = self.sample_label_list[0:10]
        if num_samples is not None:
            self.sample_path_list = self.sample_path_list[num_samples:]
            # self.sample_path_list = self.sample_path_list[0]
        self.num_samples = len(self.sample_path_list)

    def __len__(self):
        return len(self.sample_path_list)

    def __getitem__(self, index):
        if self.mode == 'test':
            return self.load_img_only(index)
        else:
            return self.load_img_with_targets(index)

    def load_img_only(self, index):
        sample_path = self.sample_path_list[index]
        lidarData = self.get_lidar(sample_path)
        lidarData, _ = get_filtered_lidar(sample_path, lidarData, cnf.boundary)
        bev_map_ori = makeBEVMap(lidarData, cnf.boundary)
        bev_map, _ = make_bev_voxel_no_i_new_framework(lidarData)
        # bev_map = torch.from_numpy(bev_map)
        bev_map = bev_map.reshape(4, 10, cnf.BEV_WIDTH*cnf.BEV_HEIGHT)
        # bev_map_per_index = bev_map_per_index.reshape(-1)    
        
        if 'apollo' in sample_path:
            frame_id = sample_path.split('/')[-2] + '_' + sample_path.split('/')[-1].split('.bin')[0]
        else:
            frame_id = sample_path.split('/')[-1].split('.bin')[0]
        metadatas = {
            'frame_id': frame_id
        }
        data_dict = dict()
        data_dict.update({
            'voxel_features': bev_map,

        })
        data_dict.update(metadatas)
        return data_dict

    def load_img_with_targets(self, index):
        sample_path = self.sample_path_list[index]
        lidarData = self.get_lidar(sample_path)
        labels, has_labels = self.get_label(index)

        # For lidarData to data aug
        if np.random.random() <= 0.66:
            # lidarData, labels = self.do_data_aug_sample(lidarData, labels)            
            lidarData, labels = self.do_data_aug_global(lidarData, labels)
            lidarData = self.do_data_aug_pyramid(lidarData, labels)
        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])

        lidarData, labels, _ = get_filtered_lidar(sample_path, lidarData, cnf.boundary, labels)
        
        bev_map, bev_map_per_index = make_bev_voxel_no_i_new_framework(lidarData)
        # bev_map, bev_map_per_index = make_bev_voxel_relative_height_no_i(lidarData)

        # bev_map = torch.from_numpy(bev_map)

        hflipped = False
        if np.random.random() < self.hflip_prob:
            hflipped = True
            bev_map = torch.flip(bev_map, [-1])
        bev_map = bev_map.reshape(4, 10, cnf.BEV_WIDTH*cnf.BEV_HEIGHT)
        targets = self.build_targets(labels, hflipped)

        gt_boxes_lidar = labels[:, 1:]
        gt_names = []
        for i in range(labels.shape[0]):
            cls_id = int(labels[i][0])
            cls_name = self.class_names[cls_id-1]
            gt_names.append(cls_name)
        gt_classes = np.array([self.class_names.index(
                               n) + 1 for n in gt_names], dtype=np.int32)
        gt_boxes_lidar = np.concatenate(
            (gt_boxes_lidar, gt_classes.reshape(-1, 1).astype(np.float32)), axis=1)

        metadatas = {
            'hflipped': hflipped
        }
        data_dict = dict()
        data_dict.update({
            'voxel_features': bev_map,
            'gt_names': gt_names,
            'gt_boxes': gt_boxes_lidar,
            'targets': targets
        })

        # return metadatas, bev_map, targets
        return data_dict

    def do_data_aug_global(self, lidarData, labels):
        gt_boxes = labels[:, 1:][:, [0, 1, 2, 4, 5, 3, 6]].copy()
        gt_boxes[:, -1] = -gt_boxes[:, -1] - math.pi / 2

        gt_names = labels[:, 0].astype('int').astype('str')

        gt_boxes, lidarData, flipped = prep.random_flip_v2(gt_boxes, lidarData)
        gt_boxes, lidarData, noise_rotation = prep.global_rotation_v3(gt_boxes, lidarData, self.global_rotation_noise)
        gt_boxes, lidarData, noise_scale = prep.global_scaling_v3(gt_boxes, lidarData, *self.global_scaling_noise)

        # From w l h to h w l, ry to -ry - pi/2
        gt_boxes = gt_boxes[:, [0, 1, 2, 5, 3, 4, 6]]
        gt_boxes[:, -1] = -gt_boxes[:, -1] - math.pi / 2

        labels = np.concatenate([gt_names.reshape(-1, 1).astype(float), gt_boxes], axis=1)        

        return lidarData, labels

    def build_data_aug_sample(self, ):
        logger = logging.getLogger("build_dbsampler")        
        # data_root_prefix = './dataset/zhijiangyihao'
        # data_root_prefix = './dataset/apollo'
        data_root_prefix = './dataset/db_sample'
        db_sampler_cfg = dict(
            type="GT-AUG",
            enable=True,
            db_info_zhijiangyihao_path=data_root_prefix + "/dbinfos_train_zhijiangyihao.pkl",
            db_info_apollo_path=data_root_prefix + "/dbinfos_train_apollo.pkl",
            db_info_zhijiangyihao_si_path=data_root_prefix + "/dbinfos_train_zhijiangyihao_si.pkl",
            # sample_groups=[{'1':10}, {'2':0}, {'3':10}, {'4':5}, {'5':5}, {'6':0}],
            sample_groups=[{'1':5}, {'2':5}, {'3':10}, {'4':10}, {'5':5}, {'6':5}],
            # sample_groups=[{'2':5}],
            db_prep_steps=[
                dict(filter_by_min_num_points={'1':5, '2':20, '3':5, '4':5, '5':5, '6':5}),
                # dict(filter_by_min_num_points={'2':5}),
                dict(filter_by_difficulty=[-1, -1, -1, -1, -1, -1],),    # todo: need to check carefully
            ],
            global_random_rotation_range_per_object=[0, 0],
            rate=1.0,
            gt_random_drop=-1,
            gt_aug_with_context=-1,
            gt_aug_similar_type=False,
        )
    
        prepors = [build_db_preprocess(c, logger=logger) for c in db_sampler_cfg['db_prep_steps']]
        db_prepor = DataBasePreprocessor(prepors)
        rate = db_sampler_cfg['rate']                                                 # 1.0
        grot_range = list(db_sampler_cfg['global_random_rotation_range_per_object'])  # [0, 0]
        groups = db_sampler_cfg['sample_groups']                                      # [dict(Car=15,),],
        info_zhijiangyihao_path = db_sampler_cfg['db_info_zhijiangyihao_path']         # object/dbinfos_train.pickle
        info_apollo_path = db_sampler_cfg['db_info_apollo_path']                       # object/dbinfos_train.pickle
        info_zhijiangyihao_si_path = db_sampler_cfg['db_info_zhijiangyihao_si_path']         # object/dbinfos_train.pickle
        gt_random_drop = db_sampler_cfg['gt_random_drop']

        gt_aug_with_context = db_sampler_cfg['gt_aug_with_context']
        gt_aug_similar_type = db_sampler_cfg['gt_aug_similar_type']

        with open(info_zhijiangyihao_path, "rb") as f:
            db_zhijiangyihao_infos = pickle.load(f)
        with open(info_apollo_path, "rb") as f:
            db_apollo_infos = pickle.load(f)
        with open(info_zhijiangyihao_si_path, "rb") as f:
            db_zhijiangyihao_si_infos = pickle.load(f)
            
        db_infos = {}
        # db_infos = db_apollo_infos
        # db_infos = db_zhijiangyihao_infos
        # db_infos = db_zhijiangyihao_si_infos
        for key in db_zhijiangyihao_infos:
            db_infos[key] = db_zhijiangyihao_infos[key] + db_apollo_infos[key]
        for key in db_zhijiangyihao_si_infos:
            db_infos[key] = db_infos[key] + db_zhijiangyihao_si_infos[key]


        sampler = DataBaseSamplerV2(db_infos, groups, db_prepor, rate, grot_range, logger=logger, gt_random_drop=gt_random_drop,\
                                    gt_aug_with_context=gt_aug_with_context, gt_aug_similar_type=gt_aug_similar_type)

        return sampler

    def do_data_aug_sample(self, lidarData, labels):
        # data_root_prefix = './dataset/zhijiangyihao'
        data_root_prefix = './dataset/db_sample'
        # data_root_prefix = './dataset/apollo'
        data_root_prefix = Path(data_root_prefix)
        gt_boxes_mask = np.ones((labels.shape[0]), dtype=np.bool_)
        labels_ori = labels
        # From h w l to w l h, ry to -ry - pi/2
        gt_boxes = labels[:, 1:][:, [0, 1, 2, 4, 5, 3, 6]].copy()
        gt_boxes[:, -1] = -gt_boxes[:, -1] - math.pi / 2

        gt_names = labels[:, 0].astype('int').astype('str')
        if self.db_sampler:
            sampled_dict = self.db_sampler.sample_all(
                data_root_prefix,
                gt_boxes,
                gt_names,
                4,
                False,  # False
                gt_group_ids=None,
                calib=None,
                targeted_class_names=None,
            )        

            if sampled_dict is not None:
                sampled_gt_names = sampled_dict["gt_names"]
                sampled_gt_boxes = sampled_dict["gt_boxes"]
                sampled_points = sampled_dict["points"]
                sampled_gt_masks = sampled_dict["gt_masks"]  # all 1.

                gt_names = np.concatenate([gt_names, sampled_gt_names], axis=0)
                gt_boxes = np.concatenate([gt_boxes, sampled_gt_boxes])
                # gt_names = sampled_gt_names
                # gt_boxes = sampled_gt_boxes             
                gt_boxes_mask = np.concatenate([gt_boxes_mask, sampled_gt_masks], axis=0)

                # True, remove points in original scene with location occupied by auged gt boxes.
                if self.remove_points_after_sample:
                    masks = box_np_ops.points_in_rbbox(lidarData, sampled_gt_boxes)
                    # masks = box_np_ops.points_in_rbbox(lidarData, gt_boxes)
                    lidarData = lidarData[np.logical_not(masks.any(-1))]
                lidarData = np.concatenate([sampled_points, lidarData], axis=0)  # concat existed points and points in gt-aug boxes

            # From w l h to h w l, ry to -ry - pi/2
            gt_boxes = gt_boxes[:, [0, 1, 2, 5, 3, 4, 6]]
            gt_boxes[:, -1] = -gt_boxes[:, -1] - math.pi / 2

            labels = np.concatenate([gt_names.reshape(-1, 1).astype(float), gt_boxes], axis=1)
        return lidarData, labels

    def do_data_aug_pyramid(self, lidarData, labels):
        # From h w l to w l h, ry to -ry - pi/2
        gt_boxes = labels[:, 1:][:, [0, 1, 2, 4, 5, 3, 6]].copy()
        # gt_boxes = labels[:, 1:][:, [0, 1, 2, 3, 4, 5, 6]].copy()
        gt_boxes[:, -1] = -gt_boxes[:, -1] - math.pi / 2
        gt_boxes_car = gt_boxes[labels[:, 0] == 1].copy()
        gt_boxes_person = gt_boxes[labels[:, 0] == 3].copy()
        # for car
        lidarData = sa_da_v2.pyramid_augment_v0(gt_boxes_car, lidarData,
                                                enable_sa_dropout=0.25,
                                                enable_sa_sparsity=[0.05, 50],
                                                enable_sa_swap=[0.1, 50],
                                                )
        # for person
        lidarData = sa_da_v2.pyramid_augment_v0(gt_boxes_person, lidarData,
                                                enable_sa_dropout=0.2,
                                                enable_sa_sparsity=[0.1, 25],
                                                enable_sa_swap=[0.1, 10],
                                                )
        return lidarData

    def get_lidar(self, lidar_file):
        return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)

    def draw_lidar_with_label(self, index):
        sample_path = self.sample_path_list[index]
        print('sample_path:', sample_path)
        lidarData = self.get_lidar(sample_path)
        labels, has_labels = self.get_label(index)

        # For data aug
        lidarData, labels = self.do_data_aug_sample(lidarData, labels)

        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])

        lidarData, labels, _ = get_filtered_lidar(sample_path, lidarData, cnf.boundary, labels)
        bev_map = makeBEVMap(lidarData, cnf.boundary)

        return bev_map, labels

    def get_label(self, idx):
        labels = []
        label_path = self.sample_label_list[idx]
        for line in open(label_path, 'r'):
            line = line.rstrip()
            line_parts = line.split(' ')
            cat_id = int(line_parts[0])  # 1 for small vehicles, 2 for big vehicles, 3 for pedestrian, 4 for motorcyclist and bicyclist, 5 for traffic cones and 6 for others
            # print("cat_id:", cat_id)
            if cat_id <= -99:  # ignore Tram and Misc
                continue
            x, y, z = float(line_parts[1]), float(line_parts[2]), float(line_parts[3])
            l, w, h = float(line_parts[4]), float(line_parts[5]), float(line_parts[6])
            ry = float(line_parts[7])
            # object_label = [cat_id, x, y, z, h, w, l, ry]
            object_label = [cat_id, x, y, z, l, w, h, ry]
            labels.append(object_label)

        if len(labels) == 0:
            labels = np.zeros((1, 8), dtype=np.float32)
            has_labels = False
        else:
            labels = np.array(labels, dtype=np.float32)
            has_labels = True

        return labels, has_labels

    def build_targets(self, labels, hflipped):
        minX = cnf.boundary['minX']
        maxX = cnf.boundary['maxX']
        minY = cnf.boundary['minY']
        maxY = cnf.boundary['maxY']
        minZ = cnf.boundary['minZ']
        maxZ = cnf.boundary['maxZ']

        num_objects = min(len(labels), self.max_objects)
        hm_l, hm_w = self.hm_size

        hm_main_center = np.zeros((self.num_classes, hm_l, hm_w), dtype=np.float32)
        cen_offset = np.zeros((self.max_objects, 2), dtype=np.float32)
        direction = np.zeros((self.max_objects, 2), dtype=np.float32)
        z_coor = np.zeros((self.max_objects, 1), dtype=np.float32)
        dimension = np.zeros((self.max_objects, 3), dtype=np.float32)

        indices_center = np.zeros((self.max_objects), dtype=np.int64)
        obj_mask = np.zeros((self.max_objects), dtype=np.uint8)

        for k in range(num_objects):
            cls_id, x, y, z, h, w, l, yaw = labels[k]
            cls_id = int(cls_id)-1
            yaw = -yaw
            if not ((minX <= x <= maxX) and (minY <= y <= maxY) and (minZ <= z <= maxZ)):
                continue
            if (h <= 0) or (w <= 0) or (l <= 0):
                continue

            bbox_l = l / cnf.bound_size_x * hm_l
            bbox_w = w / cnf.bound_size_y * hm_w
            radius = compute_radius((math.ceil(bbox_l), math.ceil(bbox_w)))
            # radius = max(0, int(radius))
            radius = max(2, int(radius))

            center_y = (x - minX) / cnf.bound_size_x * hm_l  # x --> y (invert to 2D image space)
            center_x = (y - minY) / cnf.bound_size_y * hm_w  # y --> x
            center = np.array([center_x, center_y], dtype=np.float32)

            if hflipped:
                center[0] = hm_w - center[0] - 1

            center_int = center.astype(np.int32)
            if cls_id < 0:
                ignore_ids = [_ for _ in range(self.num_classes)] if cls_id == - 1 else [- cls_id - 2]
                # Consider to make mask ignore
                for cls_ig in ignore_ids:
                    gen_hm_radius(hm_main_center[cls_ig], center_int, radius)
                hm_main_center[ignore_ids, center_int[1], center_int[0]] = 0.9999
                continue
            
            try:
                # Generate heatmaps for main center
                gen_hm_radius(hm_main_center[cls_id], center, radius)
                # Index of the center
                indices_center[k] = center_int[1] * hm_w + center_int[0]
            except:
                a = 0

            # targets for center offset
            cen_offset[k] = center - center_int

            # targets for dimension
            dimension[k, 0] = h
            dimension[k, 1] = w
            dimension[k, 2] = l

            # targets for direction
            direction[k, 0] = math.sin(float(yaw))  # im
            direction[k, 1] = math.cos(float(yaw))  # re
            # im -->> -im
            if hflipped:
                direction[k, 0] = - direction[k, 0]

            # targets for depth
            z_coor[k] = z - minZ

            # Generate object masks
            obj_mask[k] = 1

        targets = {
            'hm_cen': hm_main_center,
            'cen_offset': cen_offset,
            'direction': direction,
            'z_coor': z_coor,
            'dim': dimension,
            'indices_center': indices_center,
            'obj_mask': obj_mask,
        }

        return targets

if __name__ == '__main__':
    from easydict import EasyDict as edict
    from data_process.transformation import OneOf, Random_Scaling, Random_Rotation, lidar_to_camera_box
    from utils.visualization_utils import merge_rgb_to_bev, show_rgb_image_with_boxes

    configs = edict()
    configs.distributed = False  # For testing
    configs.pin_memory = False
    configs.num_samples = None
    # configs.input_size = (608, 608)
    configs.input_size = (1216, 608)
    # configs.hm_size = (152, 152)
    configs.hm_size = (304, 152)
    configs.max_objects = 50
    configs.num_classes = 3
    configs.output_width = 608

    lidar_aug = None

    dataset = ApolloDataset(configs, mode='train', lidar_aug=lidar_aug, hflip_prob=0., num_samples=configs.num_samples)         

    for idx in range(len(dataset)): 

        bev_map, labels = dataset.draw_lidar_with_label(idx) 
        bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)

        bev_map = cv2.resize(bev_map, (cnf.BEV_WIDTH, cnf.BEV_HEIGHT))
        bev_map_ori = bev_map.copy()

        for box_idx, (cls_id, x, y, z, h, w, l, yaw) in enumerate(labels):
            if (h <= 0) or (w <= 0) or (l <= 0):
                continue
            yaw = -yaw
            y1 = int((x - cnf.boundary['minX']) / cnf.DISCRETIZATION_Y)
            x1 = int((y - cnf.boundary['minY']) / cnf.DISCRETIZATION_X)
            w1 = int(w / cnf.DISCRETIZATION_X)
            l1 = int(l / cnf.DISCRETIZATION_Y)

            drawRotatedBox(bev_map, x1, y1, w1, l1, yaw, cnf.colors[int(cls_id)], cls_id)

        bev_map_ori = cv2.rotate(bev_map_ori, cv2.ROTATE_180)
        bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
        bev_map_ori = cv2.resize(bev_map_ori, (500, 1000))
        bev_map = cv2.resize(bev_map, (500, 1000))
        cv2.imshow('bev_map_ori', bev_map_ori)
        cv2.imshow('bev_map', bev_map)

        keyboard = cv2.waitKey(0)
       