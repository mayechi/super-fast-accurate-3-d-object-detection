import os
import sys
import cv2
import numpy as np
import torch

sys.path.append('./')
from utils.simple_utils_rect import makeBEVMap, drawRotatedBox, filter_label, get_corners
from utils.lidar_to_camera import boxes3d_to_corners3d
from utils.lidar_to_camera import corners3d_to_camera3d
from utils.lidar_to_camera import camera3d_to_img_boxes
from utils.lidar_to_camera import find_index_in_image
from utils.simple_utils import get_filtered_lidar
import config.camera_config as cnf

numpy_pcd_type_mappings = [(np.dtype('float32'), ('F', 4)),
                           (np.dtype('float64'), ('F', 8)),
                           (np.dtype('uint8'), ('U', 1)),
                           (np.dtype('uint16'), ('U', 2)),
                           (np.dtype('uint32'), ('U', 4)),
                           (np.dtype('uint64'), ('U', 8)),
                           (np.dtype('int16'), ('I', 2)),
                           (np.dtype('int32'), ('I', 4)),
                           (np.dtype('int64'), ('I', 8))]
numpy_type_to_pcd_type = dict(numpy_pcd_type_mappings)
pcd_type_to_numpy_type = dict((q, p) for (p, q) in numpy_pcd_type_mappings)

flag_show = True
# flag_detec = False
def lidar3d_to_camera3d(lidar3d):
	"""
	:params lidar3d: (N, 3)
	:return camera3d: (N, 3)
	"""
	# calibration
	T_lidar_to_camera = np.array(
		[[0.0277387, -0.999607, -0.00404495, 0.108141],
		 [0.00308998, 0.00413226, -0.999987, -0.123988],
		 [0.99961, 0.0277258, 0.00320342, -0.197486]])

	sample_num = lidar3d.shape[0]
	lidar3d_hom = np.concatenate((lidar3d, np.ones((sample_num, 1))),
								   axis=1)  # (N, 3)
	pts_image = np.matmul(lidar3d_hom, T_lidar_to_camera.T)

	return pts_image

def camera3d_to_camera2d(lidar, camera3d):

	image_width = 1280
	image_height = 720

	camera_matrix = np.array(
		[[1.312364254819129e+03, 0., 5.880519199337333e+02],
		 [0., 1.312927278233663e+03, 3.316051861963202e+02], [0., 0., 1.]])
	dist_coef = np.array([[
		-0.487604444954244, 0.294488520444290, 0.001201495313292,
		7.242191805280423e-04, -0.090999234677858
	]])

	# check
	new_camera_matrix, _ = cv2.getOptimalNewCameraMatrix(
		camera_matrix, dist_coef, (image_width, image_height), 1,
		(image_width, image_height))

	zeros = np.array([[0., 0., 0.]])
	ones = np.array([[1., 0., 0.], [0., 1., 0.], [0., 0., 1.]])

	flag_z = ~(camera3d[:, 2] < 0)
	# range check
	cols = new_camera_matrix[
		0, 2] + new_camera_matrix[0, 0] * camera3d[:, 0] / camera3d[:, 2]
	rows = new_camera_matrix[
		1, 2] + new_camera_matrix[1, 1] * camera3d[:, 1] / camera3d[:, 2]
   	
	flag_0 = ~np.bitwise_or(cols < 0, rows < 0)
	flag_max = ~np.bitwise_or(cols > image_width, rows > image_height)

	flag_camera_3d = np.bitwise_and(np.bitwise_and(flag_z, flag_0), flag_max)
	camera3d = camera3d[flag_camera_3d]
	lidar = lidar[flag_camera_3d]
	
	# project
	camera_pts, _ = cv2.projectPoints(camera3d, ones, zeros, camera_matrix,
									  dist_coef)
	camera_pts = camera_pts[:, 0, :]
	camera_pts[:, 1] += 20
	
	flag_pt_0 = ~np.bitwise_or(camera_pts[:, 0] < 0., camera_pts[:, 1] < 0.)
	flag_pt_max = ~np.bitwise_or(camera_pts[:, 0] > image_width, camera_pts[:, 1] > image_height)
	flag_pts = np.bitwise_and(flag_pt_0 ,flag_pt_max)
	camera_pts = camera_pts[flag_pts]
	camera3d = camera3d[flag_pts]
	lidar = lidar[flag_pts]

	return lidar, camera_pts, camera3d

def get_2D_label(label_file):
	labels = []
	for line in open(label_file, 'r'):
		line = line.rstrip().replace('\ufeff', '')
		if line == '':
			continue
		line_parts = line.split(' ')
		x1, y1 = float(line_parts[0]), float(line_parts[1])
		x2, y2 = float(line_parts[2]), float(line_parts[3])
		cls = int(line_parts[4])
		object_label = [x1, y1, x2, y2, cls]
		labels.append(object_label)
	if len(labels) == 0:
		has_labels = False
	else:
		has_labels = True

	return labels, has_labels

def get_3D_label(label_file):
	labels = []
	for line in open(label_file, 'r'):
		line = line.rstrip()
		line_parts = line.split(' ')
		cat_id = int(line_parts[0])  # 1 for small vehicles, 2 for big vehicles, 3 for pedestrian, 4 for motorcyclist and bicyclist, 5 for traffic cones and 6 for others
		# print("cat_id:", cat_id)
		if cat_id <= -99:  # ignore Tram and Misc
			continue
		x, y, z = float(line_parts[1]), float(line_parts[2]), float(line_parts[3])
		l, w, h = float(line_parts[4]), float(line_parts[5]), float(line_parts[6])
		# w, l, h = float(line_parts[4]), float(line_parts[5]), float(line_parts[6])
		ry = float(line_parts[7])
		object_label = [cat_id, x, y, z, h, w, l, ry]
		labels.append(object_label)

	if len(labels) == 0:
		labels = np.zeros((1, 8), dtype=np.float32)
		has_labels = False
	else:
		labels = np.array(labels, dtype=np.float32)
		has_labels = True

	return labels, has_labels

def get_image_point(point_all, camera_pts, labels, has_labels):
	point_in_detec = np.zeros((0, 4))
	camera_pts_in_detec = np.zeros((0, 2))
	if has_labels:
		for label in labels:
			x1, y1, x2, y2, cls = label
			flag_x = np.bitwise_and(camera_pts[:, 0] > x1-5, camera_pts[:, 0] < x2+5)
			flag_y = np.bitwise_and(camera_pts[:, 1] > y1-5, camera_pts[:, 1] < y2+5)
			flag_xy = np.bitwise_and(flag_x, flag_y)
			point = point_all[flag_xy]
			camera = camera_pts[flag_xy]
			if point.shape[0] != 0:
				point_in_detec = np.vstack((point_in_detec, point))
				camera_pts_in_detec = np.vstack((camera_pts_in_detec, camera))
		
		if point_in_detec.shape[0] == 0:
			return False, None, None
		else:
			return has_labels, point_in_detec, camera_pts_in_detec
	else:
		return has_labels, None, None

def tran_3d_2d(lidar, labels_3D, labels_2D):
	pred_boxes_list, pre_clses_list = [], []
	for label in labels_3D:
		cls_id, x, y, z, h, w, l, yaw = label
		pred_boxes_list.append([x, y, z, l, w, h, yaw])
		pre_clses_list.append([cls_id])
	if len(pred_boxes_list) == 0:
		return False, None, None, None
	pred_boxes = np.array(pred_boxes_list)
	pred_clses = np.array(pre_clses_list)

	# For Boxes
	corners3d = boxes3d_to_corners3d(pred_boxes)
	camera3d = corners3d_to_camera3d(corners3d)
	used_index, camera2d = camera3d_to_img_boxes(camera3d)
	labels_3D = labels_3D[used_index]
	if len(camera2d) == 0:
		return False, None, None, None, None
	labels_3D, camera2d = map_label2D(labels_3D, labels_2D, camera2d)

	# For Points
	lidar_camera_c = lidar3d_to_camera3d(lidar[:, 0:-1])
	lidar_in_image, lidar_pts, _ = camera3d_to_camera2d(lidar, lidar_camera_c)
	if len(camera2d) == 0:
		return False, None, None, None, None
	else:
		_, lidar_in_rect, _ = get_image_point(lidar_in_image, lidar_pts, camera2d, True)
		return True, lidar_in_image, lidar_in_rect, labels_3D, camera2d

def boxes_iou_normal(boxes_a, boxes_b):
    """
    Args:
        boxes_a: (N, 4) [x1, y1, x2, y2]
        boxes_b: (M, 4) [x1, y1, x2, y2]

    Returns:

    """
    assert boxes_a.shape[1] == boxes_b.shape[1] == 4
    x_min = torch.max(boxes_a[:, 0, None], boxes_b[None, :, 0])
    x_max = torch.min(boxes_a[:, 2, None], boxes_b[None, :, 2])
    y_min = torch.max(boxes_a[:, 1, None], boxes_b[None, :, 1])
    y_max = torch.min(boxes_a[:, 3, None], boxes_b[None, :, 3])
    x_len = torch.clamp_min(x_max - x_min, min=0)
    y_len = torch.clamp_min(y_max - y_min, min=0)
    area_a = (boxes_a[:, 2] - boxes_a[:, 0]) * (boxes_a[:, 3] - boxes_a[:, 1])
    area_b = (boxes_b[:, 2] - boxes_b[:, 0]) * (boxes_b[:, 3] - boxes_b[:, 1])
    a_intersect_b = x_len * y_len
    iou = a_intersect_b / torch.clamp_min(area_a[:, None] + area_b[None, :] - a_intersect_b, min=1e-6)
    return iou

def map_label2D(labels_3D, labels_2D, camera2d):
	# labels_2d is 2D boxes annotated on the image
	# camera2d is converted from the 3D box labeled on the point cloud
	labels_2D_tensor = torch.tensor(labels_2D, dtype=torch.float32)[:, 0:-1]
	camera2d_tensor = torch.tensor(camera2d, dtype=torch.float32)
	iou = boxes_iou_normal(labels_2D_tensor, camera2d_tensor)
	iou_max_score, iou_max_index = torch.max(iou, 1)
	labels_2D_new, labels_3D_new = list(), list()
	for i in range(len(labels_2D)):
		if iou_max_score[i] > 0.2:
			cls_3d, cls_2d = int(labels_3D[iou_max_index[i]][0]), labels_2D[i][-1]
			if (cls_3d == 1 or cls_3d == 2) and (cls_2d == 0 or cls_2d == 4):
			# if 1:
				labels_2D_new.append(labels_2D[i])
				labels_3D_new.append(labels_3D[iou_max_index[i]])

	return np.array(labels_3D_new), labels_2D_new

def save_pointcloud_bin(pointcloud, save_path):
    points = pointcloud.point
    header = {
        'version': .7,
        'fields': ['x', 'y', 'z', 'intensity'],
        'count': [1, 1, 1, 1],
        'width': len(points),
        'height': 1,
        'viewpoint': [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
        'points': len(points),
        'type': ['F', 'F', 'F', 'U'],
        'size': [4, 4, 4, 4],
        'data': 'binary'
    }
    typenames = []
    for pcd_type, numpy_type in zip(header['type'], header['size']):
        numpy_type = pcd_type_to_numpy_type[(pcd_type, numpy_type)]
        typenames.append(numpy_type)
    np_dtype = np.dtype(list(zip(header['fields'], typenames)))
    arr = np.zeros(len(points), dtype=np_dtype)
    for i, point in enumerate(points):
        # change timestamp to timestamp_sec
        arr[i] = (
            point.x,
            point.y,
            point.z,
            point.intensity,
        )
    bin_file = os.path.join(
        save_path,
        str(int(pointcloud.measurement_time * 1e6)) + '.bin')
    arr.tofile(bin_file)

COLOR_MAP = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (102, 204, 153),
             (255, 255, 0), (255, 255, 255), (0, 0, 0), (255, 102, 0),
             (255, 153, 153), (153, 0, 51)]

lidar_dir_path = '/media/myc/Data21/zhijiangyihao/training_fusion/bin9/'
image_dir_path = '/media/myc/Data21/zhijiangyihao/training_fusion/image9/'
label_2D_dir_path = '/media/myc/Data21/zhijiangyihao/training_fusion/label9_2D/'
label_3D_dir_path = '/media/myc/Data21/zhijiangyihao/training_fusion/label9/'

lidar_in_image_dir_path = '/media/myc/Data21/zhijiangyihao/training_fusion/bin9_in_image/'

lidar_path_list = os.listdir(lidar_dir_path)
lidar_path_list.sort()
image_path_list = os.listdir(image_dir_path)
image_path_list.sort()
label_2D_path_list = os.listdir(label_2D_dir_path)
label_2D_path_list.sort()
label_3D_path_list = os.listdir(label_3D_dir_path)
label_3D_path_list.sort()

for i in range(len(lidar_path_list)):
	print(lidar_path_list[i])
	# if lidar_path_list[i] == '1630402509637274-1630402509563752.bin':
	# 	a = 0
	# else:
	# 	continue
	lidar = np.fromfile(lidar_dir_path+lidar_path_list[i], dtype=np.float32).reshape(-1, 4)
	image = cv2.imread(image_dir_path+image_path_list[i])
	labels_2D, has_labels_2D = get_2D_label(label_2D_dir_path+label_2D_path_list[i])
	labels_3D, has_labels_3D = get_3D_label(label_3D_dir_path+label_3D_path_list[i])
	if (labels_3D.shape[0] <= 0 or len(labels_2D) <= 0):
		continue

	has_labels, lidar_in_image, lidar_in_rect, labels_3D, camera2d = tran_3d_2d(lidar, labels_3D, labels_2D)

	if has_labels == True:
		lidar, _ = get_filtered_lidar('', lidar, cnf.boundary)
		lidar_in_image, _ = get_filtered_lidar('', lidar_in_image, cnf.boundary)
		lidar_in_rect, _ = get_filtered_lidar('', lidar_in_rect, cnf.boundary)
		# save_pointcloud_bin(lidar_in_detec, lidar_in_image_dir_path+lidar_path_list[i])

		if flag_show:
			bev_map = makeBEVMap(lidar, cnf.boundary)
			bev_map_in_image = makeBEVMap(lidar_in_image, cnf.boundary)
			bev_map_in_detec = makeBEVMap(lidar_in_rect, cnf.boundary)

			bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
			bev_map = cv2.resize(bev_map, (cnf.BEV_WIDTH, cnf.BEV_HEIGHT))
			bev_map_in_image = (bev_map_in_image.transpose(1, 2, 0) * 255).astype(np.uint8)
			bev_map_in_image = cv2.resize(bev_map_in_image, (cnf.BEV_WIDTH, cnf.BEV_HEIGHT))
			bev_map_in_detec = (bev_map_in_detec.transpose(1, 2, 0) * 255).astype(np.uint8)
			bev_map_in_detec = cv2.resize(bev_map_in_detec, (cnf.BEV_WIDTH, cnf.BEV_HEIGHT))
			bev_map_in_detec_label = bev_map_in_detec.copy()

			# Draw 2D labels in image, and show image
			# camera_pts = camera_pts_in_detec
			# for i in range(camera_pts.shape[0]):
			# 	depth = valid_camera3d[i, 2]
			# 	color_idx = int(depth) % 10
			# 	cv2.circle(image, (int(camera_pts[i, 0]), int(camera_pts[i, 1])), 1, COLOR_MAP[color_idx], 1)
			
			for label in camera2d:
				x1, y1, x2, y2, cls = label
				cv2.rectangle(image, (int(x1), int(y1)), (int(x2), int(y2)), (0, 0, 255), 2)
			cv2.imshow('image', image)

			# Draw 3D labels in bev_map and bev_map_in_detec
			labels_3D = filter_label(bev_map, labels_3D)
			for box_idx, (cls_id, x, y, z, h, w, l, yaw) in enumerate(labels_3D):
				if (h <= 0) or (w <= 0) or (l <= 0):
					continue
				yaw = -yaw
				y1 = int((x - cnf.boundary['minX']) / cnf.DISCRETIZATION_Y)
				x1 = int((y - cnf.boundary['minY']) / cnf.DISCRETIZATION_X)
				w1 = int(w / cnf.DISCRETIZATION_X)
				l1 = int(l / cnf.DISCRETIZATION_Y)
				drawRotatedBox(bev_map, x1, y1, w1, l1, yaw, cnf.colors[int(cls_id)], cls_id)
				drawRotatedBox(bev_map_in_detec_label, x1, y1, w1, l1, yaw, cnf.colors[int(cls_id)], cls_id)

			# Imshow bev_map and bev_map_in_detec in ROTATE_180
			bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
			cv2.imshow('bev_map', bev_map)
			bev_map_in_image = cv2.rotate(bev_map_in_image, cv2.ROTATE_180)
			cv2.imshow('bev_map_in_image', bev_map_in_image)	
			bev_map_in_detec = cv2.rotate(bev_map_in_detec, cv2.ROTATE_180)
			cv2.imshow('bev_map_in_detec', bev_map_in_detec)
			bev_map_in_detec_label = cv2.rotate(bev_map_in_detec_label, cv2.ROTATE_180)
			cv2.imshow('bev_map_in_detec_label', bev_map_in_detec_label)
			cv2.waitKey(0)
	else:
		continue
  