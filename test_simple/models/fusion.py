import torch
import torch.nn as nn

class Fusion_Voxel(nn.Module):
    def __init__(self, model_cfg):
        super(Fusion_Voxel, self).__init__()
        self.model_cfg = model_cfg
        feature_lenth, deep = self.model_cfg.feature_lenth, self.model_cfg.deep
        # feature_lenth, deep = 4, 10
        self.fusion = nn.Sequential(
                nn.Conv2d(feature_lenth, 32, kernel_size=(3, 1), stride=1, padding=(1, 0), bias=False),   
                nn.ReLU(),
                nn.Conv2d(32, 16, kernel_size=(3, 1), stride=1, padding=(1, 0), bias=False),
                nn.ReLU(),
                nn.MaxPool2d(kernel_size=(deep, 1), stride=1, padding=0)
            )
    def forward(self, batch_dict):
        x = batch_dict['voxel_features']
        batch_size = x.shape[0]
        x = self.fusion(x)
        # x = x.reshape(batch_size, 16, 1216, 608)
        x = x.reshape(batch_size, 16, 608, 1216)
        batch_dict['bev_features'] = x
        return batch_dict