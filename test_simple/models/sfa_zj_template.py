import os
import torch
import torch.nn as nn

from models import fusion, backbone, detec_head_3d

from utils import nms_utils

class SFA_ZJ_Template(nn.Module):
    def __init__(self, model_cfg, num_class, dataset):
        super().__init__()
        self.model_cfg = model_cfg
        self.num_class = num_class
        self.dataset = dataset
        self.class_names = dataset.class_names
        self.register_buffer('global_step', torch.LongTensor(1).zero_())

        self.module_topology = [
            'fusion', 'backbone', 'dense_head'
        ]

    def train(self, mode=True):
        self.training = mode
        for module in self.children():
            if module in self.model_info_dict['fixed_module_list']:
                module.eval()
            else:
                module.train(mode)
        return self

    @property
    def mode(self):
        return 'TRAIN' if self.training else 'TEST'

    def update_global_step(self):
        self.global_step += 1

    def build_networks(self):
        model_info_dict = {
            'module_list': [],
            'fixed_module_list': [],
            'grid_size': getattr(self.dataset, 'stereo_grid_size', self.dataset.grid_size),
            'point_cloud_range': self.dataset.point_cloud_range,
            'voxel_size': getattr(self.dataset, 'stereo_voxel_size', self.dataset.voxel_size),            
        }
        for module_name in self.module_topology:
            module, model_info_dict = getattr(self, 'build_%s' % module_name)(
                model_info_dict=model_info_dict
            )
            self.add_module(module_name, module)
        self.model_info_dict = model_info_dict
        return model_info_dict['module_list']

    def build_fusion(self, model_info_dict):
        if self.model_cfg.get('FUSION', None) is None:
            return None, model_info_dict

        fusion_module = fusion.Fusion_Voxel(model_cfg=self.model_cfg.FUSION)

        model_info_dict['module_list'].append(fusion_module)

        return fusion_module, model_info_dict

    def build_backbone(self, model_info_dict):
        if self.model_cfg.get('BACKBONE', None) is None:
            return None, model_info_dict

        backbone_module = backbone.BackBone_Bev(model_cfg=self.model_cfg.BACKBONE)

        model_info_dict['module_list'].append(backbone_module)

        return backbone_module, model_info_dict

    def build_dense_head(self, model_info_dict):
        if self.model_cfg.get('DENSE_HEAD', None) is None:
            return None, model_info_dict
        dense_head_module = detec_head_3d.DetHead(
            model_cfg=self.model_cfg.DENSE_HEAD,
            input_channels=self.model_cfg.DENSE_HEAD.num_channels,
            num_class=self.num_class if not self.model_cfg.DENSE_HEAD.CLASS_AGNOSTIC else 1,
            class_names=self.class_names,
            grid_size=model_info_dict['grid_size'],
            point_cloud_range=model_info_dict['point_cloud_range'],
            predict_boxes_when_training=self.model_cfg.get('ROI_HEAD', False) or self.model_cfg.DENSE_HEAD.get('predict_boxes_when_training', False)
        )
        model_info_dict['module_list'].append(dense_head_module)
        return dense_head_module, model_info_dict

    def forward(self, **kwargs):
        raise NotImplementedError

    def post_processing(self, batch_dict):
        """
        Args:
            batch_dict:
                batch_size:
                batch_cls_preds: (B, num_boxes, num_classes | 1) or (
                    N1+N2+..., num_classes | 1)
                                or [(B, num_boxes, num_class1), (B, num_boxes, num_class2) ...]
                multihead_label_mapping: [(num_class1), (num_class2), ...]
                batch_box_preds: (B, num_boxes, 7+C) or (N1+N2+..., 7+C)
                cls_preds_normalized: indicate whether batch_cls_preds is normalized
                batch_index: optional (N1+N2+...)
                has_class_labels: True/False
                roi_labels: (B, num_rois)  1 .. num_classes
                batch_pred_labels: (B, num_boxes, 1)
        Returns:

        """
        post_process_cfg = self.model_cfg.POST_PROCESSING
        batch_size = batch_dict['batch_size']
        recall_dict = {}
        pred_dicts = []
        for index in range(batch_size):
            if batch_dict.get('batch_index', None) is not None:
                assert batch_dict['batch_box_preds'].shape.__len__() == 2
                batch_mask = (batch_dict['batch_index'] == index)
            else:
                assert batch_dict['batch_box_preds'].shape.__len__() == 3
                batch_mask = index

            box_preds = batch_dict['batch_box_preds'][batch_mask]
            src_box_preds = box_preds

            if not isinstance(batch_dict['batch_cls_preds'], list):
                cls_preds = batch_dict['batch_cls_preds'][batch_mask]

                src_cls_preds = cls_preds
                assert cls_preds.shape[1] in [1, self.num_class]

                if not batch_dict['cls_preds_normalized']:
                    cls_preds = torch.sigmoid(cls_preds)
            else:
                cls_preds = [x[batch_mask]
                             for x in batch_dict['batch_cls_preds']]
                src_cls_preds = cls_preds
                if not batch_dict['cls_preds_normalized']:
                    cls_preds = [torch.sigmoid(x) for x in cls_preds]

            '''add by me'''
            # if 'toonnx' in batch_dict.keys():
            #     record_dict = {
            #         'cls_preds': cls_preds,
            #         'box_preds': box_preds,
            #     }
            #     pred_dicts.append(record_dict)
            #     continue
            '''add by me'''

            if post_process_cfg.NMS_CONFIG.MULTI_CLASSES_NMS:
                if batch_dict.get('has_class_labels', False):
                    label_key = 'roi_labels' if 'roi_labels' in batch_dict else 'batch_pred_labels'
                    label_preds = batch_dict[label_key][index]
                else:
                    label_preds = None
                if 'toonnx' in batch_dict.keys():
                    pred_scores, pred_labels, pred_boxes = nms_utils.multi_classes_nms(
                        cls_scores=cls_preds, box_preds=box_preds,
                        nms_config=post_process_cfg.NMS_CONFIG,
                        score_thresh=0,
                        label_preds=label_preds,
                        toonnx = True
                    )
                else:
                    pred_scores, pred_labels, pred_boxes = nms_utils.multi_classes_nms(
                        cls_scores=cls_preds, box_preds=box_preds,
                        nms_config=post_process_cfg.NMS_CONFIG,
                        score_thresh=post_process_cfg.SCORE_THRESH,
                        label_preds=label_preds,
                    )
                final_scores = pred_scores
                #final_labels = pred_labels + 1
                final_labels = pred_labels if 'toonnx' in batch_dict.keys() else pred_labels + 1 #changed by me  to onnx needed
                final_boxes = pred_boxes
            else:
                cls_preds, label_preds = torch.max(cls_preds, dim=-1)
                if batch_dict.get('has_class_labels', False):
                    label_key = 'roi_labels' if 'roi_labels' in batch_dict else 'batch_pred_labels'
                    label_preds = batch_dict[label_key][index]
                else:
                    label_preds = label_preds + 1
                selected, selected_scores = nms_utils.class_agnostic_nms(
                    box_scores=cls_preds, box_preds=box_preds,
                    nms_config=post_process_cfg.NMS_CONFIG,
                    score_thresh=post_process_cfg.SCORE_THRESH
                )

                if post_process_cfg.OUTPUT_RAW_SCORE:
                    max_cls_preds, _ = torch.max(src_cls_preds, dim=-1)
                    selected_scores = max_cls_preds[selected]

                final_scores = selected_scores
                final_labels = label_preds[selected]
                final_boxes = box_preds[selected]

            record_dict = {
                'pred_boxes': final_boxes,
                'pred_scores': final_scores,
                'pred_labels': final_labels,
            }

            pred_dicts.append(record_dict)
        return pred_dicts, {}