import numpy as np
import sys
import random
import os
import torch
import warnings
import pdb
from numba.core.errors import (NumbaDeprecationWarning, 
                               NumbaPendingDeprecationWarning,
                               NumbaPerformanceWarning)
warnings.simplefilter('ignore', category=NumbaDeprecationWarning)
warnings.simplefilter('ignore', category=NumbaPendingDeprecationWarning)
warnings.simplefilter('ignore', category=NumbaPerformanceWarning)
warnings.filterwarnings("ignore", category=UserWarning)
warnings.filterwarnings("ignore", category=UserWarning)
from torch.utils.tensorboard import SummaryWriter
import torch.distributed as dist
import torch.multiprocessing as mp
import torch.utils.data.distributed

from data_process.apollo_dataloader import create_new_framework_test_dataloader
from utils.model_utils import create_model
from utils.torch_utils import _sigmoid
from utils.evaluation_utils import decode, post_processing
from utils.simple_utils import map_calculate_new_framework
from utils.bev_map_val import get_map
from config.train_config_apollo import parse_train_configs

from config.config import cfg, cfg_from_yaml_file
from utils_liga import common_utils
from models.sfa_zj import SFA_ZJ
from data_process.apollo_dataset_new_framework import ApolloDataset

cnf_dict = {}
cnf_dict['minX'] = -50
cnf_dict['maxX'] = 50
cnf_dict['minY'] = -25
cnf_dict['maxY'] = 25
cnf_dict['minZ'] = -2.4-3
cnf_dict['maxZ'] = 0.6+3   
cnf_dict['BEV_HEIGHT'] = 1216
cnf_dict['BEV_WIDTH'] = 608
cnf_dict['DISCRETIZATION'] = (cnf_dict['maxX'] - cnf_dict['minX']) / (cnf_dict['BEV_HEIGHT'] * 1.0)
cnf_dict['peak_thresh'] = 0.2
cnf_dict['bound_size_x'] = cnf_dict['maxX'] - cnf_dict['minX']
cnf_dict['bound_size_y'] = cnf_dict['maxY'] - cnf_dict['minY']
cnf_dict['bound_size_z'] = cnf_dict['maxZ'] - cnf_dict['minZ']

def main():
    configs = parse_train_configs()
    main_worker(configs)

def load_data_to_gpu(configs, batch_dict):
    for key, val in batch_dict.items():
        if isinstance(val, torch.FloatTensor):
            batch_dict[key] = val.type(torch.float32).to(configs.device, non_blocking=True)
            continue        
        if isinstance(val, torch.DoubleTensor): 
            batch_dict[key] = val.type(torch.float32).to(configs.device, non_blocking=True)
            continue  
        if not isinstance(val, np.ndarray):
            continue
        if key in ['frame_id', 'gt_names']:
            continue
        if val.dtype in [np.float32, np.float64]:
            batch_dict[key] = torch.from_numpy(val).float().to(configs.device, non_blocking=True)
        elif val.dtype in [np.uint8, np.int32, np.int64]:
            batch_dict[key] = torch.from_numpy(val).long().to(configs.device, non_blocking=True)
        elif val.dtype in [bool]:
            pass
        else:
            raise ValueError(f"invalid data type {key}: {type(val)}")

def main_worker(configs):
    for i in range(70):
        model_all_path = './checkpoints_zhijiangyihao_50_no_i/fpn_resnet_50/Model_fpn_resnet_50_epoch_' + str(10+i) + '.pth'
        print('Model_fpn_resnet_50_epoch_' + str(10+i) + '.pth')

        cfg_from_yaml_file(configs.cfg_file, cfg)
        test_dataset = ApolloDataset(configs, cfg.DATA_CONFIG, mode='test', lidar_aug=None, hflip_prob=configs.hflip_prob,
                                    num_samples=configs.num_samples)
        model_all = SFA_ZJ(model_cfg=cfg.MODEL, num_class=len(cfg.CLASS_NAMES), dataset=test_dataset)
        model_all.load_state_dict(torch.load(model_all_path, map_location=lambda storage, loc: storage))

        model_all.cuda()

        # Create dataloader
        _, test_dataloader, _ = create_new_framework_test_dataloader(configs, test_dataset)
        val_loss = validate_all(test_dataloader, model_all, configs) 

        print('val_loss: {:.4e}'.format(val_loss))

def validate_all(test_dataloader, model_all, configs):

    result_dir = './result/zhijiangyihao/'
    # result_dir = './result/apollo/'
    result_1_f = open(result_dir+'1.txt', 'w')
    result_2_f = open(result_dir+'2.txt', 'w')
    result_3_f = open(result_dir+'3.txt', 'w')
    result_4_f = open(result_dir+'4.txt', 'w')
    result_5_f = open(result_dir+'5.txt', 'w')     
    # switch mode
    model_all.eval()

    with torch.no_grad():
        for batch_idx, batch_data in enumerate(test_dataloader):
            load_data_to_gpu(configs, batch_data)
            voxel_features = batch_data['voxel_features']
            frame_id = batch_data['frame_id'][0]         
            pred_dicts, ret_dicts = model_all(batch_data)

            # 整理reuslt
            map_calculate_new_framework(cnf_dict, frame_id, pred_dicts[0], result_1_f, result_2_f, result_3_f, result_4_f, result_5_f)
    
    result_1_f.close()
    result_2_f.close()
    result_3_f.close()
    result_4_f.close()
    result_5_f.close()
    detpath  = result_dir+'{:s}.txt'
    # annopath = './dataset/zhijiangyihao/training2/pointcloud/test_map/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/training2/pointcloud/ImageSets/val_id.txt' 
    annopath = './dataset/zhijiangyihao/training_fusion/test_map/{:s}.txt'
    imagesetfile = './dataset/zhijiangyihao/training_fusion/test_9+10_shuffle.list' 
    map = get_map(detpath, annopath, imagesetfile)
    return map

if __name__ == '__main__':
    main()

