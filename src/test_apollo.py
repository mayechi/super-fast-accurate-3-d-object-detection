"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.08.17
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: This script for training

"""

import time
import numpy as np
import sys
import random
import os
import cv2
import warnings

warnings.filterwarnings("ignore", category=UserWarning)

import torch
from torch.utils.tensorboard import SummaryWriter
import torch.distributed as dist
import torch.multiprocessing as mp
import torch.utils.data.distributed
from tqdm import tqdm

sys.path.append('./')

# from data_process.kitti_dataloader import create_train_dataloader, create_val_dataloader
from data_process.apollo_dataloader import create_train_dataloader, create_val_dataloader, create_test_dataloader
from data_process.kitti_bev_utils import get_corners
from models.model_utils import create_model, make_data_parallel, get_num_parameters
from models import fpn_resnet, bifpn_resnet
from utils.train_utils import create_optimizer, create_lr_scheduler, get_saved_state, save_checkpoint
from utils.torch_utils import reduce_tensor, to_python_float, _sigmoid
from utils.evaluation_utils import decode, post_processing, convert_det_to_real_values, draw_predictions
from utils.misc import AverageMeter, ProgressMeter
from utils.logger import Logger
from utils.bev_map_val import get_map
from config.train_config_apollo import parse_train_configs
from losses.losses_apollo import Compute_Loss
# from waymo_open_dataset.utils import box_utils

from train_fusion_apollo import map_calculate

show_flag = 0
write_flag = 0

def main():
    configs = parse_train_configs()

    # Re-produce results
    if configs.seed is not None:
        random.seed(configs.seed)
        np.random.seed(configs.seed)
        torch.manual_seed(configs.seed)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False

    if configs.gpu_idx is not None:
        print('You have chosen a specific GPU. This will completely disable data parallelism.')

    if configs.dist_url == "env://" and configs.world_size == -1:
        configs.world_size = int(os.environ["WORLD_SIZE"])

    configs.distributed = configs.world_size > 1 or configs.multiprocessing_distributed

    if configs.multiprocessing_distributed:
        configs.world_size = configs.ngpus_per_node * configs.world_size
        mp.spawn(main_worker, nprocs=configs.ngpus_per_node, args=(configs,))
    else:
        main_worker(configs.gpu_idx, configs)


def main_worker(gpu_idx, configs):
    configs.gpu_idx = gpu_idx
    configs.device = torch.device('cpu' if configs.gpu_idx is None else 'cuda:{}'.format(configs.gpu_idx))

    if configs.distributed:
        if configs.dist_url == "env://" and configs.rank == -1:
            configs.rank = int(os.environ["RANK"])
        if configs.multiprocessing_distributed:
            # For multiprocessing distributed training, rank needs to be the
            # global rank among all the processes
            configs.rank = configs.rank * configs.ngpus_per_node + gpu_idx

        dist.init_process_group(backend=configs.dist_backend, init_method=configs.dist_url,
                                world_size=configs.world_size, rank=configs.rank)
        configs.subdivisions = int(64 / configs.batch_size / configs.ngpus_per_node)
    else:
        configs.subdivisions = int(64 / configs.batch_size)

    configs.is_master_node = (not configs.distributed) or (
            configs.distributed and (configs.rank % configs.ngpus_per_node == 0))

    if configs.is_master_node:
        logger = Logger(configs.logs_dir, configs.saved_fn)
        logger.info('>>> Created a new logger')
        logger.info('>>> configs: {}'.format(configs))
        tb_writer = SummaryWriter(log_dir=os.path.join(configs.logs_dir, 'tensorboard'))
        # tb_writer = None
    else:
        logger = None
        tb_writer = None

    # # 加载model_extract
    # model_extract_path = "/media/myc/Work/Research/code/Super-Fast-Accurate-3D-Object-Detection/checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_114_0.4.5_extract.pth"
    # model_extract = bifpn_resnet.get_extract_model()
    # model_extract.load_state_dict(torch.load(model_extract_path, map_location=lambda storage, loc: storage))
    # model_extract.cuda(configs.gpu_idx)
    # # 加载model_detec
    # model_detec_path = "/media/myc/Work/Research/code/Super-Fast-Accurate-3D-Object-Detection/checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_114_0.4.5_detec.pth"
    # model_detec = create_model(configs)
    # model_detec.load_state_dict(torch.load(model_detec_path, map_location=lambda storage, loc: storage))
    # model_detec.cuda(configs.gpu_idx)

    # 加载整体model
    # model_all_path = "./checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_112.pth"
    for i in range(1):
        # model_all_path = './checkpoints_zhijiangyihao_50_no_i/fpn_resnet_50/Model_fpn_resnet_50_epoch_' + str(10+i) + '.pth'
        # print('Model_fpn_resnet_50_epoch_' + str(10+i) + '.pth')
        # model_all_path = '/checkpoints_zhijiangyihao_si_50_no_i/Model_fpn_resnet_50_epoch_12_si.pth'
        model_all_path = './checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_28_0.3.9.pth'
        # model_all_path = './checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_8.pth'
        model_all = create_model(configs)
        model_all.load_state_dict(torch.load(model_all_path, map_location=lambda storage, loc: storage))
        model_all.cuda(configs.gpu_idx)

        test_dataloader = create_test_dataloader(configs)
        if show_flag:
            # show_result(test_dataloader, model_extract, model_detec, configs)
            show_result_all(test_dataloader, model_all, configs)
        elif write_flag:
            write_result_all(test_dataloader, model_all, configs)
            # validate_all(test_dataloader, model_all, configs, logger)
            # wirite_result_txt(test_dataloader, model_extract, model_detec, configs)
        else:
            # val_loss = validate_all_split(test_dataloader, model_extract, model_detec, configs, logger) 
            val_loss = validate_all(test_dataloader, model_all, configs, logger)          
            print('val_loss: {:.4e}'.format(val_loss))

def validate_all(test_dataloader, model_all, configs, logger):

    result_dir = './result/zhijiangyihao/'
    # result_dir = './result/apollo/'
    result_1_f = open(result_dir+'1.txt', 'w')
    # result_2_f = open(result_dir+'2.txt', 'w')
    result_3_f = open(result_dir+'3.txt', 'w')
    # result_4_f = open(result_dir+'4.txt', 'w')
    # result_5_f = open(result_dir+'5.txt', 'w')     
    # switch mode
    model_all.eval()

    with torch.no_grad():
        for batch_idx, batch_data in enumerate(test_dataloader):
            # print('batch_idx:', batch_idx)
            # metadatas, bev_maps = batch_data
            metadatas, bev_maps, bev_maps_index = batch_data
            input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float() 
            input_bev_maps_index = bev_maps_index.to(configs.device, non_blocking=True).int()          
            # outputs = model_all(input_bev_maps, input_bev_maps_index)
            outputs = model_all(input_bev_maps)

            outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
            outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
            # detections size (batch_size, K, 10)
            detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
                                            outputs['dim'], K=50)
            detections = detections.cpu().numpy().astype(np.float32)
            detections_post = post_processing(detections, configs.num_classes, configs.down_ratio, 0.0)

            # 整理reuslt
            # map_calculate(metadatas, detections_post, result_1_f, result_3_f, result_5_f)
            map_calculate(metadatas, detections_post, result_1_f, result_3_f, None)
            # for j in range(configs.num_classes):
            #     if j == 1-1:
            #         if len(detections_post[0][j] > 0):
            #             for det in detections_post[0][j]:
            #                 _score, _x, _y, _z, _h, _w, _l, _yaw = det 
            #                 # x = _y * cnf_dict['bound_size_x'] / cnf_dict['BEV_HEIGHT'] + cnf_dict['minX']
            #                 # y = _x * cnf_dict['bound_size_y'] / cnf_dict['BEV_WIDTH'] + cnf_dict['minY']
            #                 # if x < -20 or x > 20:
            #                 #     continue   
            #                 # if y < -10 or y > 10:
            #                 #     continue                               
            #                 bev_corners = get_corners(_x, _y, _w, _l, _yaw)
            #                 x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
            #                 result_1_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
            #     # if j == 2-1:
            #     #     if len(detections_post[0][j] > 0):
            #     #         for det in detections_post[0][j]:
            #     #             _score, _x, _y, _z, _h, _w, _l, _yaw = det 
            #     #             # x = _y * cnf_dict['bound_size_x'] / cnf_dict['BEV_HEIGHT'] + cnf_dict['minX']
            #     #             # y = _x * cnf_dict['bound_size_y'] / cnf_dict['BEV_WIDTH'] + cnf_dict['minY']
            #     #             # if x < -20 or x > 20:
            #     #             #     continue   
            #     #             # if y < -10 or y > 10:
            #     #             #     continue                               
            #     #             bev_corners = get_corners(_x, _y, _w, _l, _yaw)
            #     #             x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
            #     #             result_1_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
            #     if j == 3-1:
            #         if len(detections_post[0][j] > 0):
            #             for det in detections_post[0][j]:
            #                 _score, _x, _y, _z, _h, _w, _l, _yaw = det 
            #                 # x = _y * cnf_dict['bound_size_x'] / cnf_dict['BEV_HEIGHT'] + cnf_dict['minX']
            #                 # y = _x * cnf_dict['bound_size_y'] / cnf_dict['BEV_WIDTH'] + cnf_dict['minY']
            #                 # if x < -20 or x > 20:
            #                 #     continue   
            #                 # if y < -10 or y > 10:
            #                 #     continue                               
            #                 bev_corners = get_corners(_x, _y, _w, _l, _yaw)
            #                 x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
            #                 result_3_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
            #     # if j == 4-1:
            #     #     if len(detections_post[0][j] > 0):
            #     #         for det in detections_post[0][j]:
            #     #             _score, _x, _y, _z, _h, _w, _l, _yaw = det 
            #     #             # x = _y * cnf_dict['bound_size_x'] / cnf_dict['BEV_HEIGHT'] + cnf_dict['minX']
            #     #             # y = _x * cnf_dict['bound_size_y'] / cnf_dict['BEV_WIDTH'] + cnf_dict['minY']
            #     #             # if x < -20 or x > 20:
            #     #             #     continue   
            #     #             # if y < -10 or y > 10:
            #     #             #     continue                               
            #     #             bev_corners = get_corners(_x, _y, _w, _l, _yaw)
            #     #             x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
            #     #             result_3_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
            #     if j == 5-1:
            #         if len(detections_post[0][j] > 0):
            #             for det in detections_post[0][j]:
            #                 _score, _x, _y, _z, _h, _w, _l, _yaw = det 
            #                 # x = _y * cnf_dict['bound_size_x'] / cnf_dict['BEV_HEIGHT'] + cnf_dict['minX']
            #                 # y = _x * cnf_dict['bound_size_y'] / cnf_dict['BEV_WIDTH'] + cnf_dict['minY']
            #                 # if x < -20 or x > 20:
            #                 #     continue   
            #                 # if y < -10 or y > 10:
            #                 #     continue                                            
            #                 bev_corners = get_corners(_x, _y, _w, _l, _yaw)
            #                 x1, y1, x2, y2, x3, y3, x4, y4 = \
            #                 bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], \
            #                 bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
            #                 result_5_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
    result_1_f.close()
    # result_2_f.close()
    result_3_f.close()
    # result_4_f.close()
    # result_5_f.close()
    # detpath = r'PATH_TO_BE_CONFIGURED/Task1_{:s}.txt'
    # annopath = r'PATH_TO_BE_CONFIGURED/{:s}.txt' # change the directory to the path of val/labelTxt, if you want to do evaluation on the valset
    # imagesetfile = r'PATH_TO_BE_CONFIGURED/valset.txt'
    detpath  = result_dir+'{:s}.txt'
    # annopath = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/label_for_val/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val_id.txt'
    # annopath = '/home/mayechi/Data/zhijiangyihao/f_s_stage/pointcloud/training/label_for_val/{:s}.txt'
    # imagesetfile = '/home/mayechi/Data/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val_id.txt'
    # annopath = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/label_big_width_for_val_add_apollo/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val_id_add_apollo.txt'
    # annopath = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/label_for_val_add_apollo/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val_id_add_apollo.txt'
    # annopath = './dataset/apollo/label_for_val/{:s}.txt'
    # imagesetfile = './dataset/apollo/ImageSets/val_id.txt'
    # annopath = './dataset/zhijiangyihao/training/test_map/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/training/ImageSets/val_id_add_apollo.txt'    
    # annopath = './dataset/zhijiangyihao/training/test_map/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/training/ImageSets/val_id.txt'
    # annopath = './dataset/zhijiangyihao/training2/pointcloud/test_map_add_apollo/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/training2/pointcloud/ImageSets/val_id_add_apollo.txt'
    # imagesetfile = './dataset/zhijiangyihao/training2/pointcloud/ImageSets/val_id.txt'

    # annopath = '/media/myc/Data21/zhijiangyihao/training_fusion/test_map9/{:s}.txt'
    # annopath = '/media/myc/Data21/zhijiangyihao/training_fusion/test_map9_image/{:s}.txt'
    # imagesetfile = '/media/myc/Data21/zhijiangyihao/training_fusion/test_9_shuffle.list'  
    annopath = './dataset/zhijiangyihao/training_fusion/test_map/{:s}.txt'
    imagesetfile = './dataset/zhijiangyihao/training_fusion/test_9+10_shuffle.list' 
    # imagesetfile = '/media/myc/Data21/zhijiangyihao/training_fusion/test_9_shuffle.list' 
    # try:
    map = get_map(detpath, annopath, imagesetfile)
    # except:
    #     map = -1
    return map

def validate_all_split(test_dataloader, model_extract, model_detec, configs, logger):

    result_dir = './result/zhijiangyihao/'
    # result_dir = './result/apollo/'
    result_1_f = open(result_dir+'1.txt', 'w')
    result_3_f = open(result_dir+'3.txt', 'w')
    result_5_f = open(result_dir+'5.txt', 'w')     
    # switch mode
    model_extract.eval()
    model_detec.eval()

    with torch.no_grad():
        for batch_idx, batch_data in enumerate(test_dataloader):
            # metadatas, bev_maps = batch_data
            metadatas, bev_maps, bev_maps_index = batch_data
            input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float() 
            input_bev_maps_index = bev_maps_index.to(configs.device, non_blocking=True).int()          
            x = model_extract(input_bev_maps, input_bev_maps_index)
            outputs = model_detec(x)

            outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
            outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
            # detections size (batch_size, K, 10)
            detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
                                            outputs['dim'], K=50)
            detections = detections.cpu().numpy().astype(np.float32)
            detections_post = post_processing(detections, configs.num_classes, configs.down_ratio, 0.0)

            # 整理reuslt
            for j in range(configs.num_classes):
                if j == 1-1:
                    if len(detections_post[0][j] > 0):
                        for det in detections_post[0][j]:
                            _score, _x, _y, _z, _h, _w, _l, _yaw = det 
                            # x = _y * cnf_dict['bound_size_x'] / cnf_dict['BEV_HEIGHT'] + cnf_dict['minX']
                            # y = _x * cnf_dict['bound_size_y'] / cnf_dict['BEV_WIDTH'] + cnf_dict['minY']
                            # if x < -20 or x > 20:
                            #     continue   
                            # if y < -10 or y > 10:
                            #     continue                               
                            bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                            x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                            result_1_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
                if j == 3-1:
                    if len(detections_post[0][j] > 0):
                        for det in detections_post[0][j]:
                            _score, _x, _y, _z, _h, _w, _l, _yaw = det 
                            # x = _y * cnf_dict['bound_size_x'] / cnf_dict['BEV_HEIGHT'] + cnf_dict['minX']
                            # y = _x * cnf_dict['bound_size_y'] / cnf_dict['BEV_WIDTH'] + cnf_dict['minY']
                            # if x < -20 or x > 20:
                            #     continue   
                            # if y < -10 or y > 10:
                            #     continue                               
                            bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                            x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                            result_3_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
                if j == 5-1:
                    if len(detections_post[0][j] > 0):
                        for det in detections_post[0][j]:
                            _score, _x, _y, _z, _h, _w, _l, _yaw = det 
                            # x = _y * cnf_dict['bound_size_x'] / cnf_dict['BEV_HEIGHT'] + cnf_dict['minX']
                            # y = _x * cnf_dict['bound_size_y'] / cnf_dict['BEV_WIDTH'] + cnf_dict['minY']
                            # if x < -20 or x > 20:
                            #     continue   
                            # if y < -10 or y > 10:
                            #     continue                                            
                            bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                            x1, y1, x2, y2, x3, y3, x4, y4 = \
                            bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], \
                            bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                            result_5_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
    result_1_f.close()
    result_3_f.close()
    result_5_f.close()
    # detpath = r'PATH_TO_BE_CONFIGURED/Task1_{:s}.txt'
    # annopath = r'PATH_TO_BE_CONFIGURED/{:s}.txt' # change the directory to the path of val/labelTxt, if you want to do evaluation on the valset
    # imagesetfile = r'PATH_TO_BE_CONFIGURED/valset.txt'
    detpath  = result_dir+'{:s}.txt'
    # annopath = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/label_for_val/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val_id.txt'
    # annopath = '/home/mayechi/Data/zhijiangyihao/f_s_stage/pointcloud/training/label_for_val/{:s}.txt'
    # imagesetfile = '/home/mayechi/Data/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val_id.txt'
    # annopath = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/label_big_width_for_val_add_apollo/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val_id_add_apollo.txt'
    # annopath = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/label_for_val_add_apollo/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val_id_add_apollo.txt'
    # annopath = './dataset/apollo/label_for_val/{:s}.txt'
    # imagesetfile = './dataset/apollo/ImageSets/val_id.txt'
    # annopath = './dataset/zhijiangyihao/training/test_map/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/training/ImageSets/val_id_add_apollo.txt'    
    annopath = './dataset/zhijiangyihao/training/test_map/{:s}.txt'
    imagesetfile = './dataset/zhijiangyihao/training/ImageSets/val_id.txt'
    # try:
    map = get_map(detpath, annopath, imagesetfile)
    # except:
    #     map = -1
    return map

def validate(test_dataloader, model_extract, model_detec, configs, logger):

    result_dir = './result/zhijiangyihao/'
    result_3_f = open(result_dir+'3.txt', 'w')
    result_5_f = open(result_dir+'5.txt', 'w')     
    # switch mode
    model_extract.eval()
    model_detec.eval()

    with torch.no_grad():
        for batch_idx, batch_data in enumerate(test_dataloader):
            # break
            metadatas, bev_maps, bev_map_per_index = batch_data
            # metadatas, bev_maps = batch_data
            # print(metadatas['frame_id'][0])
            input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float()
            extract_outputs = model_extract(input_bev_maps)
            bev_map_input = torch.zeros((1, 16, 1, 1216, 608), requires_grad=True).cuda()
            bev_map_input[:, :, :, bev_map_per_index.nonzero()[:, 1], bev_map_per_index.nonzero()[:, 2]] = extract_outputs
            detec_input = bev_map_input.reshape(1, 16, 1216, 608)             
            outputs = model_detec(detec_input)

            outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
            outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
            # detections size (batch_size, K, 10)
            detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
                                            outputs['dim'], K=configs.K)
            detections = detections.cpu().numpy().astype(np.float32)
            detections_post = post_processing(detections, configs.num_classes, configs.down_ratio, configs.peak_thresh)

            # 整理reuslt
            for j in range(configs.num_classes):
                if j == 3-1:
                    if len(detections_post[0][j] > 0):
                        for det in detections_post[0][j]:
                            _score, _x, _y, _z, _h, _w, _l, _yaw = det 
                            # x = _y * cnf_dict['bound_size_x'] / cnf_dict['BEV_HEIGHT'] + cnf_dict['minX']
                            # y = _x * cnf_dict['bound_size_y'] / cnf_dict['BEV_WIDTH'] + cnf_dict['minY']
                            # if x < -20 or x > 20:
                            #     continue   
                            # if y < -10 or y > 10:
                            #     continue                               
                            bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                            x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                            result_3_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
                if j == 5-1:
                    if len(detections_post[0][j] > 0):
                        for det in detections_post[0][j]:
                            _score, _x, _y, _z, _h, _w, _l, _yaw = det 
                            # x = _y * cnf_dict['bound_size_x'] / cnf_dict['BEV_HEIGHT'] + cnf_dict['minX']
                            # y = _x * cnf_dict['bound_size_y'] / cnf_dict['BEV_WIDTH'] + cnf_dict['minY']
                            # if x < -20 or x > 20:
                            #     continue   
                            # if y < -10 or y > 10:
                            #     continue                                            
                            bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                            x1, y1, x2, y2, x3, y3, x4, y4 = \
                            bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], \
                            bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                            result_5_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
    result_3_f.close()
    result_5_f.close()
    # detpath = r'PATH_TO_BE_CONFIGURED/Task1_{:s}.txt'
    # annopath = r'PATH_TO_BE_CONFIGURED/{:s}.txt' # change the directory to the path of val/labelTxt, if you want to do evaluation on the valset
    # imagesetfile = r'PATH_TO_BE_CONFIGURED/valset.txt'
    detpath  = result_dir+'{:s}.txt'
    annopath = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/label_for_val/{:s}.txt'
    imagesetfile = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val_id.txt'
    # annopath = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/label_for_val_add_apollo/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val_id_add_apollo.txt'
    # annopath = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/label_big_width_for_val_add_apollo/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val_id_add_apollo.txt'
    try:
        map = get_map(detpath, annopath, imagesetfile)
    except:
        map = -1
    return map

def show_result(test_dataloader, model_extract, model_detec, configs):

    # switch mode
    model_extract.eval()
    model_detec.eval()

    with torch.no_grad():
          for batch_idx, batch_data in enumerate(test_dataloader):

            metadatas, bev_maps, bev_map_per_index, bev_maps_ori = batch_data
            bev_map_use = bev_maps[..., (bev_map_per_index[0, :]==1)] 
            input_bev_maps = bev_map_use.to(configs.device, non_blocking=True).float()
            extract_outputs = model_extract(input_bev_maps)
            bev_map_input = torch.zeros((1, 16, 1, 1216*608), requires_grad=True).cuda()
            bev_map_input[..., bev_map_per_index[0, :]==1] = extract_outputs
            # bev_map_input[:, :, :, bev_map_per_index.nonzero()[:, 1], bev_map_per_index.nonzero()[:, 2]] = extract_outputs
            detec_input = bev_map_input.reshape(1, 16, 1216, 608)             
            outputs = model_detec(detec_input)

            outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
            outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
            detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
                                            outputs['dim'], K=configs.K)
            detections = detections.cpu().numpy().astype(np.float32)
            detections_post = post_processing(detections, configs.num_classes, configs.down_ratio, configs.peak_thresh)
            # detections_post = post_processing(detections, configs.num_classes, configs.down_ratio, 0.4)
            
            # Draw prediction in the image
            bev_map = (bev_maps_ori[0].permute(1, 2, 0).numpy() * 255).astype(np.uint8)
            bev_map = cv2.resize(bev_map, (608, 1216))
            bev_map_copy = bev_map.copy()
            bev_map = draw_predictions(bev_map, detections_post[0], configs.num_classes)
            # Rotate the bev_map
            bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
            bev_map_copy = cv2.rotate(bev_map_copy, cv2.ROTATE_180)
            bev_map_copy = cv2.resize(bev_map_copy, (500, 1000))
            cv2.imshow('oir', bev_map_copy)
            cv2.imshow('result', bev_map)
            cv2.waitKey(0)
    return

def show_result_all(test_dataloader, model_all, configs):

    # switch mode
    model_all.eval()
 
    with torch.no_grad():
          for batch_idx, batch_data in enumerate(test_dataloader):     

            metadatas, bev_maps, bev_maps_ori = batch_data
            frame_id = metadatas['frame_id'][0]

            input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float()            
            outputs = model_all(input_bev_maps)

            outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
            outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
            detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
                                            outputs['dim'], K=configs.K)
            detections = detections.cpu().numpy().astype(np.float32)
            detections_post = post_processing(detections, configs.num_classes, configs.down_ratio, 0.4)
            
            # Draw prediction in the image
            bev_map = (bev_maps_ori[0].permute(1, 2, 0).numpy() * 255).astype(np.uint8)
            bev_map = cv2.resize(bev_map, (608, 1216))
            bev_map_copy = bev_map.copy()
            bev_map = draw_predictions(bev_map, detections_post[0], configs.num_classes)
            # Rotate the bev_map
            bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
            bev_map_copy = cv2.rotate(bev_map_copy, cv2.ROTATE_180)
            bev_map_copy = cv2.resize(bev_map_copy, (500, 1000))
            # cv2.imwrite('/media/myc/Data21/zhijiangyihao/test/BIN/pcd_32+bp_bin_result/'+frame_id+'.jpg', bev_map)
            cv2.imshow('oir', bev_map_copy)
            cv2.imshow('result', bev_map)
            cv2.waitKey(0)            
            # cv2.imwrite('/media/myc/Data21/zhijiangyihao/s2/result/'+frame_id+'.jpg', bev_map)

    return

def write_result_all(test_dataloader, model_all, configs):
    # output_dir = './dataset/apollo/lidar_rcnn/data_processer_val/'
    output_dir = './dataset/apollo/lidar_rcnn/data_processer/'
    lidar_dir = os.path.join(output_dir, 'rs')    
    # switch mode
    model_all.eval()
    with torch.no_grad():
        for batch_idx, batch_data in enumerate(test_dataloader):
            metadatas, bev_maps, bev_maps_index = batch_data
            frame_name = metadatas['frame_id'][0]
            frame_result_dict = {}
            frame_result_dict['frame_name'] = frame_name
            frame_result_dict['frame_result'] = list()
            result_path =  os.path.join(lidar_dir, frame_name)

            input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float()            
            outputs = model_all(input_bev_maps)

            outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
            outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
            detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
                                            outputs['dim'], K=200)
            detections = detections.cpu().numpy().astype(np.float32)  
            detections_post = post_processing(detections, configs.num_classes, configs.down_ratio, 0.0)
            kitti_dets = convert_det_to_real_values(detections_post[0], num_classes=configs.num_classes)
            frame_boxes = list()
            frame_types = list()         
            for i in range(kitti_dets.shape[0]):
                cls = int(kitti_dets[i][0])+1
                # if cls != 2:
                #     continue
                x, y, z = kitti_dets[i][1], kitti_dets[i][2], kitti_dets[i][3]
                h, w, l = kitti_dets[i][4], kitti_dets[i][5], kitti_dets[i][6]
                yaw = kitti_dets[i][7]
                score = kitti_dets[i][-1]
                box_result_list = [x, y, z, l, w, h, yaw, cls, score]
                box_result_array = np.array(box_result_list)
                frame_boxes.append(box_result_array[np.newaxis, :])
                frame_types.append(cls)
            result_info = {'boxes': frame_boxes, 'types': frame_types}
            np.savez_compressed(result_path, **result_info)
    return    

def wirite_result_txt(test_dataloader, model_extract, model_detec, configs):

    result_dir = '/home/mayechi/Data2/zhijiangyihao/test/txt/'  
    # switch mode
    model_extract.eval()
    model_detec.eval()

    with torch.no_grad():
          for batch_idx, batch_data in enumerate(test_dataloader):

            metadatas, bev_maps, bev_map_per_index = batch_data

            input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float()
            extract_outputs = model_extract(input_bev_maps)
            bev_map_input = torch.zeros((1, 16, 1, 1216, 608), requires_grad=True).cuda()
            bev_map_input[:, :, :, bev_map_per_index.nonzero()[:, 1], bev_map_per_index.nonzero()[:, 2]] = extract_outputs
            detec_input = bev_map_input.reshape(1, 16, 1216, 608)             
            outputs = model_detec(detec_input)

            outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
            outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
            detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
                                            outputs['dim'], K=configs.K)
            detections = detections.cpu().numpy().astype(np.float32)
            detections_post = post_processing(detections, configs.num_classes, configs.down_ratio, configs.peak_thresh)
            kitti_dets = convert_det_to_real_values(detections_post[0], num_classes=configs.num_classes)

            # 整理reuslt
            with open(result_dir+metadatas['frame_id'][0]+'.txt', 'w') as f:
                for i in range(kitti_dets.shape[0]):
                    cls_id, x, y, z, h, w, l, yaw, score = kitti_dets[i]
                    bbox = np.array([x, y, z, l, w, h, yaw]).reshape(-1, 7)
                    gt_corners = box_utils.get_upright_3d_box_corners(bbox).numpy()     
                    x1, y1, z1 = gt_corners[0, 0, 0], gt_corners[0, 0, 1], gt_corners[0, 0, 2]
                    x2, y2, z2 = gt_corners[0, 1, 0], gt_corners[0, 1, 1], gt_corners[0, 1, 2]    
                    x3, y3, z3 = gt_corners[0, 2, 0], gt_corners[0, 2, 1], gt_corners[0, 2, 2]
                    x4, y4, z4 = gt_corners[0, 3, 0], gt_corners[0, 3, 1], gt_corners[0, 3, 2]
                    x5, y5, z5 = gt_corners[0, 4, 0], gt_corners[0, 4, 1], gt_corners[0, 4, 2]
                    x6, y6, z6 = gt_corners[0, 5, 0], gt_corners[0, 5, 1], gt_corners[0, 5, 2]
                    x7, y7, z7 = gt_corners[0, 6, 0], gt_corners[0, 6, 1], gt_corners[0, 6, 2]
                    x8, y8, z8 = gt_corners[0, 7, 0], gt_corners[0, 7, 1], gt_corners[0, 7, 2]                  
                    f.write(str(x1)+' '+str(y1)+' '+str(z1)+' '+str(x2)+' '+str(y2)+' '+str(z2)+' ')
                    f.write(str(x3)+' '+str(y3)+' '+str(z3)+' '+str(x4)+' '+str(y4)+' '+str(z4)+' ')
                    f.write(str(x5)+' '+str(y5)+' '+str(z5)+' '+str(x6)+' '+str(y6)+' '+str(z6)+' ')
                    f.write(str(x7)+' '+str(y7)+' '+str(z7)+' '+str(x8)+' '+str(y8)+' '+str(z8)+'\n')
            f.close()
    return

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        try:
            cleanup()
            sys.exit(0)
        except SystemExit:
            os._exit(0)
