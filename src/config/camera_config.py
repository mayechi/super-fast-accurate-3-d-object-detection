import math

import numpy as np

# Car and Van ==> Car class
# Pedestrian and Person_Sitting ==> Pedestrian Class
CLASS_NAME_TO_ID = {
    'Pedestrian': 0,
    'Car': 1,
    'Cyclist': 2,
    'Van': 1, # 1
    'Truck': -3, # -3
    'Person_sitting': 0,
    'Tram': -99,
    'Misc': -99,
    'DontCare': -1
}

colors = [[0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [255, 120, 120], [0, 120, 0], [120, 255, 255], [120, 0, 255],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],
          [0, 255, 255], [0, 0, 255], [255, 0, 0], [255, 120, 0],]

#####################################################################################
# boundary = {
#     "minX": 0,
#     "maxX": 80,
#     "minY": -40,
#     "maxY": 40,
#     "minZ": -2.73,
#     "maxZ": 1.27
# }
# boundary = {
#     "minX": -15,
#     "maxX": 15,
#     "minY": -15,
#     "maxY": 15,
#     "minZ": -2.73,
#     "maxZ": 1.27
# }
# boundary = {
#     "minX": -50,
#     "maxX": 50,
#     "minY": -25,
#     "maxY": 25,
#     "minZ": -2.73,
#     "maxZ": 1.27
# }
# car height 1.78m, Z轴的感知范围-1m-3m
# boundary = {
#     "minX": -50,
#     "maxX": 50,
#     "minY": -25,
#     "maxY": 25,
#     # "minY": -50,
#     # "maxY": 50,
#     "minZ": -2.78,
#     "maxZ": 1.22
# }
# small car height 1.78m, Z轴的感知范围-0.5m-2.5m
# boundary = {
#     "minX": -50,
#     "maxX": 50,
#     "minY": -25,
#     "maxY": 25,
#     "minZ": -2.28,
#     "maxZ": 0.72
# }
# s2 height 1.9m, Z轴的感知范围-0.5m-2.5m
boundary = {
    "minX": -50,
    "maxX": 50,
    "minY": -25, 
    "maxY": 25,
    "minZ": -2.4-3,
    "maxZ": 0.6+3
}
# road32 height 3.5m, Z轴的感知范围-0.5m-2.5m
# boundary = {
#     "minX": -50,
#     "maxX": 50,
#     "minY": -25,
#     "maxY": 25,
#     "minZ": -4,
#     "maxZ": -1
# }
# roadbp height 3m, 向下为正，Z轴的感知范围-0.5m-2.5m
# boundary = {
#     "minX": -50,
#     "maxX": 50,
#     "minY": -25,
#     "maxY": 25,
#     "minZ": -0.5,
#     "maxZ": 2.5
# }
bound_size_x = boundary['maxX'] - boundary['minX']
bound_size_y = boundary['maxY'] - boundary['minY']
bound_size_z = boundary['maxZ'] - boundary['minZ']

boundary_back = {
    "minX": -50,
    "maxX": 0,
    "minY": -25,
    "maxY": 25,
    "minZ": -2.73,
    "maxZ": 1.27
}

# BEV_WIDTH = 608  # across y axis -25m ~ 25m -40m ~ 40m
# BEV_HEIGHT = 608  # across x axis 0m ~ 50m 0m ~ 80m

BEV_WIDTH = 608  # across y axis -25m ~ 25m
# BEV_WIDTH = 1216  # across y axis -25m ~ 25m
BEV_HEIGHT = 608  # across x axis -50m ~ 50m

DISCRETIZATION = (boundary["maxX"] - boundary["minX"]) / BEV_HEIGHT

DISCRETIZATION_Y = (boundary["maxX"] - boundary["minX"]) / BEV_HEIGHT
DISCRETIZATION_X = (boundary["maxY"] - boundary["minY"]) / BEV_WIDTH
# maximum number of points per voxel
T = 35

# voxel size
vd = 0.1  # z
vh = 0.05  # y
vw = 0.05  # x

# voxel grid
W = math.ceil(bound_size_x / vw)
H = math.ceil(bound_size_y / vh)
D = math.ceil(bound_size_z / vd)

# Following parameters are calculated as an average from KITTI dataset for simplicity
#####################################################################################
R_z_90_ =  np.array([
    [0, 1, 0],
    [-1, 0, 0],
    [0, 0, 1]
])

R_z_90 =  np.array([
    [0, -1, 0],
    [1, 0, 0],
    [0, 0, 1]
])


Tr_velo_to_cam = np.array([
    [7.49916597e-03, -9.99971248e-01, -8.65110297e-04, -6.71807577e-03],
    [1.18652889e-02, 9.54520517e-04, -9.99910318e-01, -7.33152811e-02],
    [9.99882833e-01, 7.49141178e-03, 1.18719929e-02, -2.78557062e-01],
    [0, 0, 0, 1]
])

# cal mean from train set
R0 = np.array([
    [0.99992475, 0.00975976, -0.00734152, 0],
    [-0.0097913, 0.99994262, -0.00430371, 0],
    [0.00729911, 0.0043753, 0.99996319, 0],
    [0, 0, 0, 1]
])

P2 = np.array([[719.787081, 0., 608.463003, 44.9538775],
               [0., 719.787081, 174.545111, 0.1066855],
               [0., 0., 1., 3.0106472e-03],
               [0., 0., 0., 0]
               ])

R0_inv = np.linalg.inv(R0)
Tr_velo_to_cam_inv = np.linalg.inv(Tr_velo_to_cam)
P2_inv = np.linalg.pinv(P2)
#####################################################################################

# voxel filter
leaf_size = [0.05, 0.05, 0.1]