import torch
from config.train_config_apollo import parse_train_configs
from models import fpn_resnet, bifpn_resnet

configs = parse_train_configs() 
arch_parts = configs.arch.split('_')
num_layers = int(arch_parts[-1])
configs.pretrained_path = './checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_114_0.4.5.pth'
pth = torch.load(configs.pretrained_path, map_location=lambda storage, loc: storage)

# 生成检测模型
model_detec_output_path = "/media/myc/Work/Research/code/Super-Fast-Accurate-3D-Object-Detection/checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_114_0.4.5_detec.pth"
model_detec = bifpn_resnet.get_pose_net(num_layers=num_layers, heads=configs.heads, head_conv=configs.head_conv, 
                                      imagenet_pretrained=configs.imagenet_pretrained)
model_detec_dict =  model_detec.state_dict()
state_dict = {k:v for k,v in pth.items() if k in model_detec_dict.keys()}
model_detec_dict.update(state_dict)
model_detec.load_state_dict(model_detec_dict)
torch.save(model_detec.state_dict(), model_detec_output_path)

model_detec.load_state_dict(torch.load(model_detec_output_path, map_location=lambda storage, loc: storage))

# 生成特征提取模型
model_extract_output_path = "/media/myc/Work/Research/code/Super-Fast-Accurate-3D-Object-Detection/checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_114_0.4.5_extract.pth"
model_extract = bifpn_resnet.get_extract_model()
model_extract_dict =  model_extract.state_dict()
state_dict = {k:v for k,v in pth.items() if k in model_extract_dict.keys()}
model_extract_dict.update(state_dict)
model_extract.load_state_dict(model_extract_dict)
torch.save(model_extract.state_dict(), model_extract_output_path)
model_extract.load_state_dict(torch.load(model_extract_output_path, map_location=lambda storage, loc: storage))