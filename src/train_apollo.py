"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.08.17
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: This script for training

"""

import time
import numpy as np
import sys
import random
import os
import warnings

warnings.filterwarnings("ignore", category=UserWarning)

import torch
from torch.utils.tensorboard import SummaryWriter
import torch.distributed as dist
import torch.multiprocessing as mp
import torch.utils.data.distributed
from tqdm import tqdm

sys.path.append('./')

# from data_process.kitti_dataloader import create_train_dataloader, create_val_dataloader
from data_process.apollo_dataloader import create_train_dataloader, create_val_dataloader, create_test_dataloader
from data_process.kitti_bev_utils import get_corners
from models.model_utils import create_model, make_data_parallel, get_num_parameters#, update_model_from_centernet2
from utils.train_utils import create_optimizer, create_lr_scheduler, get_saved_state, save_checkpoint
from utils.torch_utils import reduce_tensor, to_python_float, _sigmoid
from utils.evaluation_utils import decode, post_processing, convert_det_to_real_values
from utils.misc import AverageMeter, ProgressMeter
from utils.logger import Logger
from utils.bev_map_val import get_map
from config.train_config_apollo import parse_train_configs
from losses.losses_apollo import Compute_Loss
# from losses.losses_bev import Compute_Loss

def main():
    configs = parse_train_configs()

    # Re-produce results
    if configs.seed is not None:
        random.seed(configs.seed)
        np.random.seed(configs.seed)
        torch.manual_seed(configs.seed)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False

    if configs.gpu_idx is not None:
        print('You have chosen a specific GPU. This will completely disable data parallelism.')

    if configs.dist_url == "env://" and configs.world_size == -1:
        configs.world_size = int(os.environ["WORLD_SIZE"])

    configs.distributed = configs.world_size > 1 or configs.multiprocessing_distributed

    if configs.multiprocessing_distributed:
        configs.world_size = configs.ngpus_per_node * configs.world_size
        mp.spawn(main_worker, nprocs=configs.ngpus_per_node, args=(configs,))
    else:
        main_worker(configs.gpu_idx, configs)


def main_worker(gpu_idx, configs):
    configs.gpu_idx = gpu_idx
    configs.device = torch.device('cpu' if configs.gpu_idx is None else 'cuda:{}'.format(configs.gpu_idx))

    if configs.distributed:
        if configs.dist_url == "env://" and configs.rank == -1:
            configs.rank = int(os.environ["RANK"])
        if configs.multiprocessing_distributed:
            # For multiprocessing distributed training, rank needs to be the
            # global rank among all the processes
            configs.rank = configs.rank * configs.ngpus_per_node + gpu_idx

        dist.init_process_group(backend=configs.dist_backend, init_method=configs.dist_url,
                                world_size=configs.world_size, rank=configs.rank)
        configs.subdivisions = int(64 / configs.batch_size / configs.ngpus_per_node)
    else:
        configs.subdivisions = int(64 / configs.batch_size)

    configs.is_master_node = (not configs.distributed) or (
            configs.distributed and (configs.rank % configs.ngpus_per_node == 0))

    if configs.is_master_node:
        logger = Logger(configs.logs_dir, configs.saved_fn)
        logger.info('>>> Created a new logger')
        logger.info('>>> configs: {}'.format(configs))
        tb_writer = SummaryWriter(log_dir=os.path.join(configs.logs_dir, 'tensorboard'))
        # tb_writer = None
    else:
        logger = None
        tb_writer = None

    # model
    model = create_model(configs)
    # backbone_dla = update_model_from_centernet2()

    # load weight from a checkpoint
    if configs.pretrained_path is not None:
        assert os.path.isfile(configs.pretrained_path), "=> no checkpoint found at '{}'".format(configs.pretrained_path)
        model.load_state_dict(torch.load(configs.pretrained_path, map_location=lambda storage, loc: storage))
        if logger is not None:
            logger.info('loaded pretrained model at {}'.format(configs.pretrained_path))

    # resume weights of model from a checkpoint
    if configs.resume_path is not None:
        assert os.path.isfile(configs.resume_path), "=> no checkpoint found at '{}'".format(configs.resume_path)
        model.load_state_dict(torch.load(configs.resume_path))
        if logger is not None:
            logger.info('resume training model from checkpoint {}'.format(configs.resume_path))

    # Data Parallel
    model = make_data_parallel(model, configs)

    # Make sure to create optimizer after moving the model to cuda
    optimizer = create_optimizer(configs, model)
    lr_scheduler = create_lr_scheduler(optimizer, configs)
    configs.step_lr_in_epoch = False if configs.lr_type in ['multi_step', 'cosin', 'one_cycle'] else True

    # resume optimizer, lr_scheduler from a checkpoint
    if configs.resume_path is not None:
        utils_path = configs.resume_path.replace('Model_', 'Utils_')
        assert os.path.isfile(utils_path), "=> no checkpoint found at '{}'".format(utils_path)
        utils_state_dict = torch.load(utils_path, map_location='cuda:{}'.format(configs.gpu_idx))
        optimizer.load_state_dict(utils_state_dict['optimizer'])
        lr_scheduler.load_state_dict(utils_state_dict['lr_scheduler'])
        configs.start_epoch = utils_state_dict['epoch'] + 1

    if configs.is_master_node:
        num_parameters = get_num_parameters(model)
        logger.info('number of trained parameters of the model: {}'.format(num_parameters))

    if logger is not None:
        logger.info(">>> Loading dataset & getting dataloader...")
    # Create dataloader
    train_dataloader, train_sampler = create_train_dataloader(configs)
    if logger is not None:
        logger.info('number of batches in training set: {}'.format(len(train_dataloader)))

    if configs.evaluate:
        val_dataloader = create_val_dataloader(configs)
        val_loss = validate(val_dataloader, model, configs)
        print('val_loss: {:.4e}'.format(val_loss))
        return

    for epoch in range(configs.start_epoch, configs.num_epochs + 1):
        # load weight from a checkpoint
        # configs.pretrained_path = 'checkpoints_zhijiangyihao_50_has_i/fpn_resnet_50/Model_fpn_resnet_50_epoch_'+str(20+epoch)+'.pth'       
        # if configs.pretrained_path is not None:
        #     assert os.path.isfile(configs.pretrained_path), "=> no checkpoint found at '{}'".format(configs.pretrained_path)
        #     model.load_state_dict(torch.load(configs.pretrained_path, map_location=lambda storage, loc: storage))
        #     if logger is not None:
        #         logger.info('loaded pretrained model at {}'.format(configs.pretrained_path))        
        if logger is not None:
            logger.info('{}'.format('*-' * 40))
            logger.info('{} {}/{} {}'.format('=' * 35, epoch, configs.num_epochs, '=' * 35))
            logger.info('{}'.format('*-' * 40))
            logger.info('>>> Epoch: [{}/{}]'.format(epoch, configs.num_epochs))

        if configs.distributed:
            train_sampler.set_epoch(epoch)
        # train for one epoch
        train_one_epoch(train_dataloader, model, optimizer, lr_scheduler, epoch, configs, logger, tb_writer)
        # if (not configs.no_val) and (epoch % configs.checkpoint_freq == 0):
        if 0:
            # val_dataloader = create_val_dataloader(configs)
            test_dataloader = create_test_dataloader(configs)
            print('number of batches in test_dataloader: {}'.format(len(test_dataloader)))
            # val_loss = validate(val_dataloader, model, configs)
            val_loss = validate(test_dataloader, model, configs, logger)          
            print('val_loss: {:.4e}'.format(val_loss))
            if logger is not None:
                logger.info('val_loss: {:.4e}'.format(val_loss))
            if tb_writer is not None:
                tb_writer.add_scalar('Val_loss', val_loss, epoch)

        # Save checkpoint
        if configs.is_master_node and ((epoch % configs.checkpoint_freq) == 0):
            model_state_dict, utils_state_dict = get_saved_state(model, optimizer, lr_scheduler, epoch, configs)
            save_checkpoint(configs.checkpoints_dir, configs.saved_fn, model_state_dict, utils_state_dict, epoch)

        if not configs.step_lr_in_epoch:
            lr_scheduler.step()
            if tb_writer is not None:
                tb_writer.add_scalar('LR', lr_scheduler.get_lr()[0], epoch)

    if tb_writer is not None:
        tb_writer.close()
    if configs.distributed:
        cleanup()


def cleanup():
    dist.destroy_process_group()


def train_one_epoch(train_dataloader, model, optimizer, lr_scheduler, epoch, configs, logger, tb_writer):
    batch_time = AverageMeter('Time', ':6.3f')
    data_time = AverageMeter('Data', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')

    progress = ProgressMeter(len(train_dataloader), [batch_time, data_time, losses],
                             prefix="Train - Epoch: [{}/{}]".format(epoch, configs.num_epochs))

    criterion = Compute_Loss(device=configs.device)
    num_iters_per_epoch = len(train_dataloader)
    # switch to train mode
    model.train()
    start_time = time.time()
    for batch_idx, batch_data in enumerate(tqdm(train_dataloader)):
        data_time.update(time.time() - start_time)
        # metadatas, bev_maps, bev_map_per_index = batch_data
        metadatas, bev_maps, targets = batch_data
        # metadatas, bev_maps, bev_maps_num_list, bev_maps_list, bev_map_x_voxel, targets = batch_data
        # metadatas, bev_maps, bev_maps_num_tensor, bev_maps_tensor, bev_map_x_voxel, targets = batch_data
        batch_size = bev_maps.size(0)
        global_step = num_iters_per_epoch * (epoch - 1) + batch_idx + 1
        # print(metadatas['frame_id'][0])
        input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float()
        for k in targets.keys():
            targets[k] = targets[k].to(configs.device, non_blocking=True)
        # bev_maps_num_tensor =  bev_maps_num_tensor.to(configs.device, non_blocking=True).int()
        # bev_maps_tensor =  bev_maps_tensor.to(configs.device, non_blocking=True).float()
        # input_bev_maps_all = bev_maps_all.to(configs.device, non_blocking=True).float()
        # for k in range(len(bev_maps_list)):
        #     bev_maps_list[k] = bev_maps_list[k].to(configs.device, non_blocking=True).float()
        # input_bev_map_per_index = bev_map_per_index.to(configs.device, non_blocking=True).float()
        # input_bev_map_x_voxel = bev_map_x_voxel.to(configs.device, non_blocking=True).float()
        # print(input_bev_maps.sum())
        # outputs = model(input_bev_maps, bev_map_per_index)
        # outputs = model(input_bev_maps)
        # bev_map_input = torch.zeros((4, 1216*608, 16, 1))
        # try:
            # outputs = model(bev_maps_num_tensor, bev_maps_tensor, input_bev_map_x_voxel)
        outputs = model(input_bev_maps)
        total_loss, loss_stats = criterion(outputs, targets)
        # except:
        # print('-------Error-------')
        # continue
        # For torch.nn.DataParallel case
        if (not configs.distributed) and (configs.gpu_idx is None):
            total_loss = torch.mean(total_loss)
        # compute gradient and perform backpropagation
        total_loss.backward()
        # except:
        #     continue
        # if global_step % configs.subdivisions == 0:
        if 1:
            optimizer.step()
            # zero the parameter gradients
            optimizer.zero_grad()
            # Adjust learning rate
            if configs.step_lr_in_epoch:
                lr_scheduler.step()
                if tb_writer is not None:
                    tb_writer.add_scalar('LR', lr_scheduler.get_lr()[0], global_step)

        if configs.distributed:
            reduced_loss = reduce_tensor(total_loss.data, configs.world_size)
        else:
            reduced_loss = total_loss.data
        losses.update(to_python_float(reduced_loss), batch_size)
        # measure elapsed time
        # torch.cuda.synchronize()
        batch_time.update(time.time() - start_time)

        if tb_writer is not None:
            if (global_step % configs.tensorboard_freq) == 0:
                loss_stats['avg_loss'] = losses.avg
                tb_writer.add_scalars('Train', loss_stats, global_step)
        # Log message
        if logger is not None:
            if (global_step % configs.print_freq) == 0:
                logger.info(progress.get_message(batch_idx))

        start_time = time.time()


# def validate(val_dataloader, model, configs):
#     losses = AverageMeter('Loss', ':.4e')
#     criterion = Compute_Loss(device=configs.device)
#     # switch to train mode
#     model.eval()
#     with torch.no_grad():
#         for batch_idx, batch_data in enumerate(tqdm(val_dataloader)):
#             if batch_data == {} or batch_data == []:
#                 continue
#             metadatas, imgs, targets = batch_data
#             batch_size = imgs.size(0)
#             for k in targets.keys():
#                 targets[k] = targets[k].to(configs.device, non_blocking=True)
#             imgs = imgs.to(configs.device, non_blocking=True).float()
#             outputs = model(imgs)
#             total_loss, loss_stats = criterion(outputs, targets)
#             # For torch.nn.DataParallel case
#             if (not configs.distributed) and (configs.gpu_idx is None):
#                 total_loss = torch.mean(total_loss)

#             if configs.distributed:
#                 reduced_loss = reduce_tensor(total_loss.data, configs.world_size)
#             else:
#                 reduced_loss = total_loss.data
#             losses.update(to_python_float(reduced_loss), batch_size)

#     return losses.avg

def validate(test_dataloader, model, configs, logger):
    # cnf_dict = {}
    # cnf_dict['minX'] = -50
    # cnf_dict['maxX'] = 50
    # cnf_dict['minY'] = -25
    # cnf_dict['maxY'] = 25
    # cnf_dict['minZ'] = -2.73
    # cnf_dict['maxZ'] = 1.27   
    # cnf_dict['BEV_HEIGHT'] = 1216
    # cnf_dict['BEV_WIDTH'] = 608
    # cnf_dict['DISCRETIZATION'] = (cnf_dict['maxX'] - cnf_dict['minX']) / (cnf_dict['BEV_HEIGHT'] * 1.0)
    # cnf_dict['peak_thresh'] = 0.2
    # cnf_dict['bound_size_x'] = cnf_dict['maxX'] - cnf_dict['minX']
    # cnf_dict['bound_size_y'] = cnf_dict['maxY'] - cnf_dict['minY']
    # cnf_dict['bound_size_z'] = cnf_dict['maxZ'] - cnf_dict['minZ']

    result_dir = './result/zhijiangyihao/'
    result_1_f = open(result_dir+'1.txt', 'w')
    result_3_f = open(result_dir+'3.txt', 'w')
    result_5_f = open(result_dir+'5.txt', 'w')     
    # switch to train mode
    model.eval()
    with torch.no_grad():
        for batch_idx, batch_data in enumerate(test_dataloader):
            # break
            # metadatas, bev_maps, bev_map_per_index = batch_data
            # metadatas, bev_maps = batch_data
            metadatas, bev_maps, bev_maps_list, bev_map_x_voxel = batch_data
            # metadatas, bev_maps = batch_data
            # print(metadatas['frame_id'][0])
            input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float()
            for k in range(len(bev_maps_list)):
                bev_maps_list[k] = bev_maps_list[k].to(configs.device, non_blocking=True).float()
            # input_bev_map_per_index = bev_map_per_index.to(configs.device, non_blocking=True).float()
            input_bev_map_x_voxel = bev_map_x_voxel.to(configs.device, non_blocking=True).float()
            # print(input_bev_maps.sum())
            # outputs = model(input_bev_maps, bev_map_per_index)
            # outputs = model(input_bev_maps)           
            # input_bev_map_per_index = bev_map_per_index.to(configs.device, non_blocking=True).float()
            # print(input_bev_maps.sum())
            # outputs = model(input_bev_maps, bev_map_per_index)
            # outputs = model(input_bev_maps)
            outputs = model(bev_maps_list, input_bev_map_x_voxel)             
            outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
            outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
            # detections size (batch_size, K, 10)
            detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
                                            outputs['dim'], K=configs.K)
            detections = detections.cpu().numpy().astype(np.float32)
            detections_post = post_processing(detections, configs.num_classes, configs.down_ratio, configs.peak_thresh)

            # 整理reuslt
            for j in range(configs.num_classes):
                if len(detections_post[0][j] > 0):
                    for det in detections_post[0][j]:
                        _score, _x, _y, _z, _h, _w, _l, _yaw = det                           
                        bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                        x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                        if j == 1-1:
                            result_1_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')                    
                        if j == 3-1:
                            result_3_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')                                       
                        if j == 5-1:
                            result_5_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')                                    
                # if j == 3-1:
                #     if len(detections_post[0][j] > 0):
                #         for det in detections_post[0][j]:
                #             _score, _x, _y, _z, _h, _w, _l, _yaw = det                           
                #             bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                #             x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                #             result_3_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
                # if j == 5-1:
                #     if len(detections_post[0][j] > 0):
                #         for det in detections_post[0][j]:
                #             _score, _x, _y, _z, _h, _w, _l, _yaw = det                                          
                #             bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                #             x1, y1, x2, y2, x3, y3, x4, y4 = \
                #             bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], \
                #             bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                #             result_5_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
    result_1_f.close()
    result_3_f.close()
    result_5_f.close()
    # detpath = r'PATH_TO_BE_CONFIGURED/Task1_{:s}.txt'
    # annopath = r'PATH_TO_BE_CONFIGURED/{:s}.txt' # change the directory to the path of val/labelTxt, if you want to do evaluation on the valset
    # imagesetfile = r'PATH_TO_BE_CONFIGURED/valset.txt'
    detpath  = result_dir+'{:s}.txt'
    # annopath = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/label_for_val/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/training/ImageSets/val_apollo_id.txt'
    annopath = './dataset/zhijiangyihao/training/test_map/{:s}.txt'
    # annopath = './dataset/apollo/label_for_val/{:s}.txt'
    # imagesetfile = './dataset/apollo/ImageSets/val_id.txt'   
    imagesetfile = './dataset/zhijiangyihao/training/ImageSets/val_id_add_apollo.txt'
    # try:
    map = get_map(detpath, annopath, imagesetfile)
    # except:
    #     map = -1
    return map

def make_offline_result(test_dataloader, model, configs, logger):
    model.eval()
    with torch.no_grad():
        for batch_idx, batch_data in enumerate(test_dataloader):
            # break
            metadatas, bev_maps = batch_data
            # print(metadatas['frame_id'][0])
            input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float()
            # print(input_bev_maps.sum())
            outputs = model(input_bev_maps)
            outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
            outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
            # detections size (batch_size, K, 10)
            detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
                                            outputs['dim'], K=configs.K)
            detections = detections.cpu().numpy().astype(np.float32)
            detections_post = post_processing(detections, configs.num_classes, configs.down_ratio, configs.peak_thresh)
            kitti_dets = convert_det_to_real_values(detections_post[0], configs, num_classes=configs.num_classes)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        try:
            cleanup()
            sys.exit(0)
        except SystemExit:
            os._exit(0)
