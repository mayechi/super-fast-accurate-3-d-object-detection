import os
import numpy as np

def boxes3d_to_corners3d_kitti_camera(boxes3d, bottom_center=True):
    """
    :param boxes3d: (N, 7) [x, y, z, l, h, w, ry] in camera coords, see the definition of ry in KITTI dataset
    :param bottom_center: whether y is on the bottom center of object
    :return: corners3d: (N, 8, 3)
        7 -------- 4
       /|         /|
      6 -------- 5 .
      | |        | |
      . 3 -------- 0
      |/         |/
      2 -------- 1
    """
    boxes_num = boxes3d.shape[0]
    l, h, w = boxes3d[:, 3], boxes3d[:, 4], boxes3d[:, 5]
    x_corners = np.array([l / 2., l / 2., -l / 2., -l / 2., l / 2., l / 2., -l / 2., -l / 2], dtype=np.float32).T
    z_corners = np.array([w / 2., -w / 2., -w / 2., w / 2., w / 2., -w / 2., -w / 2., w / 2.], dtype=np.float32).T
    if bottom_center:
        y_corners = np.zeros((boxes_num, 8), dtype=np.float32)
        y_corners[:, 4:8] = -h.reshape(boxes_num, 1).repeat(4, axis=1)  # (N, 8)
    else:
        y_corners = np.array([h / 2., h / 2., h / 2., h / 2., -h / 2., -h / 2., -h / 2., -h / 2.], dtype=np.float32).T

    ry = boxes3d[:, 6]
    zeros, ones = np.zeros(ry.size, dtype=np.float32), np.ones(ry.size, dtype=np.float32)
    rot_list = np.array([[np.cos(ry), zeros, -np.sin(ry)],
                         [zeros, ones, zeros],
                         [np.sin(ry), zeros, np.cos(ry)]])  # (3, 3, N)
    R_list = np.transpose(rot_list, (2, 0, 1))  # (N, 3, 3)

    temp_corners = np.concatenate((x_corners.reshape(-1, 8, 1), y_corners.reshape(-1, 8, 1),
                                   z_corners.reshape(-1, 8, 1)), axis=2)  # (N, 8, 3)
    rotated_corners = np.matmul(temp_corners, R_list)  # (N, 8, 3)
    x_corners, y_corners, z_corners = rotated_corners[:, :, 0], rotated_corners[:, :, 1], rotated_corners[:, :, 2]

    x_loc, y_loc, z_loc = boxes3d[:, 0], boxes3d[:, 1], boxes3d[:, 2]

    x = x_loc.reshape(-1, 1) + x_corners.reshape(-1, 8)
    y = y_loc.reshape(-1, 1) + y_corners.reshape(-1, 8)
    z = z_loc.reshape(-1, 1) + z_corners.reshape(-1, 8)

    corners = np.concatenate((x.reshape(-1, 8, 1), y.reshape(-1, 8, 1), z.reshape(-1, 8, 1)), axis=2)

    return corners.astype(np.float32)

# Input dir
lidar_data_dir = '/home/mayechi/Data/zhijiangyihao/second_stage/test_pair/pointcloud'
image_data_dir = '/home/mayechi/Data/zhijiangyihao/second_stage/test_pair/images'
lidar_label_dir = '/home/mayechi/Data/zhijiangyihao/second_stage/test_pair/pointcloud_label/'
imgae_label_dir = '/home/mayechi/Data/zhijiangyihao/second_stage/test_pair/images_label'

# Output dir
lidar_corners_dir = '/home/mayechi/Data/zhijiangyihao/second_stage/test_pair/pointcloud_corners_label/'

lidar_data_list = os.listdir(lidar_data_dir)
image_data_list = os.listdir(image_data_dir)
lidar_label_name_list = os.listdir(lidar_label_dir)
imgae_label_name_list = os.listdir(imgae_label_dir)

# Transform lidar_label to lidar_corners_label
for lidar_label_name in lidar_label_name_list:
    with open(lidar_label_dir+lidar_label_name, 'r') as f:
        lidar_label_list = f.readlines()
        boxes_list = []
        cats_list = []
        for lidar_label in lidar_label_list:
            lidar_label = lidar_label.rstrip()
            lidar_label = lidar_label.split(' ')
            cat_id = lidar_label[0]  # 1 for small vehicles, 2 for big vehicles, 3 for pedestrian, 4 for motorcyclist and bicyclist, 5 for traffic cones and 6 for others
            x, y, z = float(lidar_label[1]), float(lidar_label[2]), float(lidar_label[3])
            l, w, h = float(lidar_label[4]), float(lidar_label[5]), float(lidar_label[6])
            ry = float(lidar_label[7])
            boxes_list.append([x, y, z, l, h, w, ry])
            cats_list.append(cat_id)
        f.close()
    boxes_array = np.array(boxes_list)
    corners3d = boxes3d_to_corners3d_kitti_camera(boxes_array)    
    with open(lidar_corners_dir+lidar_label_name, 'w') as f:
        output_list = []
        for i in range(corners3d.shape[0]):
            output_list.append(cats_list[i]+' ')
            x1, y1, z1 = corners3d[i][0]
            output_list.append(str(x1)+' '+str(y1)+' '+str(z1)+' ')
            x2, y2, z2 = corners3d[i][1]
            output_list.append(str(x2)+' '+str(y2)+' '+str(z2)+' ')
            x3, y3, z3 = corners3d[i][2]
            output_list.append(str(x3)+' '+str(y3)+' '+str(z3)+' ')
            x4, y4, z4 = corners3d[i][3]
            output_list.append(str(x4)+' '+str(y4)+' '+str(z4)+' ')
            x5, y5, z5 = corners3d[i][4]
            output_list.append(str(x5)+' '+str(y5)+' '+str(z5)+' ')
            x6, y6, z6 = corners3d[i][5]
            output_list.append(str(x6)+' '+str(y6)+' '+str(z6)+' ')
            x7, y7, z7 = corners3d[i][6]
            output_list.append(str(x7)+' '+str(y7)+' '+str(z7)+' ')
            x8, y8, z8 = corners3d[i][7]
            output_list.append(str(x8)+' '+str(y8)+' '+str(z8)+'\n')
            cat_id = cats_list[i]
        f.writelines(output_list)
        f.close()
            




