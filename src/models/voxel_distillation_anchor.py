from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import numpy as np
import yaml
from easydict import EasyDict

import torch
import torch.nn as nn
import torch.utils.model_zoo as model_zoo
import torch.nn.functional as F
from utils.torch_utils import _sigmoid

from utils.evaluation_utils import decode, post_processing
from data_process.kitti_data_utils import get_filtered_lidar
from data_process.kitti_bev_utils import makeBEVMap
import config.apollo_config_distillation as cnf

from models.anchor_base import dense_heads
from liga.utils import nms_utils

# Voxel, 3d spconv
from models.scn_distillation import SpMiddleFHD
from models.submodule_distillation import convbn, hourglass2d

BN_MOMENTUM = 0.1

class PoseResNet(nn.Module):

    def __init__(self, heads, head_conv, mode='train'):
        self.mode = mode 
        self.inplanes = 64
        self.deconv_with_bias = False
        self.heads = heads
        super(PoseResNet, self).__init__()

        # Bev network
        self.input_channels = 160
        self.num_channels = 64
        self.GN = True
        self.height_compress = nn.Sequential(
                    nn.Conv2d(32, 16, kernel_size=(3,1), stride=1, padding=(1,0)),
                    nn.ReLU(inplace=True),
                    nn.Conv2d(16, 8, kernel_size=(3,1), stride=1, padding=(1,0)),
                    nn.ReLU(inplace=True),
                    )        
        self.rpn3d_conv2 = nn.Sequential(
            convbn(self.input_channels, self.num_channels, 3, 1, 1, 1, gn=self.GN),
            nn.ReLU(inplace=True))
        self.rpn3d_conv3 = hourglass2d(self.num_channels, gn=self.GN)   

        # Voxel, init 3d spconv
        self.input_shape = np.array([776, 608, 20])
        self.backbone_3d = SpMiddleFHD()

        # Anchor head build
        self.grid_size = self.input_shape / 2
        self.point_cloud_range = np.array([cnf.boundary['minX'], cnf.boundary['minY'], cnf.boundary['minZ'], 
                                           cnf.boundary['maxX'], cnf.boundary['maxY'], cnf.boundary['maxZ']])
        self.predict_boxes_when_training = False                                 
        model_cfg_file = "./src/config/anchor_head.yaml"
        with open(model_cfg_file, 'r') as f:
            self.model_cfg = EasyDict(yaml.load(f, Loader=yaml.FullLoader))

        self.dense_head_module = dense_heads.__all__[self.model_cfg.DENSE_HEAD.NAME](self.model_cfg.DENSE_HEAD, self.num_channels, 6, 
                                                    self.model_cfg.CLASS_NAMES, self.grid_size, self.point_cloud_range, self.predict_boxes_when_training)

        self.num_class = len(self.model_cfg.CLASS_NAMES)


    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion, momentum=BN_MOMENTUM),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    # def forward(self, points, voxels, coordinates, num_points_per_voxel, input_features, batch_size):
    def forward(self, batch_data):

        batch_size = batch_data['batch_size']
        input_features = batch_data['input_features']
        coordinates = batch_data['coordinates']
        voxels = batch_data['voxels']

        # Voxel feature extractor
        input_features = input_features.type_as(voxels).contiguous()
        spatial_features = self.backbone_3d(input_features.cuda(), coordinates, batch_size, self.input_shape)
        N, C, D, H, W = spatial_features.shape
        spatial_features = spatial_features.view(*spatial_features.shape[:3], H, W)
        N, C, D, H, W = spatial_features.shape
        spatial_features = spatial_features.view(N, C * D, H, W)

        # Bev feature extractor        
        x = self.rpn3d_conv2(spatial_features)
        ret = self.rpn3d_conv3(x, None, None)[0]
        batch_data['spatial_features_2d'] = ret

        # Anchor head 
        batch_data = self.dense_head_module(batch_data)
        if self.mode == 'train':
            loss_rpn, tb_dict = self.dense_head_module.get_loss()
            tb_dict = {
                'loss_rpn': loss_rpn.item(),
                **tb_dict
            }        
            loss = loss_rpn

            return loss, tb_dict
        else:
            pred_dicts = self.post_processing(batch_data)
            
            return pred_dicts

    def apply_kfpn(self, outs):
        outs = torch.cat([out.unsqueeze(-1) for out in outs], dim=-1)
        softmax_outs = F.softmax(outs, dim=-1)
        ret_outs = (outs * softmax_outs).sum(dim=-1)
        return ret_outs

    def init_weights(self, num_layers, pretrained=True):
        if pretrained:
            # TODO: Check initial weights for head later
            for fpn_idx in [0]:  # 3 FPN layers
                for head in self.heads:
                    final_layer = self.__getattr__('fpn{}_{}'.format(fpn_idx, head))
                    for i, m in enumerate(final_layer.modules()):
                        if isinstance(m, nn.Conv2d):
                            # nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                            # print('=> init {}.weight as normal(0, 0.001)'.format(name))
                            # print('=> init {}.bias as 0'.format(name))
                            if m.weight.shape[0] == self.heads[head]:
                                if 'hm' in head:
                                    nn.init.constant_(m.bias, -2.19)
                                else:
                                    nn.init.normal_(m.weight, std=0.001)
                                    nn.init.constant_(m.bias, 0)

    def post_processing(self, batch_dict):
        """
        Args:
            batch_dict:
                batch_size:
                batch_cls_preds: (B, num_boxes, num_classes | 1) or (
                    N1+N2+..., num_classes | 1)
                                or [(B, num_boxes, num_class1), (B, num_boxes, num_class2) ...]
                multihead_label_mapping: [(num_class1), (num_class2), ...]
                batch_box_preds: (B, num_boxes, 7+C) or (N1+N2+..., 7+C)
                cls_preds_normalized: indicate whether batch_cls_preds is normalized
                batch_index: optional (N1+N2+...)
                has_class_labels: True/False
                roi_labels: (B, num_rois)  1 .. num_classes
                batch_pred_labels: (B, num_boxes, 1)
        Returns:

        """
        post_process_cfg = self.model_cfg.POST_PROCESSING
        batch_size = batch_dict['batch_size']
        pred_dicts = []
        for index in range(batch_size):
            if batch_dict.get('batch_index', None) is not None:
                assert batch_dict['batch_box_preds'].shape.__len__() == 2
                batch_mask = (batch_dict['batch_index'] == index)
            else:
                assert batch_dict['batch_box_preds'].shape.__len__() == 3
                batch_mask = index

            box_preds = batch_dict['batch_box_preds'][batch_mask]

            if not isinstance(batch_dict['batch_cls_preds'], list):
                cls_preds = batch_dict['batch_cls_preds'][batch_mask]

                src_cls_preds = cls_preds
                assert cls_preds.shape[1] in [1, self.num_class]

                if not batch_dict['cls_preds_normalized']:
                    cls_preds = torch.sigmoid(cls_preds)
            else:
                cls_preds = [x[batch_mask]
                                for x in batch_dict['batch_cls_preds']]
                src_cls_preds = cls_preds
                if not batch_dict['cls_preds_normalized']:
                    cls_preds = [torch.sigmoid(x) for x in cls_preds]


            if post_process_cfg.NMS_CONFIG.MULTI_CLASSES_NMS:
                if batch_dict.get('has_class_labels', False):
                    label_key = 'roi_labels' if 'roi_labels' in batch_dict else 'batch_pred_labels'
                    label_preds = batch_dict[label_key][index]
                else:
                    label_preds = None
                if 'toonnx' in batch_dict.keys():
                    pred_scores, pred_labels, pred_boxes = nms_utils.multi_classes_nms(
                        cls_scores=cls_preds, box_preds=box_preds,
                        nms_config=post_process_cfg.NMS_CONFIG,
                        score_thresh=0,
                        label_preds=label_preds,
                        toonnx = True
                    )
                else:
                    pred_scores, pred_labels, pred_boxes = nms_utils.multi_classes_nms(
                        cls_scores=cls_preds, box_preds=box_preds,
                        nms_config=post_process_cfg.NMS_CONFIG,
                        score_thresh=post_process_cfg.SCORE_THRESH,
                        label_preds=label_preds,
                    )
                final_scores = pred_scores
                #final_labels = pred_labels + 1
                final_labels = pred_labels if 'toonnx' in batch_dict.keys() else pred_labels + 1 #changed by me  to onnx needed
                final_boxes = pred_boxes
            else:
                cls_preds, label_preds = torch.max(cls_preds, dim=-1)
                if batch_dict.get('has_class_labels', False):
                    label_key = 'roi_labels' if 'roi_labels' in batch_dict else 'batch_pred_labels'
                    label_preds = batch_dict[label_key][index]
                else:
                    label_preds = label_preds + 1
                selected, selected_scores = nms_utils.class_agnostic_nms(
                    box_scores=cls_preds, box_preds=box_preds,
                    nms_config=post_process_cfg.NMS_CONFIG,
                    score_thresh=post_process_cfg.SCORE_THRESH
                )

                if post_process_cfg.OUTPUT_RAW_SCORE:
                    max_cls_preds, _ = torch.max(src_cls_preds, dim=-1)
                    selected_scores = max_cls_preds[selected]

                final_scores = selected_scores
                final_labels = label_preds[selected]
                final_boxes = box_preds[selected]

            record_dict = {
                'pred_boxes': final_boxes,
                'pred_scores': final_scores,
                'pred_labels': final_labels,
            }

            pred_dicts.append(record_dict)

            return pred_dicts

def get_net(heads, head_conv, mode):
    model = PoseResNet(heads, head_conv, mode)
    return model