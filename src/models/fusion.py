import torch
from torch import nn
from torch.nn import functional as F
import torchplus
from torchplus.nn import Empty, GroupNorm, Sequential
from torchplus.ops.array_ops import gather_nd, scatter_nd
from torchplus.tools import change_default_args
import sys
if '/opt/ros/kinetic/lib/python2.7/dist-packages' in sys.path:
    sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')

class fusion(nn.Module):
    def __init__(self, configs):
        super(fusion, self).__init__()
        self.K = configs.K
        self.name = 'fusion_layer'
        self.corner_points_feature = Sequential(
            nn.Conv2d(24,48,1),
            nn.ReLU(),
            nn.Conv2d(48,96,1),
            nn.ReLU(),
            nn.Conv2d(96,96,1),
            nn.ReLU(),
            nn.Conv2d(96,4,1),
        )
        self.fuse_2d_3d = Sequential(
            # nn.Conv2d(4,18,1),
            nn.Conv2d(3,18,1),
            # nn.Conv2d(5,18,1),
            # nn.Conv2d(8,18,1),
            nn.ReLU(),
            nn.Conv2d(18,36,1),
            nn.ReLU(),
            nn.Conv2d(36,36,1),
            nn.ReLU(),
            nn.Conv2d(36,1,1),
            # nn.Conv2d(36,3,1),
        )  
        # self.fuse_2d_3d_all = Sequential(
        #     nn.Conv2d(4,18,1),
        #     # nn.Conv2d(3,18,1),
        #     # nn.Conv2d(5,18,1),
        #     # nn.Conv2d(8,18,1),
        #     nn.ReLU(),
        #     nn.Conv2d(18,36,1),
        #     nn.ReLU(),
        #     nn.Conv2d(36,36,1),
        #     nn.ReLU(),
        #     nn.Conv2d(36,1,1),
        #     # nn.Conv2d(36,3,1),
        # )  
        # self.fuse_2d_3d_Car = Sequential(
        #     nn.Conv2d(4,18,1),
        #     # nn.Conv2d(3,1s8,1),
        #     # nn.Conv2d(5,18,1),
        #     # nn.Conv2d(8,18,1),
        #     nn.ReLU(),
        #     nn.Conv2d(18,36,1),
        #     nn.ReLU(),
        #     nn.Conv2d(36,36,1),
        #     nn.ReLU(),
        #     nn.Conv2d(36,1,1),
        #     # nn.Conv2d(36,3,1),
        # )     
        # self.fuse_2d_3d_Pedestrian = Sequential(
        #     nn.Conv2d(4,18,1),
        #     # nn.Conv2d(3,1s8,1),
        #     # nn.Conv2d(5,18,1),
        #     # nn.Conv2d(8,18,1),
        #     nn.ReLU(),
        #     nn.Conv2d(18,36,1),
        #     nn.ReLU(),
        #     nn.Conv2d(36,36,1),
        #     nn.ReLU(),
        #     nn.Conv2d(36,1,1),
        #     # nn.Conv2d(36,3,1),
        # )    
        # self.fuse_2d_3d_Cyclist = Sequential(
        #     nn.Conv2d(4,18,1),
        #     # nn.Conv2d(3,1s8,1),
        #     # nn.Conv2d(5,18,1),
        #     # nn.Conv2d(8,18,1),
        #     nn.ReLU(),
        #     nn.Conv2d(18,36,1),
        #     nn.ReLU(),
        #     nn.Conv2d(36,36,1),
        #     nn.ReLU(),
        #     nn.Conv2d(36,1,1),
        #     # nn.Conv2d(36,3,1),
        # )  
        self.maxpool = Sequential(
            nn.MaxPool2d([200,1],1),
        )


    def forward(self,input_1, tensor_index, tensor_iou, valid_num):
        flag = -1
        if tensor_index[0,0] == -1:
            # out_1 = torch.zeros(1,200,70400,dtype = input_1.dtype,device = input_1.device)
            # tensor_index先是2D，后是3D
            # out_1 = torch.zeros(1, 200, self.K, dtype = input_1.dtype, device = input_1.device)
            out_1 = torch.zeros(3, 1, 200, self.K, dtype = input_1.dtype, device = input_1.device)
            # out_1 = torch.zeros(3, 200, self.K, dtype = input_1.dtype, device = input_1.device)
            # out_1[:,:,:] = -9999999
            out_1[:,:,:,:] = -9999999
            flag = 0
        else:
            x = self.fuse_2d_3d(input_1)
            # x = self.fuse_2d_3d_all(input_1)

            # x_Pedestrian = self.fuse_2d_3d_all(input_1[0].unsqueeze(0))
            # x_Car = self.fuse_2d_3d(input_1[1].unsqueeze(0))
            # x_Cyclist = self.fuse_2d_3d_all(input_1[2].unsqueeze(0))
            # x = torch.cat([x_Pedestrian, x_Car, x_Cyclist], 0)

            # out_1 = torch.zeros(1,200,70400,dtype = input_1.dtype,device = input_1.device)
            # out_1 = torch.zeros(1, 200, self.K, dtype = input_1.dtype, device = input_1.device)
            # out_1 = torch.zeros(6, 1, 200, self.K, dtype = input_1.dtype, device = input_1.device)
            # w_1 = torch.zeros(200, self.K, dtype = input_1.dtype, device = input_1.device)
            # out_1 = torch.zeros(6, 1, valid_num[0].item(), valid_num[1].item(), dtype = input_1.dtype, device = input_1.device)
            out_1 = torch.zeros(6, 1, 200, valid_num[1].item(), dtype = input_1.dtype, device = input_1.device)
            w_1 = torch.zeros(valid_num[0].item(), valid_num[1].item(), dtype = input_1.dtype, device = input_1.device)
            # out_1 = torch.zeros(3, 200, self.K, dtype = input_1.dtype, device = input_1.device)
            # out_1[:,:,:] = -9999999
            out_1[:,:,:,:] = -9999999
            # out_1[:,tensor_index[:,0],tensor_index[:,1]] = x[0,:,0,:]
            out_1[:,:,tensor_index[:,0],tensor_index[:,1]] = x[:,:,0,:]
            w_1[tensor_index[:,0],tensor_index[:,1]] = tensor_iou.reshape(-1)
            flag = 1
        # 将IOU经过softmax作为融合分数的权重
        # w_1 = w_1 / torch.sum(w_1, 0)
        # w_1[torch.isnan((w_1 / torch.sum(w_1, 0)))] = 0
        # w_1 = w_1.expand_as(out_1)
        # x = (w_1 * out_1).sum(2)
        
        # 直接去对3d box最大促进作用的
        x = self.maxpool(out_1)
        
        #x, _ = torch.max(out_1,1)

        # x = x.squeeze().reshape(1, -1, 1)
        x = x.squeeze().reshape(6, 1, -1, 1)
        # x = x.permute(1, 2, 0)
        
        return x, flag
