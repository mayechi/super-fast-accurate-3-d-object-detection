
from numpy.core.fromnumeric import shape
import torch
import torch.nn as nn
from detectron2.structures import (
    BitMasks,
    Boxes,
    BoxMode,
    Instances,
    Keypoints,
    PolygonMasks,
    RotatedBoxes,
    polygons_to_bitmask,
)

class centernet2_model(nn.Module):

    def __init__(self, backbone, proposal_generator, roi_heads):
        super(centernet2_model, self).__init__()
        self.backbone = backbone
        self.proposal_generator = proposal_generator
        self.roi_heads = roi_heads

    def forward(self, x, targets):   

        gt_instances = list()
        for i in range(x.shape[0]):
            gt_instance = Instances((1216, 608))
            clses, boxes = targets['cls'], targets['rect_max_objects']
            gt_instance.gt_boxes = Boxes(boxes[0,targets['obj_mask'][0].bool()])
            gt_instance.gt_classes = clses[0, targets['obj_mask'][0].bool()]
            gt_instances.append(gt_instance)

        input_size = [(1216, 608) for i in range(x.shape[0])]
        
        features = self.backbone(x)

        proposals, proposal_losses = self.proposal_generator(input_size, features, gt_instances)

        #  _, detector_losses = self.roi_heads(input_size, features, proposals, gt_instances)
        


        return proposal_losses
        # _, detector_losses = self.roi_heads(input_size, features, proposals, gt_instances)


        
    