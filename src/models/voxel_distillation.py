from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import numpy as np

import torch
import torch.nn as nn
import torch.utils.model_zoo as model_zoo
import torch.nn.functional as F
from utils.torch_utils import _sigmoid

from utils.evaluation_utils import decode, post_processing
from data_process.kitti_data_utils import get_filtered_lidar
from data_process.kitti_bev_utils import makeBEVMap
import config.kitti_config as cnf

# Voxel, 3d spconv
from models.scn_distillation import SpMiddleFHD
from models.submodule_distillation import convbn, hourglass2d

BN_MOMENTUM = 0.1

class PoseResNet(nn.Module):

    def __init__(self, heads, head_conv):     
        self.inplanes = 64
        self.deconv_with_bias = False
        self.heads = heads
        super(PoseResNet, self).__init__()

        # Bev network
        self.input_channels = 160
        self.num_channels = 64
        self.GN = True
        self.height_compress = nn.Sequential(
                    nn.Conv2d(32, 16, kernel_size=(3,1), stride=1, padding=(1,0)),
                    nn.ReLU(inplace=True),
                    nn.Conv2d(16, 8, kernel_size=(3,1), stride=1, padding=(1,0)),
                    nn.ReLU(inplace=True),
                    )        
        self.rpn3d_conv2 = nn.Sequential(
            convbn(self.input_channels, self.num_channels, 3, 1, 1, 1, gn=self.GN),
            nn.ReLU(inplace=True))
        self.rpn3d_conv3 = hourglass2d(self.num_channels, gn=self.GN)   
        self.hm_h, self.hm_w = 388, 304

        # fpn_channels = [64]
        # for fpn_idx, fpn_c in enumerate(fpn_channels):
        #     for head in sorted(self.heads):
        #         num_output = self.heads[head]
        #         if head_conv > 0:
        #             fc = nn.Sequential(
        #                 nn.Conv2d(fpn_c, head_conv, kernel_size=3, padding=1, bias=True),
        #                 nn.ReLU(inplace=True),
        #                 nn.Conv2d(head_conv, num_output, kernel_size=1, stride=1, padding=0))
        #         else:
        #             fc = nn.Conv2d(in_channels=fpn_c, out_channels=num_output, kernel_size=1, stride=1, padding=0)

        #         self.__setattr__('fpn{}_{}'.format(fpn_idx, head), fc)

        # For feature pyramid
        fpn_channels = [128, 128, 64]
        for fpn_idx, fpn_c in enumerate(fpn_channels):
            for head in sorted(self.heads):
                num_output = self.heads[head]
                if head_conv > 0:
                    fc = nn.Sequential(
                        nn.Conv2d(fpn_c, head_conv, kernel_size=3, padding=1, bias=True),
                        nn.ReLU(inplace=True),
                        nn.Conv2d(head_conv, num_output, kernel_size=1, stride=1, padding=0))
                else:
                    fc = nn.Conv2d(in_channels=fpn_c, out_channels=num_output, kernel_size=1, stride=1, padding=0)

                self.__setattr__('fpn{}_{}'.format(fpn_idx, head), fc)


        # Voxel, init 3d spconv
        self.input_shape = np.array([776, 608, 20])
        self.backbone_3d = SpMiddleFHD()


    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion, momentum=BN_MOMENTUM),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, points, voxels, coordinates, num_points_per_voxel, input_features, batch_size):
        
        # Voxel feature extractor
        # num_input_features = 3
        # input_density = np.minimum(1.0, np.log(num_points_per_voxel + 1) / np.log(64))
        # input_features = voxels[:, :, :num_input_features].sum(dim=1, keepdim=False) / num_points_per_voxel.type_as(voxels).view(-1, 1).contiguous()
        input_features = input_features.type_as(voxels).contiguous()
        spatial_features = self.backbone_3d(input_features.cuda(), coordinates, batch_size, self.input_shape)
        N, C, D, H, W = spatial_features.shape
        spatial_features = spatial_features.view(*spatial_features.shape[:3], H, W)
        N, C, D, H, W = spatial_features.shape
        spatial_features = spatial_features.view(N, C * D, H, W)

        # Bev feature extractor        
        x = self.rpn3d_conv2(spatial_features)
        # x = self.rpn3d_conv3(x, None, None)[0]
        out3, out2, out1 = self.rpn3d_conv3(x, None, None)

        ret = {}
        for head in self.heads:
            temp_outs = []
            for fpn_idx, fdn_input in enumerate([out1, out2, out3]):
                fpn_out = self.__getattr__('fpn{}_{}'.format(fpn_idx, head))(fdn_input)
                _, _, fpn_out_h, fpn_out_w = fpn_out.size()
                if (fpn_out_w != self.hm_w) or (fpn_out_h != self.hm_h):
                    fpn_out = F.interpolate(fpn_out, size=(self.hm_h, self.hm_w))
                temp_outs.append(fpn_out)
            final_out = self.apply_kfpn(temp_outs)
            ret[head] = final_out

        # ret = {}
        # for head in self.heads:
        #     fpn_out = self.__getattr__('fpn{}_{}'.format(0, head))(x)
        #     final_out = self.apply_kfpn([fpn_out])
        #     ret[head] = final_out

        return ret

    def apply_kfpn(self, outs):
        outs = torch.cat([out.unsqueeze(-1) for out in outs], dim=-1)
        softmax_outs = F.softmax(outs, dim=-1)
        ret_outs = (outs * softmax_outs).sum(dim=-1)
        return ret_outs

    def init_weights(self, num_layers, pretrained=True):
        if pretrained:
            # TODO: Check initial weights for head later
            for fpn_idx in [0]:  # 3 FPN layers
                for head in self.heads:
                    final_layer = self.__getattr__('fpn{}_{}'.format(fpn_idx, head))
                    for i, m in enumerate(final_layer.modules()):
                        if isinstance(m, nn.Conv2d):
                            # nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                            # print('=> init {}.weight as normal(0, 0.001)'.format(name))
                            # print('=> init {}.bias as 0'.format(name))
                            if m.weight.shape[0] == self.heads[head]:
                                if 'hm' in head:
                                    nn.init.constant_(m.bias, -2.19)
                                else:
                                    nn.init.normal_(m.weight, std=0.001)
                                    nn.init.constant_(m.bias, 0)

def get_net(heads, head_conv):
    model = PoseResNet(heads, head_conv)
    return model