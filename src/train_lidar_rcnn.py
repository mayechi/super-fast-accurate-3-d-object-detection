import os
import time
import numpy as np
import random
from tqdm import tqdm
import torch
import torch.distributed as dist
from torch.nn import functional as F
from torch.utils.tensorboard import SummaryWriter
from utils.logger import Logger
from utils.train_utils import create_optimizer, create_lr_scheduler, get_saved_state, save_checkpoint
from data_process.apollo_dataloader import create_train_dataloader_lidar_rcnn, create_test_dataloader_lidar_rcnn
from utils.torch_utils import to_python_float, _sigmoid
from config.train_config_apollo import parse_train_configs
from utils.misc import AverageMeter, ProgressMeter
from losses.losses_apollo import Compute_Loss
from models.lidar_rcnn_model_loss import Loss
from models.point_net import PointNet
from utils.bev_map_val import get_map
from data_process.kitti_bev_utils import get_corners

import math
import pdb

def main():
    configs = parse_train_configs()

    # Re-produce results
    if configs.seed is not None:
        random.seed(configs.seed)
        np.random.seed(configs.seed)
        torch.manual_seed(configs.seed)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False

    if configs.gpu_idx is not None:
        print('You have chosen a specific GPU. This will completely disable data parallelism.')

    if configs.dist_url == "env://" and configs.world_size == -1:
        configs.world_size = int(os.environ["WORLD_SIZE"])

    configs.distributed = configs.world_size > 1 or configs.multiprocessing_distributed

    main_worker(configs.gpu_idx, configs)

def main_worker(gpu_idx, configs):
    configs.gpu_idx = gpu_idx
    configs.device = torch.device('cpu' if configs.gpu_idx is None else 'cuda:{}'.format(configs.gpu_idx))

    if configs.distributed:
        if configs.dist_url == "env://" and configs.rank == -1:
            configs.rank = int(os.environ["RANK"])
        if configs.multiprocessing_distributed:
            # For multiprocessing distributed training, rank needs to be the
            # global rank among all the processes
            configs.rank = configs.rank * configs.ngpus_per_node + gpu_idx

        dist.init_process_group(backend=configs.dist_backend, init_method=configs.dist_url,
                                world_size=configs.world_size, rank=configs.rank)
        configs.subdivisions = int(64 / configs.batch_size / configs.ngpus_per_node)
    else:
        configs.subdivisions = int(64 / configs.batch_size)

    configs.is_master_node = (not configs.distributed) or (
            configs.distributed and (configs.rank % configs.ngpus_per_node == 0))

    if configs.is_master_node:
        logger = Logger(configs.logs_dir, configs.saved_fn)
        logger.info('>>> Created a new logger')
        logger.info('>>> configs: {}'.format(configs))
        tb_writer = SummaryWriter(log_dir=os.path.join(configs.logs_dir, 'tensorboard'))
        # tb_writer = None
    else:
        logger = None
        tb_writer = None

    # model
    model = PointNet(pts_dim=9, x=2, CLS_NUM=1+1)
    model.init_weights()
    # model_loss = FullModel(model)
    model = model.to(configs.device)

    # load weight from a checkpoint
    if configs.pretrained_path is not None:
        assert os.path.isfile(configs.pretrained_path), "=> no checkpoint found at '{}'".format(configs.pretrained_path)
        model.load_state_dict(torch.load(configs.pretrained_path, map_location=lambda storage, loc: storage))
        if logger is not None:
            logger.info('loaded pretrained model at {}'.format(configs.pretrained_path))

    # resume weights of model from a checkpoint
    if configs.resume_path is not None:
        assert os.path.isfile(configs.resume_path), "=> no checkpoint found at '{}'".format(configs.resume_path)
        model.load_state_dict(torch.load(configs.resume_path))
        if logger is not None:
            logger.info('resume training model from checkpoint {}'.format(configs.resume_path))

    # Make sure to create optimizer after moving the model to cuda
    optimizer = create_optimizer(configs, model)
    lr_scheduler = create_lr_scheduler(optimizer, configs)
    configs.step_lr_in_epoch = False if configs.lr_type in ['multi_step', 'cosin', 'one_cycle'] else True

    # resume optimizer, lr_scheduler from a checkpoint
    if configs.resume_path is not None:
        utils_path = configs.resume_path.replace('Model_', 'Utils_')
        assert os.path.isfile(utils_path), "=> no checkpoint found at '{}'".format(utils_path)
        utils_state_dict = torch.load(utils_path, map_location='cuda:{}'.format(configs.gpu_idx))
        optimizer.load_state_dict(utils_state_dict['optimizer'])
        lr_scheduler.load_state_dict(utils_state_dict['lr_scheduler'])
        configs.start_epoch = utils_state_dict['epoch'] + 1

    if logger is not None:
        logger.info(">>> Loading dataset & getting dataloader...")
    # Create dataloader
    train_dataloader, train_sampler = create_train_dataloader_lidar_rcnn(configs)
    if logger is not None:
        logger.info('number of batches in training set: {}'.format(len(train_dataloader)))

    for epoch in range(configs.start_epoch, configs.num_epochs + 1):
        # load weight from a checkpoint
        # configs.pretrained_path = 'checkpoints_zhijiangyihao_50_has_i/fpn_resnet_50/Model_fpn_resnet_50_epoch_'+str(20+epoch)+'.pth'       
        # if configs.pretrained_path is not None:
        #     assert os.path.isfile(configs.pretrained_path), "=> no checkpoint found at '{}'".format(configs.pretrained_path)
        #     model.load_state_dict(torch.load(configs.pretrained_path, map_location=lambda storage, loc: storage))
        #     if logger is not None:
        #         logger.info('loaded pretrained model at {}'.format(configs.pretrained_path))        
        if logger is not None:
            logger.info('{}'.format('*-' * 40))
            logger.info('{} {}/{} {}'.format('=' * 35, epoch, configs.num_epochs, '=' * 35))
            logger.info('{}'.format('*-' * 40))
            logger.info('>>> Epoch: [{}/{}]'.format(epoch, configs.num_epochs))

        # train for one epoch
        train_one_epoch(train_dataloader, model, optimizer, lr_scheduler, epoch, configs, logger, tb_writer)
        # if (not configs.no_val) and (epoch % configs.checkpoint_freq == 0):
        if 1:
            test_dataloader = create_test_dataloader_lidar_rcnn(configs)
            print('number of batches in test_dataloader: {}'.format(len(test_dataloader)))
            # val_loss = validate(val_dataloader, model, configs)
            val_loss = validate(test_dataloader, model, configs, logger)          
            print('val_loss: {:.4e}'.format(val_loss))
            if logger is not None:
                logger.info('val_loss: {:.4e}'.format(val_loss))
            if tb_writer is not None:
                tb_writer.add_scalar('Val_loss', val_loss, epoch)

        # Save checkpoint
        # if configs.is_master_node and ((epoch % configs.checkpoint_freq) == 0):
        #     model_state_dict, utils_state_dict = get_saved_state(model, optimizer, lr_scheduler, epoch, configs)
        #     save_checkpoint(configs.checkpoints_dir, configs.saved_fn, model_state_dict, utils_state_dict, epoch)

        if not configs.step_lr_in_epoch:
            lr_scheduler.step()
            if tb_writer is not None:
                tb_writer.add_scalar('LR', lr_scheduler.get_lr()[0], epoch)

    if tb_writer is not None:
        tb_writer.close()

def reduce_tensor(inp):
    """
    Reduce the loss from all processes so that
    process with rank 0 has the averaged results.
    """
    world_size = 1
    if world_size < 2:
        return inp
    with torch.no_grad():
        reduced_inp = inp
        dist.reduce(reduced_inp, dst=0)
    return reduced_inp

def rotz(t):
    ''' Rotation about the z-axis. '''
    c = np.cos(t)
    s = np.sin(t)
    return np.array([[c, -s],
                    [s,  c]])

def from_prediction_to_label_format(centers, sizes, headings,
                                    pred_bbox):
    l, w, h = (np.exp(sizes) * pred_bbox[:, [3, 4, 5]]).T
    ry = headings.reshape(-1)
    tx, ty, tz = (centers * pred_bbox[:, [3, 4, 5]]).T
    return l, w, h, tx, ty, tz, ry

def back_to_lidar_coords(f_bbox, pred_bbox):
    # pdb.set_trace()
    f_bbox[-1] = f_bbox[-1] + pred_bbox[6]
    f_bbox[:2] = rotz(pred_bbox[6]) @ f_bbox[:2]
    f_bbox[:3] += pred_bbox[:3]
    return f_bbox

def validate(test_dataloader, model, configs, logger):
    cnf_dict = {}
    cnf_dict['minX'] = -50
    cnf_dict['maxX'] = 50
    cnf_dict['minY'] = -25
    cnf_dict['maxY'] = 25
    cnf_dict['minZ'] = -2.28
    cnf_dict['maxZ'] = 0.72   
    cnf_dict['BEV_HEIGHT'] = 1216
    cnf_dict['BEV_WIDTH'] = 608
    cnf_dict['DISCRETIZATION'] = (cnf_dict['maxX'] - cnf_dict['minX']) / (cnf_dict['BEV_HEIGHT'] * 1.0)
    cnf_dict['bound_size_x'] = cnf_dict['maxX'] - cnf_dict['minX']
    cnf_dict['bound_size_y'] = cnf_dict['maxY'] - cnf_dict['minY']
    cnf_dict['bound_size_z'] = cnf_dict['maxZ'] - cnf_dict['minZ']

    result_dir = './result/apollo_lidar_rcnn/'
    # result_1_f = open(result_dir+'1.txt', 'w')
    result_3_f = open(result_dir+'3.txt', 'w')
    # result_5_f = open(result_dir+'5.txt', 'w')

    model.eval()
    # result_list = []
    with torch.no_grad():
        for batch_idx, batch_data in enumerate(test_dataloader):
            # pdb.set_trace()
            frame_id_b, pts, pred_bbox, pred_bbox_ori, gt_box, gt_cls = batch_data

            # pre box
            x_b_ori, y_b_ori, z_b_ori, l_b_ori, w_b_ori, h_b_ori, yaw_b_ori, cls_b_ori, cls_score_b_ori = pred_bbox_ori.T

            # gt
            # x_b_ori, y_b_ori, z_b_ori, l_b_ori, w_b_ori, h_b_ori, yaw_b_ori, cls_b_ori = gt_box.T
            # cls_score_b_ori = torch.ones_like(cls_b_ori)

            pred_bbox = pred_bbox.float().to(configs.device)
            pts = pts.float().to(configs.device)
            logits, centers, sizes, headings = model(pts, pred_bbox)
            # print('testing_centers:', centers)
            logits = logits.cpu().numpy()
            centers = centers.cpu().numpy()

            sizes = sizes.cpu().numpy()
            headings = headings.cpu().numpy()
            pred_bbox = pred_bbox.cpu().numpy()
            # pdb.set_trace()
            # post decode
            l_b, w_b, h_b, x_b, y_b, z_b, yaw_b = from_prediction_to_label_format(
                                                  centers, sizes, headings, pred_bbox)
            result = np.array([x_b, y_b, z_b, l_b, w_b, h_b, yaw_b]).T          
            cls_score_b = F.softmax(torch.from_numpy(logits), dim=-1).numpy()
            cls_b, cls_score_b = np.argmax(cls_score_b, 1), np.max(cls_score_b, 1)

            # transform ori to bev format
            # for i in range(cls_b_ori.shape[0]):
            #     if cls_b_ori[i].item() == 0:
            #         continue
            #     frame_id = frame_id_b[i]
            #     l_ori = l_b_ori[i].item() / cnf_dict['bound_size_x'] * cnf_dict['BEV_HEIGHT']
            #     w_ori = w_b_ori[i].item() / cnf_dict['bound_size_y'] * cnf_dict['BEV_WIDTH']
            #     center_input_y = (x_b_ori[i].item() - cnf_dict['minX']) / cnf_dict['bound_size_x'] * cnf_dict['BEV_HEIGHT']  # x --> y (invert to 2D image space)
            #     center_input_x = (y_b_ori[i].item() - cnf_dict['minY']) / cnf_dict['bound_size_y'] * cnf_dict['BEV_WIDTH']  # y --> x  
            #     bev_corners = get_corners(center_input_x, center_input_y, w_ori, l_ori, yaw_b_ori[i].item())
            #     x1, y1, x2, y2, x3, y3, x4, y4 = \
            #     bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
            #     cls, cls_score = cls_b_ori[i].item()+1, cls_score_b_ori[i].item()
            #     # cls, cls_score = cls_b_ori[i].item(), cls_score_b_ori[i].item()

            # transform to bev format
            for i in range(cls_b.shape[0]):
                # if cls_b[i] == 0:
                #     continue
                frame_id = frame_id_b[i]
                f_bbox = back_to_lidar_coords(result[i].copy(), pred_bbox[i])
                x, y, z, l, w, h, yaw = f_bbox
                # 将pre_box融合
                # x, y, z = x_b_ori, y_b_ori, z_b_ori
                yaw = yaw_b_ori[i].item()
                cls_score = cls_score_b_ori[i].item()
                cls = cls_b_ori[i].item()
                # x, y = x_b_ori[i].item(), y_b_ori[i].item()
                # l, w = l_b_ori[i].item(), w_b_ori[i].item()

                cls, cls_score = cls_b[i], cls_score_b[i]

                l = l / cnf_dict['bound_size_x'] * cnf_dict['BEV_HEIGHT']
                w = w / cnf_dict['bound_size_y'] * cnf_dict['BEV_WIDTH']
                center_input_y = (x - cnf_dict['minX']) / cnf_dict['bound_size_x'] * cnf_dict['BEV_HEIGHT']  # x --> y (invert to 2D image space)
                center_input_x = (y - cnf_dict['minY']) / cnf_dict['bound_size_y'] * cnf_dict['BEV_WIDTH']  # y --> x  
                bev_corners = get_corners(center_input_x, center_input_y, w, l, yaw)
                x1, y1, x2, y2, x3, y3, x4, y4 = \
                bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]

                # 整理reuslt
                # result = frame_id+' '+str(cls_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n'
                # if result not in result_list:
                if 1:
                    # result_list.append(result)
                    if cls == 1:   
                        result_3_f.write(frame_id+' '+str(cls_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
                    elif cls == 2:
                        pdb.set_trace()
                    # if cls == 5:
                    #     result_5_f.write(frame_id+' '+str(cls_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
                    # if cls == 1:
                    #     result_1_f.write(frame_id+' '+str(cls_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
    
    # result_1_f.close()
    result_3_f.close()
    # result_5_f.close()
    # pdb.set_trace() 
    detpath  = result_dir+'{:s}.txt'
    # annopath = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/label_for_val/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val_id.txt'
    # annopath = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/label_for_val_add_apollo/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val_id_add_apollo.txt'
    # annopath = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/label_big_width_for_val_add_apollo/{:s}.txt'
    # imagesetfile = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val_id_add_apollo.txt'
    annopath = './dataset/apollo/label_for_val/{:s}.txt'
    imagesetfile = './dataset/apollo/ImageSets/val_id.txt'
    # annopath = './dataset/apollo/label_for_train/{:s}.txt'
    # imagesetfile = './dataset/apollo/ImageSets/train_id.txt'
    try:
        map = get_map(detpath, annopath, imagesetfile)
    except:
        map = -1
    return map
    # return

def train_one_epoch(train_dataloader, model, optimizer, lr_scheduler, epoch, configs, logger, tb_writer):
    criterion = Loss()
    batch_time = AverageMeter('Time', ':6.3f')
    data_time = AverageMeter('Data', ':6.3f')
    ave_loss = AverageMeter()
    ave_cls_loss = AverageMeter()
    ave_center_loss = AverageMeter()
    ave_size_loss = AverageMeter()
    ave_heading_loss = AverageMeter()

    progress = ProgressMeter(len(train_dataloader), [batch_time, data_time,],
                             prefix="Train - Epoch: [{}/{}]".format(epoch, configs.num_epochs))

    
    num_iters_per_epoch = len(train_dataloader)
    # switch to train mode
    model.train()
    start_time = time.time()
    for batch_idx, batch_data in enumerate(tqdm(train_dataloader)):
        global_step = num_iters_per_epoch * (epoch - 1) + batch_idx + 1
        data_time.update(time.time() - start_time)

        pts, pred_bbox, cls_labels, reg_labels = batch_data
        cls_labels = cls_labels.float().to(configs.device)
        reg_labels = reg_labels.float().to(configs.device)
        pred_bbox = pred_bbox.float().to(configs.device)
        pts = pts.float().to(configs.device)
        logits, centers, sizes, headings = model(pts, pred_bbox)
        loss, cls_loss, center_loss, size_loss, heading_loss = criterion(logits, centers, sizes, headings, pred_bbox, cls_labels, reg_labels)
        # print('training_centers:', centers)
        # 使用BatchNorm层时，会得到nan数据，需要continue掉
        # pdb.set_trace()
        if math.isnan(loss):
            continue
        
        reduced_loss = reduce_tensor(loss)
        reduced_cls_loss = reduce_tensor(cls_loss)
        reduced_center_loss = reduce_tensor(center_loss)
        reduced_size_loss = reduce_tensor(size_loss)
        reduced_heading_loss = reduce_tensor(heading_loss)

        ave_loss.update(reduced_loss.item())
        ave_cls_loss.update(reduced_cls_loss.item())
        ave_center_loss.update(reduced_center_loss.item())
        ave_size_loss.update(reduced_size_loss.item())
        ave_heading_loss.update(reduced_heading_loss.item())

        model.zero_grad()
        loss.backward()
        optimizer.step() 

        torch.cuda.synchronize()

        if configs.step_lr_in_epoch:
            lr_scheduler.step()
            if tb_writer is not None:
                tb_writer.add_scalar('LR', lr_scheduler.get_lr()[0], global_step)               

        batch_time.update(time.time() - start_time)
        # update average loss
        # ave_loss.update(loss.item())
        # pdb.set_trace()
        if logger is not None:
            if global_step % configs.print_freq == 0:
                # logger.info(progress.get_message(batch_idx))
                # msg = 'Epoch: [{}/{}] Iter:[{}], Time: {:.2f}, ' \
                #     'Loss: {:.6f}, cls_loss: {:.6f}, center_loss: {:.6f}, size_loss: {:.6f}, heading_loss: {:.6f}' .format(
                #     epoch, configs.num_epochs, batch_idx,
                #     batch_time.avg, ave_loss.avg, ave_cls_loss.avg, ave_center_loss.avg, ave_size_loss.avg, ave_heading_loss.avg)
                msg = 'Loss: {:.6f}, cls_loss: {:.6f}, center_loss: {:.6f}, size_loss: {:.6f}, heading_loss: {:.6f}' .format(
                      ave_loss.avg, ave_cls_loss.avg, ave_center_loss.avg, ave_size_loss.avg, ave_heading_loss.avg)
                logger.info(msg)

        start_time = time.time()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        os._exit(0)