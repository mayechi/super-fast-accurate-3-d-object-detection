# import numpy as np
# import cv2

# def velo2img(cloud_array) :
#     #velo_to_camera cal parameter#
#     T_velo_to_camera = np.array([[7.533745e-03, -9.999714e-01, -6.166020e-04, -4.069766e-03],
#                                     [1.480249e-02, 7.280733e-04, -9.998902e-01, -7.631618e-02],
#                                     [9.998621e-01, 7.523790e-03, 1.480755e-02, -2.717806e-01],
#                                     [0, 0, 0, 1]], np.float64)
    
#     #cam0 parameter#
#     R_rect_00 = np.array([[9.999239e-01, 9.837760e-03, -7.445048e-03, 0],
#                             [-9.869795e-03, 9.999421e-01, -4.278459e-03, 0],
#                             [7.402527e-03, 4.351614e-03, 9.999631e-01, 0],
#                             [0, 0, 0, 1]], np.float64)
#     #cam0 to cam2 parameter#
#     P_rect_02 = np.array([[7.215377e+02, 0.000000e+00, 6.095593e+02, 4.485728e+01],
#                             [0.000000e+00, 7.215377e+02, 1.728540e+02, 2.163791e-01],
#                             [0.000000e+00, 0.000000e+00, 1.000000e+00, 2.745884e-03]], np.float64)      
    
#     P_velo_to_img = np.dot(np.dot(P_rect_02, R_rect_00), T_velo_to_camera)
#     cloud_array_3d = cloud_array.copy()
#     cloud_array_3d[:, -1] = 1
#     cloud_array_3d = cloud_array_3d.T
#     cloud_array_3d = cloud_array_3d[:, cloud_array_3d[0]>0]
#     img2d = np.dot(P_velo_to_img, cloud_array_3d)
#     img2d_1 = (img2d/img2d[-1]).T
#     return cloud_array_3d.T, img2d_1[:, 0:-1]

# lidar_file = '/home/mayechi/Data/kitti/training/velodyne/000000.bin'
# img_file = '/home/mayechi/Data/kitti/training/image_2/000000.png'
# points = np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)
# cloud_array, uvs = velo2img(points)
# img = cv2.imread(img_file)
# mask = np.where((uvs[:, 0] < img.shape[1]) & (uvs[:, 0] >= 0) &
#                 (uvs[:, 1] < img.shape[0]) & (uvs[:, 1] >= 0))
# cloud_array = cloud_array[mask]
# uvs = uvs[mask].astype(np.int)

# bgr = img[list(uvs[:, 1]), list(uvs[:, 0])]
# f = np.concatenate((cloud_array, bgr), axis=1)
 



# img = cv2.imread(img_file)
# BGR_list = list()
# for i in range(uvs.shape[0]):
#     x = int(uvs[i, 0])
#     y = int(uvs[i, 1])
    
#     if x > img.shape[1] or x < 0 or y > img.shape[0] or y < 0:
#         continue
#     BGR = img[y, x, :]
#     BGR_list.append(BGR)
#     cv2.circle(img, (x, y), 1, (0, 0, 255), -1)
# cv2.imshow('img', img)
# cv2.waitKey(0)
# np.concatenate(BGR_list)

# import os
# from shutil import move
# import shutil

# bin_dir = '/home/mayechi/Data/zhijiangyihao/second_stage/pointcloud/training/bin/'
# label_dir = '/home/mayechi/Data2/zhijiangyihao/training_no_train/label7/'

# output_dir = '/home/mayechi/Data2/zhijiangyihao/training_no_train/label7_w/'

# files_list = os.listdir(label_dir)
# for file in files_list:
#     with open(output_dir+file, 'w') as f:
#         for i, line in enumerate(open(label_dir+file, 'r')):
#             line = line.rstrip()
#             line_parts = line.split(' ')
#             x, y, z, l, w, h, ry, cls = line_parts[0], line_parts[1], line_parts[2], line_parts[3], line_parts[4], line_parts[5], line_parts[6], line_parts[7]
#             if i == 0:
#                 f.write(cls+' '+x+' '+y+' '+z+' '+l+' '+w+' '+h+' '+ry)
#             else:
#                 f.write('\n')
#                 f.write(cls+' '+x+' '+y+' '+z+' '+l+' '+w+' '+h+' '+ry)
#     f.close()
#     # move(label_dir+file, output_dir+file+'.txt')
#     # temp = file.replace('.txt', '.bin')
#     # move(bin_dir+temp, output_dir+temp)



# train_txt_file = '/home/mayechi/Data2/zhijiangyihao/training2/pointcloud/ImageSets/train_id.txt'
# val_txt_file = '/home/mayechi/Data2/zhijiangyihao/training2/pointcloud/ImageSets/val_id.txt'

# train_txt_output = '/home/mayechi/Data2/test/train_id.txt'
# with open(train_txt_output, 'w') as f_train_o:
#     with open(train_txt_file) as f_train:
#         train_list = f_train.readlines()
#         for train_id in train_list:
#             if train_id.find('-') != -1:
#                 f_train_o.write(train_id)
#     f_train.close()
# f_train_o.close()


# val_txt_output = '/home/mayechi/Data2/test/val_id.txt'
# with open(val_txt_output, 'w') as f_val_o:
#     with open(val_txt_file) as f_val:
#         val_list = f_val.readlines()
#         for val_id in val_list:
#             if val_id.find('-') != -1:
#                 f_val_o.write(val_id)
#     f_val.close()
# f_val_o.close()

# bin9_dir = '/media/myc/Data21/zhijiangyihao/training_fusion/bin9/'
# label9_dir = '/media/myc/Data21/zhijiangyihao/training_fusion/label9/'
# image9_dir = '/media/myc/Data21/zhijiangyihao/training_fusion/image9/'
# output_dir = '/media/myc/Data21/zhijiangyihao/training_fusion/out/'

# bin9_list = os.listdir(bin9_dir)
# bin9_list = sorted(bin9_list)
# label9_list = os.listdir(label9_dir)
# label9_list = sorted(label9_list)
# image9_list = os.listdir(image9_dir)
# image9_list = sorted(image9_list)
# for i, bin9_file in enumerate(bin9_list):
#     bin9_id = bin9_file.split('.bin')[0]
#     bin9_id_0 = bin9_file.split('-')[0]
#     label9_id = label9_list[i].split('.txt')[0]
#     if bin9_id_0 == label9_id:
#         shutil.copy(label9_dir+label9_list[i], output_dir+bin9_id+'.txt')
#         # shutil.copy(image9_dir+image9_list[i], output_dir+bin9_id_0+'.jpg')

# import cv2
# jpg_dir = '/home/myc/桌面/temp/jpg/'
# out_dir = '/home/myc/桌面/temp/out/'
# jpg_list = os.listdir(jpg_dir)
# for i, jpg_file in enumerate(jpg_list):
#     jpg = cv2.imread(jpg_dir+jpg_file)
#     jpg = cv2.resize(jpg, (286, 400))
#     cv2.imwrite(out_dir+str(i)+'.jpg', jpg)

import cv2
import os
img_root = '/home/myc/桌面/video_case/'  
image_name_list = os.listdir(img_root)
image_name_list.sort()
fps = 5  

#可以用(*'DVIX')或(*'X264'),如果都不行先装ffmepg
fourcc = cv2.VideoWriter_fourcc(*'XVID')
videoWriter = cv2.VideoWriter('/home/myc/桌面/test.mp4', fourcc, fps,(1280, 720), True)

for i in range(len(image_name_list)):
    frame = cv2.imread(img_root + image_name_list[i])
    videoWriter.write(frame)
videoWriter.release()
# cv2.destroyAllWindows()

