"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.08.17
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: Testing script
"""

import argparse
import sys
import os
import time
import pdb
import warnings

warnings.filterwarnings("ignore", category=UserWarning)

from easydict import EasyDict as edict
import cv2
import torch
from torch.utils.tensorboard import SummaryWriter
import numpy as np
import subprocess
import random

sys.path.append('../')

from data_process.kitti_dataloader import create_test_dataloader, create_fusion_train_dataloader, create_fusion_val_dataloader
import torchplus
from models.model_utils import create_model
from models import fusion
from utils.train_utils import create_optimizer, create_lr_scheduler, get_saved_state, save_checkpoint
from utils.misc import make_folder, time_synchronized
from utils.evaluation_utils import decode, post_processing_all_scores, draw_predictions, convert_det_to_kitti_format
from utils.evaluation_utils import generate_fusion_input, generate_kitti_3d_detection
from utils.torch_utils import _sigmoid
from utils.torch_utils import reduce_tensor, to_python_float
# from utils.calibration_kitti import Calibration
from data_process.kitti_data_utils import Calibration
import config.kitti_config as cnf
from config.train_config import parse_train_configs
from data_process.transformation import lidar_to_camera_box
from utils.visualization_utils import merge_rgb_to_bev, show_rgb_image_with_boxes
from utils import eval_clocs
from utils.logger import Logger
from utils.misc import AverageMeter, ProgressMeter

from losses.losses import SigmoidFocalClassificationLoss


ID_TYPE_CONVERSION_inv = {
    'Pedestrian': 0,
    'Car': 1,
    'Cyclist': 2,
    'Van': 1
}

def train_one_epoch(train_dataloader, model, fusion_layer, optimizer, lr_scheduler, epoch, configs, logger, tb_writer):
    batch_time = AverageMeter('Time', ':6.3f')
    data_time = AverageMeter('Data', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')

    progress = ProgressMeter(len(train_dataloader), [batch_time, data_time, losses],
                             prefix="Train - Epoch: [{}/{}]".format(epoch, configs.num_epochs))
    num_iters_per_epoch = len(train_dataloader)
    start_time = time.time()

    for batch_idx, batch_data in enumerate(train_dataloader):
        # if batch_idx == 581:
        #     a = 0
        metadatas, bev_maps, img_rgbs, labels = batch_data
        batch_size = bev_maps.size(0)
        global_step = num_iters_per_epoch * (epoch - 1) + batch_idx + 1
        input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float()
        t1 = time_synchronized()
        with torch.no_grad():
            outputs = model(input_bev_maps)
            outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
            outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
        # detections size (batch_size, K, 10)
        detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
                                        outputs['dim'], K=configs.K)
        detections = detections.cpu().numpy().astype(np.float32)
        all_scores = all_scores.cpu().squeeze().numpy()

        detections = post_processing_all_scores(detections, all_scores, configs.num_classes, configs.down_ratio, -1)
        t2 = time_synchronized()

        detections = detections[0]  # only first batch

        img_path = metadatas['img_path'][0]
        img_rgb = img_rgbs[0].numpy()
        # img_rgb = cv2.resize(img_rgb, (img_rgb.shape[1], img_rgb.shape[0]))
        # img_bgr = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2BGR)

        calib = Calibration(img_path.replace(".png", ".txt").replace("image_2", "calib"))
        # kitti_dets = convert_det_to_real_values(detections)

        img_shape = (img_rgb.shape[0], img_rgb.shape[1])
        image_id = metadatas['img_path'][0].split('.png')[0].split('/')[-1]
        # calib = Calibration(img_path.replace(".png", ".txt").replace("image_2", "calib"))
        calib = Calibration(img_path.replace(".png", ".txt").replace("image_2", "calib"))
        label = img_path.replace(".png", ".txt").replace("image_2", "label_2")
        # label = None
        
        # 读取2D检测结果
        detection_2d_path = os.path.join(detection_2d_train_dir, label.split('/')[-1])
        with open(detection_2d_path, 'r') as ann:
            content_list = list()
            for ann_ind, txt in enumerate(ann):
                temp = txt[:-1].split('&')[0].split(' ')
                temp_all_scores = txt[:-1].split('&')[-1].split(' ')
                # temp = txt[:-1].split(' ')
                cls_id = temp[0]
                if cls_id in ID_TYPE_CONVERSION_inv:
                    cls_id = ID_TYPE_CONVERSION_inv[cls_id]
                else:
                    continue                    
                x_min, y_min, x_max, y_max = float(temp[4]), float(temp[5]), float(temp[6]), float(temp[7])
                car_score, p_score, cyc_score = float(temp_all_scores[0]), float(temp_all_scores[1]), float(temp_all_scores[2])
                if detection_2d_true_flag:
                    score = 0.95
                else:
                    score = float(temp[15])
                # content = [cls_id, x_min, y_min, x_max, y_max, score]
                content = [cls_id, x_min, y_min, x_max, y_max, score, p_score, car_score, cyc_score]
                content_list.append(content)
        if len(content_list) == 0:
            content_list.append([-1, -1, -1, -1, -1, -1])
        top_predictions = np.array(content_list)

        # 计算3D检测和2D检测的IOU，并融合成特征，得到融合的输入；对labels的坐标系进行转化，转成camera坐标系
        res, labels_dicts, all_3d_output, top_predictions, fusion_input, tensor_index, tensor_iou = \
        generate_fusion_input(batch_idx, cnf, calib, img_shape, configs.num_classes, detections, top_predictions, labels)

        # 将融合后新的Feature送进融合网络
        cls_preds, flag = fusion_layer(fusion_input.cuda(), tensor_index.cuda(), tensor_iou.cuda())

        # cls_preds = torch.sigmoid(cls_preds)
        res_scores, res_clses = cls_preds.reshape(3,-1).max(dim=0)
        res_scores_mask = np.zeros_like(res_scores.detach().cpu().numpy(), dtype=np.int)
        res_scores_mask[np.unique(res_scores.detach().cpu().numpy(), return_index=True)[1]] = 1
        res_scores[res_scores_mask!=1] = -10
        cls_preds = res_scores.reshape(1, -1, 1)
        res_clses = res_clses.cpu().reshape(-1, 1).numpy()

        # 计算pre_3d_box和labels的iou，定义哪些pre_3d_box应该为正样本，分数应该大一些
        if labels.shape[1] == 0:
            target_for_fusion = np.zeros((1, configs.K, 1))
            target_multi_for_fusion = np.zeros((1, configs.K, 3))
            positives = torch.zeros(1, configs.K).type(torch.float32).cuda()
            negatives = torch.zeros(1, configs.K).type(torch.float32).cuda()
            negatives[:, :] = 1  
        else:
            labels_boxes_camera = labels_dicts[0]['labels_camera']
            labels_clses = labels_dicts[0]['labels_cls']
            pred_boxes_camera = res[0]['box3d_camera']
            pred_clses = res[0]['label_preds']
            # iou_bev, iou_bev_label = eval_clocs.d3_box_overlap(labels_boxes_camera, pred_boxes_camera, labels_clses, pred_clses, criterion=-1)
            iou_bev, iou_bev_label = eval_clocs.d3_box_overlap(labels_boxes_camera, pred_boxes_camera, labels_clses, res_clses, criterion=-1)
            iou_bev_max = np.amax(iou_bev, axis=0)
            iou_bev_max_label = iou_bev_label[np.argmax(iou_bev, axis=0), range(50)].reshape(1, -1, 3)
            target_for_fusion = ((iou_bev_max >= 0.7)*1).reshape(1, -1, 1)
            target_multi_for_fusion = iou_bev_max_label * target_for_fusion
            positive_index = ((iou_bev_max >= 0.7)*1).reshape(1, -1)
            positives = torch.from_numpy(positive_index).type(torch.float32).cuda()
            negative_index = ((iou_bev_max <= 0.5)*1).reshape(1, -1)
            negatives = torch.from_numpy(negative_index).type(torch.float32).cuda()


        one_hot_targets = torch.from_numpy(target_for_fusion).type(torch.float32).cuda()
        # one_hot_targets = torch.from_numpy(target_multi_for_fusion).type(torch.float32).cuda()

        negative_cls_weights = negatives.type(torch.float32) * 1.0
        cls_weights = negative_cls_weights + 1.0 * positives.type(torch.float32)
        pos_normalizer = positives.sum(1, keepdim=True).type(torch.float32)
        cls_weights /= torch.clamp(pos_normalizer, min=1.0) 
        if flag == 1:
            loss_stats = dict()
            cls_losses = focal_loss._compute_loss(cls_preds, one_hot_targets, cls_weights.cuda())  # [N, M]
            cls_losses_reduced = cls_losses.sum()/labels.shape[0]
            # cls_loss_sum = cls_loss_sum + cls_losses_reduced
            # if train_cfg.enable_mixed_precision:
            #     loss *= loss_scale
            cls_losses_reduced.backward()
            optimizer.step()
            optimizer.zero_grad()
            # Adjust learning rate
            if configs.step_lr_in_epoch:
                lr_scheduler.step()
                if tb_writer is not None:
                    tb_writer.add_scalar('LR', lr_scheduler.get_lr()[0], global_step)

            losses.update(to_python_float(cls_losses_reduced), batch_size)
            # measure elapsed time
            # torch.cuda.synchronize()
            batch_time.update(time.time() - start_time)

            if tb_writer is not None:
                if (global_step % configs.tensorboard_freq) == 0:
                    loss_stats['avg_loss'] = losses.avg
                    tb_writer.add_scalars('Train', loss_stats, global_step)
            # Log message
            if logger is not None:
                if (global_step % configs.print_freq) == 0:
                    logger.info(progress.get_message(batch_idx))

            start_time = time.time()

def validate(val_dataloader, model, fusion_layer, configs):
    # switch to train mode
    fusion_layer.eval()
    with torch.no_grad():
        for batch_idx, batch_data in enumerate(val_dataloader):
            metadatas, bev_maps, img_rgbs, labels = batch_data
            batch_size = bev_maps.size(0)
            global_step = num_iters_per_epoch * (epoch - 1) + batch_idx + 1
            input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float()
            t1 = time_synchronized()
            with torch.no_grad():
                outputs = model(input_bev_maps)
                outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
                outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
            # detections size (batch_size, K, 10)
            detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
                                            outputs['dim'], K=configs.K)
            detections = detections.cpu().numpy().astype(np.float32)
            all_scores = all_scores.cpu().squeeze().numpy()

            detections = post_processing_all_scores(detections, all_scores, configs.num_classes, configs.down_ratio, -1)
            t2 = time_synchronized()

            detections = detections[0]  # only first batch

            img_idx = metadatas['img_idx'][0]
            img_path = metadatas['img_path'][0]
            img_rgb = img_rgbs[0].numpy()
            # img_rgb = cv2.resize(img_rgb, (img_rgb.shape[1], img_rgb.shape[0]))
            # img_bgr = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2BGR)

            calib = Calibration(img_path.replace(".png", ".txt").replace("image_2", "calib"))
            # kitti_dets = convert_det_to_real_values(detections)

            img_shape = (img_rgb.shape[0], img_rgb.shape[1])
            image_id = metadatas['img_path'][0].split('.png')[0].split('/')[-1]
            # calib = Calibration(img_path.replace(".png", ".txt").replace("image_2", "calib"))
            calib = Calibration(img_path.replace(".png", ".txt").replace("image_2", "calib"))
            label = img_path.replace(".png", ".txt").replace("image_2", "label_2")
            # label = None
            
            # 读取2D检测结果
            detection_2d_path = os.path.join(detection_2d_val_dir, label.split('/')[-1])
            with open(detection_2d_path, 'r') as ann:
                content_list = list()
                for ann_ind, txt in enumerate(ann):
                    temp = txt[:-1].split('&')[0].split(' ')
                    temp_all_scores = txt[:-1].split('&')[-1].split(' ')
                    # temp = txt[:-1].split(' ')
                    cls_id = temp[0]
                    if cls_id in ID_TYPE_CONVERSION_inv:
                        cls_id = ID_TYPE_CONVERSION_inv[cls_id]
                    else:
                        continue                    
                    x_min, y_min, x_max, y_max = float(temp[4]), float(temp[5]), float(temp[6]), float(temp[7])
                    car_score, p_score, cyc_score = float(temp_all_scores[0]), float(temp_all_scores[1]), float(temp_all_scores[2])
                    if detection_2d_true_flag:
                        score = 0.95
                    else:
                        score = float(temp[15])
                    # content = [cls_id, x_min, y_min, x_max, y_max, score]
                    content = [cls_id, x_min, y_min, x_max, y_max, score, p_score, car_score, cyc_score]
                    content_list.append(content)
            if len(content_list) == 0:
                content_list.append([-1, -1, -1, -1, -1, -1])
            top_predictions = np.array(content_list)

            # 计算3D检测和2D检测的IOU，并融合成特征，得到融合的输入；对labels的坐标系进行转化，转成camera坐标系
            res, labels_dicts, all_3d_output, top_predictions, fusion_input, tensor_index, tensor_iou = \
            generate_fusion_input(batch_idx, cnf, calib, img_shape, configs.num_classes, detections, top_predictions, labels, img_rgb)
            # continue

            # 计算pre_3d_box和labels的iou，定义哪些pre_3d_box应该为正样本，分数应该大一些
            if labels.shape[1] == 0:
                target_for_fusion = np.zeros((1, configs.K, 1))
                target_multi_for_fusion = np.zeros((1, configs.K, 3))
                positives = torch.zeros(1, configs.K).type(torch.float32).cuda()
                negatives = torch.zeros(1, configs.K).type(torch.float32).cuda()
                negatives[:, :] = 1  
            else:
                labels_boxes_camera = labels_dicts[0]['labels_camera']
                labels_clses = labels_dicts[0]['labels_cls']
                pred_boxes_camera = res[0]['box3d_camera']
                pred_clses = res[0]['label_preds']
                iou_bev, iou_bev_label = eval_clocs.d3_box_overlap(labels_boxes_camera, pred_boxes_camera, labels_clses, pred_clses, criterion=-1)
                # iou_bev, iou_bev_label = eval_clocs.d3_box_overlap(labels_boxes_camera, pred_boxes_camera, labels_clses, res_clses, criterion=-1)
                iou_bev_max = np.amax(iou_bev, axis=0)
                iou_bev_max_label = iou_bev_label[np.argmax(iou_bev, axis=0), range(configs.K)].reshape(1, -1, 3)
                target_for_fusion = ((iou_bev_max >= 0.7)*1).reshape(1, -1, 1)
                target_multi_for_fusion = iou_bev_max_label * target_for_fusion
                positive_index = ((iou_bev_max >= 0.7)*1).reshape(1, -1)
                positives = torch.from_numpy(positive_index).type(torch.float32).cuda()
                negative_index = ((iou_bev_max <= 0.5)*1).reshape(1, -1)
                negatives = torch.from_numpy(negative_index).type(torch.float32).cuda()

            # 将融合后新的Feature送进融合网络
            cls_preds, flag = fusion_layer(fusion_input.cuda(), tensor_index.cuda(), tensor_iou.cuda())
            cls_preds = torch.sigmoid(cls_preds)
            # 对网络进行推理测试，将融合模型得到的3D检测分数替换原始3D检测的分数，并计算验证集下的3DmAP
            # res[0]['kitti_res'][:, -1] = cls_preds.cpu().reshape(-1)

            res_scores, res_clses = cls_preds.reshape(3,-1).max(dim=0)
            res_scores_mask = np.zeros_like(res_scores.cpu().numpy(), dtype=np.int)
            res_scores_mask[np.unique(res_scores.cpu().numpy(), return_index=True)[1]] = 1
            res_scores[res_scores_mask!=1] = 0
            res[0]['kitti_res'][:, 0] = res_clses.cpu().reshape(-1)
            res[0]['kitti_res'][:, -1] = res_scores.cpu().reshape(-1)

            # res[0]['kitti_res'][:, -1] = cls_preds.max(dim=2)[0].cpu().reshape(-1)

            res_selected = res[0]['kitti_res'][:, -1] > 0.2
            kitti_res = res[0]['kitti_res'][res_selected]
            kitti_txt = pred_txt = './result/kitti/val/data/' + str(img_idx) + '.txt'
            generate_kitti_3d_detection(kitti_res, kitti_txt)

    logger.info("Evaluate on KITTI dataset")
    output_folder = './result/kitti/val'
    output_dir = os.path.abspath(output_folder)
    # os.chdir('./src/data_process/kitti_eval')
    label_dir = '/home/mayechi/Data/kitti/training/label_2_val'
    # label_dir = '/home/mayechi/Data/kitti/training/label_2_train'
    if not os.path.isfile('./src/data_process/kitti_eval/evaluate_object_offline'):
        subprocess.Popen('g++ -O3 -DNDEBUG -o evaluate_object_3d_offline evaluate_object_3d_offline.cpp', shell=True)
    command = "./src/data_process/kitti_eval/evaluate_object_offline {} {}".format(label_dir, output_dir)
    output = subprocess.check_output(command, shell=True, universal_newlines=True).strip()

    try:
        car_bev_map = output[output.find('car_detection_ground AP'):output.find('car_detection_ground AP')+54]
        logger.info(car_bev_map)
        car_bev_map_ave = (float(car_bev_map.split(' ')[2]) + float(car_bev_map.split(' ')[3]) + float(car_bev_map.split(' ')[4])) / 3
        ped_bev_map = output[output.find('pedestrian_detection_ground AP'):output.find('pedestrian_detection_ground AP')+61]
        logger.info(ped_bev_map)
        ped_bev_map_ave = (float(ped_bev_map.split(' ')[2]) + float(ped_bev_map.split(' ')[3]) + float(ped_bev_map.split(' ')[4])) / 3
        cyc_bev_map = output[output.find('cyclist_detection_ground AP'):output.find('cyclist_detection_ground AP')+58]
        logger.info(cyc_bev_map)
        cyc_bev_map_ave = (float(cyc_bev_map.split(' ')[2]) + float(cyc_bev_map.split(' ')[3]) + float(cyc_bev_map.split(' ')[4])) / 3    
        # return ped_bev_map_ave

        # car_bev_map, ped_bev_map, cyc_bev_map = output.split('\n')[41], output.split('\n')[43], output.split('\n')[45] 
        # car_3dmap, ped_3dmap, cyc_3dmap = output.split('\n')[47], output.split('\n')[49], output.split('\n')[51] 
        # logger.info(car_bev_map)
        # logger.info(ped_bev_map)
        # logger.info(cyc_bev_map)
        # logger.info(car_3dmap)
        # logger.info(ped_3dmap)
        # logger.info(cyc_3dmap)
        # car_3dmap_ave = (float(car_3dmap.split(' ')[2]) + float(car_3dmap.split(' ')[3]) + float(car_3dmap.split(' ')[4])) / 3
        # ped_3dmap_ave = (float(ped_3dmap.split(' ')[2]) + float(ped_3dmap.split(' ')[3]) + float(ped_3dmap.split(' ')[4])) / 3
        # cyc_3dmap_ave = (float(cyc_3dmap.split(' ')[2]) + float(cyc_3dmap.split(' ')[3]) + float(cyc_3dmap.split(' ')[4])) / 3
        # all_3d_map_ave = (car_3dmap_ave + ped_3dmap_ave + cyc_3dmap_ave) / 3

        # car_bev_map_ave = (float(car_bev_map.split(' ')[2]) + float(car_bev_map.split(' ')[3]) + float(car_bev_map.splis
        all_bev_map_ave = (car_bev_map_ave + ped_bev_map_ave + cyc_bev_map_ave) / 3    
    except:
        all_bev_map_ave = -1
    return all_bev_map_ave



if __name__ == '__main__':

    configs = parse_train_configs()
    configs.distributed = configs.world_size > 1 or configs.multiprocessing_distributed
    configs.is_master_node = (not configs.distributed) or (
            configs.distributed and (configs.rank % configs.ngpus_per_node == 0))    
    if configs.is_master_node:
        logger = Logger(configs.logs_dir, configs.saved_fn)
        logger.info('>>> Created a new logger')
        logger.info('>>> configs: {}'.format(configs))
        tb_writer = SummaryWriter(log_dir=os.path.join(configs.logs_dir, 'tensorboard'))
    else:
        logger = None
        tb_writer = None

    model = create_model(configs)
    print('\n\n' + '-*=' * 30 + '\n\n')
    assert os.path.isfile(configs.pretrained_path), "No file at {}".format(configs.pretrained_path)

    model.load_state_dict(torch.load(configs.pretrained_path, map_location=lambda storage, loc: storage))
    print('Loaded weights from {}\n'.format(configs.pretrained_path))

    configs.device = torch.device('cpu' if configs.no_cuda else 'cuda:{}'.format(configs.gpu_idx))
    model = model.to(device=configs.device)

    out_cap = None
    # detection_2d_dir = '/home/mayechi/Work/Research/code/ma_detec/result/val/data'
    detection_2d_train_dir = '/home/mayechi/Work/Research/code/ma_detec/result/train_clocs/data'
    
    detection_2d_val_dir = '/home/mayechi/Work/Research/code/ma_detec/result/val_clocs/data'
    # detection_2d_val_dir = '/home/mayechi/Work/Research/code/CLOCs/d2_detection_data_val/data'
    # detection_2d_val_dir = '/home/mayechi/Data/kitti/training/label_2_val'
    detection_2d_true_flag = 0

    clocs_model_dir = './CLOCs_SecCas_pretrained'
    clocs_model_car_path = './CLOCs_SecCas_pretrained/fusion_layer-11136.tckpt'
    model.eval()

    # Fusion model
    fusion_layer = fusion.fusion(configs)
    fusion_layer.train()
    fusion_layer.cuda()
    focal_loss = SigmoidFocalClassificationLoss()
    cls_loss_sum = 0
    ckpt_path = None
    if ckpt_path is None:
        print("load existing model for fusion layer")
        # torchplus.train.try_restore_latest_checkpoints(clocs_model_dir, [fusion_layer], clocs_model_car_path)
    else:
        torchplus.train.restore(ckpt_path, fusion_layer)
    
    # create optimizer
    optimizer = create_optimizer(configs, fusion_layer)
    lr_scheduler = create_lr_scheduler(optimizer, configs)
    configs.step_lr_in_epoch = False if configs.lr_type in ['multi_step', 'cosin', 'one_cycle'] else True    
    optimizer.zero_grad()

    # create train_dataloader
    train_dataloader = create_fusion_train_dataloader(configs)
    num_iters_per_epoch = len(train_dataloader)

    for epoch in range(configs.start_epoch, configs.num_epochs + 1):
        if logger is not None:
            logger.info('{}'.format('*-' * 40))
            logger.info('{} {}/{} {}'.format('=' * 35, epoch, configs.num_epochs, '=' * 35))
            logger.info('{}'.format('*-' * 40))
            logger.info('>>> Epoch: [{}/{}]'.format(epoch, configs.num_epochs))

        if configs.distributed:
            train_sampler.set_epoch(epoch)
        # train for one epoch
        train_one_epoch(train_dataloader, model, fusion_layer, optimizer, lr_scheduler, epoch, configs, logger, tb_writer)

        # 每训练完1个epoch，进行验证集验证，并存储
        # if (not configs.no_val) and (epoch % configs.checkpoint_freq == 0):
        if 1:
            val_dataloader = create_fusion_val_dataloader(configs)
            print('number of batches in val_dataloader: {}'.format(len(val_dataloader)))
            all_bev_map_ave = validate(val_dataloader, model, fusion_layer, configs)
            print('all_bev_map_ave: {:.4e}'.format(all_bev_map_ave/10.0))
            if tb_writer is not None:
                tb_writer.add_scalar('all_bev_map_ave', all_bev_map_ave/10.0, epoch)

        # Save checkpoint
        # if configs.is_master_node and ((epoch % configs.checkpoint_freq) == 0):
        if 1:
            model_state_dict, utils_state_dict = get_saved_state(fusion_layer, optimizer, lr_scheduler, epoch, configs)
            save_checkpoint(configs.checkpoints_dir, 'fusion_all', model_state_dict, utils_state_dict, epoch)

        if not configs.step_lr_in_epoch:
            lr_scheduler.step()
            if tb_writer is not None:
                tb_writer.add_scalar('LR', lr_scheduler.get_lr()[0], epoch)

    if tb_writer is not None:
        tb_writer.close()
    if configs.distributed:
        cleanup()
 
                       

   
