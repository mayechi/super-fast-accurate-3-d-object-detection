"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.08.17
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: Testing script
"""

import argparse
import sys
import os
import time
import pdb
import warnings

warnings.filterwarnings("ignore", category=UserWarning)

from easydict import EasyDict as edict
import cv2
import torch
from torch.utils.tensorboard import SummaryWriter
import numpy as np
import subprocess
import random

sys.path.append('../')

from data_process.apollo_dataloader import create_fusion_train_dataloader, create_fusion_val_dataloader, create_fusion_no_nn_test_dataloader
from data_process.kitti_bev_utils import get_corners
import torchplus
from models.model_utils import create_model
from models import fusion
from utils.train_utils import create_optimizer, create_lr_scheduler, get_saved_state, save_checkpoint
from utils.misc import make_folder, time_synchronized
from utils.evaluation_utils import decode, post_processing_all_scores_no_nn, draw_predictions_fusion, convert_det_to_kitti_format
from utils.evaluation_utils import generate_fusion_input, generate_fusion_input_apollo, generate_fusion_input_apollo_no_nn, lidar_result_in_imgae, generate_kitti_3d_detection
from utils.torch_utils import _sigmoid
from utils.torch_utils import reduce_tensor, to_python_float
from utils.bev_map_val import get_map
# from utils.calibration_kitti import Calibration
from data_process.kitti_data_utils import Calibration
import config.apollo_config as cnf
from config.train_config_apollo import parse_train_configs
from data_process.transformation import lidar_to_camera_box
from utils.visualization_utils import merge_rgb_to_bev, show_rgb_image_with_boxes
from utils import eval_clocs
from utils.logger import Logger
from utils.misc import AverageMeter, ProgressMeter
from utils.lidar_to_camera import whether_in_image

from losses.losses import SigmoidFocalClassificationLoss

detection_2d_test_dir = '/media/myc/Work/Research/code/ma_detec/result_zhijiangyihao/data'
# detection_2d_test_dir = '/media/myc/Work/Research/code/ma_detec/result_zhijiangyihao/data_9'

result_dir = './result/zhijiangyihao/'
result_1_f = open(result_dir+'1.txt', 'w')
result_3_f = open(result_dir+'3.txt', 'w')
result_5_f = open(result_dir+'5.txt', 'w')

def map_calculate_bev(metadatas, detections_post):
    # 整理reuslt
    for j in range(6):
        if j == 1-1:
            if len(detections_post[0][j] > 0):
                for det in detections_post[0][j]:
                    _score, _x, _y, _z, _h, _w, _l, _yaw = det[0:8]
                    box_list = bev_to_lidar_coordinate([_x, _y, _z, _h, _w, _l, _yaw]) 
                    flag_in_image = whether_in_image(box_list)
                    if flag_in_image == False:
                        continue
                    bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                    x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                    result_1_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if j == 3-1:
            if len(detections_post[0][j] > 0):
                for det in detections_post[0][j]:
                    _score, _x, _y, _z, _h, _w, _l, _yaw = det[0:8] 
                    box_list = bev_to_lidar_coordinate([_x, _y, _z, _h, _w, _l, _yaw]) 
                    flag_in_image = whether_in_image(box_list)
                    if flag_in_image == False:
                        continue                                                  
                    bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                    x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                    result_3_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if j == 5-1:
            if len(detections_post[0][j] > 0):
                for det in detections_post[0][j]:
                    _score, _x, _y, _z, _h, _w, _l, _yaw = det[0:8]
                    box_list = bev_to_lidar_coordinate([_x, _y, _z, _h, _w, _l, _yaw]) 
                    flag_in_image = whether_in_image(box_list)
                    if flag_in_image == False:
                        continue                                                       
                    bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                    x1, y1, x2, y2, x3, y3, x4, y4 = \
                    bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], \
                    bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                    result_5_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')    

def map_calculate_lidar(metadatas, result_3d_list):
    for result_3d in result_3d_list:
        cls_id, x, y, z, l, w, h, yaw, _score = result_3d

        l = l / cnf.bound_size_x * cnf.BEV_HEIGHT
        w = w / cnf.bound_size_y * cnf.BEV_WIDTH
        center_input_y = (x - cnf.boundary['minX']) / cnf.bound_size_x * cnf.BEV_HEIGHT  # x --> y (invert to 2D image space)
        center_input_x = (y - cnf.boundary['minY']) / cnf.bound_size_y * cnf.BEV_WIDTH  # y --> x
        yaw = -yaw
        
        bev_corners = get_corners(center_input_x, center_input_y, w, l, yaw)
        x1, y1, x2, y2, x3, y3, x4, y4 = \
        bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], \
        bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
        if cls_id == 1-1:
            result_1_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if cls_id == 3-1:
            result_3_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if cls_id == 5-1:
            result_5_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')

def bev_to_lidar_coordinate(bev_list):
    _x, _y, _z, _h, _w, _l, _yaw = bev_list
    _yaw = -_yaw
    x = _y / cnf.BEV_HEIGHT * cnf.bound_size_x + cnf.boundary['minX']
    y = _x / cnf.BEV_WIDTH * cnf.bound_size_y + cnf.boundary['minY']
    z = _z + cnf.boundary['minZ']
    w = _w / cnf.BEV_WIDTH * cnf.bound_size_y
    l = _l / cnf.BEV_HEIGHT * cnf.bound_size_x
    
    return [x, y, z, l, w, _h, _yaw]

def test_fusion(test_dataloader, model, configs):

    model.eval()
    for batch_idx, batch_data in enumerate(test_dataloader):
        # break
        # print('batch_idx:', batch_idx)
        
        metadatas, bev_maps, bev_maps_ori, bev_maps_index, img, labels, labels_bev = batch_data
        if metadatas == {}:
            continue
        print('frame_id:', metadatas['frame_id'][0])
        input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float() 
        input_bev_maps_index = bev_maps_index.to(configs.device, non_blocking=True).int()          
        with torch.no_grad():
            outputs = model(input_bev_maps)

        outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
        outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
        # detections size (batch_size, K, 10)
        detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
                                        outputs['dim'], K=50)        
        detections = detections.cpu().numpy().astype(np.float32)
        all_scores = all_scores.cpu().squeeze().numpy()

        detections_post = post_processing_all_scores_no_nn(detections, all_scores, configs.num_classes, configs.down_ratio, 0.2)
        detections = detections_post[0]  # only first batch

        # map_calculate_bev(metadatas, detections_post)
        # continue

        # Draw prediction in the bev image
        bev_map = (bev_maps_ori[0].permute(1, 2, 0).numpy() * 255).astype(np.uint8)
        bev_map = cv2.resize(bev_map, (608, 1216))
        bev_map = draw_predictions_fusion(bev_map, detections, configs.num_classes)
        bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)

        # bev_map = None

        frame_id = metadatas['frame_id'][0]
        if 0:
            # 读取2D检测结果
            image_frame_id = metadatas['image_frame_id'][0]
            detection_2d_name = image_frame_id + '.txt'
            detection_2d_path = os.path.join(detection_2d_test_dir, detection_2d_name)
            with open(detection_2d_path, 'r') as ann:
                content_list = list()
                for ann_ind, txt in enumerate(ann):
                    temp = txt[:-1].split(' ')
                    temp_all_scores = temp[6:]
                    cls_id = int(temp[0])-1
                    x_min, y_min, x_max, y_max = float(temp[1]), float(temp[2]), float(temp[3]), float(temp[4])
                    score_1, score_2, score_3, score_4, score_5, score_6 = \
                    float(temp_all_scores[0]), float(temp_all_scores[1]), float(temp_all_scores[2]), \
                    float(temp_all_scores[3]), float(temp_all_scores[4]), float(temp_all_scores[5])
                    score = float(temp[5])
                    content = [cls_id, x_min, y_min, x_max, y_max, score, score_1, score_2, score_3, score_4, score_5, score_6]
                    content_list.append(content)
            if len(content_list) == 0:
                content_list.append([-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1])
            top_predictions = np.array(content_list)
        else:
            top_predictions = None
        
        # 计算3D检测和2D检测的IOU，并融合成特征，得到融合的输入；对labels的坐标系进行转化，转成camera坐标系
        img_rgb = img[0].numpy()
        res, labels_dicts, all_3d_output, top_predictions, fusion_input, tensor_index, tensor_iou, valid_num, match_list, iou_hide_mat = \
        generate_fusion_input_apollo_no_nn(batch_idx, cnf, configs.num_classes, detections, top_predictions, labels, labels_bev, img_rgb.copy(), bev_map)

        pred_3d_all_scores, pred_2d_all_scores = res[0]['pred_3d_all_scores'], res[0]['pred_2d_all_scores']
        pred_3d_scores = res[0]['scores']
        pred_3d_cls = res[0]['pred_clses']
        pred_boxes_lidar = res[0]['box3d_lidar']
        result_3d_list = []
        result_3d_id_list = []
        
        # 直接用原始的3D检测结果
        # for i in range(pred_boxes_lidar.shape[0]):
        #     cls_id = pred_3d_cls[i][0]
        #     socre = pred_3d_scores[i][0]
        #     result_3d = pred_boxes_lidar[i].tolist() 
        #     result_3d.insert(0, cls_id)
        #     result_3d.append(socre)
        #     result_3d_list.append(result_3d)

        # 得到融合的3D检测结果
        for i in range(len(match_list)):
            pred_3d_id, pred_2d_id = match_list[i][0], match_list[i][1]
            result_3d_id_list.append(pred_3d_id)
            pred_3d_all_score, pred_2d_all_score = pred_3d_all_scores[pred_3d_id], pred_2d_all_scores[pred_2d_id]
            pred_add_all_score = pred_3d_all_score + pred_2d_all_score
            all_score = np.concatenate((pred_3d_all_score, pred_2d_all_score))
            # top_id, top_socre = np.argmax(all_score), np.max(all_score)
            # top_id, top_socre = np.argmax(pred_2d_all_score), np.max(all_score)
            top_id, top_socre = np.argmax(pred_add_all_score), np.max(all_score)
            # top_id, top_socre = np.argmax(pred_3d_all_score), np.max(pred_3d_all_score)
            # top_id, top_socre = pred_3d_cls[pred_3d_id][0], pred_3d_scores[pred_3d_id][0]
            if top_id > 5:
                top_id = top_id - 6
            result_3d = pred_boxes_lidar[pred_3d_id].tolist()  
            result_3d.insert(0, top_id)
            result_3d.append(top_socre)
            result_3d_list.append(result_3d)

        # show 融合之后的感知结果
        lidar_result_in_imgae(result_3d_list, img_rgb.copy())
      
        # # 针对遮挡，保留3d
        for i in range(pred_boxes_lidar.shape[0]):
            if i not in result_3d_id_list:
                iou_3d_max = np.max(iou_hide_mat[i])
                # cls_id, score_3d = np.argmax(pred_3d_all_scores[i]), np.max(pred_3d_all_scores[i])
                cls_id, score_3d = pred_3d_cls[i][0], pred_3d_scores[i][0]
                # if iou_3d_max > 0.5 and score_3d > 0.5:
                if iou_3d_max > 0.3 or score_3d > 0.4:
                    result_3d = pred_boxes_lidar[i].tolist() 
                    result_3d.insert(0, cls_id)
                    result_3d.append(score_3d)
                    result_3d_list.append(result_3d)        

        # show 融合之后的感知结果
        lidar_result_in_imgae(result_3d_list, img_rgb.copy())

        # 验证map指标
        # map_calculate_lidar(metadatas, result_3d_list)



    # result_1_f.close()
    # result_3_f.close()
    # result_5_f.close()
    # detpath  = result_dir+'{:s}.txt'
    # annopath = '/media/myc/Data21/zhijiangyihao/training_fusion/test_map10_image/{:s}.txt'
    # # annopath = '/media/myc/Data21/zhijiangyihao/training_fusion/test_map9/{:s}.txt'
    # imagesetfile = '/media/myc/Data21/zhijiangyihao/training_fusion/test_10_shuffle.list'
    # map = get_map(detpath, annopath, imagesetfile)
    return

if __name__ == '__main__':

    configs = parse_train_configs()

    model = create_model(configs)
    model_path = '/media/myc/Work/Research/code/Super-Fast-Accurate-3D-Object-Detection/checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_19_0.3.8.pth'
    # model_path = '/media/myc/Work/Research/code/Super-Fast-Accurate-3D-Object-Detection/checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_114_0.4.5.pth'
    
    model.load_state_dict(torch.load(model_path, map_location=lambda storage, loc: storage))
    model.cuda(configs.gpu_idx)
    print('Loaded weights from {}\n'.format(configs.pretrained_path))

    configs.device = torch.device('cpu' if configs.no_cuda else 'cuda:{}'.format(configs.gpu_idx))
    model = model.to(device=configs.device)

    test_dataloader = create_fusion_no_nn_test_dataloader(configs)
    test_fusion(test_dataloader, model, configs)
                       

   
