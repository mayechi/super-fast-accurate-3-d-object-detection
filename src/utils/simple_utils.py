from __future__ import print_function
import numpy as np
import math
import numba
import cv2
import config.apollo_config as cnf

def compute_radius(det_size, min_overlap=0.7):
    height, width = det_size

    a1 = 1
    b1 = (height + width)
    c1 = width * height * (1 - min_overlap) / (1 + min_overlap)
    sq1 = np.sqrt(b1 ** 2 - 4 * a1 * c1)
    r1 = (b1 + sq1) / 2

    a2 = 4
    b2 = 2 * (height + width)
    c2 = (1 - min_overlap) * width * height
    sq2 = np.sqrt(b2 ** 2 - 4 * a2 * c2)
    r2 = (b2 + sq2) / 2

    a3 = 4 * min_overlap
    b3 = -2 * min_overlap * (height + width)
    c3 = (min_overlap - 1) * width * height
    sq3 = np.sqrt(b3 ** 2 - 4 * a3 * c3)
    r3 = (b3 + sq3) / 2

    return min(r1, r2, r3)


def gaussian2D(shape, sigma=1):
    m, n = [(ss - 1.) / 2. for ss in shape]
    y, x = np.ogrid[-m:m + 1, -n:n + 1]
    h = np.exp(-(x * x + y * y) / (2 * sigma * sigma))
    h[h < np.finfo(h.dtype).eps * h.max()] = 0

    return h


def gen_hm_radius(heatmap, center, radius, k=1):
    diameter = 2 * radius + 1
    gaussian = gaussian2D((diameter, diameter), sigma=diameter / 6)

    x, y = int(center[0]), int(center[1])

    height, width = heatmap.shape[0:2]

    left, right = min(x, radius), min(width - x, radius + 1)
    top, bottom = min(y, radius), min(height - y, radius + 1)

    masked_heatmap = heatmap[y - top:y + bottom, x - left:x + right]
    masked_gaussian = gaussian[radius - top:radius + bottom, radius - left:radius + right]
    if min(masked_gaussian.shape) > 0 and min(masked_heatmap.shape) > 0:  # TODO debug
        np.maximum(masked_heatmap, masked_gaussian * k, out=masked_heatmap)

    return heatmap


def get_filtered_lidar(sample_path, lidar, boundary, labels=None, rgb=None):
    minX = boundary['minX']
    maxX = boundary['maxX']
    minY = boundary['minY']
    maxY = boundary['maxY']
    minZ = boundary['minZ']
    maxZ = boundary['maxZ']

    # filter master
    # minX_my = -3.7
    # maxX_my = 0.2
    # minY_my = -0.8
    # maxY_my = 0.8

    # Remove master
    # if 'S2_1' in sample_path:
    #     mask = np.where(((lidar[:, 0] <= minX_my) | (lidar[:, 0] >= maxX_my)) |
    #                     ((lidar[:, 1] <= minY_my) | (lidar[:, 1] >= maxY_my)))
    #     lidar = lidar[mask]

    # for bp lidar
    # lidar[:, 2] = -lidar[:, 2] + 3.35

    # for r32 lidar
    # lidar[:, 2] = lidar[:, 2] + 3.55

    # Remove the point out of range x,y,z
    mask = np.where((lidar[:, 0] >= minX) & (lidar[:, 0] <= maxX) &
                    (lidar[:, 1] >= minY) & (lidar[:, 1] <= maxY) &
                    (lidar[:, 2] >= minZ) & (lidar[:, 2] <= maxZ))
    lidar = lidar[mask]
    lidar[:, 2] = lidar[:, 2] - minZ

    
    if labels is not None:
        label_x = (labels[:, 1] >= minX) & (labels[:, 1] < maxX)
        label_y = (labels[:, 2] >= minY) & (labels[:, 2] < maxY)
        label_z = (labels[:, 3] >= minZ) & (labels[:, 3] < maxZ)
        mask_label = label_x & label_y & label_z
        labels = labels[mask_label]
        if rgb is not None:
            rgb = rgb[mask]        
            return lidar, labels, rgb
        else:
            return lidar, labels, None
    else:
        if rgb is not None:
            rgb = rgb[mask] 
            return lidar, rgb     
        else:
            return lidar, None


def box3d_corners_to_center(box3d_corner):
    # (N, 8, 3) -> (N, 7)
    assert box3d_corner.ndim == 3

    xyz = np.mean(box3d_corner, axis=1)

    h = abs(np.mean(box3d_corner[:, 4:, 2] - box3d_corner[:, :4, 2], axis=1, keepdims=True))
    w = (np.sqrt(np.sum((box3d_corner[:, 0, [0, 1]] - box3d_corner[:, 1, [0, 1]]) ** 2, axis=1, keepdims=True)) +
         np.sqrt(np.sum((box3d_corner[:, 2, [0, 1]] - box3d_corner[:, 3, [0, 1]]) ** 2, axis=1, keepdims=True)) +
         np.sqrt(np.sum((box3d_corner[:, 4, [0, 1]] - box3d_corner[:, 5, [0, 1]]) ** 2, axis=1, keepdims=True)) +
         np.sqrt(np.sum((box3d_corner[:, 6, [0, 1]] - box3d_corner[:, 7, [0, 1]]) ** 2, axis=1, keepdims=True))) / 4

    l = (np.sqrt(np.sum((box3d_corner[:, 0, [0, 1]] - box3d_corner[:, 3, [0, 1]]) ** 2, axis=1, keepdims=True)) +
         np.sqrt(np.sum((box3d_corner[:, 1, [0, 1]] - box3d_corner[:, 2, [0, 1]]) ** 2, axis=1, keepdims=True)) +
         np.sqrt(np.sum((box3d_corner[:, 4, [0, 1]] - box3d_corner[:, 7, [0, 1]]) ** 2, axis=1, keepdims=True)) +
         np.sqrt(np.sum((box3d_corner[:, 5, [0, 1]] - box3d_corner[:, 6, [0, 1]]) ** 2, axis=1, keepdims=True))) / 4

    yaw = (np.arctan2(box3d_corner[:, 2, 1] - box3d_corner[:, 1, 1],
                      box3d_corner[:, 2, 0] - box3d_corner[:, 1, 0]) +
           np.arctan2(box3d_corner[:, 3, 1] - box3d_corner[:, 0, 1],
                      box3d_corner[:, 3, 0] - box3d_corner[:, 0, 0]) +
           np.arctan2(box3d_corner[:, 2, 0] - box3d_corner[:, 3, 0],
                      box3d_corner[:, 3, 1] - box3d_corner[:, 2, 1]) +
           np.arctan2(box3d_corner[:, 1, 0] - box3d_corner[:, 0, 0],
                      box3d_corner[:, 0, 1] - box3d_corner[:, 1, 1]))[:, np.newaxis] / 4

    return np.concatenate([h, w, l, xyz, yaw], axis=1).reshape(-1, 7)


def box3d_center_to_conners(box3d_center):
    h, w, l, x, y, z, yaw = box3d_center
    Box = np.array([[-l / 2, -l / 2, l / 2, l / 2, -l / 2, -l / 2, l / 2, l / 2],
                    [w / 2, -w / 2, -w / 2, w / 2, w / 2, -w / 2, -w / 2, w / 2],
                    [0, 0, 0, 0, h, h, h, h]])

    rotMat = np.array([
        [np.cos(yaw), -np.sin(yaw), 0.0],
        [np.sin(yaw), np.cos(yaw), 0.0],
        [0.0, 0.0, 1.0]])

    velo_box = np.dot(rotMat, Box)
    cornerPosInVelo = velo_box + np.tile(np.array([x, y, z]), (8, 1)).T
    box3d_corner = cornerPosInVelo.transpose()

    return box3d_corner.astype(np.float32)

def get_corners(x, y, w, l, yaw):
    bev_corners = np.zeros((4, 2), dtype=np.float32)
    cos_yaw = np.cos(yaw)
    sin_yaw = np.sin(yaw)
    # front left
    bev_corners[0, 0] = x - w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[0, 1] = y - w / 2 * sin_yaw + l / 2 * cos_yaw

    # rear left
    bev_corners[1, 0] = x - w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[1, 1] = y - w / 2 * sin_yaw - l / 2 * cos_yaw

    # rear right
    bev_corners[2, 0] = x + w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[2, 1] = y + w / 2 * sin_yaw - l / 2 * cos_yaw

    # front right
    bev_corners[3, 0] = x + w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[3, 1] = y + w / 2 * sin_yaw + l / 2 * cos_yaw

    return bev_corners

def drawRotatedBox(img, x, y, w, l, yaw, color, c, rect=None, score=1.0):
    font = cv2.FONT_HERSHEY_SIMPLEX
    bev_corners = get_corners(x, y, w, l, yaw)
    corners_int = bev_corners.reshape(-1, 1, 2).astype(int)

    cv2.polylines(img, [corners_int], True, color, 1)
    corners_int = bev_corners.reshape(-1, 2).astype(int)
    cv2.line(img, (corners_int[0, 0], corners_int[0, 1]), (corners_int[3, 0], corners_int[3, 1]), (255, 255, 0), 1)
    center = (int((corners_int[0, 0]+corners_int[2, 0])/2), int((corners_int[0, 1]+corners_int[2, 1])/2))
    # cv2.putText(img, str(c)[0:-2], (center[0], center[1]), font, 1, (255, 255, 0), 1)
    # cv2.putText(img, str(score)[0:4], (corners_int[0, 0], corners_int[0, 1]), font, 1, (0, 255, 0), 1)
    cv2.putText(img, str(int(c))[0:1], (corners_int[3, 0], corners_int[3, 1]), font, 1, (0, 0, 255), 1)
    if rect is not None:
        pt1_x, pt1_y = int(rect[0]), int(rect[1])
        pt2_x, pt2_y = int(rect[2]), int(rect[3])
        cv2.rectangle(img, (pt1_x, pt1_y), (pt2_x, pt2_y), (255, 255, 0), 1)

def makeBEVMap(PointCloud_, boundary, rgb=None):
    Height = cnf.BEV_HEIGHT + 1
    Width = cnf.BEV_WIDTH + 1

    # Discretize Feature Map
    PointCloud = np.copy(PointCloud_)

    PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / cnf.DISCRETIZATION_Y) + Height / 2)
    PointCloud[:, 1] = np.int_(np.floor(PointCloud[:, 1] / cnf.DISCRETIZATION_X) + Width / 2)

    # sort-3times
    indices = np.lexsort((-PointCloud[:, 2], PointCloud[:, 1], PointCloud[:, 0]))
    PointCloud = PointCloud[indices]
    if rgb is not None:
        rgb = rgb[indices]

    # Height Map
    heightMap = np.zeros((Height, Width))

    _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
    PointCloud_frac = PointCloud[indices]
    # some important problem is image coordinate is (y,x), not (x,y)
    max_height = float(np.abs(boundary['maxZ'] - boundary['minZ']))

    heightMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = PointCloud_frac[:, 2] / max_height

    # Intensity Map & DensityMap
    intensityMap = np.zeros((Height, Width))
    densityMap = np.zeros((Height, Width))

    _, indices, counts = np.unique(PointCloud[:, 0:2], axis=0, return_index=True, return_counts=True)
    PointCloud_top = PointCloud[indices]

    normalizedCounts = np.minimum(1.0, np.log(counts + 1) / np.log(64))

    intensityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = PointCloud_top[:, 3] / 255.0 # hesai40p的反射强度0~255
    densityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = normalizedCounts

    if rgb is not None:
        ImgMap = np.zeros((Height, Width, 3))
        _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
        PointCloud_frac = PointCloud[indices]        
        rgb_frac = rgb[indices].reshape(-1, 3)
        ImgMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = rgb_frac / 255.0
        ImgMap = np.transpose(ImgMap, (2, 0, 1))
        RGB_Map = np.zeros((3, Height - 1, Width - 1))
        RGB_Map[0:3, :, :] = ImgMap[:, :cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  
    else:
        RGB_Map = np.zeros((3, Height - 1, Width - 1))
        RGB_Map[2, :, :] = densityMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # r_map
        RGB_Map[1, :, :] = heightMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # g_map
           

    return RGB_Map

# @numba.jit(nopython=True)
def make_bev_voxel_no_i(PointCloud_, rgb=None):
    minX = -50
    maxX = 50
    minY = -25
    maxY = 25
    minZ = -2.4-3
    maxZ = 0.6+3

    Height = 1216
    Width = 608
    Deep = 10
 
    Deep = 10
    CLOUD_R = PointCloud_.shape[0]
    bev_map = np.zeros((4, 10, Height, Width))
    
    bev_map_per_index = np.zeros((Height, Width), dtype=np.int32)

    # voxel
    voxel_num = np.zeros((10, Height, Width), dtype=np.int32)
    voxel_all_feature = np.zeros((3, 10, Height, Width))
    voxel_ave_feature = np.zeros((4, 10, Height, Width))

    indices = np.argsort(-PointCloud_[:, 2])
    PointCloud_ = PointCloud_[indices]  
    range_list = list(range(CLOUD_R)) 
    random_shuffle_list = range_list

    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]
        z_ori = z + minZ

        row = Height * (x - minX) / (maxX - minX)
        col = Width * (y - minY) / (maxY - minY)
        r = int(math.floor(row))
        c = int(math.floor(col))

        deep = Deep * (z_ori - minZ) / (maxZ - minZ)
        d = int(math.floor(deep))

        if r >= Height or c >= Width or d >= Deep:
            continue
        bev_map_per_index[r, c] = 1

        voxel_num[d, r, c] += 1
        voxel_all_feature[0, d, r, c] += x
        voxel_all_feature[1, d, r, c] += y
        # voxel_all_feature[2, d, r, c] += z
        voxel_all_feature[2, d, r, c] += z_ori

        voxel_ave_feature[0, d, r, c] = voxel_all_feature[0, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[1, d, r, c] = voxel_all_feature[1, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[2, d, r, c] = voxel_all_feature[2, d, r, c] / voxel_num[d, r, c]

        # add density feature
        voxel_ave_feature[3, d, r, c] = np.minimum(1.0, np.log(voxel_num[d, r, c] + 1) / np.log(64))

    voxel_ave_feature = voxel_ave_feature.transpose    
    bev_map = voxel_ave_feature.reshape(-1, Height, Width)


    return bev_map, bev_map_per_index

# @numba.jit(nopython=True)
def make_bev_voxel_relative_height_no_i(PointCloud_, rgb=None):
    minX = -50
    maxX = 50
    minY = -25
    maxY = 25
    minZ = -2.4
    maxZ = 0.6

    Height = 1216
    Width = 608
    Deep = 10  
    CLOUD_R = PointCloud_.shape[0]
    bev_map = np.zeros((4, 10, Height, Width))
    
    bev_map_per_index = np.zeros((Height, Width), dtype=np.int32)

    # voxel
    voxel_num = np.zeros((10, Height, Width), dtype=np.int32)
    voxel_all_feature = np.zeros((3, 10, Height, Width))
    voxel_ave_feature = np.zeros((4, 10, Height, Width))

    # 将点云按照z从高到低排序
    indices = np.argsort(-PointCloud_[:, 2])
    PointCloud_ = PointCloud_[indices]  
    range_list = list(range(CLOUD_R)) 
    random_shuffle_list = range_list

    bev_map_min_z = np.ones((Height, Width), dtype=np.float) * 1000
    # bev_map_max_z = np.ones((Height, Width), dtype=np.float) * (-1000)
    # # Calculate the smallest z for each pillar
    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]

        row = Height * (x - minX) / (maxX - minX)
        col = Width * (y - minY) / (maxY - minY)
        r = int(math.floor(row))
        c = int(math.floor(col))
        if r >= Height or c >= Width:
            continue
        
        if z < bev_map_min_z[r, c]:
            bev_map_min_z[r, c] = z
        # if z > bev_map_max_z[r, c]:
        #     bev_map_max_z[r, c] = z

    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]

        row = Height * (x - minX) / (maxX - minX)
        col = Width * (y - minY) / (maxY - minY)
        r = int(math.floor(row))
        c = int(math.floor(col))

        if r >= Height or c >= Width:
            continue
        if bev_map_min_z[r, c] >= 1000 or z - bev_map_min_z[r, c] > 4.0:
            continue

        z_relative = z - bev_map_min_z[r, c]
        deep = Deep * (z - bev_map_min_z[r, c]) / 4.0
        d = int(math.floor(deep))

        if d >= Deep:
            continue

        bev_map_per_index[r, c] = 1

        voxel_num[d, r, c] += 1
        voxel_all_feature[0, d, r, c] += x
        voxel_all_feature[1, d, r, c] += y
        voxel_all_feature[2, d, r, c] += z_relative

        voxel_ave_feature[0, d, r, c] = voxel_all_feature[0, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[1, d, r, c] = voxel_all_feature[1, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[2, d, r, c] = voxel_all_feature[2, d, r, c] / voxel_num[d, r, c]

        # add density feature
        voxel_ave_feature[3, d, r, c] = np.minimum(1.0, np.log(voxel_num[d, r, c] + 1) / np.log(64))
    
    # 三个维度，可以翻转
    bev_map = voxel_ave_feature.reshape(-1, Height, Width)
    return bev_map, bev_map_per_index 

def map_calculate(metadatas, detections_post, result_1_f, result_2_f, result_3_f, result_4_f, result_5_f, result_6_f):
    for j in range(6):
        if j == 1-1:
            if len(detections_post[0][j] > 0):
                for det in detections_post[0][j]:
                    _score, _x, _y, _z, _h, _w, _l, _yaw = det[0:8]
                    # box_list = bev_to_lidar_coordinate([_x, _y, _z, _h, _w, _l, _yaw]) 
                    # flag_in_image = whether_in_image(box_list)
                    # if flag_in_image == False:
                    #     continue
                    bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                    x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                    result_1_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if j == 2-1:
            if len(detections_post[0][j] > 0):
                for det in detections_post[0][j]:
                    _score, _x, _y, _z, _h, _w, _l, _yaw = det[0:8]
                    # box_list = bev_to_lidar_coordinate([_x, _y, _z, _h, _w, _l, _yaw]) 
                    # flag_in_image = whether_in_image(box_list)
                    # if flag_in_image == False:
                    #     continue
                    bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                    x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                    result_2_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if j == 3-1:
            if len(detections_post[0][j] > 0):
                for det in detections_post[0][j]:
                    _score, _x, _y, _z, _h, _w, _l, _yaw = det[0:8] 
                    # box_list = bev_to_lidar_coordinate([_x, _y, _z, _h, _w, _l, _yaw]) 
                    # flag_in_image = whether_in_image(box_list)
                    # if flag_in_image == False:
                    #     continue                                                  
                    bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                    x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                    result_3_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if j == 4-1:
            if len(detections_post[0][j] > 0):
                for det in detections_post[0][j]:
                    _score, _x, _y, _z, _h, _w, _l, _yaw = det[0:8] 
                    # box_list = bev_to_lidar_coordinate([_x, _y, _z, _h, _w, _l, _yaw]) 
                    # flag_in_image = whether_in_image(box_list)
                    # if flag_in_image == False:
                    #     continue                                                  
                    bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                    x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                    result_4_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if j == 5-1:
            if len(detections_post[0][j] > 0):
                for det in detections_post[0][j]:
                    _score, _x, _y, _z, _h, _w, _l, _yaw = det[0:8]
                    # box_list = bev_to_lidar_coordinate([_x, _y, _z, _h, _w, _l, _yaw]) 
                    # flag_in_image = whether_in_image(box_list)
                    # if flag_in_image == False:
                    #     continue                                                       
                    bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                    x1, y1, x2, y2, x3, y3, x4, y4 = \
                    bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], \
                    bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                    result_5_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')    
        if j == 6-1:
            if len(detections_post[0][j] > 0):
                for det in detections_post[0][j]:
                    _score, _x, _y, _z, _h, _w, _l, _yaw = det[0:8]
                    # box_list = bev_to_lidar_coordinate([_x, _y, _z, _h, _w, _l, _yaw]) 
                    # flag_in_image = whether_in_image(box_list)
                    # if flag_in_image == False:
                    #     continue                                                       
                    bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                    x1, y1, x2, y2, x3, y3, x4, y4 = \
                    bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], \
                    bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                    result_6_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')    

def map_calculate_new_framework(cnf_dict, frame_id, lidar_detections, result_1_f, result_2_f, result_3_f, result_4_f, result_5_f, result_6_f):
    
    for i in range(lidar_detections['pred_boxes'].shape[0]):
        x, y, z, l, w, h, yaw = lidar_detections['pred_boxes'][i].cpu().numpy()
        cls_id = lidar_detections['pred_labels'][i].item()
        score = lidar_detections['pred_scores'][i].item()
        if x < cnf_dict['minX'] or x > cnf_dict['maxX']:
            continue
        if y < cnf_dict['minY'] or y > cnf_dict['maxY']:
            continue
        if z < cnf_dict['minZ'] or z > cnf_dict['maxZ']:
            continue   
        l = l / cnf_dict['bound_size_x'] * cnf_dict['BEV_HEIGHT']
        w = w / cnf_dict['bound_size_y'] * cnf_dict['BEV_WIDTH']
        center_input_y = (x - cnf_dict['minX']) / cnf_dict['bound_size_x'] * cnf_dict['BEV_HEIGHT']  # x --> y (invert to 2D image space)
        center_input_x = (y - cnf_dict['minY']) / cnf_dict['bound_size_y'] * cnf_dict['BEV_WIDTH']  # y --> x
        yaw = -yaw               

        bev_corners = get_corners(center_input_x, center_input_y, w, l, yaw)
        x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
        if cls_id == 1:
            result_1_f.write(frame_id+' '+str(score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if cls_id == 2:
            result_2_f.write(frame_id+' '+str(score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if cls_id == 3:
            result_3_f.write(frame_id+' '+str(score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if cls_id == 4:
            result_4_f.write(frame_id+' '+str(score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if cls_id == 5:
            result_5_f.write(frame_id+' '+str(score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if cls_id == 6:
            result_6_f.write(frame_id+' '+str(score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')    
    return


