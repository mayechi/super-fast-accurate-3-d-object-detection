"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.08.17
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: The utils for evaluation
# Refer from: https://github.com/xingyizhou/CenterNet
"""

from __future__ import division
import sys
from numpy.core.fromnumeric import reshape

import torch
import math
import numpy as np
import torch.nn.functional as F
import cv2
import csv
# import lap
import pdb
import time

sys.path.append('../')
data_from = 'apollo'
if data_from == 'kitti':
    import config.kitti_config as cnf
elif data_from == 'apollo':
    import config.apollo_config as cnf
else:
    import config.simulation_config as cnf
from data_process.kitti_bev_utils import drawRotatedBox
from . import box_utils
from . import eval_clocs
from utils.lidar_to_camera import boxes3d_to_corners3d
from utils.lidar_to_camera import corners3d_to_camera3d
from utils.lidar_to_camera import camera3d_to_img_boxes
from utils.lidar_to_camera import filter_boxes

ID_TYPE_CONVERSION = {
    0: 'Pedestrian',
    1: 'Car',
    2: 'Cyclist'
}
ID_TYPE_CONVERSION_inv = {
    'Pedestrian': 0,
    'Car': 1,
    'Cyclist': 2,
    'Van': 1
}

def _nms(heat, kernel=3):
    pad = (kernel - 1) // 2
    hmax = F.max_pool2d(heat, (kernel, kernel), stride=1, padding=pad)
    keep = (hmax == heat).float()

    return heat * keep


def _gather_feat(feat, ind, mask=None):
    dim = feat.size(2)
    ind = ind.unsqueeze(2).expand(ind.size(0), ind.size(1), dim)
    feat = feat.gather(1, ind)
    if mask is not None:
        mask = mask.unsqueeze(2).expand_as(feat)
        feat = feat[mask]
        feat = feat.view(-1, dim)
    return feat


def _transpose_and_gather_feat(feat, ind):
    feat = feat.permute(0, 2, 3, 1).contiguous()
    feat = feat.view(feat.size(0), -1, feat.size(3))
    feat = _gather_feat(feat, ind)
    return feat


def _topk(scores, K=40):
    batch, cat, height, width = scores.size()

    topk_scores, topk_inds = torch.topk(scores.view(batch, cat, -1), K)

    topk_inds = topk_inds % (height * width)
    topk_ys = (topk_inds / width).int().float()
    topk_xs = (topk_inds % width).int().float()
    # all_scores_0 = scores[:, :, topk_ys[0, 0].long(), topk_xs[0, 0].long()].permute(0, 2, 1)
    # all_scores_1 = scores[:, :, topk_ys[0, 1].long(), topk_xs[0, 1].long()].permute(0, 2, 1)
    # all_scores_2 = scores[:, :, topk_ys[0, 2].long(), topk_xs[0, 2].long()].permute(0, 2, 1)
    # all_scores_012 = torch.cat((all_scores_0, all_scores_1, all_scores_2))
    
    # topk_person_inds = topk_inds[:, 2, :]
    # topk_person_scores = topk_scores[:, 2, :]
    # topk_person_ys = (topk_person_inds / width).int().float().view(batch, K)
    # topk_person_xs = (topk_person_inds % width).int().float().view(batch, K)   



    topk_score, topk_ind = torch.topk(topk_scores.view(batch, -1), K)
    topk_clses = (topk_ind / K).int()
    topk_inds = _gather_feat(topk_inds.view(batch, -1, 1), topk_ind).view(batch, K)
    topk_ys = _gather_feat(topk_ys.view(batch, -1, 1), topk_ind).view(batch, K)
    topk_xs = _gather_feat(topk_xs.view(batch, -1, 1), topk_ind).view(batch, K)

    all_scores = scores[:, :, topk_ys.squeeze().long(), topk_xs.squeeze().long()]

    return topk_score, topk_inds, topk_clses, topk_ys, topk_xs, all_scores


def _topk_channel(scores, K=40):
    batch, cat, height, width = scores.size()

    topk_scores, topk_inds = torch.topk(scores.view(batch, cat, -1), K)

    topk_inds = topk_inds % (height * width)
    topk_ys = (topk_inds / width).int().float()
    topk_xs = (topk_inds % width).int().float()

    return topk_scores, topk_inds, topk_ys, topk_xs


def decode(hm_cen, cen_offset, direction, z_coor, dim, K=40):
    batch_size, num_classes, height, width = hm_cen.size()

    hm_cen = _nms(hm_cen)
    scores, inds, clses, ys, xs, all_scores = _topk(hm_cen, K=K)
    if cen_offset is not None:
        cen_offset = _transpose_and_gather_feat(cen_offset, inds)
        cen_offset = cen_offset.view(batch_size, K, 2)
        xs = xs.view(batch_size, K, 1) + cen_offset[:, :, 0:1]
        ys = ys.view(batch_size, K, 1) + cen_offset[:, :, 1:2]
    else:
        xs = xs.view(batch_size, K, 1) + 0.5
        ys = ys.view(batch_size, K, 1) + 0.5

    direction = _transpose_and_gather_feat(direction, inds)
    direction = direction.view(batch_size, K, 2)
    z_coor = _transpose_and_gather_feat(z_coor, inds)
    z_coor = z_coor.view(batch_size, K, 1)
    dim = _transpose_and_gather_feat(dim, inds)
    dim = dim.view(batch_size, K, 3)
    clses = clses.view(batch_size, K, 1).float()
    scores = scores.view(batch_size, K, 1)
    all_scores = all_scores.permute(0, 2, 1)

    # (scores x 1, ys x 1, xs x 1, z_coor x 1, dim x 3, direction x 2, clses x 1)
    # (scores-0:1, ys-1:2, xs-2:3, z_coor-3:4, dim-4:7, direction-7:9, clses-9:10)
    # detections: [batch_size, K, 10]
    detections = torch.cat([scores, xs, ys, z_coor, dim, direction, clses], dim=2)

    return detections, all_scores

def decode_wl(hm_cen, cen_offset, direction, z_coor, wl, h, K=40):
    batch_size, num_classes, height, width = hm_cen.size()

    hm_cen = _nms(hm_cen)
    scores, inds, clses, ys, xs, all_scores = _topk(hm_cen, K=K)
    if cen_offset is not None:
        cen_offset = _transpose_and_gather_feat(cen_offset, inds)
        cen_offset = cen_offset.view(batch_size, K, 2)
        xs = xs.view(batch_size, K, 1) + cen_offset[:, :, 0:1]
        ys = ys.view(batch_size, K, 1) + cen_offset[:, :, 1:2]
    else:
        xs = xs.view(batch_size, K, 1) + 0.5
        ys = ys.view(batch_size, K, 1) + 0.5

    direction = _transpose_and_gather_feat(direction, inds)
    direction = direction.view(batch_size, K, 2)
    z_coor = _transpose_and_gather_feat(z_coor, inds)
    z_coor = z_coor.view(batch_size, K, 1)
    wl = _transpose_and_gather_feat(wl, inds)
    wl = wl.view(batch_size, K, 2)
    wl = wl * cnf.bound_size_y / cnf.BEV_WIDTH

    h = _transpose_and_gather_feat(h, inds)
    h = h.view(batch_size, K, 1)    

    dim = torch.cat((h, wl), dim=2)
    clses = clses.view(batch_size, K, 1).float()
    scores = scores.view(batch_size, K, 1)
    all_scores = all_scores.permute(0, 2, 1)

    # (scores x 1, ys x 1, xs x 1, z_coor x 1, dim x 3, direction x 2, clses x 1)
    # (scores-0:1, ys-1:2, xs-2:3, z_coor-3:4, dim-4:7, direction-7:9, clses-9:10)
    # detections: [batch_size, K, 10]
    detections = torch.cat([scores, xs, ys, z_coor, dim, direction, clses], dim=2)

    return detections, all_scores

def decode_rotation(hm_cen, cen_offset, wl, z_coor, h, K=40):
    batch_size, num_classes, height, width = hm_cen.size()

    hm_cen = _nms(hm_cen)
    scores, inds, clses, ys, xs, all_scores = _topk(hm_cen, K=K)
    if cen_offset is not None:
        cen_offset = _transpose_and_gather_feat(cen_offset, inds)
        cen_offset = cen_offset.view(batch_size, K, 2)
        xs = xs.view(batch_size, K, 1) + cen_offset[:, :, 0:1]
        ys = ys.view(batch_size, K, 1) + cen_offset[:, :, 1:2]
    else:
        xs = xs.view(batch_size, K, 1) + 0.5
        ys = ys.view(batch_size, K, 1) + 0.5

    wl = _transpose_and_gather_feat(wl, inds)
    wl = wl.view(batch_size, K, 4)
    # add
    # cls_theta = _transpose_and_gather_feat(cls_theta, inds)
    # cls_theta = cls_theta.view(batch_size, K, 1)
    # mask = (cls_theta>0.8).float().view(batch_size, K, 1)
    # mask = (cls_theta>-1).float().view(batch_size, K, 1)
    # tt_x = (xs+wl[..., 0:1])*mask + (xs)*(1.-mask)
    # tt_y = (ys+wl[..., 1:2])*mask + (ys-wl[..., 5:6]/2)*(1.-mask)
    # rr_x = (xs+wl[..., 2:3])*mask + (xs+wl[..., 4:5]/2)*(1.-mask)
    # rr_y = (ys+wl[..., 3:4])*mask + (ys)*(1.-mask)
    tt_x = xs+wl[..., 0:1]
    tt_y = ys+wl[..., 1:2]
    rr_x = xs+wl[..., 2:3]
    rr_y = ys+wl[..., 3:4]
    bb_x = 2*xs - tt_x
    bb_y = 2*ys - tt_y
    ll_x = 2*xs - rr_x
    ll_y = 2*ys - rr_y
    # bb_x = (xs+wh[..., 4:5])*mask + (xs)*(1.-mask)
    # bb_y = (ys+wh[..., 5:6])*mask + (ys+wh[..., 9:10]/2)*(1.-mask)
    # ll_x = (xs+wh[..., 6:7])*mask + (xs-wh[..., 8:9]/2)*(1.-mask)
    # ll_y = (ys+wh[..., 7:8])*mask + (ys)*(1.-mask)    
    tt = torch.cat((tt_x, tt_y), dim=2)
    rr = torch.cat((rr_x, rr_y), dim=2)
    bb = torch.cat((bb_x, bb_y), dim=2)
    ll = torch.cat((ll_x, ll_y), dim=2)

    yaw = torch.atan2((tt[:, :, 0] - bb[:, :, 0]), (tt[:, :, 1] - bb[:, :, 1]))
    yaw = yaw.view(batch_size, K, 1)
    # tl = tt + ll - cen_pt
    # bl = bb + ll - cen_pt
    # tr = tt + rr - cen_pt
    # br = bb + rr - cen_pt
    
    w = F.pairwise_distance(rr.view(-1, 2), ll.view(-1, 2))
    w = w.view(batch_size, K, 1)
    l = F.pairwise_distance(tt.view(-1, 2), bb.view(-1, 2))
    l = l.view(batch_size, K, 1)

    z_coor = _transpose_and_gather_feat(z_coor, inds)
    z_coor = z_coor.view(batch_size, K, 1)
    h = _transpose_and_gather_feat(h, inds)
    h = h.view(batch_size, K, 1)
    dim = torch.cat((h, w, l), dim=2)


    clses = clses.view(batch_size, K, 1).float()
    scores = scores.view(batch_size, K, 1)
    all_scores = all_scores.permute(0, 2, 1)

    # (scores x 1, ys x 1, xs x 1, z_coor x 1, dim x 3, direction x 2, clses x 1)
    # (scores-0:1, ys-1:2, xs-2:3, z_coor-3:4, dim-4:7, direction-7:9, clses-9:10)
    # detections: [batch_size, K, 10]
    detections = torch.cat([scores, xs, ys, z_coor, dim, yaw, clses], dim=2)

    return detections, all_scores

def get_yaw(direction):
    return np.arctan2(direction[:, 0:1], direction[:, 1:2])

def post_processing(detections, num_classes=3, down_ratio=4, peak_thresh=0.2):
    """
    :param detections: [batch_size, K, 10]
    # (scores x 1, xs x 1, ys x 1, z_coor x 1, dim x 3, direction x 2, clses x 1)
    # (scores-0:1, xs-1:2, ys-2:3, z_coor-3:4, dim-4:7, direction-7:9, clses-9:10)
    :return:
    """
    # TODO: Need to consider rescale to the original scale: x, y

    # 去除不同类别下，完全重复的检测结果
    # xyz_dim_list = []
    # for i in range(detections[0].shape[0]):
    #     xyz_dim = detections[0, i, 1:-1].tolist()
    #     if xyz_dim in xyz_dim_list:
    #         detections[0, i, 0] = -2
    #     else:
    #         xyz_dim_list.append(xyz_dim)

    ret = []
    for i in range(detections.shape[0]):
        top_preds = {}
        classes = detections[i, :, -1]
        for j in range(num_classes):
            inds = (classes == j)
            # x, y, z, h, w, l, yaw
            top_preds[j] = np.concatenate([
                detections[i, inds, 0:1],
                detections[i, inds, 1:2] * down_ratio,
                detections[i, inds, 2:3] * down_ratio,
                detections[i, inds, 3:4],
                detections[i, inds, 4:5],
                detections[i, inds, 5:6] / cnf.bound_size_y * cnf.BEV_WIDTH,
                detections[i, inds, 6:7] / cnf.bound_size_x * cnf.BEV_HEIGHT,
                get_yaw(detections[i, inds, 7:9]).astype(np.float32)], axis=1)
            # Filter by peak_thresh
            if len(top_preds[j]) > 0:
                keep_inds = (top_preds[j][:, 0] > peak_thresh)
                top_preds[j] = top_preds[j][keep_inds]
        ret.append(top_preds)

    return ret

def post_processing_all_scores_no_nn(detections, all_scores, num_classes=3, down_ratio=4, peak_thresh=0.2):
    """
    :param detections: [batch_size, K, 10]
    # (scores x 1, xs x 1, ys x 1, z_coor x 1, dim x 3, direction x 2, clses x 1)
    # (scores-0:1, xs-1:2, ys-2:3, z_coor-3:4, dim-4:7, direction-7:9, clses-9:10)
    :return:
    """
    # TODO: Need to consider rescale to the original scale: x, y

    # 去除不同类别下，完全重复的检测结果
    xyz_dim_list = []
    for i in range(detections[0].shape[0]):
        xyz_dim = detections[0, i, 1:-1].tolist()
        if xyz_dim in xyz_dim_list:
            detections[0, i, 0] = -2
        else:
            xyz_dim_list.append(xyz_dim)
    
    ret = []
    for i in range(detections.shape[0]):
        top_preds = {}
        classes = detections[i, :, -1]
        for j in range(num_classes):
            inds = (classes == j)
            # x, y, z, h, w, l, yaw
            top_preds[j] = np.concatenate([
                detections[i, inds, 0:1],
                detections[i, inds, 1:2] * down_ratio,
                detections[i, inds, 2:3] * down_ratio,
                detections[i, inds, 3:4],
                detections[i, inds, 4:5],
                detections[i, inds, 5:6] / cnf.bound_size_y * cnf.BEV_WIDTH,
                detections[i, inds, 6:7] / cnf.bound_size_x * cnf.BEV_HEIGHT,
                get_yaw(detections[i, inds, 7:9]).astype(np.float32), 
                all_scores[inds, :]], axis=1)
            # Filter by peak_thresh
            if len(top_preds[j]) > 0:
                keep_inds = (top_preds[j][:, 0] > peak_thresh)
                top_preds[j] = top_preds[j][keep_inds]
        ret.append(top_preds)

    return ret

def post_processing_all_scores(detections, all_scores, num_classes=3, down_ratio=4, peak_thresh=0.2):
    """
    :param detections: [batch_size, K, 10]
    # (scores x 1, xs x 1, ys x 1, z_coor x 1, dim x 3, direction x 2, clses x 1)
    # (scores-0:1, xs-1:2, ys-2:3, z_coor-3:4, dim-4:7, direction-7:9, clses-9:10)
    :return:
    """
    # TODO: Need to consider rescale to the original scale: x, y

    # 去除不同类别下，完全重复的检测结果
    # xyz_dim_list = []
    # for i in range(detections[0].shape[0]):
    #     xyz_dim = detections[0, i, 1:-1].tolist()
    #     if xyz_dim in xyz_dim_list:
    #         detections[0, i, 0] = -2
    #     else:
    #         xyz_dim_list.append(xyz_dim)
    
    ret = []
    for i in range(detections.shape[0]):
        top_preds = {}
        classes = detections[i, :, -1]
        for j in range(num_classes):
            inds = (classes == j)
            j = str(j)
            # x, y, z, h, w, l, yaw
            top_preds[j] = np.concatenate([
                detections[i, inds, 0:1],
                detections[i, inds, 1:2] * down_ratio,
                detections[i, inds, 2:3] * down_ratio,
                detections[i, inds, 3:4],
                detections[i, inds, 4:5],
                detections[i, inds, 5:6] / cnf.bound_size_y * cnf.BEV_WIDTH,
                detections[i, inds, 6:7] / cnf.bound_size_x * cnf.BEV_HEIGHT,
                get_yaw(detections[i, inds, 7:9]).astype(np.float32), 
                all_scores[inds, :]], axis=1)
            # Filter by peak_thresh
            if len(top_preds[j]) > 0:
                keep_inds = (top_preds[j][:, 0] > peak_thresh)
                top_preds[j] = top_preds[j][keep_inds]
        ret.append(top_preds)

    return ret

def post_processing_rotation(detections, num_classes=3, down_ratio=4, peak_thresh=0.2):
    """
    :param detections: [batch_size, K, 10]
    # (scores x 1, xs x 1, ys x 1, z_coor x 1, dim x 3, direction x 2, clses x 1)
    # (scores-0:1, xs-1:2, ys-2:3, z_coor-3:4, dim-4:7, direction-7:9, clses-9:10)
    :return:
    """
    # TODO: Need to consider rescale to the original scale: x, y

    ret = []
    for i in range(detections.shape[0]):
        top_preds = {}
        classes = detections[i, :, -1]
        for j in range(num_classes):
            inds = (classes == j)
            # x, y, z, h, w, l, yaw
            top_preds[j] = np.concatenate([
                detections[i, inds, 0:1],
                detections[i, inds, 1:2] * down_ratio, # x
                detections[i, inds, 2:3] * down_ratio, # y
                detections[i, inds, 3:4], # z
                detections[i, inds, 4:5], # h
                detections[i, inds, 5:6] * down_ratio, # w
                detections[i, inds, 6:7] * down_ratio, # l
                detections[i, inds, 7:8],], axis=1) # yaw
                # get_yaw(detections[i, inds, 7:9]).astype(np.float32)], axis=1)
            # Filter by peak_thresh
            if len(top_preds[j]) > 0:
                keep_inds = (top_preds[j][:, 0] > peak_thresh)
                top_preds[j] = top_preds[j][keep_inds]
        ret.append(top_preds)

    return ret

def draw_predictions(img, detections, num_classes=3):
    for j in range(num_classes):
        # if j not in [2]:
        #     continue
        if len(detections[j] > 0):
            for det in detections[j]:
                # (scores-0:1, x-1:2, y-2:3, z-3:4, dim-4:7, yaw-7:8)
                _score, _x, _y, _z, _h, _w, _l, _yaw = det
                drawRotatedBox(img, _x, _y, _w, _l, _yaw, cnf.colors[int(j)], j, None, _score)

    return img

def draw_predictions_fusion(img, detections, num_classes=3):
    for j in range(num_classes):
        if len(detections[j] > 0):
            for det in detections[j]:
                # (scores-0:1, x-1:2, y-2:3, z-3:4, dim-4:7, yaw-7:8)
                _score, _x, _y, _z, _h, _w, _l, _yaw = det[:8]
                drawRotatedBox(img, _x, _y, _w, _l, _yaw, cnf.colors[int(j)], j, None, _score)

    return img

def convert_det_to_real_values(detections, num_classes=3):
    kitti_dets = []
    for cls_id in range(num_classes):
        if len(detections[cls_id] > 0):
            for det in detections[cls_id]:
                # (scores-0:1, x-1:2, y-2:3, z-3:4, dim-4:7, yaw-7:8)
                _score, _x, _y, _z, _h, _w, _l, _yaw = det
                _yaw = -_yaw
                x = _y / cnf.BEV_HEIGHT * cnf.bound_size_x + cnf.boundary['minX']
                y = _x / cnf.BEV_WIDTH * cnf.bound_size_y + cnf.boundary['minY']
                z = _z + cnf.boundary['minZ']
                w = _w / cnf.BEV_WIDTH * cnf.bound_size_y
                l = _l / cnf.BEV_HEIGHT * cnf.bound_size_x

                # kitti_dets.append([cls_id, x, y, z, _h, w, l, _yaw])
                kitti_dets.append([cls_id, x, y, z, _h, w, l, _yaw, _score])

    return np.array(kitti_dets)

def generate_kitti_3d_detection(prediction, predict_txt):
    with open(predict_txt, 'w', newline='') as f:
        w = csv.writer(f, delimiter=' ', lineterminator='\n')
        if len(prediction) == 0:
            w.writerow([])
        else:
            for p in prediction:
                p = p.numpy()
                p = p.round(4)
                type = ID_TYPE_CONVERSION[int(p[0])]
                row = [type, 0, 0] + p[1:].tolist()
                w.writerow(row)

    check_last_line_break(predict_txt)

def check_last_line_break(predict_txt):
    f = open(predict_txt, 'rb+')
    try:
        f.seek(-1, os.SEEK_END)
    except:
        pass
    else:
        if f.__next__() == b'\n':
            f.seek(-1, os.SEEK_END)
            f.truncate()
    f.close()

def compute_iou(rec1, rec2):
    """
    computing IoU
    :param rec1: (y0, x0, y1, x1), which reflects
            (top, left, bottom, right)
    :param rec2: (y0, x0, y1, x1)
    :return: scala value of IoU
    """
    # computing area of each rectangles
    S_rec1 = (rec1[2] - rec1[0]) * (rec1[3] - rec1[1])
    S_rec2 = (rec2[2] - rec2[0]) * (rec2[3] - rec2[1])
 
    # computing the sum_area
    sum_area = S_rec1 + S_rec2
 
    # find the each edge of intersect rectangle
    left_line = max(rec1[1], rec2[1])
    right_line = min(rec1[3], rec2[3])
    top_line = max(rec1[0], rec2[0])
    bottom_line = min(rec1[2], rec2[2])
 
    # judge if there is an intersect
    if left_line >= right_line or top_line >= bottom_line:
        return 0
    else:
        intersect = (right_line - left_line) * (bottom_line - top_line)
        return (intersect / (sum_area - intersect))*1.0
    

def convert_det_to_kitti_format(image_id, detections, cnf, calib, image_shape, num_classes=3, label=None):

    def get_template_prediction(num_samples):
        ret_dict = {
            'name': np.zeros(num_samples), 'truncated': np.zeros(num_samples),
            'occluded': np.zeros(num_samples), 'alpha': np.zeros(num_samples),
            'bbox': np.zeros([num_samples, 4]), 'dimensions': np.zeros([num_samples, 3]),
            'location': np.zeros([num_samples, 3]), 'rotation_y': np.zeros(num_samples),
            'score': np.zeros(num_samples), 'boxes_lidar': np.zeros([num_samples, 7])
        }
        return ret_dict

    pred_txt = './result/kitti/val/data/' + str(image_id) + '.txt'
    # class_names_list = ['Car', 'Pedestrian', 'Cyclist']
    kitti_dets = []
    pred_scores_list = []
    pred_boxes_list = []
    pred_labels_list = []
    for cls_id in range(num_classes):
        # if len(detections[cls_id] > 0):
        if detections[cls_id].shape[0] > 0:    
            for det in detections[cls_id]:
                # (scores-0:1, x-1:2, y-2:3, z-3:4, dim-4:7, yaw-7:8)
                _score, _x, _y, _z, _h, _w, _l, _yaw = det
                '''*********别忘记************'''
                # ori
                _yaw = -_yaw
                # rotation
                # _yaw = _yaw

                x = _y / cnf.BEV_HEIGHT * cnf.bound_size_x + cnf.boundary['minX']
                y = _x / cnf.BEV_WIDTH * cnf.bound_size_y + cnf.boundary['minY']
                z = _z + cnf.boundary['minZ']
                # z = 0
                # _h = 300
                w = _w / cnf.BEV_WIDTH * cnf.bound_size_y
                l = _l / cnf.BEV_HEIGHT * cnf.bound_size_x
                pred_scores_list.append([_score])
                pred_boxes_list.append([x, y, z, _h, w, l, _yaw])
                pred_labels_list.append([cls_id])
                kitti_dets.append([cls_id, x, y, z, _h, w, l, _yaw, _score])

    # write kitti axis det result
    # write_det(pred_txt, kitti_dets)
    # return 

    pred_scores = np.array(pred_scores_list)
    pred_boxes = np.array(pred_boxes_list)
    pred_labels = np.array(pred_labels_list)
    pred_dict = get_template_prediction(pred_scores.shape[0])
    if pred_scores.shape[0] == 0:
        with open(pred_txt, 'w') as f_temp:
            f_temp.close()
        return pred_dict
    pred_boxes_camera = box_utils.boxes3d_lidar_to_kitti_camera(pred_boxes, calib)
    pred_boxes_img = box_utils.boxes3d_kitti_camera_to_imageboxes(
        pred_boxes_camera, calib, image_shape=image_shape
    )
    # pred_boxes[:, 3] = 300
    pred_dict['name'] = pred_labels
    pred_dict['alpha'] = (-np.arctan2(-pred_boxes[:, 1], pred_boxes[:, 0]) + pred_boxes_camera[:, 6]).reshape(-1, 1)
    pred_dict['bbox'] = pred_boxes_img
    pred_dict['dimensions'] = pred_boxes[:, 3:6]
    pred_dict['location'] = pred_boxes_camera[:, 0:3]
    pred_dict['rotation_y'] = pred_boxes_camera[:, 6].reshape(-1, 1)
    pred_dict['score'] = pred_scores
    pred_dict['boxes_lidar'] = pred_boxes

    # label = None
    has_score = False
    if label is not None:
        rect_1_list = list()
        cls_1_list = list()
        if has_score:
            score_1_list = list()
        with open(label, 'r') as ann:
            for ann_ind, txt in enumerate(ann):
                tmp = txt[:-1].split(' ')
                cls_id = tmp[0]
                if cls_id in ID_TYPE_CONVERSION_inv:
                    cls_id = ID_TYPE_CONVERSION_inv[cls_id]
                else:
                    continue
                bbox = [float(tmp[4]), float(tmp[5]), float(tmp[6]), float(tmp[7])]
                rect_1 = (bbox[1], bbox[0], bbox[3], bbox[2])
                if has_score:
                    score_1 = float(tmp[-1])
                    score_1_list.append(score_1)
                rect_1_list.append(rect_1)
                cls_1_list.append(cls_id)
        ann_num = len(rect_1_list)

        if ann_num == 0 or pred_boxes_img.shape[0] == 0:
            return
        score_mat = np.zeros([ann_num, pred_boxes_img.shape[0]])

        for i in range(ann_num):
            rect_1 = rect_1_list[i]
            cls_id_1 = cls_1_list[i]
            for j in range(pred_boxes_img.shape[0]):
                rect_2 = (pred_boxes_img[j, 1], pred_boxes_img[j, 0], pred_boxes_img[j, 3], pred_boxes_img[j, 2]) 
                cls_id_2 = pred_dict['name'][j][0]
                score = compute_iou(rect_1, rect_2)
                if cls_id_1 == cls_id_2:
                    score_mat[i, j] = score
        cost, x, y = lap.lapjv(1-score_mat, extend_cost=True, cost_limit=1-0.3)
        new_3d = []
        result = []      
        for i, j in enumerate(y):
            if j == -1:
                new_3d.append(i)  
            else:
                result.append([j, i, 1.0])
        for i in range(pred_boxes_img.shape[0]):
            flag_match = 0
            for j in range(len(result)):
                row, col, _ = result[j]
                if i == col:
                    iou_score = score_mat[row, col]
                    flag_match = 1
                    if iou_score > 0.7:
                        if has_score:
                            score_1 = score_1_list[row]
                        else:
                            score_1 = 1.0
                        pred_dict['score'][col][0] = min(pred_dict['score'][col][0], score_1)  
                        # pred_dict['score'][col][0] = max(pred_dict['score'][col][0], score_1)     
            if flag_match == 0:
                # if pred_dict['score'][i][0] > 0.9:
                #     img_path = label.replace('label_2', 'image_2').replace('.txt', '.png')
                #     img = cv2.imread(img_path)
                #     bbox = pred_boxes_img[i]
                #     cv2.rectangle(img, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), (0, 255, 255), 2)
                #     for rect_1 in rect_1_list:
                #         bbox = (rect_1[1], rect_1[0], rect_1[3], rect_1[2])
                #         cv2.rectangle(img, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), (0, 0, 255), 2)

                #     cv2.imshow('img', img)
                #     cv2.waitKey(0)
                #     pdb.set_trace()
                #     print(pred_dict['score'][i])
                #     print(pred_dict['name'][i][0])
                # if pred_dict['score'][i][0] > 0.5: 
                #     a = 0
                pred_dict['score'][i][0] = 0        

    pre_tuple = (pred_dict['name'], pred_dict['alpha'], pred_dict['bbox'], pred_dict['dimensions'], pred_dict['location'], pred_dict['rotation_y'], pred_dict['score'])
    pre_tensor = torch.from_numpy(np.hstack(pre_tuple))
    generate_kitti_3d_detection(pre_tensor, pred_txt)

def write_det(pred_txt, kitti_dets):
    #  kitti_dets.append([cls_id, x, y, z, _h, w, l, _yaw, _score])
    # write_path = self.wirte_dir + '{:06d}.txt'.format(sample_id)
    with open(pred_txt, 'w') as f:
        for i in range(len(kitti_dets)):
            cat_id = int(kitti_dets[i][0])
            if cat_id not in cnf.CLASS_ID_TO_NAME:
                continue
            cat = cnf.CLASS_ID_TO_NAME[cat_id]
            z, x, y = str(labels[i, 1]), str(-labels[i, 2]), str(-labels[i, 3])
            h, w, l = str(kitti_dets[i][4]), str(kitti_dets[i][5]), str(kitti_dets[i][6])
            ry = str(kitti_dets[i][7])
            score = str(kitti_dets[i][8])
            f.write(cat+' 0 0 0 '+'0'+' '+'0'+' '+'0'+' '+'0'+' '+h+' '+w+' '+l+' '+x+' '+y+' '+z+' '+ry+' '+score+'\n')
    f.close()


# def generate_fusion_input(img_idx, configs, detections, top_predictions, cnf, calib, image_shape):
def generate_fusion_input(img_idx, cnf, calib, image_shape, num_classes, detections, top_predictions, labels, img_rgb):
    def get_template_prediction(num_samples):
        ret_dict = {
            'name': np.zeros(num_samples), 'truncated': np.zeros(num_samples),
            'occluded': np.zeros(num_samples), 'alpha': np.zeros(num_samples),
            'bbox': np.zeros([num_samples, 4]), 'dimensions': np.zeros([num_samples, 3]),
            'location': np.zeros([num_samples, 3]), 'rotation_y': np.zeros(num_samples),
            'score': np.zeros(num_samples), 'boxes_lidar': np.zeros([num_samples, 7])
        }
        return ret_dict    

    kitti_dets = []
    pred_scores_list = []
    all_scores_list = []
    pred_boxes_list = []
    pred_labels_list = []
    for cls_id in range(num_classes):
        # if len(detections[cls_id] > 0):
        if detections[cls_id].shape[0] > 0:    
            for det in detections[cls_id]:
                # (scores-0:1, x-1:2, y-2:3, z-3:4, dim-4:7, yaw-7:8)
                _score, _x, _y, _z, _h, _w, _l, _yaw, car_score, p_score, cyc_score = det
                _yaw = -_yaw
                x = _y / cnf.BEV_HEIGHT * cnf.bound_size_x + cnf.boundary['minX']
                y = _x / cnf.BEV_WIDTH * cnf.bound_size_y + cnf.boundary['minY']
                z = _z + cnf.boundary['minZ']
                w = _w / cnf.BEV_WIDTH * cnf.bound_size_y
                l = _l / cnf.BEV_HEIGHT * cnf.bound_size_x
                pred_scores_list.append([_score])
                all_scores_list.append([car_score, p_score, cyc_score])
                pred_boxes_list.append([x, y, z, _h, w, l, _yaw])
                pred_labels_list.append([cls_id])
                kitti_dets.append([1, x, y, z, _h, w, l, _yaw, _score])

    pred_scores = np.array(pred_scores_list)
    all_scores= np.array(all_scores_list)
    pred_boxes = np.array(pred_boxes_list)
    pred_labels = np.array(pred_labels_list)
    pred_dict = get_template_prediction(pred_scores.shape[0])
    # if pred_scores.shape[0] == 0:
    #     return
    pred_boxes_camera = box_utils.boxes3d_lidar_to_kitti_camera(pred_boxes, calib)
    labels_boxes_camera = box_utils.boxes3d_lidar_to_kitti_camera(labels[:, :, 1:].reshape(-1, 7), calib)
    pred_boxes_img = box_utils.boxes3d_kitti_camera_to_imageboxes(
        pred_boxes_camera, calib, image_shape=image_shape
    )
    # Show kitt 3D result to image 2D
    img_rgb_show = img_rgb.copy()
    mask_2d = (pred_scores > 0.3).reshape(-1)
    pred_mask_boxes = pred_boxes_img[mask_2d]
    for i in range(pred_mask_boxes.shape[0]):
        bbox = pred_mask_boxes[i]
        cv2.rectangle(img_rgb_show, (bbox[0], bbox[1]), (bbox[2], bbox[3]), (0, 0, 255), 2)
    cv2.imshow('show', img_rgb_show)
    keyboard = cv2.waitKey(0)
    return None, None, None, None, None, None, None

    pred_dict['name'] = pred_labels
    pred_dict['alpha'] = (-np.arctan2(-pred_boxes[:, 1], pred_boxes[:, 0]) + pred_boxes_camera[:, 6]).reshape(-1, 1)
    pred_dict['bbox'] = pred_boxes_img
    pred_dict['dimensions'] = pred_boxes[:, 3:6]
    pred_dict['location'] = pred_boxes_camera[:, 0:3]
    pred_dict['rotation_y'] = pred_boxes_camera[:, 6].reshape(-1, 1)
    pred_dict['score'] = pred_scores
    pred_dict['boxes_lidar'] = pred_boxes

    kitti_res_tuple = (pred_dict['name'], pred_dict['alpha'], pred_dict['bbox'], pred_dict['dimensions'], pred_dict['location'], pred_dict['rotation_y'], pred_dict['score'])
    kitti_res_tensor = torch.from_numpy(np.hstack(kitti_res_tuple))

    # predictions and labels
    predictions_dicts, labels_dicts = [], []
    predictions_dict = {
        "kitti_res": kitti_res_tensor,
        "bbox": pred_boxes_img,
        "box3d_camera": pred_boxes_camera.reshape(-1, 7),
        "box3d_lidar": pred_boxes,
        "scores": pred_scores.reshape(-1, 1),
        "label_preds": pred_labels,
        "image_idx": img_idx,
    }
    labels_dict = {
        "lables_lidar": labels[:, :, 1:].numpy().reshape(-1, 7),
        "labels_camera": labels_boxes_camera,
        "labels_cls":labels[:, :, 0].numpy().astype(np.int).reshape(-1, 1),
    }
    predictions_dicts.append(predictions_dict)
    labels_dicts.append(labels_dict)
    # dis_to_lidar = torch.norm(pred_boxes[:,:2],p=2,dim=1,keepdim=True)/82.0
    dis_to_lidar = np.linalg.norm(pred_boxes[:, :2],ord=2,axis=1,keepdims=True)/82.0
    box_2d_detector = np.zeros((200, 4))
    box_2d_detector[0:top_predictions.shape[0], :] = top_predictions[:, 1:5]
    box_2d_detector = top_predictions[:, 1:5]
    box_2d_scores = top_predictions[:, 5].reshape(-1, 1)
    box_2d_all_scores = top_predictions[:, 6:].reshape(-1, 3)
    box_2d_labels = top_predictions[:, 0].astype(np.int).reshape(-1, 1)

    time_iou_build_start = time.time()
    # overlaps1 = np.zeros((900000, 4), dtype=pred_boxes_img.dtype)
    # overlaps1 = np.zeros((900000, 3, 4), dtype=pred_boxes_img.dtype)
    overlaps1 = np.zeros((900000, 3, 3), dtype=pred_boxes_img.dtype)
    # overlaps1 = np.zeros((900000, 5), dtype=pred_boxes_img.dtype)
    # overlaps1 = np.zeros((900000, 8), dtype=pred_boxes_img.dtype)
    tensor_index1 = np.zeros((900000, 2), dtype=pred_boxes_img.dtype)
    tensor_iou = np.zeros((900000, 1), dtype=pred_boxes_img.dtype)
    overlaps1[:, :] = -1
    tensor_index1[:, :] = -1
    iou_test, tensor_index, tensor_iou, max_num = eval_clocs.build_stage2_training(pred_boxes_img,
                                                                                box_2d_detector,
                                                                                -1,
                                                                                pred_scores.reshape(-1, 1),
                                                                                all_scores.reshape(-1, 3),
                                                                                box_2d_scores,
                                                                                box_2d_all_scores,
                                                                                pred_labels,
                                                                                box_2d_labels,
                                                                                dis_to_lidar,                                        
                                                                                overlaps1,
                                                                                tensor_index1,
                                                                                tensor_iou)

    # overlaps2 = np.zeros((900000, 2, 3), dtype=pred_boxes_img.dtype)
    # tensor_index2 = np.zeros((900000, 2), dtype=pred_boxes_img.dtype)
    # overlaps2[:, :] = -1
    # tensor_index2[:, :] = -1
    # iou_test, tensor_index, max_num = eval_clocs.build_stage2_training_new(pred_boxes_img,
    #                                     box_2d_detector,
    #                                     -1,
    #                                     pred_scores.reshape(-1, 1),
    #                                     box_2d_scores,
    #                                     pred_labels,
    #                                     box_2d_labels,
    #                                     sparseness_3d,
    #                                     sparseness_2d                                        
    #                                     overlaps2,
    #                                     tensor_index2)
    iou_test_tensor = torch.FloatTensor(iou_test)  #iou_test_tensor shape: [160000,4]
    tensor_index_tensor = torch.LongTensor(tensor_index)
    tensor_iou_tensor = torch.FloatTensor(tensor_iou)
    # iou_test_tensor = iou_test_tensor.permute(1,0)
    iou_test_tensor = iou_test_tensor.permute(1,2,0)
    # iou_test_tensor = iou_test_tensor.reshape(1,4,1,900000)
    # iou_test_tensor = iou_test_tensor.reshape(3,4,1,900000)
    iou_test_tensor = iou_test_tensor.reshape(3,3,1,900000)
    # iou_test_tensor = iou_test_tensor.reshape(1,5,1,900000)
    # iou_test_tensor = iou_test_tensor.reshape(1,8,1,900000)
    tensor_index_tensor = tensor_index_tensor.reshape(-1,2)
    tensor_iou_tensor = tensor_iou_tensor.reshape(-1,1)
    if max_num == 0:
        # non_empty_iou_test_tensor = torch.zeros(1,4,1,2)
        # non_empty_iou_test_tensor = torch.zeros(3,4,1,2)
        non_empty_iou_test_tensor = torch.zeros(3,3,1,2)
        # non_empty_iou_test_tensor = torch.zeros(1,5,1,2)
        # non_empty_iou_test_tensor = torch.zeros(1,8,1,2)
        non_empty_iou_test_tensor[:,:,:,:] = -1
        non_empty_tensor_index_tensor = torch.zeros(2,2)
        non_empty_tensor_index_tensor[:,:] = -1
        non_empty_tensor_iou_tensor = torch.zeros(1,2)      
    else:
        non_empty_iou_test_tensor = iou_test_tensor[:,:,:,:max_num]
        non_empty_tensor_index_tensor = tensor_index_tensor[:max_num,:]
        non_empty_tensor_iou_tensor = tensor_iou_tensor[:max_num,:]
    res, iou_test, tensor_index, tensor_iou = predictions_dicts, non_empty_iou_test_tensor, non_empty_tensor_index_tensor, non_empty_tensor_iou_tensor     

    return res, labels_dicts, detections, top_predictions, iou_test, tensor_index, tensor_iou

def generate_fusion_input_apollo_no_nn(img_idx, cnf, num_classes, detections, top_predictions, labels, labels_bev, img_rgb=None, bev_map=None):
    def get_template_prediction(num_samples):
        ret_dict = {
            'name': np.zeros(num_samples), 'truncated': np.zeros(num_samples),
            'occluded': np.zeros(num_samples), 'alpha': np.zeros(num_samples),
            'bbox': np.zeros([num_samples, 4]), 'dimensions': np.zeros([num_samples, 3]),
            'location': np.zeros([num_samples, 3]), 'rotation_y': np.zeros(num_samples),
            'score': np.zeros(num_samples), 'boxes_lidar': np.zeros([num_samples, 7])
        }
        return ret_dict    

    kitti_dets = []
    pred_scores_list = []
    all_scores_list = []
    pred_boxes_list = []
    pred_clses_list = []
    pred_bev_boxes_list = []
    for cls_id in range(num_classes):
        # if len(detections[cls_id] > 0):
        if detections[cls_id].shape[0] > 0:    
            for det in detections[cls_id]:
                # (scores-0:1, x-1:2, y-2:3, z-3:4, dim-4:7, yaw-7:8)
                _score, _x, _y, _z, _h, _w, _l, _yaw, \
                score_1, score_2, score_3, score_4, score_5, score_6 = det
                # bev image result
                pred_bev_boxes_list.append([_x, _y, _w, _l, _yaw])
                _yaw = -_yaw
                x = _y / cnf.BEV_HEIGHT * cnf.bound_size_x + cnf.boundary['minX']
                y = _x / cnf.BEV_WIDTH * cnf.bound_size_y + cnf.boundary['minY']
                z = _z + cnf.boundary['minZ']
                w = _w / cnf.BEV_WIDTH * cnf.bound_size_y
                l = _l / cnf.BEV_HEIGHT * cnf.bound_size_x
                pred_scores_list.append([_score])
                all_scores_list.append([score_1, score_2, score_3, score_4, score_5, score_6])
                pred_boxes_list.append([x, y, z, l, w, _h, _yaw])
                pred_clses_list.append([cls_id])
                kitti_dets.append([1, x, y, z, _h, w, l, _yaw, _score])

    # debug
    # pred_scores_list.append([1.0])
    # pred_boxes_list.append([6.8, -9.9, -1.75, 0.46, 0.38, 0.71, -2.14484])
    # pred_clses_list.append([4])


    pred_scores = np.array(pred_scores_list)
    all_scores= np.array(all_scores_list)
    pred_boxes = np.array(pred_boxes_list)
    pred_clses = np.array(pred_clses_list)
    pred_bev_boxes = np.array(pred_bev_boxes_list)
    # pred_dict = get_template_prediction(pred_scores.shape[0])

    # Stay boxes only in the image coordinate
    used_index = filter_boxes(pred_boxes)
    pred_scores = pred_scores[used_index]
    all_scores = all_scores[used_index]
    pred_boxes = pred_boxes[used_index]
    pred_clses = pred_clses[used_index]
    pred_bev_boxes = pred_bev_boxes[used_index]

    # Lidar 3D to Image 2d or whether show or not
    pred_boxes_img = trans_zs(pred_boxes, pred_scores, None, img_rgb, bev_map)
    # pred_boxes_img = trans_zs(pred_boxes, pred_scores, top_predictions[:, 1:6], img_rgb, bev_map)
    # return None, None, None, None, None, None, None, None
    # pred_boxes_img = trans_zs(pred_boxes, pred_scores, top_predictions[:, 1:6], None, None)
    
    # dis_to_lidar = torch.norm(pred_boxes[:,:2],p=2,dim=1,keepdim=True)/82.0
    dis_to_lidar = np.linalg.norm(pred_boxes[:, :2],ord=2,axis=1,keepdims=True)/82.0
    box_2d_detector = np.zeros((200, 4))
    box_2d_detector[0:top_predictions.shape[0], :] = top_predictions[:, 1:5]
    box_2d_detector = top_predictions[:, 1:5]
    box_2d_scores = top_predictions[:, 5].reshape(-1, 1)
    box_2d_all_scores = top_predictions[:, 6:].reshape(-1, 6)
    box_2d_labels = top_predictions[:, 0].astype(np.int).reshape(-1, 1)
    valid_num = torch.tensor((box_2d_detector.shape[0], pred_boxes_img.shape[0]))

    overlaps1 = np.zeros((900000, 6, 3), dtype=pred_boxes_img.dtype)
    tensor_index1 = np.zeros((900000, 2), dtype=pred_boxes_img.dtype)
    tensor_iou = np.zeros((900000, 1), dtype=pred_boxes_img.dtype)
    overlaps1[:, :] = -1
    tensor_index1[:, :] = -1
    # criterion=-1 交集/并集， criterion=1 交集/2D，criterion=0 交集/3D 
    iou_test, tensor_index, tensor_iou, iou_mat, iou_hide_mat, max_num = eval_clocs.build_stage2_training_apollo(pred_boxes_img,
                                                                                          box_2d_detector,
                                                                                          -1,
                                                                                          pred_scores.reshape(-1, 1),
                                                                                          all_scores.reshape(-1, 6),
                                                                                          box_2d_scores,
                                                                                          box_2d_all_scores,
                                                                                          pred_clses,
                                                                                          box_2d_labels,
                                                                                          dis_to_lidar,                                        
                                                                                          overlaps1,
                                                                                          tensor_index1,
                                                                                          tensor_iou)
    # 计算 3d 和 2d 的基于矩阵最大数的一对一匹配
    # My method for match
    match_list = []                
    while (iou_mat == 0).all() == False:
        temp, temp_score = match_find_max(iou_mat)
        for i in range(len(temp)):
            if temp[i] != -1:
                iou_mat[i, :] = 0
                iou_mat[:, temp[i]] = 0
                if temp_score[i] > 0.2:
                    match_list.append([i, temp[i], temp_score[i]])

    iou_test_tensor = torch.FloatTensor(iou_test)  #iou_test_tensor shape: [160000,4]
    tensor_index_tensor = torch.LongTensor(tensor_index)
    tensor_iou_tensor = torch.FloatTensor(tensor_iou)
    # iou_test_tensor = iou_test_tensor.permute(1,0)
    iou_test_tensor = iou_test_tensor.permute(1,2,0)
    # iou_test_tensor = iou_test_tensor.reshape(1,4,1,900000)
    # iou_test_tensor = iou_test_tensor.reshape(3,4,1,900000)
    iou_test_tensor = iou_test_tensor.reshape(6,3,1,900000)
    # iou_test_tensor = iou_test_tensor.reshape(1,5,1,900000)
    # iou_test_tensor = iou_test_tensor.reshape(1,8,1,900000)
    tensor_index_tensor = tensor_index_tensor.reshape(-1,2)
    tensor_iou_tensor = tensor_iou_tensor.reshape(-1,1)
    if max_num == 0:
        # non_empty_iou_test_tensor = torch.zeros(1,4,1,2)
        # non_empty_iou_test_tensor = torch.zeros(3,4,1,2)
        non_empty_iou_test_tensor = torch.zeros(6,3,1,2)
        # non_empty_iou_test_tensor = torch.zeros(1,5,1,2)
        # non_empty_iou_test_tensor = torch.zeros(1,8,1,2)
        non_empty_iou_test_tensor[:,:,:,:] = -1
        non_empty_tensor_index_tensor = torch.zeros(2,2)
        non_empty_tensor_index_tensor[:,:] = -1
        non_empty_tensor_iou_tensor = torch.zeros(1,2)      
    else:
        non_empty_iou_test_tensor = iou_test_tensor[:,:,:,:max_num]
        non_empty_tensor_index_tensor = tensor_index_tensor[:max_num,:]
        non_empty_tensor_iou_tensor = tensor_iou_tensor[:max_num,:]

    # predictions and labels
    predictions_dicts, labels_dicts = [], []
    predictions_dict = {
        "bbox": pred_boxes_img,
        "box3d_lidar": pred_boxes,
        "scores": pred_scores.reshape(-1, 1),
        "pred_clses": pred_clses,
        "pred_bev_boxes": pred_bev_boxes,
        "pred_3d_all_scores": all_scores.reshape(-1, 6),
        "pred_2d_all_scores": box_2d_all_scores.reshape(-1, 6),
        "image_idx": img_idx,
    }
    labels_dict = {
        "labels_lidar": labels[..., 1:].numpy().reshape(-1, 7),
        "labels_bev": labels_bev.numpy().reshape(-1, 5),
        "labels_cls":labels[..., 0].numpy().astype(np.int).reshape(-1, 1),
    }
    predictions_dicts.append(predictions_dict)
    labels_dicts.append(labels_dict)

    res, iou_test, tensor_index, tensor_iou = predictions_dicts, non_empty_iou_test_tensor, non_empty_tensor_index_tensor, non_empty_tensor_iou_tensor     

    return res, labels_dicts, detections, top_predictions, iou_test, tensor_index, tensor_iou, valid_num, match_list, iou_hide_mat

def generate_fusion_input_apollo(img_idx, cnf, num_classes, detections, top_predictions, labels, labels_bev, img_rgb=None, bev_map=None):
    def get_template_prediction(num_samples):
        ret_dict = {
            'name': np.zeros(num_samples), 'truncated': np.zeros(num_samples),
            'occluded': np.zeros(num_samples), 'alpha': np.zeros(num_samples),
            'bbox': np.zeros([num_samples, 4]), 'dimensions': np.zeros([num_samples, 3]),
            'location': np.zeros([num_samples, 3]), 'rotation_y': np.zeros(num_samples),
            'score': np.zeros(num_samples), 'boxes_lidar': np.zeros([num_samples, 7])
        }
        return ret_dict    

    kitti_dets = []
    pred_scores_list = []
    all_scores_list = []
    pred_boxes_list = []
    pred_clses_list = []
    pred_bev_boxes_list = []
    for cls_id in range(num_classes):
        # if len(detections[cls_id] > 0):
        if detections[cls_id].shape[0] > 0:    
            for det in detections[cls_id][0]:
                det = det.numpy() 
                # (scores-0:1, x-1:2, y-2:3, z-3:4, dim-4:7, yaw-7:8)
                _score, _x, _y, _z, _h, _w, _l, _yaw, \
                score_1, score_2, score_3, score_4, score_5, score_6 = det
                # bev image result
                pred_bev_boxes_list.append([_x, _y, _w, _l, _yaw])
                _yaw = -_yaw
                x = _y / cnf.BEV_HEIGHT * cnf.bound_size_x + cnf.boundary['minX']
                y = _x / cnf.BEV_WIDTH * cnf.bound_size_y + cnf.boundary['minY']
                z = _z + cnf.boundary['minZ']
                w = _w / cnf.BEV_WIDTH * cnf.bound_size_y
                l = _l / cnf.BEV_HEIGHT * cnf.bound_size_x
                pred_scores_list.append([_score])
                all_scores_list.append([score_1, score_2, score_3, score_4, score_5, score_6])
                pred_boxes_list.append([x, y, z, l, w, _h, _yaw])
                pred_clses_list.append([cls_id])
                kitti_dets.append([1, x, y, z, _h, w, l, _yaw, _score])

    pred_scores = np.array(pred_scores_list)
    all_scores= np.array(all_scores_list)
    pred_boxes = np.array(pred_boxes_list)
    pred_clses = np.array(pred_clses_list)
    pred_bev_boxes = np.array(pred_bev_boxes_list)
    # pred_dict = get_template_prediction(pred_scores.shape[0])

    # Stay boxes only in the image coordinate
    used_index = filter_boxes(pred_boxes)
    pred_scores = pred_scores[used_index]
    all_scores = all_scores[used_index]
    pred_boxes = pred_boxes[used_index]
    pred_clses = pred_clses[used_index]
    pred_bev_boxes = pred_bev_boxes[used_index]

    # Lidar 3D to Image 2d or whether show or not
    pred_boxes_img = trans_zs_no_nn(pred_boxes, pred_scores, top_predictions[:, 1:6], img_rgb, bev_map)
    # return None, None, None, None, None, None, None, None
    # pred_boxes_img = trans_zs(pred_boxes, pred_scores, top_predictions[:, 1:6], None, None)
    
    # dis_to_lidar = torch.norm(pred_boxes[:,:2],p=2,dim=1,keepdim=True)/82.0
    dis_to_lidar = np.linalg.norm(pred_boxes[:, :2],ord=2,axis=1,keepdims=True)/82.0
    box_2d_detector = np.zeros((200, 4))
    box_2d_detector[0:top_predictions.shape[0], :] = top_predictions[:, 1:5]
    box_2d_detector = top_predictions[:, 1:5]
    box_2d_scores = top_predictions[:, 5].reshape(-1, 1)
    box_2d_all_scores = top_predictions[:, 6:].reshape(-1, 6)
    box_2d_labels = top_predictions[:, 0].astype(np.int).reshape(-1, 1)
    valid_num = torch.tensor((box_2d_detector.shape[0], pred_boxes_img.shape[0]))

    overlaps1 = np.zeros((900000, 6, 3), dtype=pred_boxes_img.dtype)
    tensor_index1 = np.zeros((900000, 2), dtype=pred_boxes_img.dtype)
    tensor_iou = np.zeros((900000, 1), dtype=pred_boxes_img.dtype)
    overlaps1[:, :] = -1
    tensor_index1[:, :] = -1
    # criterion=-1 交集/并集， criterion=1 交集/2D，criterion=0 交集/3D 
    iou_test, tensor_index, tensor_iou, iou_mat, iou_hide_mat, max_num = eval_clocs.build_stage2_training_apollo(pred_boxes_img,
                                                                                          box_2d_detector,
                                                                                          -1,
                                                                                          pred_scores.reshape(-1, 1),
                                                                                          all_scores.reshape(-1, 6),
                                                                                          box_2d_scores,
                                                                                          box_2d_all_scores,
                                                                                          pred_clses,
                                                                                          box_2d_labels,
                                                                                          dis_to_lidar,                                        
                                                                                          overlaps1,
                                                                                          tensor_index1,
                                                                                          tensor_iou)

    iou_test_tensor = torch.FloatTensor(iou_test)  #iou_test_tensor shape: [160000,4]
    tensor_index_tensor = torch.LongTensor(tensor_index)
    tensor_iou_tensor = torch.FloatTensor(tensor_iou)
    # iou_test_tensor = iou_test_tensor.permute(1,0)
    iou_test_tensor = iou_test_tensor.permute(1,2,0)
    # iou_test_tensor = iou_test_tensor.reshape(1,4,1,900000)
    # iou_test_tensor = iou_test_tensor.reshape(3,4,1,900000)
    iou_test_tensor = iou_test_tensor.reshape(6,3,1,900000)
    # iou_test_tensor = iou_test_tensor.reshape(1,5,1,900000)
    # iou_test_tensor = iou_test_tensor.reshape(1,8,1,900000)
    tensor_index_tensor = tensor_index_tensor.reshape(-1,2)
    tensor_iou_tensor = tensor_iou_tensor.reshape(-1,1)
    if max_num == 0:
        # non_empty_iou_test_tensor = torch.zeros(1,4,1,2)
        # non_empty_iou_test_tensor = torch.zeros(3,4,1,2)
        non_empty_iou_test_tensor = torch.zeros(6,3,1,2)
        # non_empty_iou_test_tensor = torch.zeros(1,5,1,2)
        # non_empty_iou_test_tensor = torch.zeros(1,8,1,2)
        non_empty_iou_test_tensor[:,:,:,:] = -1
        non_empty_tensor_index_tensor = torch.zeros(2,2)
        non_empty_tensor_index_tensor[:,:] = -1
        non_empty_tensor_iou_tensor = torch.zeros(1,2)      
    else:
        non_empty_iou_test_tensor = iou_test_tensor[:,:,:,:max_num]
        non_empty_tensor_index_tensor = tensor_index_tensor[:max_num,:]
        non_empty_tensor_iou_tensor = tensor_iou_tensor[:max_num,:]

    # predictions and labels
    predictions_dicts, labels_dicts = [], []
    predictions_dict = {
        "bbox": pred_boxes_img,
        "box3d_lidar": pred_boxes,
        "scores": pred_scores.reshape(-1, 1),
        "pred_clses": pred_clses,
        "pred_bev_boxes": pred_bev_boxes,
        "pred_3d_all_scores": all_scores.reshape(-1, 6),
        "pred_2d_all_scores": box_2d_all_scores.reshape(-1, 6),
        "image_idx": img_idx,
    }
    labels_dict = {
        "labels_lidar": labels[..., 1:].numpy().reshape(-1, 7),
        "labels_bev": labels_bev.numpy().reshape(-1, 5),
        "labels_cls":labels[..., 0].numpy().astype(np.int).reshape(-1, 1),
    }
    predictions_dicts.append(predictions_dict)
    labels_dicts.append(labels_dict)

    res, iou_test, tensor_index, tensor_iou = predictions_dicts, non_empty_iou_test_tensor, non_empty_tensor_index_tensor, non_empty_tensor_iou_tensor     

    return res, labels_dicts, detections, top_predictions, iou_test, tensor_index, tensor_iou, valid_num

def trans_zs(boxes_lidar_3d, boxes_lidar_score, boxes_image_2d=None, image=None, bev_map=None):

    corners3d = boxes3d_to_corners3d(boxes_lidar_3d)
    camera3d = corners3d_to_camera3d(corners3d)
    img_boxes = camera3d_to_img_boxes(camera3d)

    if image is not None:
        # show lidar_3d in image
        for i in range(len(img_boxes)):
            box = img_boxes[i]
            score = boxes_lidar_score[i]
            cv2.rectangle(image, (int(box[0]), int(box[1])),
                            (int(box[2]), int(box[3])), (0, 0, 255), 1)
            cv2.putText(image, str(score[0])[:4], (int(box[0]), int(box[1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 255), 1)                            

        if boxes_image_2d is not None:
        # show image 2d
            for i in range(boxes_image_2d.shape[0]):
                box = boxes_image_2d[i, :-1]
                score = boxes_image_2d[i, -1]
                cv2.rectangle(image, (int(box[0]), int(box[1])),
                                (int(box[2]), int(box[3])), (0, 255, 255), 1)
                cv2.putText(image, str(score)[:4], (int(box[0]), int(box[1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 1)
        cv2.imshow('image', image)
        cv2.imshow('bev_map', bev_map)
        cv2.waitKey(0)
    
    return np.array(img_boxes)

def trans_zs_no_nn(boxes_lidar_3d, boxes_lidar_score, boxes_image_2d, image=None, bev_map=None):

    corners3d = boxes3d_to_corners3d(boxes_lidar_3d)
    camera3d = corners3d_to_camera3d(corners3d)
    img_boxes = camera3d_to_img_boxes(camera3d)

    if image is not None:                          
        # show image 2d
        for i in range(boxes_image_2d.shape[0]):
            box = boxes_image_2d[i, :-1]
            score = boxes_image_2d[i, -1]
            cv2.rectangle(image, (int(box[0]), int(box[1])),
                            (int(box[2]), int(box[3])), (0, 255, 255), 1)
            cv2.putText(image, str(score)[:4], (int(box[0]), int(box[1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 1)
        cv2.imshow('image', image)
        cv2.waitKey(0)
    
    return np.array(img_boxes)

def lidar_result_in_imgae(result_3d_list, image):
    if len(result_3d_list) > 0:
        boxes_lidar_3d = np.array(result_3d_list)[:, 1:-1]
        boxes_lidar_scores = np.array(result_3d_list)[:, -1]
        boxes_lidar_clses = np.array(result_3d_list)[:, 0]
        corners3d = boxes3d_to_corners3d(boxes_lidar_3d)
        camera3d = corners3d_to_camera3d(corners3d)
        img_boxes = camera3d_to_img_boxes(camera3d)
    else:
        img_boxes = []

    if image is not None:
        # show lidar_3d in image
        for i in range(len(img_boxes)):
            box = img_boxes[i]
            score = boxes_lidar_scores[i]
            cls = boxes_lidar_clses[i]
            cv2.rectangle(image, (int(box[0]), int(box[1])),
                            (int(box[2]), int(box[3])), (0, 0, 255), 1)
            cv2.putText(image, str(score)[:4], (int(box[0]), int(box[1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 255), 1)
            cv2.putText(image, str(int(cls)), (int(box[2]), int(box[3])), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 255), 1)                             

        cv2.imshow('image_fusion', image)
        cv2.waitKey(0)
    
    return np.array(img_boxes)

def match_find_max(ori):
    temp = []
    temp_score = []
    for i in range(len(ori)):
        col_argmax = np.argmax(ori[i])
        col_max = np.max(ori[i])
        row_argmax = np.argmax(ori[:, col_argmax])
        row_max = np.max(ori[:, col_argmax])
        if i == row_argmax and col_max != 0 and row_max != 0:
            temp.append(col_argmax)
            temp_score.append(col_max)
        else:
            temp.append(-1)
            temp_score.append(-1)
    return temp, temp_score

