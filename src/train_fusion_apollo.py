"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.08.17
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: Testing script
"""

import argparse
import sys
import os
import time
import pdb
import warnings

# from test_fusion_apollo_no_nn import map_calculate_lidar

warnings.filterwarnings("ignore", category=UserWarning)

from easydict import EasyDict as edict
import cv2
import torch
from torch.utils.tensorboard import SummaryWriter
import numpy as np
import subprocess
import random
import pickle

sys.path.append('../')

from data_process.apollo_dataloader import create_fusion_train_dataloader, create_fusion_val_dataloader
from data_process.kitti_bev_utils import get_corners
import torchplus
from models.model_utils import create_model
from models import fusion
from utils.train_utils import create_optimizer, create_lr_scheduler, get_saved_state, save_checkpoint
from utils.misc import make_folder, time_synchronized
from utils.evaluation_utils import decode, post_processing, post_processing_all_scores, draw_predictions_fusion, convert_det_to_kitti_format
from utils.evaluation_utils import generate_fusion_input, generate_fusion_input_apollo, generate_kitti_3d_detection, lidar_result_in_imgae
from utils.torch_utils import _sigmoid
from utils.torch_utils import reduce_tensor, to_python_float
from utils.bev_map_val import get_map
# from utils.calibration_kitti import Calibration
from data_process.kitti_data_utils import Calibration
import config.apollo_config as cnf
from config.train_config_apollo import parse_train_configs
from data_process.transformation import lidar_to_camera_box
from utils.visualization_utils import merge_rgb_to_bev, show_rgb_image_with_boxes
from utils import eval_clocs
from utils.logger import Logger
from utils.misc import AverageMeter, ProgressMeter
from utils.lidar_to_camera import whether_in_image

from losses.losses import SigmoidFocalClassificationLoss


ID_TYPE_CONVERSION_inv = {
    'Pedestrian': 0,
    'Car': 1,
    'Cyclist': 2,
    'Van': 1
}

def map_calculate(metadatas, detections_post, result_1_f, result_3_f, result_5_f):
    # 整理reuslt
    for j in range(6):
        if j == 1-1:
            if len(detections_post[0][j] > 0):
                for det in detections_post[0][j]:
                    _score, _x, _y, _z, _h, _w, _l, _yaw = det[0:8]
                    # box_list = bev_to_lidar_coordinate([_x, _y, _z, _h, _w, _l, _yaw]) 
                    # flag_in_image = whether_in_image(box_list)
                    # if flag_in_image == False:
                    #     continue
                    bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                    x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                    result_1_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if j == 3-1:
            if len(detections_post[0][j] > 0):
                for det in detections_post[0][j]:
                    _score, _x, _y, _z, _h, _w, _l, _yaw = det[0:8] 
                    # box_list = bev_to_lidar_coordinate([_x, _y, _z, _h, _w, _l, _yaw]) 
                    # flag_in_image = whether_in_image(box_list)
                    # if flag_in_image == False:
                    #     continue                                                  
                    bev_corners = get_corners(_x, _y, _w, _l, _yaw)
                    x1, y1, x2, y2, x3, y3, x4, y4 = bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
                    result_3_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        # if j == 5-1:
        #     if len(detections_post[0][j] > 0):
        #         for det in detections_post[0][j]:
        #             _score, _x, _y, _z, _h, _w, _l, _yaw = det[0:8]
        #             # box_list = bev_to_lidar_coordinate([_x, _y, _z, _h, _w, _l, _yaw]) 
        #             # flag_in_image = whether_in_image(box_list)
        #             # if flag_in_image == False:
        #             #     continue                                                       
        #             bev_corners = get_corners(_x, _y, _w, _l, _yaw)
        #             x1, y1, x2, y2, x3, y3, x4, y4 = \
        #             bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], \
        #             bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
        #             result_5_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')    

def map_calculate_lidar(metadatas, result_3d_list, result_1_f, result_3_f, result_5_f):
    for result_3d in result_3d_list:
        cls_id, x, y, z, l, w, h, yaw, _score = result_3d

        l = l / cnf.bound_size_x * cnf.BEV_HEIGHT
        w = w / cnf.bound_size_y * cnf.BEV_WIDTH
        center_input_y = (x - cnf.boundary['minX']) / cnf.bound_size_x * cnf.BEV_HEIGHT  # x --> y (invert to 2D image space)
        center_input_x = (y - cnf.boundary['minY']) / cnf.bound_size_y * cnf.BEV_WIDTH  # y --> x
        yaw = -yaw
        
        bev_corners = get_corners(center_input_x, center_input_y, w, l, yaw)
        x1, y1, x2, y2, x3, y3, x4, y4 = \
        bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], \
        bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
        if cls_id == 1-1:
            result_1_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if cls_id == 3-1:
            result_3_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')
        if cls_id == 5-1:
            result_5_f.write(metadatas['frame_id'][0]+' '+str(_score)+' '+str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+'\n')

def bev_to_lidar_coordinate(bev_list):
    _x, _y, _z, _h, _w, _l, _yaw = bev_list
    _yaw = -_yaw
    x = _y / cnf.BEV_HEIGHT * cnf.bound_size_x + cnf.boundary['minX']
    y = _x / cnf.BEV_WIDTH * cnf.bound_size_y + cnf.boundary['minY']
    z = _z + cnf.boundary['minZ']
    w = _w / cnf.BEV_WIDTH * cnf.bound_size_y
    l = _l / cnf.BEV_HEIGHT * cnf.bound_size_x
    
    return [x, y, z, l, w, _h, _yaw]

def train_one_epoch(train_dataloader, model, fusion_layer, optimizer, lr_scheduler, epoch, configs, logger, tb_writer):

    # detection_2d_dir = '/media/myc/Work/Research/code/ma_detec/result_zhijiangyihao/data_10/0.2'
    # detection_2d_dir = '/media/myc/Work/Research/code/ma_detec/result_zhijiangyihao/data_9/'
    detection_2d_dir = '/media/myc/Work/Research/code/ma_detec/result_zhijiangyihao/data/'

    batch_time = AverageMeter('Time', ':6.3f')
    data_time = AverageMeter('Data', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')

    progress = ProgressMeter(len(train_dataloader), [batch_time, data_time, losses],
                             prefix="Train - Epoch: [{}/{}]".format(epoch, configs.num_epochs))
    num_iters_per_epoch = len(train_dataloader)
    start_time = time.time()

    for batch_idx, batch_data in enumerate(train_dataloader):
        # print('batch_idx:', batch_idx)
        global_step = num_iters_per_epoch * (epoch - 1) + batch_idx + 1
        batch_size = 1
        # metadatas, bev_maps, bev_maps_ori, bev_maps_index, img, labels, labels_bev = batch_data
        metadatas, img, labels, labels_bev, detections = batch_data
        # input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float() 
        # input_bev_maps_index = bev_maps_index.to(configs.device, non_blocking=True).int()          
        # # outputs = model_all(input_bev_maps, input_bev_maps_index)
        # with torch.no_grad():
        #     outputs = model(input_bev_maps)

        # outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
        # outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
        # # detections size (batch_size, K, 10)
        # detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
        #                                 outputs['dim'], K=50)        
        # detections = detections.cpu().numpy().astype(np.float32)
        # all_scores = all_scores.cpu().squeeze().numpy()

        # detections_post = post_processing_all_scores(detections, all_scores, configs.num_classes, configs.down_ratio, -1)
        # detections = detections_post[0]  # only first batch
        # map_calculate(metadatas, detections_post)
        # continue

        # Draw prediction in the bev image
        # bev_map = (bev_maps_ori[0].permute(1, 2, 0).numpy() * 255).astype(np.uint8)
        # bev_map = cv2.resize(bev_map, (608, 1216))
        # bev_map = draw_predictions_fusion(bev_map, detections, configs.num_classes)
        # bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)

        bev_map = None


        frame_id = metadatas['frame_id'][0]
        if 1:
            # 读取2D检测结果
            detection_2d_name = frame_id + '.txt'
            detection_2d_path = os.path.join(detection_2d_dir, detection_2d_name)
            with open(detection_2d_path, 'r') as ann:
                content_list = list()
                for ann_ind, txt in enumerate(ann):
                    temp = txt[:-1].split(' ')
                    temp_all_scores = temp[6:]
                    cls_id = int(temp[0])-1
                    x_min, y_min, x_max, y_max = float(temp[1]), float(temp[2]), float(temp[3]), float(temp[4])
                    score_1, score_2, score_3, score_4, score_5, score_6 = \
                    float(temp_all_scores[0]), float(temp_all_scores[1]), float(temp_all_scores[2]), \
                    float(temp_all_scores[3]), float(temp_all_scores[4]), float(temp_all_scores[5])
                    score = float(temp[5])
                    content = [cls_id, x_min, y_min, x_max, y_max, score, score_1, score_2, score_3, score_4, score_5, score_6]
                    content_list.append(content)
            if len(content_list) == 0:
                content_list.append([-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1])
            top_predictions = np.array(content_list)
        else:
            top_predictions = None
        
        # 计算3D检测和2D检测的IOU，并融合成特征，得到融合的输入；对labels的坐标系进行转化，转成camera坐标系
        img_rgb = img[0].numpy()
        res, labels_dicts, all_3d_output, top_predictions, fusion_input, tensor_index, tensor_iou, valid_num = \
        generate_fusion_input_apollo(batch_idx, cnf, configs.num_classes, detections, top_predictions, labels, labels_bev, img_rgb, bev_map)
        if res == None:
            continue

        # 将融合后新的Feature送进融合网络
        cls_preds, flag = fusion_layer(fusion_input.cuda(), tensor_index.cuda(), tensor_iou.cuda(), valid_num.cuda())

        # cls_preds = torch.sigmoid(cls_preds)
        res_scores, res_clses = cls_preds.reshape(6,-1).max(dim=0)

        # res_scores_mask = np.zeros_like(res_scores.detach().cpu().numpy(), dtype=np.int)
        # res_scores_mask[np.unique(res_scores.detach().cpu().numpy(), return_index=True)[1]] = 1
        # res_scores[res_scores_mask!=1] = -10
        # cls_preds = res_scores.reshape(1, -1, 1)
        # res_clses = res_clses.cpu().reshape(-1, 1).numpy()

        # 计算pre_3d_box和labels的iou，定义哪些pre_3d_box应该为正样本，分数应该大一些
        if labels.shape[1] == 0:
            target_for_fusion = np.zeros((1, valid_num[1].item()*6, 1))
            # target_multi_for_fusion = np.zeros((1, valid_num[1].item(), 6))
            positives = torch.zeros(1, valid_num[1].item()*6).type(torch.float32).cuda()
            negatives = torch.zeros(1, valid_num[1].item()*6).type(torch.float32).cuda()
            negatives[:, :] = 1  
        else:
            labels_boxes_lidar = labels_dicts[0]['labels_lidar']
            labels_clses = labels_dicts[0]['labels_cls']
            labels_boxes_bev = labels_dicts[0]['labels_bev']
            pred_boxes_lidar = res[0]['box3d_lidar']
            pred_boxes_bev = res[0]['pred_bev_boxes']
            pred_clses = res[0]['pred_clses']
            # 
            # iou_bev, iou_bev_label = eval_clocs.d3_box_overlap(labels_boxes_camera, pred_boxes_camera, labels_clses, pred_clses, criterion=-1)
            # iou_bev, iou_bev_label = eval_clocs.d3_box_overlap(labels_boxes_lidar, pred_boxes_lidar, labels_clses, res_clses, criterion=-1)
            # iou_bev, iou_bev_label = eval_clocs.cal_boxes_rotate_iou(labels_boxes_bev, pred_boxes_bev, labels_clses, res_clses, criterion=-1)
            iou_bev = eval_clocs.cal_boxes_rotate_iou(labels_boxes_bev, pred_boxes_bev, labels_clses)
            # iou_bev = eval_clocs.cal_boxes_rotate_iou(labels_boxes_bev, pred_boxes_bev)
            iou_bev_max = np.amax(iou_bev, axis=1)
            target_for_fusion = ((iou_bev_max >= 0.4)*1).reshape(1, -1, 1)
            # target_for_fusion = ((iou_bev_max >= 0.4)*1).reshape(1, -1, 6)
            positive_index = ((iou_bev_max >= 0.4)*1).reshape(1, -1)
            positives = torch.from_numpy(positive_index).type(torch.float32).cuda()
            negative_index = ((iou_bev_max <= 0.1)*1).reshape(1, -1)
            negatives = torch.from_numpy(negative_index).type(torch.float32).cuda()


        one_hot_targets = torch.from_numpy(target_for_fusion).type(torch.float32).cuda()
        # one_hot_targets = torch.from_numpy(target_multi_for_fusion).type(torch.float32).cuda()

        negative_cls_weights = negatives.type(torch.float32) * 1.0
        # cls_weights = negative_cls_weights + 1.0 * positives.type(torch.float32)
        # 正样本需要权重更大一些
        cls_weights = negative_cls_weights + 2.0 * positives.type(torch.float32)
        pos_normalizer = positives.sum(1, keepdim=True).type(torch.float32)
        # pos_normalizer = positives.sum().type(torch.float32)
        cls_weights /= torch.clamp(pos_normalizer, min=1.0)

        # cls_weights = torch.ones((1, valid_num[1].item()*6)).type(torch.float32)
        if flag == 1:
            loss_stats = dict()
            # cls_preds = cls_preds.permute(1, 2, 0, 3).reshape(1, -1, 6)
            # if cls_preds.shape[1] != one_hot_targets.shape[1]:
            #     a = 0
            cls_preds = cls_preds.reshape(1, -1, 1)
            # cls_preds_print = torch.sigmoid(cls_preds)
            # print('cls_preds[0, 25, 0]:', cls_preds_print[0, 25, 0])
            # print('cls_preds[0, 29, 0]:', cls_preds_print[0, 29, 0])
            # print('cls_preds[0, 39, 0]:', cls_preds_print[0, 29, 0])
            # try:
            cls_losses = focal_loss._compute_loss(cls_preds, one_hot_targets, cls_weights.cuda())  # [N, M]
            # except:
            #     a = 0
            cls_losses_reduced = cls_losses.sum() / cls_losses.shape[1]
            # cls_loss_sum = cls_loss_sum + cls_losses_reduced
            # if train_cfg.enable_mixed_precision:
            #     loss *= loss_scale
            cls_losses_reduced.backward()
            optimizer.step()
            optimizer.zero_grad()
            # Adjust learning rate
            if configs.step_lr_in_epoch:
                lr_scheduler.step()
                if tb_writer is not None:
                    tb_writer.add_scalar('LR', lr_scheduler.get_lr()[0], global_step)

            losses.update(to_python_float(cls_losses_reduced), batch_size)
            # measure elapsed time
            # torch.cuda.synchronize()
            batch_time.update(time.time() - start_time)

            if tb_writer is not None:
                if (global_step % configs.tensorboard_freq) == 0:
                    loss_stats['avg_loss'] = losses.avg
                    tb_writer.add_scalars('Train', loss_stats, global_step)
            # Log message
            if logger is not None:
                if (global_step % configs.print_freq) == 0:
                    logger.info(progress.get_message(batch_idx))

            start_time = time.time()
    # result_1_f.close()
    # result_3_f.close()
    # result_5_f.close()
    # detpath  = result_dir+'{:s}.txt'
    # annopath = '/media/myc/Data21/zhijiangyihao/training_fusion/test_map9_image/{:s}.txt'
    # imagesetfile = '/media/myc/Data21/zhijiangyihao/training_fusion/test.list'  
    # map = get_map(detpath, annopath, imagesetfile)
    return


def validate(val_dataloader, model, fusion_layer, configs):

    # detection_2d_dir = '/media/myc/Work/Research/code/ma_detec/result_zhijiangyihao/data_10/0.2/'
    detection_2d_dir = '/media/myc/Work/Research/code/ma_detec/result_zhijiangyihao/data/'
    result_dir = './result/zhijiangyihao/'
    det_result_dir = './result/det_3d/'
    result_1_f = open(result_dir+'1.txt', 'w')
    result_3_f = open(result_dir+'3.txt', 'w')
    result_5_f = open(result_dir+'5.txt', 'w')       
    # switch mode
    fusion_layer.eval()
    with torch.no_grad():
        for batch_idx, batch_data in enumerate(val_dataloader):
            # break
            # metadatas, bev_maps, bev_maps_ori, bev_maps_index, img, labels, labels_bev, detections = batch_data
            metadatas, img, labels, labels_bev, detections = batch_data

            # # 用模型在线推理检测结果，并用直接写下检测结果
            # input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float() 
            # input_bev_maps_index = bev_maps_index.to(configs.device, non_blocking=True).int()          
            # with torch.no_grad():
            #     outputs = model(input_bev_maps)

            # outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
            # outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
            # # detections size (batch_size, K, 10)
            # detections_pre, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
            #                                 outputs['dim'], K=50)        
            # detections_pre = detections_pre.cpu().numpy().astype(np.float32)
            # all_scores = all_scores.cpu().squeeze().numpy()

            # detections_post = post_processing_all_scores(detections_pre, all_scores, configs.num_classes, configs.down_ratio, -1)
            # detections = detections_post[0]  # only first batch  

            # frame_id = metadatas['frame_id'][0]
            # np.savez_compressed(det_result_dir+frame_id, **detections)
            # continue

            frame_id = metadatas['frame_id'][0]
            # print(frame_id)
            if 1:
                # 读取2D检测结果
                detection_2d_name = frame_id + '.txt'
                detection_2d_path = os.path.join(detection_2d_dir, detection_2d_name)
                with open(detection_2d_path, 'r') as ann:
                    content_list = list()
                    for ann_ind, txt in enumerate(ann):
                        temp = txt[:-1].split(' ')
                        temp_all_scores = temp[6:]
                        cls_id = int(temp[0])-1
                        x_min, y_min, x_max, y_max = float(temp[1]), float(temp[2]), float(temp[3]), float(temp[4])
                        score_1, score_2, score_3, score_4, score_5, score_6 = \
                        float(temp_all_scores[0]), float(temp_all_scores[1]), float(temp_all_scores[2]), \
                        float(temp_all_scores[3]), float(temp_all_scores[4]), float(temp_all_scores[5])
                        score = float(temp[5])
                        content = [cls_id, x_min, y_min, x_max, y_max, score, score_1, score_2, score_3, score_4, score_5, score_6]
                        content_list.append(content)
                if len(content_list) == 0:
                    content_list.append([-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1])
                top_predictions = np.array(content_list)
            else:
                top_predictions = None
            # 计算3D检测和2D检测的IOU，并融合成特征，得到融合的输入；对labels的坐标系进行转化，转成camera坐标系
            img_rgb = img[0].numpy()
            bev_map = None
            res, labels_dicts, all_3d_output, top_predictions, fusion_input, tensor_index, tensor_iou, valid_num = \
            generate_fusion_input_apollo(batch_idx, cnf, configs.num_classes, detections, top_predictions, labels, labels_bev, img_rgb.copy(), bev_map)
            if res == None:
                continue

            # 将融合后新的Feature送进融合网络
            cls_preds, flag = fusion_layer(fusion_input.cuda(), tensor_index.cuda(), tensor_iou.cuda(), valid_num.cuda())
            cls_preds = torch.sigmoid(cls_preds)
            # 对网络进行推理测试，将融合模型得到的3D检测分数替换原始3D检测的分数，并计算验证集下的3DmAP
            # res[0]['kitti_res'][:, -1] = cls_preds.cpu().reshape(-1)

            res_scores, res_clses = cls_preds.reshape(6,-1).max(dim=0)
            res_scores_mask = np.zeros_like(res_scores.cpu().numpy(), dtype=np.int)
            res_scores_mask[np.unique(res_scores.cpu().numpy(), return_index=True)[1]] = 1
            res_scores[res_scores_mask!=1] = 0
            res_scores = res_scores.cpu().reshape(-1)
            res_clses = res_clses.cpu().reshape(-1)

            pred_boxes_lidar = res[0]['box3d_lidar']
            pred_3d_scores = res[0]['scores']
            pred_3d_clses = res[0]['pred_clses']

            # 直接用原始的3D检测结果
            # result_3d_list = []
            # for i in range(pred_boxes_lidar.shape[0]):
            #     cls_id = pred_3d_clses[i][0]
            #     socre = pred_3d_scores[i][0]
            #     result_3d = pred_boxes_lidar[i].tolist() 
            #     result_3d.insert(0, cls_id)
            #     result_3d.append(socre)
            #     result_3d_list.append(result_3d)
            # 原始结果
            result_3d_list = []
            for i in range(pred_boxes_lidar.shape[0]):
                pred_box_lidar = pred_boxes_lidar[i].tolist()
                pred_cls_lidar = pred_3d_clses[i].tolist()
                pred_score_lidar = pred_3d_scores[i].tolist()
                result_3d = pred_cls_lidar + pred_box_lidar + pred_score_lidar
                result_3d_list.append(result_3d)
            # show 感知结果
            lidar_result_in_imgae(result_3d_list, img_rgb.copy())                

            # 将融合后的分数覆盖原来的分数
            result_3d_list = []
            for i in range(pred_boxes_lidar.shape[0]):
                pred_box_lidar = pred_boxes_lidar[i].tolist()
                pred_cls_lidar = [res_clses[i].item()]
                pred_score_lidar = [res_scores[i].item()]
                if pred_score_lidar[0] < 0.2:
                    continue
                result_3d = pred_cls_lidar + pred_box_lidar + pred_score_lidar
                result_3d_list.append(result_3d)

            # show 融合之后的感知结果
            lidar_result_in_imgae(result_3d_list, img_rgb.copy())
            map_calculate_lidar(metadatas, result_3d_list, result_1_f, result_3_f, result_5_f)
                    

    result_1_f.close()
    result_3_f.close()
    result_5_f.close()
    detpath  = result_dir+'{:s}.txt'
    # annopath = '/media/myc/Data21/zhijiangyihao/training_fusion/test_map10_image/{:s}.txt'
    # imagesetfile = '/media/myc/Data21/zhijiangyihao/training_fusion/test_10_shuffle.list'   
    annopath = '/media/myc/Data21/zhijiangyihao/training_fusion/test_map_image/{:s}.txt'
    imagesetfile = '/media/myc/Data21/zhijiangyihao/training_fusion/val.txt'
    map = get_map(detpath, annopath, imagesetfile)
    return map



if __name__ == '__main__':

    configs = parse_train_configs()
    configs.distributed = configs.world_size > 1 or configs.multiprocessing_distributed
    configs.is_master_node = (not configs.distributed) or (
            configs.distributed and (configs.rank % configs.ngpus_per_node == 0))    
    if configs.is_master_node:
        logger = Logger(configs.logs_dir, configs.saved_fn)
        logger.info('>>> Created a new logger')
        logger.info('>>> configs: {}'.format(configs))
        tb_writer = SummaryWriter(log_dir=os.path.join(configs.logs_dir, 'tensorboard'))
    else:
        logger = None
        tb_writer = None

    model = create_model(configs)
    model_path = '/media/myc/Work/Research/code/Super-Fast-Accurate-3D-Object-Detection/checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_116_0.3.6.pth'
    model.load_state_dict(torch.load(model_path, map_location=lambda storage, loc: storage))
    model.cuda(configs.gpu_idx)

    configs.device = torch.device('cpu' if configs.no_cuda else 'cuda:{}'.format(configs.gpu_idx))
    model = model.to(device=configs.device)

    out_cap = None
    # detection_2d_dir = '/media/myc/Work/Research/code/ma_detec/result_zhijiangyihao/data_9'
    detection_2d_dir = '/media/myc/Work/Research/code/ma_detec/result_zhijiangyihao/data_10/0.2'
    # detection_2d_train_dir = '/media/myc/Work/Research/code/ma_detec/result_zhijiangyihao/data_9'
    # detection_2d_val_dir = '/media/myc/Work/Research/code/ma_detec/result_zhijiangyihao/data_9'

    detection_2d_true_flag = 0

    clocs_model_dir = './CLOCs_SecCas_pretrained'
    clocs_model_path = './CLOCs_SecCas_pretrained/Model_fusion_all_epoch_22.pth'
    model.eval()

    # Fusion model
    fusion_layer = fusion.fusion(configs)
    fusion_layer.train()
    fusion_layer.cuda()
    focal_loss = SigmoidFocalClassificationLoss()
    cls_loss_sum = 0
    # ckpt_path = None
    ckpt_path = './checkpoints_fusion/Model_fusion_all_epoch_102.pth'
    if ckpt_path is None:
        print("load existing model for fusion layer")        
        torchplus.train.try_restore_latest_checkpoints(clocs_model_dir, [fusion_layer], clocs_model_path)
    else:
        torchplus.train.restore(ckpt_path, fusion_layer)
    
    # create optimizer
    optimizer = create_optimizer(configs, fusion_layer)
    lr_scheduler = create_lr_scheduler(optimizer, configs)
    configs.step_lr_in_epoch = False if configs.lr_type in ['multi_step', 'cosin', 'one_cycle'] else True    
    optimizer.zero_grad()

    # val_dataloader = create_fusion_val_dataloader(configs)
    # print('number of batches in val_dataloader: {}'.format(len(val_dataloader)))
    # map = validate(val_dataloader, model, fusion_layer, configs)
    # print('map: {:.4e}'.format(map))

    # create train_dataloader
    train_dataloader = create_fusion_train_dataloader(configs)
    num_iters_per_epoch = len(train_dataloader)
    for epoch in range(configs.start_epoch, configs.num_epochs + 1):
        if logger is not None:
            logger.info('{}'.format('*-' * 40))
            logger.info('{} {}/{} {}'.format('=' * 35, epoch, configs.num_epochs, '=' * 35))
            logger.info('{}'.format('*-' * 40))
            logger.info('>>> Epoch: [{}/{}]'.format(epoch, configs.num_epochs))

        # train for one epoch
        # train_one_epoch(train_dataloader, model, fusion_layer, optimizer, lr_scheduler, epoch, configs, logger, tb_writer)

        # 每训练完1个epoch，进行验证集验证，并存储
        if 1:
            val_dataloader = create_fusion_val_dataloader(configs)
            print('number of batches in val_dataloader: {}'.format(len(val_dataloader)))
            map = validate(val_dataloader, model, fusion_layer, configs)
            print('map: {:.4e}'.format(map))
            if tb_writer is not None:
                tb_writer.add_scalar('map', map, epoch)

        # Save checkpoint
        # if configs.is_master_node and ((epoch % configs.checkpoint_freq) == 0):
        if 1:
            model_state_dict, utils_state_dict = get_saved_state(fusion_layer, optimizer, lr_scheduler, epoch, configs)
            save_checkpoint(configs.checkpoints_dir, 'fusion_all', model_state_dict, utils_state_dict, epoch)

        if not configs.step_lr_in_epoch:
            lr_scheduler.step()
            if tb_writer is not None:
                tb_writer.add_scalar('LR', lr_scheduler.get_lr()[0], epoch)

    if tb_writer is not None:
        tb_writer.close()
    if configs.distributed:
        cleanup()
 
                       

   
