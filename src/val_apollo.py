"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.08.17
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: Testing script
"""

import argparse
import sys
import os
import time
import warnings

warnings.filterwarnings("ignore", category=UserWarning)

from easydict import EasyDict as edict
import cv2
import torch
import numpy as np

sys.path.append('../')

from data_process.apollo_dataset import ApolloDataset
# from data_process.kitti_dataloader import create_test_dataloader
from data_process.apollo_dataloader import create_val_dataloader
from data_process.kitti_bev_utils import makeBEVMap, drawRotatedBox, get_corners
from models.model_utils import create_model
from utils.misc import make_folder, time_synchronized
from utils.evaluation_utils import decode, post_processing, draw_predictions, convert_det_to_real_values
from utils.torch_utils import _sigmoid
import config.kitti_config as cnf
from data_process.transformation import lidar_to_camera_box
from utils.visualization_utils import merge_rgb_to_bev, show_rgb_image_with_boxes
from data_process.kitti_data_utils import Calibration

import pdb

def parse_test_configs():
    parser = argparse.ArgumentParser(description='Testing config for the Implementation')
    parser.add_argument('--saved_fn', type=str, default='fpn_resnet_50', metavar='FN',
                        help='The name using for saving logs, models,...')
    parser.add_argument('-a', '--arch', type=str, default='fpn_resnet_50', metavar='ARCH',
                        help='The name of the model architecture')
    parser.add_argument('--pretrained_path', type=str,
                        default='./checkpoints_Apollo_Kitti/fpn_resnet_50/fpn_resnet_50_epoch_58.pth', metavar='PATH',
                        help='the path of the pretrained checkpoint')
    parser.add_argument('--K', type=int, default=50,
                        help='the number of top K')
    parser.add_argument('--no_cuda', action='store_true',
                        help='If true, cuda is not used.')
    parser.add_argument('--gpu_idx', default=0, type=int,
                        help='GPU index to use.')
    parser.add_argument('--num_samples', type=int, default=None,
                        help='Take a subset of the dataset to run and debug')
    parser.add_argument('--num_workers', type=int, default=1,
                        help='Number of threads for loading data')
    parser.add_argument('--batch_size', type=int, default=1,
                        help='mini-batch size (default: 4)')
    parser.add_argument('--peak_thresh', type=float, default=0.3)
    parser.add_argument('--save_test_output', action='store_true',
                        help='If true, the output image of the testing phase will be saved')
    parser.add_argument('--output_format', type=str, default='image', metavar='PATH',
                        help='the type of the test output (support image or video)')
    parser.add_argument('--output_video_fn', type=str, default='out_fpn_resnet_18', metavar='PATH',
                        help='the video filename if the output format is video')
    parser.add_argument('--output-width', type=int, default=608,
                        help='the width of showing output, the height maybe vary')

    configs = edict(vars(parser.parse_args()))
    configs.pin_memory = True
    configs.distributed = False  # For testing on 1 GPU only

    configs.input_size = (608, 608)
    configs.hm_size = (152, 152)
    configs.down_ratio = 4
    configs.max_objects = 50

    configs.imagenet_pretrained = False
    configs.head_conv = 64
    configs.num_classes = 6
    configs.num_center_offset = 2
    configs.num_z = 1
    configs.num_dim = 3
    configs.num_direction = 2  # sin, cos

    configs.heads = {
        'hm_cen': configs.num_classes,
        'cen_offset': configs.num_center_offset,
        'direction': configs.num_direction,
        'z_coor': configs.num_z,
        'dim': configs.num_dim
    }
    configs.num_input_features = 4

    ####################################################################
    ##############Dataset, Checkpoints, and results dir configs#########
    ####################################################################
    configs.root_dir = './'
    # configs.dataset_dir = os.path.join(configs.root_dir, 'dataset', 'kitti')
    configs.dataset_dir = os.path.join(configs.root_dir, 'dataset', 'Sensor')

    if configs.save_test_output:
        configs.results_dir = os.path.join(configs.root_dir, 'results', configs.saved_fn)
        make_folder(configs.results_dir)

    return configs


if __name__ == '__main__':
    configs = parse_test_configs()

    model = create_model(configs)
    print('\n\n' + '-*=' * 30 + '\n\n')
    assert os.path.isfile(configs.pretrained_path), "No file at {}".format(configs.pretrained_path)

    model.load_state_dict(torch.load(configs.pretrained_path, map_location=lambda storage, loc: storage))
    print('Loaded weights from {}\n'.format(configs.pretrained_path))

    configs.device = torch.device('cpu' if configs.no_cuda else 'cuda:{}'.format(configs.gpu_idx))
    model = model.to(device=configs.device)

    out_cap = None

    model.eval()

    val_dataloader = create_val_dataloader(configs)
    val_dataset = ApolloDataset(configs, mode='val', lidar_aug=None, hflip_prob=0., num_samples=configs.num_samples)
    with torch.no_grad():
        for batch_idx, batch_data in enumerate(val_dataloader):
            _, bev_maps, _ = batch_data
            input_bev_maps = bev_maps.to(configs.device, non_blocking=True).float()
            t1 = time_synchronized()
            outputs = model(input_bev_maps)
            outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
            outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
            # detections size (batch_size, K, 10)
            detections = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
                                outputs['dim'], K=configs.K)
            detections = detections.cpu().numpy().astype(np.float32)
            detections = post_processing(detections, configs.num_classes, configs.down_ratio, configs.peak_thresh)
            t2 = time_synchronized()

            detections = detections[0]  # only first batch
            # Draw prediction in the image
            bev_map_pre = (bev_maps.squeeze().permute(1, 2, 0).numpy() * 255).astype(np.uint8)
            bev_map_pre = cv2.resize(bev_map_pre, (cnf.BEV_WIDTH, cnf.BEV_HEIGHT))
            bev_map_pre = draw_predictions(bev_map_pre, detections.copy(), configs.num_classes)

            # Rotate the bev_map
            bev_map_pre = cv2.rotate(bev_map_pre, cv2.ROTATE_180)
            
            print('\tDone testing the {}th sample, time: {:.1f}ms, speed {:.2f}FPS'.format(batch_idx, (t2 - t1) * 1000,
                                                                                           1 / (t2 - t1)))                    
            print(str(batch_idx))
            cv2.imshow("bev_map_pre", bev_map_pre)
            # cv2.waitKey(0)

            # Apollo验证集中的真值展示
            bev_map_gt, labels = val_dataset.draw_lidar_with_label(batch_idx)
            bev_map_gt = (bev_map_gt.transpose(1, 2, 0) * 255).astype(np.uint8)
            bev_map_gt = cv2.resize(bev_map_gt, (cnf.BEV_HEIGHT, cnf.BEV_WIDTH))

            for box_idx, (cls_id, x, y, z, h, w, l, yaw) in enumerate(labels):
                # Draw rotated box
                yaw = -yaw
                y1 = int((x - cnf.boundary['minX']) / cnf.DISCRETIZATION)
                x1 = int((y - cnf.boundary['minY']) / cnf.DISCRETIZATION)
                w1 = int(w / cnf.DISCRETIZATION)
                l1 = int(l / cnf.DISCRETIZATION)

                drawRotatedBox(bev_map_gt, x1, y1, w1, l1, yaw, cnf.colors[int(cls_id)-1], cls_id-1)
            # Rotate the bev_map
            bev_map_gt = cv2.rotate(bev_map_gt, cv2.ROTATE_180)

            cv2.imshow('bev_map_gt', bev_map_gt)
            if cv2.waitKey(0) & 0xff == 27:
                break                     
            
            continue
    cv2.destroyAllWindows()
 