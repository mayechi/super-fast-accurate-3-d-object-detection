"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.08.17
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: This script for training

"""

import numpy as np
import sys
import cv2
import torch
import numba
import math

sys.path.append('./')
from models import fpn_resnet
from utils.torch_utils import _sigmoid
from utils.evaluation_utils import decode, post_processing, draw_predictions


data_from = 'apollo'
boundary = {
    "minX": -50,
    "maxX": 50,
    "minY": -25, 
    "maxY": 25,
    "minZ": -2.4,
    "maxZ": 0.6
}
BEV_WIDTH = 608
BEV_HEIGHT = 1216
DISCRETIZATION = (boundary["maxX"] - boundary["minX"]) / BEV_HEIGHT
DISCRETIZATION_Y = (boundary["maxX"] - boundary["minX"]) / BEV_HEIGHT
DISCRETIZATION_X = (boundary["maxY"] - boundary["minY"]) / BEV_WIDTH
id_path = '/media/myc/Data21/zhijiangyihao/test/test.list'
model_all_path = './checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_19_0.3.8.pth'
# model_all_path = './checkpoints_zhijiangyihao_50_no_i/Model_fpn_resnet_50_epoch_36.pth'

def main():
    num_layers = 50
    heads = {
        'hm_cen': 6,
        'cen_offset': 2,
        'direction': 2,
        'z_coor': 1,
        'dim': 3
    }    
    model_all = fpn_resnet.get_pose_net(num_layers=num_layers, heads=heads, head_conv=64,
                                        imagenet_pretrained=True)
    model_all.load_state_dict(torch.load(model_all_path, map_location=lambda storage, loc: storage))
    model_all.cuda(0)
    show_result_all(model_all)

def get_lidar(lidar_file):
  return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)

def get_filtered_lidar(sample_path, lidar, boundary, labels=None, rgb=None):
    minX = boundary['minX']
    maxX = boundary['maxX']
    minY = boundary['minY']
    maxY = boundary['maxY']
    minZ = boundary['minZ']
    maxZ = boundary['maxZ']

    # filter master
    minX_my = -3.7
    maxX_my = 0.2
    minY_my = -0.8
    maxY_my = 0.8

    # Remove master
    if 'S2_1' in sample_path:
        mask = np.where(((lidar[:, 0] <= minX_my) | (lidar[:, 0] >= maxX_my)) |
                        ((lidar[:, 1] <= minY_my) | (lidar[:, 1] >= maxY_my)))
        lidar = lidar[mask]

    # Remove the point out of range x,y,z
    mask = np.where((lidar[:, 0] >= minX) & (lidar[:, 0] <= maxX) &
                    (lidar[:, 1] >= minY) & (lidar[:, 1] <= maxY) &
                    (lidar[:, 2] >= minZ) & (lidar[:, 2] <= maxZ))
    lidar = lidar[mask]
    lidar[:, 2] = lidar[:, 2] - minZ

    
    if labels is not None:
        label_x = (labels[:, 1] >= minX) & (labels[:, 1] < maxX)
        label_y = (labels[:, 2] >= minY) & (labels[:, 2] < maxY)
        label_z = (labels[:, 3] >= minZ) & (labels[:, 3] < maxZ)
        mask_label = label_x & label_y & label_z
        labels = labels[mask_label]
        if rgb is not None:
            rgb = rgb[mask]        
            return lidar, labels, rgb
        else:
            return lidar, labels, None
    else:
        if rgb is not None:
            rgb = rgb[mask] 
            return lidar, rgb     
        else:
            return lidar, None

def makeBEVMap(PointCloud_, boundary, rgb=None):
    Height = BEV_HEIGHT + 1
    Width = BEV_WIDTH + 1

    # Discretize Feature Map
    PointCloud = np.copy(PointCloud_)
    # PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / DISCRETIZATION))
    if data_from == 'kitti':
        # 针对Kitti数据集，只检测正前方
        PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / DISCRETIZATION))
        PointCloud[:, 1] = np.int_(np.floor(PointCloud[:, 1] / DISCRETIZATION) + Width / 2)
    else:
        # 针对Apollo数据集，检测360°
        PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / DISCRETIZATION_Y) + Height / 2)
        PointCloud[:, 1] = np.int_(np.floor(PointCloud[:, 1] / DISCRETIZATION_X) + Width / 2)


    # sort-3times
    indices = np.lexsort((-PointCloud[:, 2], PointCloud[:, 1], PointCloud[:, 0]))
    PointCloud = PointCloud[indices]
    if rgb is not None:
        rgb = rgb[indices]

    # Height Map
    heightMap = np.zeros((Height, Width))

    _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
    PointCloud_frac = PointCloud[indices]
    # some important problem is image coordinate is (y,x), not (x,y)
    max_height = float(np.abs(boundary['maxZ'] - boundary['minZ']))

    heightMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = PointCloud_frac[:, 2] / max_height

    # Intensity Map & DensityMap
    intensityMap = np.zeros((Height, Width))
    densityMap = np.zeros((Height, Width))

    _, indices, counts = np.unique(PointCloud[:, 0:2], axis=0, return_index=True, return_counts=True)
    PointCloud_top = PointCloud[indices]

    normalizedCounts = np.minimum(1.0, np.log(counts + 1) / np.log(64))

    intensityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = PointCloud_top[:, 3] / 255.0 # hesai40p的反射强度0~255
    densityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = normalizedCounts

    if rgb is not None:
        # Height Map
        # ImgMap = np.zeros((Height, Width, 1))
        ImgMap = np.zeros((Height, Width, 3))
        _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
        PointCloud_frac = PointCloud[indices]        
        # rgb_frac = rgb[indices].reshape(-1, 1)
        rgb_frac = rgb[indices].reshape(-1, 3)
        ImgMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = rgb_frac / 255.0
        ImgMap = np.transpose(ImgMap, (2, 0, 1))
        # RGB_Map = np.zeros((3, Height - 1, Width - 1))
        RGB_Map = np.zeros((3, Height - 1, Width - 1))
        
        RGB_Map[0:3, :, :] = ImgMap[:, :BEV_HEIGHT, :BEV_WIDTH]
        # RGB_Map[1, :, :] = ImgMap[:, :BEV_HEIGHT, :BEV_WIDTH]

        # RGB_Map[4, :, :] = densityMap[:BEV_HEIGHT, :BEV_WIDTH]  # r_map
        # RGB_Map[3, :, :] = heightMap[:BEV_HEIGHT, :BEV_WIDTH]  # g_map
        # RGB_Map[0, :, :] = intensityMap[:BEV_HEIGHT, :BEV_WIDTH]  # b_map     
    else:
        if data_from == 'kitti':        
            RGB_Map = np.zeros((3, Height - 1, Width - 1))
            RGB_Map[2, :, :] = densityMap[:BEV_HEIGHT, :BEV_WIDTH]  # r_map
            RGB_Map[1, :, :] = heightMap[:BEV_HEIGHT, :BEV_WIDTH]  # g_map
            RGB_Map[0, :, :] = intensityMap[:BEV_HEIGHT, :BEV_WIDTH]  # b_map
        else:
            RGB_Map = np.zeros((3, Height - 1, Width - 1))
            RGB_Map[2, :, :] = densityMap[:BEV_HEIGHT, :BEV_WIDTH]  # r_map
            RGB_Map[1, :, :] = heightMap[:BEV_HEIGHT, :BEV_WIDTH]  # g_map
            # RGB_Map[0, :, :] = intensityMap[:BEV_HEIGHT, :BEV_WIDTH]  # b_map            

    return RGB_Map

@numba.jit(nopython=True)
def make_bev_voxel_no_i(PointCloud_, rgb=None):
    minX = -50
    maxX = 50
    minY = -25
    maxY = 25
    minZ = -2.28
    maxZ = 0.72

    Height = 1216
    Width = 608
    Deep = 10  
    CLOUD_R = PointCloud_.shape[0]
    bev_map = np.zeros((4, 10, Height, Width))
    
    bev_map_per_index = np.zeros((Height, Width), dtype=np.int32)

    # voxel
    voxel_num = np.zeros((10, Height, Width), dtype=np.int32)
    voxel_all_feature = np.zeros((3, 10, Height, Width))
    voxel_ave_feature = np.zeros((4, 10, Height, Width))

    # 将点云按照z从高到低排序
    indices = np.argsort(-PointCloud_[:, 2])
    PointCloud_ = PointCloud_[indices]  
    range_list = list(range(CLOUD_R)) 
    random_shuffle_list = range_list

    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]
        z_ori = z + minZ

        row = Height * (x - minX) / (maxX - minX)
        col = Width * (y - minY) / (maxY - minY)
        r = int(math.floor(row))
        c = int(math.floor(col))

        deep = Deep * (z_ori - minZ) / (maxZ - minZ)
        d = int(math.floor(deep))

        if r >= Height or c >= Width or d >= Deep:
            continue
        bev_map_per_index[r, c] = 1

        voxel_num[d, r, c] += 1
        voxel_all_feature[0, d, r, c] += x
        voxel_all_feature[1, d, r, c] += y
        voxel_all_feature[2, d, r, c] += z

        voxel_ave_feature[0, d, r, c] = voxel_all_feature[0, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[1, d, r, c] = voxel_all_feature[1, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[2, d, r, c] = voxel_all_feature[2, d, r, c] / voxel_num[d, r, c]

        # add density feature
        voxel_ave_feature[3, d, r, c] = np.minimum(1.0, np.log(voxel_num[d, r, c] + 1) / np.log(64))
    
    # 三个维度，可以翻转
    bev_map = voxel_ave_feature.reshape(-1, Height, Width)
    return bev_map, bev_map_per_index 

def show_result_all(model_all):

	# switch mode
	model_all.eval()

	sample_path_list = [x[0:-1] for x in open(id_path).readlines()]
	for sample_path in sample_path_list:
		lidarData = get_lidar(sample_path)
		get_filtered_lidar(sample_path, lidarData, boundary)

		lidarData, _ = get_filtered_lidar(sample_path, lidarData, boundary)
		bev_maps_ori = makeBEVMap(lidarData, boundary)
		bev_maps_ori = torch.from_numpy(bev_maps_ori)

		bev_maps, bev_map_per_index = make_bev_voxel_no_i(lidarData)

		bev_maps = torch.from_numpy(bev_maps)

		bev_maps = bev_maps.view(4, 10, BEV_HEIGHT*BEV_WIDTH)
		bev_map_per_index = bev_map_per_index.reshape(-1)

		if 'apollo' in sample_path:
			frame_id = sample_path.split('/')[-2] + '_' + sample_path.split('/')[-1].split('.bin')[0]
		else:
			frame_id = sample_path.split('/')[-1].split('.bin')[0]
		metadatas = {
			'frame_id': frame_id
		}

		with torch.no_grad():
			frame_id = metadatas['frame_id']

			input_bev_maps = bev_maps.to(0, non_blocking=True).float()
			input_bev_maps = torch.unsqueeze(input_bev_maps, 0)      
			outputs = model_all(input_bev_maps)

			outputs['hm_cen'] = _sigmoid(outputs['hm_cen'])
			outputs['cen_offset'] = _sigmoid(outputs['cen_offset'])
			detections, all_scores = decode(outputs['hm_cen'], outputs['cen_offset'], outputs['direction'], outputs['z_coor'],
											outputs['dim'], K=50)
			detections = detections.cpu().numpy().astype(np.float32)
			detections_post = post_processing(detections, 6, 2, 0.2)

			# Draw prediction in the image
			bev_map = (bev_maps_ori.permute(1, 2, 0).numpy() * 255).astype(np.uint8)
			bev_map = cv2.resize(bev_map, (608, 1216))
			bev_map_copy = bev_map.copy()
			bev_map = draw_predictions(bev_map, detections_post[0], 6)
			# Rotate the bev_map
			# bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
			bev_map_copy = cv2.rotate(bev_map_copy, cv2.ROTATE_180)
			bev_map_copy = cv2.resize(bev_map_copy, (500, 1000))
			# cv2.imwrite('/home/myc/桌面/temp/fusion_test/lidar_result/' + frame_id + '.jpg', bev_map)
			cv2.imshow('oir', bev_map_copy)
			cv2.imshow('result', bev_map)
			cv2.waitKey(0)            

if __name__ == '__main__':
    main()
