import pickle
import pdb
from pathlib import Path

import numpy as np
import math
from det3d.core import box_np_ops

# For zhijiangyihao
data_path = './dataset/zhijiangyihao'
lidar_dir = './dataset/zhijiangyihao/training2/pointcloud/bin/'
label_dir = './dataset/zhijiangyihao/training2/pointcloud/label/'
id_path = './dataset/zhijiangyihao/training2/pointcloud/ImageSets/train_id.txt'
sample_lidar_path_list = [lidar_dir+x[0:-1]+'.bin' for x in open(id_path).readlines()]
sample_label_list = [label_dir+x[0:-1]+'.txt' for x in open(id_path).readlines()]

# For apollo
# data_path = './dataset/apollo'
# lidar_apollo_path = './dataset/apollo/ImageSets/train.txt'
# label_apollo_path = './dataset/apollo/ImageSets/train_label.txt'
# sample_lidar_path_list = [x[0:-1] for x in open(lidar_apollo_path).readlines()]
# sample_label_list = [x[0:-1] for x in open(label_apollo_path).readlines()]
# sample_lidar_path_list = sample_lidar_path_list[0:100]
# sample_label_list = sample_label_list[0:100]

# For simulation
# data_path = './dataset/zhijiangyihao_si'
# lidar_apollo_path = './dataset/zhijiangyihao_si/Truck_data/ImageSets/train.txt'
# label_apollo_path = './dataset/zhijiangyihao_si/Truck_data/ImageSets/train_label.txt'
# sample_lidar_path_list = [x[0:-1] for x in open(lidar_apollo_path).readlines()]
# sample_label_list = [x[0:-1] for x in open(label_apollo_path).readlines()]

# prepare dbinfo_path and db_path.
db_path = './dataset/db_sample'
root_path = Path(db_path)
db_path = root_path / "gt_database"
dbinfo_path = root_path / "dbinfos_train_zhijiangyihao.pkl"
db_path.mkdir(parents=True, exist_ok=True)

all_db_infos = {}
group_counter = 0

for index in range(len(sample_lidar_path_list)):
    lidar_file = sample_lidar_path_list[index]
    label_file = sample_label_list[index]
    # For zhijiangyihao name
    id_name = lidar_file.split('/')[-1].split('.bin')[0]
    # For apollo name
    # id_name = lidar_file.split('/')[-2] + '_' + lidar_file.split('/')[-1].split('.bin')[0]

    points = np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)
    gt_boxes_list, names_list = [], []
    # For si data
    # flag_label_dis = True
    for line in open(label_file, 'r'):
        line = line.rstrip()
        line_parts = line.split(' ')
        cat_id = line_parts[0] # 1 for small vehicles, 2 for big vehicles, 3 for pedestrian, 4 for motorcyclist and bicyclist, 5 for traffic cones and 6 for others
        x, y, z = float(line_parts[1]), float(line_parts[2]), float(line_parts[3])
        l, w, h = float(line_parts[4]), float(line_parts[5]), float(line_parts[6])
        ry = float(line_parts[7])
        # flag_label_dis = math.sqrt(x*x + y*y) > 5
        # if flag_label_dis == False:
        #     break
        gt_boxes_list.append([x, y, z, w, l, h, -ry-math.pi/2])
        names_list.append(cat_id)
    # if flag_label_dis == False:
    #     continue
    if len(gt_boxes_list) == 0:
        continue
    gt_boxes = np.array(gt_boxes_list, dtype=np.float32)
    names = np.array(names_list)

    group_dict = {}
    group_ids = np.full([gt_boxes.shape[0]], -1, dtype=np.int64)
    group_ids = np.arange(gt_boxes.shape[0], dtype=np.int64)
    difficulty = np.zeros(gt_boxes.shape[0], dtype=np.int32)
    num_obj = gt_boxes.shape[0]

    # todo: maybe we need add some contexual points here.
    offset = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]   # [x, y, z, w, l, h, ry]
    point_indices_for_num = box_np_ops.points_in_rbbox(points, gt_boxes)
    point_indices = box_np_ops.points_in_rbbox(points, gt_boxes + offset)
    for i in range(num_obj):   # in a single scene.
        filename = f"{id_name}_{names[i]}_{i}.bin"
        filepath = db_path / filename
        # if filename == "1628817765934_3_24.bin":
        #     a = 0
        gt_points = points[point_indices[:, i]]
        num_points_in_gt = point_indices_for_num[:, i].sum()
        gt_points[:, :3] -= gt_boxes[i, :3]  # only record relative distance
        with open(filepath, "w") as f:       # db: gt points in each gt_box are saved
            gt_points[:, :4].tofile(f)
        db_dump_path = str(db_path.stem + "/" + filename)

        db_info = {
            "name": names[i],
            "path": db_dump_path,
            "image_idx": id_name,
            "gt_idx": i,
            "box3d_lidar": gt_boxes[i],
            "num_points_in_gt": num_points_in_gt,
            "difficulty": difficulty[i]  # todo: not accurate, all are set as 0.
        }
        local_group_id = group_ids[i]
        if local_group_id not in group_dict:
            group_dict[local_group_id] = group_counter
            group_counter += 1
        db_info["group_id"] = group_dict[local_group_id]
        if names[i] in all_db_infos:                 # all_db_infos are grouped by class_names (like car, pedestrian, cyclist, cycle)
            all_db_infos[names[i]].append(db_info)   # all db infos include info of all db
        else:
            all_db_infos[names[i]] = [db_info]
with open(dbinfo_path, "wb") as f:
    pickle.dump(all_db_infos, f)    