"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.08.17
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: This script for the KITTI dataset
"""

import sys
import os
import math
import random
from builtins import int

import numpy as np
from torch.utils.data import Dataset
import pickle
import cv2
import torch

sys.path.append('./src/')

from data_process.kitti_data_utils import gen_hm_radius, compute_radius, Calibration, get_filtered_lidar, get_filtered_lidar_simulation
from data_process.kitti_bev_utils import makeBEVMap, make_bev_ma, make_bev_voxel, make_bev_voxel_no_i, make_bev_voxel_no_i_add_adaptive_and_var, drawRotatedBox, get_corners, filter_label
from data_process import transformation
from data_process.universal_data_utils import voxel_filter, wirte_label
from data_process.waymo.data_utils import *
# import config.kitti_config as cnf
import config.apollo_config as cnf
# import config.simulation_config as cnf
import pdb
import time

class ApolloProcessDataset(Dataset):
    def __init__(self, configs, mode='train', lidar_aug=None, hflip_prob=None, num_samples=None):
        self.input_size = configs.input_size
        self.hm_size = configs.hm_size

        self.num_classes = configs.num_classes
        self.max_objects = configs.max_objects

        assert mode in ['train', 'val', 'test'], 'Invalid mode: {}'.format(mode)
        self.mode = mode
        self.is_test = (self.mode == 'test')

        self.lidar_aug = lidar_aug
        self.hflip_prob = hflip_prob
        self.iou_threshold = [-1.0, 0.1, 0.3, 0.3, 0.3, 0.3, 0.3]
        self.points_num = 512

        if mode == 'train':
            # self.lidar_label_path = './dataset/apollo/lidar_rcnn/data_processer_val/target/val.pkl'
            # self.lidar_label_path = './dataset/apollo/lidar_rcnn/data_processer/target/train.pkl'
            self.lidar_label_path = './dataset/apollo/lidar_rcnn/data_processer/target/train_person.pkl'
            # self.lidar_label_path = './dataset/apollo/lidar_rcnn/data_processer_val/target/val_person.pkl'

        if mode == 'val':
            # self.lidar_label_path = './dataset/apollo/lidar_rcnn/data_processer_val/target/val.pkl'
            # self.lidar_label_path = './dataset/apollo/lidar_rcnn/data_processer/target/train.pkl'
            self.lidar_label_path = './dataset/apollo/lidar_rcnn/data_processer_val/target/val_person.pkl'

        elif mode == 'test':
            # self.lidar_label_path = './dataset/apollo/lidar_rcnn/data_processer_val/target/val.pkl'
            # self.lidar_label_path = './dataset/apollo/lidar_rcnn/data_processer/target/train.pkl'
            self.lidar_label_path = './dataset/apollo/lidar_rcnn/data_processer_val/target/val_person.pkl'

        if mode == 'train' or mode == 'val':
            with open(self.lidar_label_path, 'rb') as fo:
                self.sample_list = pickle.load(fo, encoding='bytes')            

        elif mode == 'test':
            with open(self.lidar_label_path, 'rb') as fo:
                self.sample_list = pickle.load(fo, encoding='bytes') 

        # num_samples = 256
        if num_samples is not None:
            self.sample_list = self.sample_list[:num_samples]
        self.num_samples = len(self.sample_list)

    def __len__(self):
        return len(self.sample_list)

    def __getitem__(self, index):
        if self.mode == 'test':
            return self.load_data_only(index)
        else:
            return self.load_data_with_targets(index)

    def load_data_only(self, index):
        data_group = self.sample_list[index]
        frame_id, pcd, proposal, gt_box, gt_cls = data_group
        if gt_cls == 3.0:
            gt_cls = 1
            gt_box[-1] = 1.0
        # if gt_box[-1] == 1:
        #     print(gt_cls)
        gt_cls = relabel_by_iou(proposal, gt_box, gt_cls, self.iou_threshold)
        if gt_cls == 0:
            gt_box[-1] = 0
        proposal_ori = proposal.copy()
        proposal = proposal.astype(np.single)[:7]
        pcd = pcd.astype(np.single)[:, [0, 1, 2]]
        point_set = process_pcd(pcd, proposal, self.points_num)
        return frame_id, point_set.astype(np.float32), proposal.astype(np.float32), proposal_ori.astype(np.float32), gt_box.astype(np.float32), gt_cls

    def load_data_with_targets(self, index):
        data_group = self.sample_list[index]
        _, pcd, proposal, gt_box, gt_cls = data_group
        if gt_cls == 3.0:
            gt_cls = 1
            gt_box[-1] = 1.0
        # print('gt_cls', gt_cls)
        pcd = pcd.astype(np.single)[:, [0, 1, 2]]
        proposal = proposal.astype(np.single)[:7]
        gt_box = gt_box.astype(np.single)[:7]
        # if gt_cls == 1 or gt_cls == 2:
        #     # only use jitter for vechilel
        #     proposal = jitter(proposal, 0.5)
        gt_cls = relabel_by_iou(proposal, gt_box, gt_cls, self.iou_threshold)
        if gt_cls == 0:
            gt_box[-1] = 0
        # set proposals without points as nagtive
        if pcd.shape[0] == 0:
            gt_cls = 0
        point_set = process_pcd(pcd, proposal, self.points_num)
        # heading residual
        gt_box[-1] = get_heading_residual(gt_box[-1], proposal[-1])

        # move gt box to pred center
        gt_box[:3] -= proposal[:3]
        gt_box[:2] = rotz(-proposal[-1]) @ gt_box[:2]
        return point_set.astype(np.float32), proposal.astype(np.float32), gt_cls, gt_box.astype(np.float32)        
         
    def get_image(self, idx):
        img_path = os.path.join(self.image_dir, '{:06d}.png'.format(idx))
        img = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)

        return img_path, img

    def get_lidar(self, lidar_file):
    #     point_cloud = np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)
    #     point_cloud = voxel_filter(point_cloud, cnf.leaf_size, random=False)
        return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)
        # return point_cloud

    def draw_lidar_with_label(self, index):
        sample_path = self.sample_path_list[index]
        print('sample_path:', sample_path)
        lidarData = self.get_lidar(sample_path)
        labels, has_labels = self.get_label(index)
        # if has_labels:
        #     labels[:, -1] = -labels[:, -1] - np.pi / 2

        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])

        lidarData, labels, _ = get_filtered_lidar(lidarData, cnf.boundary, labels)
        # lidarData, labels = get_filtered_lidar_simulation(lidarData, cnf.boundary, labels)
        bev_map = makeBEVMap(lidarData, cnf.boundary)

        return bev_map, labels

    def get_label(self, idx):
        labels = []
        label_path = self.sample_label_list[idx]
        # print("label_path:", label_path)
        for line in open(label_path, 'r'):
            line = line.rstrip()
            line_parts = line.split(' ')
            cat_id = int(line_parts[0])  # 1 for small vehicles, 2 for big vehicles, 3 for pedestrian, 4 for motorcyclist and bicyclist, 5 for traffic cones and 6 for others
            # print("cat_id:", cat_id)
            if cat_id <= -99:  # ignore Tram and Misc
                continue
            x, y, z = float(line_parts[1]), float(line_parts[2]), float(line_parts[3])
            l, w, h = float(line_parts[4]), float(line_parts[5]), float(line_parts[6])
            # w, l, h = float(line_parts[4]), float(line_parts[5]), float(line_parts[6])
            ry = float(line_parts[7])

            object_label = [cat_id, x, y, z, h, w, l, ry]
            labels.append(object_label)

        if len(labels) == 0:
            labels = np.zeros((1, 8), dtype=np.float32)
            has_labels = False
        else:
            labels = np.array(labels, dtype=np.float32)
            has_labels = True

        return labels, has_labels

    # def get_label(self, idx):
    #     labels = []
    #     label_path = self.sample_label_list[idx]
    #     # print("label_path:", label_path)
    #     for line in open(label_path, 'r'):
    #         line = line.rstrip()
    #         line_parts = line.split(' ')
    #         cat_id = int(line_parts[-1])  # 1 for small vehicles, 2 for big vehicles, 3 for pedestrian, 4 for motorcyclist and bicyclist, 5 for traffic cones and 6 for others
    #         # print("cat_id:", cat_id)
    #         if cat_id <= -99:  # ignore Tram and Misc
    #             continue
    #         x, y, z = float(line_parts[0]), float(line_parts[1]), float(line_parts[2])
    #         w, h, l = float(line_parts[3]), float(line_parts[4]), float(line_parts[5])
    #         ry = float(line_parts[6])

    #         object_label = [cat_id, x, y, z, h, w, l, ry]
    #         labels.append(object_label)

    #     if len(labels) == 0:
    #         labels = np.zeros((1, 8), dtype=np.float32)
    #         has_labels = False
    #     else:
    #         labels = np.array(labels, dtype=np.float32)
    #         has_labels = True

    #     return labels, has_labels

    def build_targets(self, labels, hflipped):
        minX = cnf.boundary['minX']
        maxX = cnf.boundary['maxX']
        minY = cnf.boundary['minY']
        maxY = cnf.boundary['maxY']
        minZ = cnf.boundary['minZ']
        maxZ = cnf.boundary['maxZ']

        num_objects = min(len(labels), self.max_objects)
        hm_l, hm_w = self.hm_size

        hm_main_center = np.zeros((self.num_classes, hm_l, hm_w), dtype=np.float32)
        cen_offset = np.zeros((self.max_objects, 2), dtype=np.float32)
        direction = np.zeros((self.max_objects, 2), dtype=np.float32)
        z_coor = np.zeros((self.max_objects, 1), dtype=np.float32)
        dimension = np.zeros((self.max_objects, 3), dtype=np.float32)

        indices_center = np.zeros((self.max_objects), dtype=np.int64)
        obj_mask = np.zeros((self.max_objects), dtype=np.uint8)

        for k in range(num_objects):
            cls_id, x, y, z, h, w, l, yaw = labels[k]
            cls_id = int(cls_id)-1
            # Invert yaw angle
            yaw = -yaw
            if not ((minX <= x <= maxX) and (minY <= y <= maxY) and (minZ <= z <= maxZ)):
                continue
            if (h <= 0) or (w <= 0) or (l <= 0):
                continue

            bbox_l = l / cnf.bound_size_x * hm_l
            bbox_w = w / cnf.bound_size_y * hm_w
            radius = compute_radius((math.ceil(bbox_l), math.ceil(bbox_w)))
            radius = max(0, int(radius))

            center_y = (x - minX) / cnf.bound_size_x * hm_l  # x --> y (invert to 2D image space)
            center_x = (y - minY) / cnf.bound_size_y * hm_w  # y --> x
            center = np.array([center_x, center_y], dtype=np.float32)

            if hflipped:
                center[0] = hm_w - center[0] - 1

            center_int = center.astype(np.int32)
            if cls_id < 0:
                ignore_ids = [_ for _ in range(self.num_classes)] if cls_id == - 1 else [- cls_id - 2]
                # Consider to make mask ignore
                for cls_ig in ignore_ids:
                    gen_hm_radius(hm_main_center[cls_ig], center_int, radius)
                hm_main_center[ignore_ids, center_int[1], center_int[0]] = 0.9999
                continue
            
            try:
                # Generate heatmaps for main center
                gen_hm_radius(hm_main_center[cls_id], center, radius)
                # Index of the center
                indices_center[k] = center_int[1] * hm_w + center_int[0]
            except:
                a = 0

            # targets for center offset
            cen_offset[k] = center - center_int

            # targets for dimension
            dimension[k, 0] = h
            dimension[k, 1] = w
            dimension[k, 2] = l

            # targets for direction
            direction[k, 0] = math.sin(float(yaw))  # im
            direction[k, 1] = math.cos(float(yaw))  # re
            # im -->> -im
            if hflipped:
                direction[k, 0] = - direction[k, 0]

            # targets for depth
            z_coor[k] = z - minZ

            # Generate object masks
            obj_mask[k] = 1

        targets = {
            'hm_cen': hm_main_center,
            'cen_offset': cen_offset,
            'direction': direction,
            'z_coor': z_coor,
            'dim': dimension,
            'indices_center': indices_center,
            'obj_mask': obj_mask,
        }

        return targets

    def build_targets_spherical(self, labels, hflipped):
        minX = cnf.boundary['minX']
        maxX = cnf.boundary['maxX']
        minY = cnf.boundary['minY']
        maxY = cnf.boundary['maxY']
        minZ = cnf.boundary['minZ']
        maxZ = cnf.boundary['maxZ']

        num_objects = min(len(labels), self.max_objects)
        hm_l, hm_w = self.hm_size

        hm_main_center = np.zeros((self.num_classes, hm_l, hm_w), dtype=np.float32)
        cen_offset = np.zeros((self.max_objects, 2), dtype=np.float32)
        direction = np.zeros((self.max_objects, 2), dtype=np.float32)
        z_coor = np.zeros((self.max_objects, 1), dtype=np.float32)
        dimension = np.zeros((self.max_objects, 3), dtype=np.float32)

        indices_center = np.zeros((self.max_objects), dtype=np.int64)
        obj_mask = np.zeros((self.max_objects), dtype=np.uint8)

        for k in range(num_objects):
            cls_id, x, y, z, h, w, l, yaw = labels[k]
            cls_id = int(cls_id)-1
            # Invert yaw angle
            yaw = -yaw
            if not ((minX <= x <= maxX) and (minY <= y <= maxY) and (minZ <= z <= maxZ)):
                continue
            if (h <= 0) or (w <= 0) or (l <= 0):
                continue

            bbox_l = l / cnf.bound_size_x * hm_l
            bbox_w = w / cnf.bound_size_y * hm_w
            radius = compute_radius((math.ceil(bbox_l), math.ceil(bbox_w)))
            radius = max(0, int(radius))

            center_y = (x - minX) / cnf.bound_size_x * hm_l  # x --> y (invert to 2D image space)
            center_x = (y - minY) / cnf.bound_size_y * hm_w  # y --> x
            center = np.array([center_x, center_y], dtype=np.float32)

            if hflipped:
                center[0] = hm_w - center[0] - 1

            center_int = center.astype(np.int32)
            if cls_id < 0:
                ignore_ids = [_ for _ in range(self.num_classes)] if cls_id == - 1 else [- cls_id - 2]
                # Consider to make mask ignore
                for cls_ig in ignore_ids:
                    gen_hm_radius(hm_main_center[cls_ig], center_int, radius)
                hm_main_center[ignore_ids, center_int[1], center_int[0]] = 0.9999
                continue

            # Generate heatmaps for main center
            gen_hm_radius(hm_main_center[cls_id], center, radius)
            # Index of the center
            indices_center[k] = center_int[1] * hm_w + center_int[0]

            # targets for center offset
            cen_offset[k] = center - center_int

            # targets for dimension
            dimension[k, 0] = h
            dimension[k, 1] = w
            dimension[k, 2] = l

            # targets for direction
            direction[k, 0] = math.sin(float(yaw))  # im
            direction[k, 1] = math.cos(float(yaw))  # re
            # im -->> -im
            if hflipped:
                direction[k, 0] = - direction[k, 0]

            # targets for depth
            z_coor[k] = z - minZ

            # Generate object masks
            obj_mask[k] = 1

        targets = {
            'hm_cen': hm_main_center,
            'cen_offset': cen_offset,
            'direction': direction,
            'z_coor': z_coor,
            'dim': dimension,
            'indices_center': indices_center,
            'obj_mask': obj_mask,
        }

        return targets


if __name__ == '__main__':
    from easydict import EasyDict as edict
    from data_process.transformation import OneOf, Random_Scaling, Random_Rotation, lidar_to_camera_box
    from utils.visualization_utils import merge_rgb_to_bev, show_rgb_image_with_boxes

    configs = edict()
    configs.distributed = False  # For testing
    configs.pin_memory = False
    configs.num_samples = None
    # configs.input_size = (608, 608)
    configs.input_size = (1216, 608)
    # configs.hm_size = (152, 152)
    configs.hm_size = (304, 152)
    configs.max_objects = 50
    configs.num_classes = 3
    configs.output_width = 608

    # lidar_aug = OneOf([
    #     Random_Rotation(limit_angle=np.pi / 4, p=1.),
    #     Random_Scaling(scaling_range=(0.95, 1.05), p=1.),
    # ], p=1.)
    lidar_aug = None

    dataset = ApolloDataset(configs, mode='train', lidar_aug=lidar_aug, hflip_prob=0., num_samples=configs.num_samples)

    print('\n\nPress n to see the next sample >>> Press Esc to quit...')

    # write label for val
    # for idx in range(len(dataset)): 
    #     print(idx)
    #     dataset.load_img_with_targets(idx)
        
    # base_path = '/home/mayechi/Data/Sensor/20210119170143/testing/'
    # f_train = open(base_path + 't.txt', 'w')                

    for idx in range(len(dataset)): 
        # if idx < 15               
        # if idx < 549:
        #     continue
        print('idx:', idx+1)
        bev_map, labels = dataset.draw_lidar_with_label(idx) 
        bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
        # labels = filter_label(bev_map, labels)
        # print('labels num:', len(labels))
        if len(labels) == 0:
            continue
        # bev_map = cv2.resize(bev_map, (cnf.BEV_HEIGHT, cnf.BEV_WIDTH))
        bev_map = cv2.resize(bev_map, (cnf.BEV_WIDTH, cnf.BEV_HEIGHT))
        bev_map_ori = bev_map.copy()

        for box_idx, (cls_id, x, y, z, h, w, l, yaw) in enumerate(labels):
            # Draw rotated box
            yaw = -yaw
            y1 = int((x - cnf.boundary['minX']) / cnf.DISCRETIZATION_Y)
            x1 = int((y - cnf.boundary['minY']) / cnf.DISCRETIZATION_X)
            w1 = int(w / cnf.DISCRETIZATION_X)
            l1 = int(l / cnf.DISCRETIZATION_Y)
            # # bev 的四个顶点写下
            # corners = get_corners(x1, y1, w1, l1, yaw)

            drawRotatedBox(bev_map, x1, y1, w1, l1, yaw, cnf.colors[int(cls_id)], cls_id)
        # Rotate the bev_map 
        bev_map_ori = cv2.rotate(bev_map_ori, cv2.ROTATE_180)
        bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
        bev_map_ori = cv2.resize(bev_map_ori, (500, 1000))
        # bev_map = cv2.resize(bev_map, (int(cnf.BEV_WIDTH/1.5), int(cnf.BEV_HEIGHT/1.5)))
        cv2.imshow('bev_map_ori', bev_map_ori)
        cv2.imshow('bev_map', bev_map)

        keyboard = cv2.waitKey(0)
        # if keyboard == ord('a'):
        #     # with open(base_path + 'train_use.txt', 'w') as f_train:
        #     f_train.write(dataset.sample_path_list[idx])
        #     f_train.write('\n')
        #     # with open(base_path + 'train_label_use.txt', 'w') as f_train_label:
        #     f_train_label.write(dataset.sample_label_list[idx])
        #     f_train_label.write('\n')
        # else:
        #     continue
        if cv2.waitKey(0) & 0xff == 27:
            break
        elif cv2.waitKey(0) & 0xff == ord('2'):
            cv2.imwrite('/home/mayechi/桌面/write/2/'+str(idx)+'.bmp', bev_map_ori)
    # f_train.close()
    # f_train_label.close()
       