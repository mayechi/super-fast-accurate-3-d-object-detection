import os
import re
import struct
import warnings

import lzf
import numpy as np
import shutil

numpy_pcd_type_mappings = [(np.dtype('float32'), ('F', 4)),
                           (np.dtype('float64'), ('F', 8)),
                           (np.dtype('uint8'), ('U', 1)),
                           (np.dtype('uint16'), ('U', 2)),
                           (np.dtype('uint32'), ('U', 4)),
                           (np.dtype('uint64'), ('U', 8)),
                           (np.dtype('int16'), ('I', 2)),
                           (np.dtype('int32'), ('I', 4)),
                           (np.dtype('int64'), ('I', 8))]
numpy_type_to_pcd_type = dict(numpy_pcd_type_mappings)
pcd_type_to_numpy_type = dict((q, p) for (p, q) in numpy_pcd_type_mappings)

def read_txt(filename):
    """ Reads and txt file and return the elements as pandas Dataframes.
    Parameters
    ----------
    filename: str
        Path to the txt file.
    Returns
    -------
    pandas Dataframe.
    """
    points = []
    for line in open(filename, 'r'):
        line = line.rstrip()
        line_parts = line.split(' ')
        y, z, x = -float(line_parts[0])/1000, float(line_parts[1])/1000, float(line_parts[2])/1000
        intensity = 0.0
        points.append([x, y, z, intensity])
    points_array = np.array(points)

    return points_array

def save_pointcloud_bin(pointcloud, save_path, measurement_time):
    # points = pointcloud.point
    points = pointcloud
    header = {
        'version': .7,
        'fields': ['x', 'y', 'z', 'intensity'],
        'count': [1, 1, 1, 1],
        'width': len(points),
        'height': 1,
        'viewpoint': [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
        'points': len(points),
        'type': ['F', 'F', 'F', 'U'],
        'size': [4, 4, 4, 4],
        'data': 'binary'
    }
    typenames = []
    for pcd_type, numpy_type in zip(header['type'], header['size']):
        numpy_type = pcd_type_to_numpy_type[(pcd_type, numpy_type)]
        typenames.append(numpy_type)
    np_dtype = np.dtype(list(zip(header['fields'], typenames)))
    arr = np.zeros(len(points), dtype=np_dtype)
    for i, point in enumerate(points):
        # change timestamp to timestamp_sec
        arr[i] = (
            point[0],# point.x,
            point[1],# point.y,
            point[2],# point.z,
            point[3],# point.intensity,
        )
    bin_file = os.path.join(
        save_path,
        measurement_time + '.bin')
    arr.tofile(bin_file)

def save_as_pcd(scan, path):
  if os.path.exists(path):
    os.remove(path)

  handle = open(path, 'a')
  header = ['# .PCD v0.7 - Point Cloud Data file format\n', 'VERSION 0.7\n', 'FIELDS x y z i\n', 'SIZE 4 4 4 4\n', 'TYPE F F F F\n', 'COUNT 1 1 1 1\n']
  handle.writelines(header)
  handle.write('WIDTH ' + str(scan.shape[0]) + '\n')
  handle.write('HEIGHT 1\n')
  handle.write('VIEWPOINT 0 0 0 1 0 0 0\n')
  handle.write('POINTS ' + str(scan.shape[0]) + '\n')
  handle.write('DATA ascii\n')

  for i in range(scan.shape[0]):
    pt_str = str(scan[i, 0]) + ' ' + str(scan[i, 1]) + ' ' + str(scan[i, 2]) + ' ' + str(scan[i, 3] / 255.0) + '\n'
    handle.write(pt_str)

  handle.close()

if __name__ == "__main__":
    txt_dir_path = '/media/myc/Data21/zhijiangyihao/zkhy/SmarterEye_Data/pointcloud/'
    bin_dir_path = '/media/myc/Data21/zhijiangyihao/zkhy/SmarterEye_Data/bin/'
    pcd_dir_path = '/media/myc/Data21/zhijiangyihao/zkhy/SmarterEye_Data/pcd/'

    txt_path_list = os.listdir(txt_dir_path)
    # txt_path_list.sorted()
    for i in range(len(txt_path_list)):
        txt_data = read_txt(txt_dir_path+txt_path_list[i])
        name = txt_path_list[i].split('.txt')[0]
        save_pointcloud_bin(txt_data, bin_dir_path, name)
        save_as_pcd(txt_data, pcd_dir_path+name+'.pcd')


