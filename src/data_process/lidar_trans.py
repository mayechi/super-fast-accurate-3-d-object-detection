import os
from nbformat import write
import numpy as np
from scipy.spatial.transform import Rotation as R

numpy_pcd_type_mappings = [(np.dtype('float32'), ('F', 4)),
                           (np.dtype('float64'), ('F', 8)),
                           (np.dtype('uint8'), ('U', 1)),
                           (np.dtype('uint16'), ('U', 2)),
                           (np.dtype('uint32'), ('U', 4)),
                           (np.dtype('uint64'), ('U', 8)),
                           (np.dtype('int16'), ('I', 2)),
                           (np.dtype('int32'), ('I', 4)),
                           (np.dtype('int64'), ('I', 8))]
numpy_type_to_pcd_type = dict(numpy_pcd_type_mappings)
pcd_type_to_numpy_type = dict((q, p) for (p, q) in numpy_pcd_type_mappings)

def save_as_pcd(scan, path):
  if os.path.exists(path):
    os.remove(path)

  handle = open(path, 'a')
  header = ['# .PCD v0.7 - Point Cloud Data file format\n', 'VERSION 0.7\n', 'FIELDS x y z i\n', 'SIZE 4 4 4 4\n', 'TYPE F F F F\n', 'COUNT 1 1 1 1\n']
  handle.writelines(header)
  handle.write('WIDTH ' + str(scan.shape[0]) + '\n')
  handle.write('HEIGHT 1\n')
  handle.write('VIEWPOINT 0 0 0 1 0 0 0\n')
  handle.write('POINTS ' + str(scan.shape[0]) + '\n')
  handle.write('DATA ascii\n')

  for i in range(scan.shape[0]):
    pt_str = str(scan[i, 0]) + ' ' + str(scan[i, 1]) + ' ' + str(scan[i, 2]) + ' ' + str(scan[i, 3] / 255.0) + '\n'
    handle.write(pt_str)

  handle.close()

def save_as_bin(pointcloud, save_path):
    # points = pointcloud.point
    points = pointcloud
    header = {
        'version': .7,
        'fields': ['x', 'y', 'z', 'intensity'],
        'count': [1, 1, 1, 1],
        'width': len(points),
        'height': 1,
        'viewpoint': [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
        'points': len(points),
        'type': ['F', 'F', 'F', 'U'],
        'size': [4, 4, 4, 4],
        'data': 'binary'
    }
    typenames = []
    for pcd_type, numpy_type in zip(header['type'], header['size']):
        numpy_type = pcd_type_to_numpy_type[(pcd_type, numpy_type)]
        typenames.append(numpy_type)
    np_dtype = np.dtype(list(zip(header['fields'], typenames)))
    arr = np.zeros(len(points), dtype=np_dtype)
    for i, point in enumerate(points):
        # change timestamp to timestamp_sec
        arr[i] = (
            point[0],# point.x,
            point[1],# point.y,
            point[2],# point.z,
            point[3],# point.intensity,
        )
    bin_file = save_path
    # bin_file = os.path.join(
    #     save_path,
    #     str(int(pointcloud.measurement_time * 1e6)) + '.bin')
    arr.tofile(bin_file)

def filter_master(lidar):
    # filter master
    minX_my = -3.7
    maxX_my = 0.2
    minY_my = -0.8
    maxY_my = 0.8

    mask = np.where(((lidar[:, 0] <= minX_my) | (lidar[:, 0] >= maxX_my)) |
                    ((lidar[:, 1] <= minY_my) | (lidar[:, 1] >= maxY_my)))
    lidar = lidar[mask]
    return lidar

pose_file_path = '/media/myc/Work/Research/code/apollo/data/bag/3_0310/_20220310_20220310113800.record.00004-parsed/apollo_localization_zj_lidar_pose.txt'
lidar_dir_path = '/media/myc/Work/Research/code/apollo/data/bag/3_0310/_20220310_20220310113800.record.00004-parsed/apollo_sensor_hesai40_compensated_PointCloud2/'
write_pcd_dir_path = '/media/myc/Data21/zhijiangyihao/test/unsupervised_test/pcd/'
write_bin_dir_path = '/media/myc/Data21/zhijiangyihao/test/unsupervised_test/bin/'

lidar_to_imu_matirx = np.identity(4)
lidar_to_imu_qua = [-0.015196, 0.0116337, 0.71877, 0.694977]
lidar_to_imu_matirx[:3, :3] = R.from_quat(lidar_to_imu_qua).as_matrix()
lidar_to_imu_matirx[0, 3] =  -0.118678
lidar_to_imu_matirx[1, 3] = 3.11112
lidar_to_imu_matirx[2, 3] = 1.58035

m_time_pose_dict = dict()
m_time_pose_list = list()
# imu_to_world_qua_list = list()
with open(pose_file_path) as pose_file:
    pose_list = pose_file.readlines()
    for pose in pose_list:
        m_time, h_time, x, y, z, qw, qx, qy, qz, _, _, _, _ = pose.split(' ')
        imu_to_world_matirx = np.identity(4)
        imu_to_world_qua = [float(qx), float(qy), float(qz), float(qw)]
        # imu_to_world_qua_list.append(imu_to_world_qua)
        imu_to_world_matirx[:3, :3] = R.from_quat(imu_to_world_qua).as_matrix()
        imu_to_world_matirx[0, 3] = float(x) - 778353
        imu_to_world_matirx[1, 3] = float(y) - 3351496
        imu_to_world_matirx[2, 3] = float(z)  
        # m_time_pose[float(m_time)*1000000] = [float(x), float(y), float(z), float(qw), float(qx), float(qy), float(qz)]
        m_time_pose_dict[float(m_time)*1000000] = imu_to_world_matirx
        m_time_pose_list.append(imu_to_world_matirx)

lidar_pose_dict = dict()
lidar_pose_list = list()
lidar_file_list = os.listdir(lidar_dir_path)
lidar_file_list.sort()
# lidar_file_list = lidar_file_list[150:]
for lidar_file in lidar_file_list:
    lidar_time = float(lidar_file.split('.bin')[0])
    diff_min_time = lidar_time
    for m_time in m_time_pose_dict:
        diff_time = abs(lidar_time-m_time)
        if diff_time < diff_min_time:
            diff_min_time = diff_time
            imu_to_world_matirx = m_time_pose_dict[m_time]
    if diff_min_time < 50*1000:
        lidar_pose_dict[lidar_dir_path+lidar_file] = imu_to_world_matirx
        lidar_pose_list.append(imu_to_world_matirx)
    else:
        lidar_pose_dict[lidar_dir_path+lidar_file] = None
        lidar_pose_list.append(None)

lidar_file_pair_list = list()
lidar_pose_pair_list = list()
# for i in range(10, len(lidar_file_list)):
for i in range(5, len(lidar_file_list)):
    #  lidar_file_pair = [lidar_file_list[i-10], lidar_file_list[i-5], lidar_file_list[i]]
    #  lidar_file_pose = [lidar_pose_list[i-10], lidar_pose_list[i-5], lidar_pose_list[i]]
     lidar_file_pair = [lidar_file_list[i-5], lidar_file_list[i]]
     lidar_file_pose = [lidar_pose_list[i-5], lidar_pose_list[i]]
     lidar_file_pair_list.append(lidar_file_pair)
     lidar_pose_pair_list.append(lidar_file_pose)

for i in range(len(lidar_file_pair_list)):
    imu_to_world_matirx_base = lidar_pose_pair_list[i][-1]
    lidar_0 = np.fromfile(lidar_dir_path+lidar_file_pair_list[i][0], dtype=np.float32).reshape(-1, 4)
    lidar_1 = np.fromfile(lidar_dir_path+lidar_file_pair_list[i][1], dtype=np.float32).reshape(-1, 4)
    # lidar_2 = np.fromfile(lidar_dir_path+lidar_file_pair_list[i][2], dtype=np.float32).reshape(-1, 4)
    lidar_0 = filter_master(lidar_0)
    lidar_1 = filter_master(lidar_1)
    
    lidar_0[:, -1] = 1.0
    lidar_1[:, -1] = 1.0
    # lidar_2[:, -1] = 1.0
    imu_0 = np.dot(lidar_to_imu_matirx, lidar_0.T)
    imu_1 = np.dot(lidar_to_imu_matirx, lidar_1.T)
    # imu_2 = np.dot(lidar_to_imu_matirx, lidar_2.T)
    world_0 = np.dot(lidar_pose_pair_list[i][0], imu_0) 
    world_1 = np.dot(lidar_pose_pair_list[i][1], imu_1) 
    # world_2 = np.dot(lidar_pose_pair_list[i][2], imu_2) 
    imu_0_base = np.dot(np.linalg.inv(imu_to_world_matirx_base), world_0)
    imu_1_base = np.dot(np.linalg.inv(imu_to_world_matirx_base), world_1)
    # imu_2_base = np.dot(np.linalg.inv(imu_to_world_matirx_base), world_2)
    lidar_0_base = np.dot(np.linalg.inv(lidar_to_imu_matirx), imu_0_base).T
    lidar_1_base = np.dot(np.linalg.inv(lidar_to_imu_matirx), imu_1_base).T
    # lidar_2_base = np.dot(np.linalg.inv(lidar_to_imu_matirx), imu_2_base).T

    # pcd_0_write_path = write_pcd_dir_path + lidar_file_pair_list[i][0].split('.bin')[0] + '_pair_' + str(i) + '.pcd'
    # pcd_1_write_path = write_pcd_dir_path + lidar_file_pair_list[i][1].split('.bin')[0] + '_pair_' + str(i) + '.pcd'
    # pcd_2_write_path = write_pcd_dir_path + lidar_file_pair_list[i][2].split('.bin')[0] + '_pair_' + str(i) + '.pcd'
    # bin_0_write_path = write_bin_dir_path + lidar_file_pair_list[i][0].split('.bin')[0] + '_pair_' + str(i) + '.bin'
    # bin_1_write_path = write_bin_dir_path + lidar_file_pair_list[i][1].split('.bin')[0] + '_pair_' + str(i) + '.bin'
    # bin_2_write_path = write_bin_dir_path + lidar_file_pair_list[i][2].split('.bin')[0] + '_pair_' + str(i) + '.bin'
    pcd_0_write_path = write_pcd_dir_path + str(i) + '_pair_0' + '.pcd'
    pcd_1_write_path = write_pcd_dir_path + str(i) + '_pair_1' + '.pcd'
    # pcd_2_write_path = write_pcd_dir_path + str(i) + '_pair_2' + '.pcd'
    bin_0_write_path = write_bin_dir_path + str(i) + '_pair_0' + '.bin'
    bin_1_write_path = write_bin_dir_path + str(i) + '_pair_1' + '.bin'
    # bin_2_write_path = write_bin_dir_path + str(i) + '_pair_2' + '.bin'
    save_as_pcd(lidar_0_base, pcd_0_write_path)
    save_as_pcd(lidar_1_base, pcd_1_write_path)
    # save_as_pcd(lidar_2_base, pcd_2_write_path)
    save_as_bin(lidar_0_base, bin_0_write_path)
    save_as_bin(lidar_1_base, bin_1_write_path)
    # save_as_bin(lidar_2_base, bin_2_write_path)





# imu_to_world_matirx_base = lidar_pose_list[0]
# lidar_file_50ms_list = list()
# lidar_pose_50ms_list = list()
# for i in range(len(lidar_file_list)):
#     if i % 5 == 0:
#         lidar_file_50ms_list.append(lidar_file_list[i])
#         lidar_pose_50ms_list.append(lidar_pose_list[i])
# #每3个为一pair
# lidar_file_pair_list = list()
# for i in range(len(lidar_file_50ms_list)-2):
#     lidar_file_pair_list.append([lidar_file_50ms_list[i], lidar_file_50ms_list[i+1], lidar_file_50ms_list[i+2]])

    # lidar = np.fromfile(lidar_dir_path+lidar_file_50ms_list[i], dtype=np.float32).reshape(-1, 4)
    # lidar[:, -1] = 1.0
    # imu = np.dot(lidar_to_imu_matirx, lidar.T)
    # world = np.dot(lidar_pose_list[i], imu)          
    # imu_base = np.dot(np.linalg.inv(imu_to_world_matirx_base), world)
    # lidar_base = np.dot(np.linalg.inv(lidar_to_imu_matirx), imu_base).T
    # pcd_write_path = write_pcd_dir_path + lidar_file_list[i].split('.bin')[0] + '_base.pcd'
    # bin_write_path = write_bin_dir_path + lidar_file_list[i].split('.bin')[0] + '_base.bin'
    # save_as_pcd(lidar_base, pcd_write_path)
    # save_as_bin(lidar_base, bin_write_path)
