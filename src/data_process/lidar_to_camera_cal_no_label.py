import os
import sys
import cv2
import numpy as np
from scipy.spatial.transform import Rotation as R
from torch import float16

sys.path.append('./src/')
from data_process.kitti_bev_utils import makeBEVMap, drawRotatedBox
from data_process.kitti_data_utils import get_filtered_lidar
import config.camera_config as cnf

numpy_pcd_type_mappings = [(np.dtype('float32'), ('F', 4)),
                           (np.dtype('float64'), ('F', 8)),
                           (np.dtype('uint8'), ('U', 1)),
                           (np.dtype('uint16'), ('U', 2)),
                           (np.dtype('uint32'), ('U', 4)),
                           (np.dtype('uint64'), ('U', 8)),
                           (np.dtype('int16'), ('I', 2)),
                           (np.dtype('int32'), ('I', 4)),
                           (np.dtype('int64'), ('I', 8))]
numpy_type_to_pcd_type = dict(numpy_pcd_type_mappings)
pcd_type_to_numpy_type = dict((q, p) for (p, q) in numpy_pcd_type_mappings)

flag_show = True

# Init camera parameters A1
camera_matrix = np.array(
	[[7.359154133588565e+02, 0., 6.036442268230935e+02],
		[0., 7.343884074869948e+02, 3.458195889190509e+02], [0., 0., 1.]], dtype=float)
dist_coef = np.array([[
	-0.336478976183514, 0.148937372439656, -2.705154152254336e-04, 
	5.630827901146963e-04, -0.036999996154070,
]], dtype=float)
# R = np.identity(3)
# remap1, remap2 = cv2.initUndistortRectifyMap(camera_matrix, dist_coef, R, camera_matrix, (1280, 720), cv2.CV_32FC1)

def lidar3d_to_camera3d(lidar3d):
	"""
	:params lidar3d: (N, 3)
	:return camera3d: (N, 3)
	"""
	
	# calibration
	# 4 car
	T_camera_to_rear = np.zeros((4, 4), dtype=float)
	r = R.from_quat([-7.7385026678869540e-01, -9.5848737257190186e-03, 9.4430875126212278e-03, 6.3322564925938418e-01])
	T_camera_to_rear[0:3, 0:3] = r.as_matrix()
	T_camera_to_rear[0, 3] = -1.8406905710942112e-01
	T_camera_to_rear[1, 3] = 3.2674858510201865e+00
	T_camera_to_rear[2, 3] = 1.0960765464781894e+00
	T_camera_to_rear[3, 3] = 1.0
	T_rear_to_camera = np.linalg.inv(T_camera_to_rear)

	T_lidar_to_rear = np.zeros((4, 4), dtype=float)
	r = R.from_quat([-0.0183149, 0.00850772, 0.716704, 0.697086])
	T_lidar_to_rear[0:3, 0:3] = r.as_matrix()
	T_lidar_to_rear[0, 3] = -0.128643
	T_lidar_to_rear[1, 3] = 3.07625
	T_lidar_to_rear[2, 3] = 1.49873
	T_lidar_to_rear[3, 3] = 1.0

	T_lidar_to_camera = np.matmul(T_rear_to_camera, T_lidar_to_rear)
	# T_lidar_to_camera = T_lidar_to_camera[0:3, :]

	# 1 car
	# T_lidar_to_camera = np.array(
	# 	[[0.0277387, -0.999607, -0.00404495, 0.108141],
	# 	 [0.00308998, 0.00413226, -0.999987, -0.123988],
	# 	 [0.99961, 0.0277258, 0.00320342, -0.197486]])
	R.from_matrix(T_lidar_to_camera[0:3, 0:3]).as_quat()
	sample_num = lidar3d.shape[0]
	lidar3d_hom = np.concatenate((lidar3d, np.ones((sample_num, 1))),
								   axis=1)  # (N, 3)
	# pts_image = np.matmul(lidar3d_hom, T_lidar_to_camera.T)
	pts_image = (np.matmul(T_lidar_to_camera, lidar3d_hom.T)).T

	return pts_image[:, 0:3]

def camera3d_to_img_boxes(lidar, camera3d):
	"""
	:param camera3d: (N, 8, 3) corners in camera coordinate
	:return: (N, 4) 4:(xmin,ymin,xmax,ymax) pts in rgb coordinate
	"""

	image_width = 1280
	image_height = 720

	# 1 car
	camera_matrix = np.array(
		[[7.359154133588565e+02, 0., 6.036442268230935e+02],
			[0., 7.343884074869948e+02, 3.458195889190509e+02], [0., 0., 1.]], dtype=float)
	dist_coef = np.array([[
		-0.336478976183514, 0.148937372439656, -2.705154152254336e-04, 
		5.630827901146963e-04, -0.036999996154070,
	]], dtype=float)

	# 4 car
	# camera_matrix = np.array(
	# 	[[8.597117441273326e+02, 0., 6.457184046914338e+02],
	# 	 [0., 8.639005809289114e+02, 3.140552408008491e+02], [0., 0., 1.]], dtype=float)
	# dist_coef = np.array([[
	# 	-0.466189352030079, 0.244478431086829, 1.975803452451033e-05, 
	# 	3.989482808566306e-04, -0.071599686364195,
	# ]], dtype=float)	
	
	# use ori image
	new_camera_matrix, _ = cv2.getOptimalNewCameraMatrix(
		camera_matrix, dist_coef, (image_width, image_height), 1,
		(image_width, image_height))
	
	# use undistort image
	new_camera_matrix = camera_matrix
	dist_coef = np.zeros_like(dist_coef)

	zeros = np.array([[0., 0., 0.]], dtype=float)
	ones = np.array([[1., 0., 0.], [0., 1., 0.], [0., 0., 1.]], dtype=float)

	flag_z = ~(camera3d[:, 2] < 0)
	# cols = new_camera_matrix[
	# 	0, 2] + new_camera_matrix[0, 0] * camera3d[:, 0] / camera3d[:, 2]
	# rows = new_camera_matrix[
	# 	1, 2] + new_camera_matrix[1, 1] * camera3d[:, 1] / camera3d[:, 2]
   	
	# flag_0 = ~np.bitwise_or(cols < 0, rows < 0)
	# flag_max = ~np.bitwise_or(cols > image_width, rows > image_height)

	# flag_camera_3d = np.bitwise_and(np.bitwise_and(flag_z, flag_0), flag_max)
	# camera3d = camera3d[flag_camera_3d]
	# lidar = lidar[flag_camera_3d]
	
	camera3d = camera3d[flag_z]
	lidar = lidar[flag_z]
	# project
	camera_pts, _ = cv2.projectPoints(camera3d, ones, zeros, camera_matrix,
									  dist_coef)
	camera_pts = camera_pts[:, 0, :]
	camera_pts[:, 1] += 20
	
	flag_pt_0 = ~np.bitwise_or(camera_pts[:, 0] < 0., camera_pts[:, 1] < 0.)
	flag_pt_max = ~np.bitwise_or(camera_pts[:, 0] > image_width, camera_pts[:, 1] > image_height)
	flag_pts = np.bitwise_and(flag_pt_0 ,flag_pt_max)
	camera_pts = camera_pts[flag_pts]
	camera3d = camera3d[flag_pts]
	lidar = lidar[flag_pts]

	return lidar, camera_pts, camera3d

def get_2D_label(label_file):
	labels = []
	for line in open(label_file, 'r'):
		line = line.rstrip().replace('\ufeff', '')
		line_parts = line.split(' ')
		x1, y1 = float(line_parts[0]), float(line_parts[1])
		x2, y2 = float(line_parts[2]), float(line_parts[3])
		cls = int(line_parts[4])
		object_label = [x1, y1, x2, y2, cls]
		labels.append(object_label)
	if len(labels) == 0:
		has_labels = False
	else:
		has_labels = True

	return labels, has_labels

def get_3D_label(label_file):
	labels = []
	for line in open(label_file, 'r'):
		line = line.rstrip()
		line_parts = line.split(' ')
		cat_id = int(line_parts[0])  # 1 for small vehicles, 2 for big vehicles, 3 for pedestrian, 4 for motorcyclist and bicyclist, 5 for traffic cones and 6 for others
		# print("cat_id:", cat_id)
		if cat_id <= -99:  # ignore Tram and Misc
			continue
		x, y, z = float(line_parts[1]), float(line_parts[2]), float(line_parts[3])
		l, w, h = float(line_parts[4]), float(line_parts[5]), float(line_parts[6])
		# w, l, h = float(line_parts[4]), float(line_parts[5]), float(line_parts[6])
		ry = float(line_parts[7])
		object_label = [cat_id, x, y, z, h, w, l, ry]
		labels.append(object_label)

	if len(labels) == 0:
		labels = np.zeros((1, 8), dtype=np.float32)
		has_labels = False
	else:
		labels = np.array(labels, dtype=np.float32)
		has_labels = True

	return labels, has_labels

def get_image_point(point_all, camera_pts, labels, has_labels):
	point_in_detec = np.zeros((0, 4))
	camera_pts_in_detec = np.zeros((0, 2))
	if has_labels:
		for label in labels:
			x1, y1, x2, y2, cls = label
			flag_x = np.bitwise_and(camera_pts[:, 0] > x1-5, camera_pts[:, 0] < x2+5)
			flag_y = np.bitwise_and(camera_pts[:, 1] > y1-5, camera_pts[:, 1] < y2+5)
			flag_xy = np.bitwise_and(flag_x, flag_y)
			point = point_all[flag_xy]
			camera = camera_pts[flag_xy]
			if point.shape[0] != 0:
				point_in_detec = np.vstack((point_in_detec, point))
				camera_pts_in_detec = np.vstack((camera_pts_in_detec, camera))
		
		if point_in_detec.shape[0] == 0:
			return False, None, None
		else:
			return has_labels, point_in_detec, camera_pts_in_detec
	else:
		return has_labels, None, None

def cal_points_img(lidar_file):
    lidar_time = int(lidar_file.split('.')[0])
    temp = 0
    # temp =     +160
    lidar_time = lidar_time - temp
    diff_min = lidar_time
    diff_min_index = 0
    for i, image_file in enumerate(image_path_list):
        image_time = int(image_file.split('/')[-1].split('.')[0])
        diff = abs(lidar_time - image_time)
        if diff < diff_min:
            diff_min = diff
            diff_min_index = i
    # print('diff_min:', diff_min/1000)
    print('diff_min:', diff_min)
    print('lidar_file:', lidar_file)
    if diff_min < 30000: 
        return diff_min_index
    else:
        return -1

def save_pointcloud_bin(pointcloud, save_path):
    points = pointcloud.point
    header = {
        'version': .7,
        'fields': ['x', 'y', 'z', 'intensity'],
        'count': [1, 1, 1, 1],
        'width': len(points),
        'height': 1,
        'viewpoint': [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
        'points': len(points),
        'type': ['F', 'F', 'F', 'U'],
        'size': [4, 4, 4, 4],
        'data': 'binary'
    }
    typenames = []
    for pcd_type, numpy_type in zip(header['type'], header['size']):
        numpy_type = pcd_type_to_numpy_type[(pcd_type, numpy_type)]
        typenames.append(numpy_type)
    np_dtype = np.dtype(list(zip(header['fields'], typenames)))
    arr = np.zeros(len(points), dtype=np_dtype)
    for i, point in enumerate(points):
        # change timestamp to timestamp_sec
        arr[i] = (
            point.x,
            point.y,
            point.z,
            point.intensity,
        )
    bin_file = os.path.join(
        save_path,
        str(int(pointcloud.measurement_time * 1e6)) + '.bin')
    arr.tofile(bin_file)

COLOR_MAP = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (102, 204, 153),
             (255, 255, 0), (255, 255, 255), (0, 0, 0), (255, 102, 0),
             (255, 153, 153), (153, 0, 51)]

# lidar_dir_path = '/media/myc/Data21/zhijiangyihao/training_fusion/bin9/'
# image_dir_path = '/media/myc/Data21/zhijiangyihao/training_fusion/image9/'
lidar_dir_path = '/media/myc/Work/Research/code/apollo/data/bag/myc/2/apollo_sensor_lidar_fusion_compensated_PointCloud2/'
image_dir_path = '/media/myc/Work/Research/code/apollo/data/bag/myc/2/apollo_sensor_drivers_camera_panoramic_camera_front_left/'
# label_2D_dir_path = '/media/myc/Data21/zhijiangyihao/training_fusion/label9_2D/'
# label_3D_dir_path = '/media/myc/Data21/zhijiangyihao/training_fusion/label9/'

# lidar_in_image_dir_path = '/media/myc/Data21/zhijiangyihao/training_fusion/bin9_in_image/'

lidar_path_list = os.listdir(lidar_dir_path)
lidar_path_list.sort()
image_path_list = os.listdir(image_dir_path)
image_path_list.sort()
# label_2D_path_list = os.listdir(label_2D_dir_path)
# label_2D_path_list.sort()
# label_3D_path_list = os.listdir(label_3D_dir_path)
# label_3D_path_list.sort()

for i in range(len(lidar_path_list)):
	lidar = np.fromfile(lidar_dir_path+lidar_path_list[i], dtype=np.float32).reshape(-1, 4)

	# image_file_index = i
	image_file_index = cal_points_img(lidar_path_list[i])
	if image_file_index == -1:
		continue

	camera_3d = lidar3d_to_camera3d(lidar[:, 0:-1])
	lidar_in_image, camera_pts, valid_camera3d = camera3d_to_img_boxes(lidar, camera_3d)
	image = cv2.imread(image_dir_path+image_path_list[image_file_index])
	imge_undistort = cv2.undistort(image, camera_matrix, dist_coef, None, camera_matrix)
	image = imge_undistort
	# cv2.imshow('image', image)
	# cv2.imshow('imge_undistort', imge_undistort)
	# cv2.waitKey(0)
	# continue
	# labels_2D, has_labels_2D = get_2D_label(label_2D_dir_path+label_2D_path_list[i])
	# labels_3D, has_labels_3D = get_3D_label(label_3D_dir_path+label_3D_path_list[i])
	# has_point, lidar_in_detec, camera_pts_in_detec = get_image_point(lidar_in_image, camera_pts, labels_2D, has_labels_2D)

	lidar, _ = get_filtered_lidar('', lidar, cnf.boundary)
	lidar_in_image, _ = get_filtered_lidar('', lidar_in_image, cnf.boundary)
	# lidar_in_detec, _ = get_filtered_lidar('', lidar_in_detec, cnf.boundary)
	# save_pointcloud_bin(lidar_in_detec, lidar_in_image_dir_path+lidar_path_list[i])


	if flag_show:
		bev_map = makeBEVMap(lidar, cnf.boundary)
		bev_map_in_image = makeBEVMap(lidar_in_image, cnf.boundary)
		# bev_map_in_detec = makeBEVMap(lidar_in_detec, cnf.boundary)

		bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
		bev_map = cv2.resize(bev_map, (cnf.BEV_WIDTH, cnf.BEV_HEIGHT))
		bev_map_in_image = (bev_map_in_image.transpose(1, 2, 0) * 255).astype(np.uint8)
		bev_map_in_image = cv2.resize(bev_map_in_image, (cnf.BEV_WIDTH, cnf.BEV_HEIGHT))
		# bev_map_in_detec = (bev_map_in_detec.transpose(1, 2, 0) * 255).astype(np.uint8)
		# bev_map_in_detec = cv2.resize(bev_map_in_detec, (cnf.BEV_WIDTH, cnf.BEV_HEIGHT))

		# Draw 2D labels in image, and show image
		# camera_pts = camera_pts_in_detec
		for i in range(camera_pts.shape[0]):
			depth = valid_camera3d[i, 2]
			color_idx = int(depth) % 10
			cv2.circle(image, (int(camera_pts[i, 0]), int(camera_pts[i, 1])), 1, COLOR_MAP[color_idx], 1)
		
		# for label in labels_2D:
		# 	x1, y1, x2, y2, cls = label
		# 	cv2.rectangle(image, (int(x1), int(y1)), (int(x2), int(y2)), (0, 0, 255), 2)
		cv2.imshow('image', image)

		# Draw 3D labels in bev_map and bev_map_in_detec
		# for box_idx, (cls_id, x, y, z, h, w, l, yaw) in enumerate(labels_3D):
		# 	if (h <= 0) or (w <= 0) or (l <= 0):
		# 		continue
		# 	yaw = -yaw
		# 	y1 = int((x - cnf.boundary['minX']) / cnf.DISCRETIZATION_Y)
		# 	x1 = int((y - cnf.boundary['minY']) / cnf.DISCRETIZATION_X)
		# 	w1 = int(w / cnf.DISCRETIZATION_X)
		# 	l1 = int(l / cnf.DISCRETIZATION_Y)
		# 	drawRotatedBox(bev_map, x1, y1, w1, l1, yaw, cnf.colors[int(cls_id)], cls_id)
		# 	drawRotatedBox(bev_map_in_detec, x1, y1, w1, l1, yaw, cnf.colors[int(cls_id)], cls_id)

		# Imshow bev_map and bev_map_in_detec in ROTATE_180
		bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
		cv2.imshow('bev_map', bev_map)
		bev_map_in_image = cv2.rotate(bev_map_in_image, cv2.ROTATE_180)
		cv2.imshow('bev_map_in_image', bev_map_in_image)	
		# bev_map_in_detec = cv2.rotate(bev_map_in_detec, cv2.ROTATE_180)
		# cv2.imshow('bev_map_in_detec', bev_map_in_detec)
		cv2.waitKey(0)
  