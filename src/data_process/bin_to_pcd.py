import os
import numpy as np
import time
import argparse


def parse_configs():
  parser = argparse.ArgumentParser(description='Config')

  parser.add_argument('--bin_path', type=str, default='/media/myc/Work/Research/code/apollo/data/bag/myc/ori', help="The path of bin")
  parser.add_argument('--pcd_path', type=str, default='/home/myc/桌面/pcd_ori', help="The path of pcd")

  args = parser.parse_args()
  return args


def read_bin_file(path):
  scan = np.fromfile(path, dtype=np.float32).reshape([-1, 4])
  return scan


def save_as_pcd(scan, path):
  if os.path.exists(path):
    os.remove(path)

  handle = open(path, 'a')
  header = ['# .PCD v0.7 - Point Cloud Data file format\n', 'VERSION 0.7\n', 'FIELDS x y z i\n', 'SIZE 4 4 4 4\n', 'TYPE F F F F\n', 'COUNT 1 1 1 1\n']
  handle.writelines(header)
  handle.write('WIDTH ' + str(scan.shape[0]) + '\n')
  handle.write('HEIGHT 1\n')
  handle.write('VIEWPOINT 0 0 0 1 0 0 0\n')
  handle.write('POINTS ' + str(scan.shape[0]) + '\n')
  handle.write('DATA ascii\n')

  for i in range(scan.shape[0]):
    pt_str = str(scan[i, 0]) + ' ' + str(scan[i, 1]) + ' ' + str(scan[i, 2]) + ' ' + str(scan[i, 3] / 255.0) + '\n'
    handle.write(pt_str)

  handle.close()


def run(configs):
  bin_file_list = os.listdir(configs.bin_path)
  bin_file_list.sort()

  for bin_file in bin_file_list:
    start = time.time()

    scan_numpy = read_bin_file(configs.bin_path + '/' + bin_file)
    save_as_pcd(scan_numpy, configs.pcd_path + '/' + os.path.splitext(bin_file)[0] + '.pcd')

    end = time.time()
    print("[bin_to_pcd] name: {} time: {}".format(os.path.splitext(bin_file)[0], (end - start)))


if __name__ == "__main__":
  configs = parse_configs()

  run(configs)
