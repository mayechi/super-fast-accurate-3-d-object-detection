"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
"""

import math
from pdb import set_trace
import pdb
import sys

import cv2
import numpy as np
import random
import numba

from torch import float16
sys.path.append('../')

data_from = 'voxel'
if data_from == 'kitti':
    import config.kitti_config as cnf
elif data_from == 'apollo':
    import config.apollo_config as cnf
elif data_from == 'camera':
    import config.camera_config as cnf
else:
    import config.simulation_config as cnf

def makeBEVMap(PointCloud_, boundary, rgb=None):
    Height = cnf.BEV_HEIGHT + 1
    Width = cnf.BEV_WIDTH + 1

    # Discretize Feature Map
    PointCloud = np.copy(PointCloud_)
    # PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / cnf.DISCRETIZATION))
    if data_from == 'kitti' or data_from == 'camera':
        # 针对Kitti数据集，只检测正前方
        PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / cnf.DISCRETIZATION))
        PointCloud[:, 1] = np.int_(np.floor(PointCloud[:, 1] / cnf.DISCRETIZATION) + Width / 2)
    else:
        # 针对Apollo数据集，检测360°
        PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / cnf.DISCRETIZATION_Y) + Height / 2)
        PointCloud[:, 1] = np.int_(np.floor(PointCloud[:, 1] / cnf.DISCRETIZATION_X) + Width / 2)


    # sort-3times
    indices = np.lexsort((-PointCloud[:, 2], PointCloud[:, 1], PointCloud[:, 0]))
    PointCloud = PointCloud[indices]
    if rgb is not None:
        rgb = rgb[indices]

    # Height Map
    heightMap = np.zeros((Height, Width))

    _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
    PointCloud_frac = PointCloud[indices]
    # some important problem is image coordinate is (y,x), not (x,y)
    max_height = float(np.abs(boundary['maxZ'] - boundary['minZ']))

    heightMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = PointCloud_frac[:, 2] / max_height

    # Intensity Map & DensityMap
    intensityMap = np.zeros((Height, Width))
    densityMap = np.zeros((Height, Width))

    _, indices, counts = np.unique(PointCloud[:, 0:2], axis=0, return_index=True, return_counts=True)
    PointCloud_top = PointCloud[indices]

    normalizedCounts = np.minimum(1.0, np.log(counts + 1) / np.log(64))

    intensityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = PointCloud_top[:, 3] / 255.0 # hesai40p的反射强度0~255
    densityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = normalizedCounts

    if rgb is not None:
        # Height Map
        # ImgMap = np.zeros((Height, Width, 1))
        ImgMap = np.zeros((Height, Width, 3))
        _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
        PointCloud_frac = PointCloud[indices]        
        # rgb_frac = rgb[indices].reshape(-1, 1)
        rgb_frac = rgb[indices].reshape(-1, 3)
        ImgMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = rgb_frac / 255.0
        ImgMap = np.transpose(ImgMap, (2, 0, 1))
        # RGB_Map = np.zeros((3, Height - 1, Width - 1))
        RGB_Map = np.zeros((3, Height - 1, Width - 1))
        
        RGB_Map[0:3, :, :] = ImgMap[:, :cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]
        # RGB_Map[1, :, :] = ImgMap[:, :cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]

        # RGB_Map[4, :, :] = densityMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # r_map
        # RGB_Map[3, :, :] = heightMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # g_map
        # RGB_Map[0, :, :] = intensityMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # b_map     
    else:
        if data_from == 'kitti':        
            RGB_Map = np.zeros((3, Height - 1, Width - 1))
            RGB_Map[2, :, :] = densityMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # r_map
            RGB_Map[1, :, :] = heightMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # g_map
            RGB_Map[0, :, :] = intensityMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # b_map
        else:
            RGB_Map = np.zeros((3, Height - 1, Width - 1))
            RGB_Map[2, :, :] = densityMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # r_map
            RGB_Map[1, :, :] = heightMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # g_map
            # RGB_Map[0, :, :] = intensityMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # b_map            

    return RGB_Map

def make_bev_ma(PointCloud_, boundary, rgb=None):
    Height = cnf.BEV_HEIGHT
    Width = cnf.BEV_WIDTH
    CLOUD_R = PointCloud_.shape[0]
    bev_map = np.zeros((4, 10, Height, Width))
    bev_map_per_index = np.zeros((Height, Width), dtype=int)

    # 点云随机选取10个点
    # random_shuffle_list = list(range(CLOUD_R))
    # random.shuffle(random_shuffle_list)

    # 将点云按照z从高到低排序
    # indices = np.lexsort((-PointCloud_[:, 2], PointCloud_[:, 1], PointCloud_[:, 0]))
    indices = np.argsort(-PointCloud_[:, 2])
    PointCloud_ = PointCloud_[indices]  
    range_list = list(range(CLOUD_R)) 
    random_shuffle_list = range_list

    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]
        i = PointCloud_[i, 3] / 255.0 # hesai40p的反射强度0~255

        row = Height * (x - cnf.boundary["minX"]) / (cnf.boundary["maxX"] - cnf.boundary["minX"])
        col = Width * (y - cnf.boundary["minY"]) / (cnf.boundary["maxY"] - cnf.boundary["minY"])
        h = (z - cnf.boundary["minZ"]) / (cnf.boundary["maxZ"] - cnf.boundary["minZ"])
        r = int(math.floor(row))
        c = int(math.floor(col))

        if r >= Height or c >= Width:
            continue
        
        per_index = bev_map_per_index[r, c]
        bev_map_per_index[r, c] += 1
        if per_index > 9:
            # per_index = 9
            continue
        bev_map[0, per_index, r, c] = x
        bev_map[1, per_index, r, c] = y
        bev_map[2, per_index, r, c] = z
        bev_map[3, per_index, r, c] = i

    # 压缩成有效的点
    # RGB_Map = np.zeros((4, 10, bev_map_per_index.nonzero()[0].shape[0]))
    # RGB_Map = bev_map[:, :, bev_map_per_index.nonzero()[0], bev_map_per_index.nonzero()[1]]

    # 保证可以还原回去
    # bev_map_1 = np.zeros((4, 10, Height, Width))
    # bev_map_1[:, :, bev_map_per_index.nonzero()[0], bev_map_per_index.nonzero()[1]] = RGB_Map
    # return RGB_Map, bev_map_per_index
    
    # 三个维度，可以翻转
    bev_map = bev_map.reshape(-1, Height, Width)

    # 最原始的，占的显存比较大，空间换时间
    # bev_map = bev_map.reshape(4, 10, Height*Width)

    return bev_map, bev_map_per_index

def make_bev_voxel(PointCloud_, boundary, rgb=None):
    Height = cnf.BEV_HEIGHT
    Width = cnf.BEV_WIDTH
    Deep = 10
    CLOUD_R = PointCloud_.shape[0]
    bev_map = np.zeros((4, 10, Height, Width))
    bev_map_per_index = np.zeros((Height, Width), dtype=int)

    # voxel
    voxel_num = np.zeros((10, Height, Width), dtype=int)
    voxel_all_feature = np.zeros((4, 10, Height, Width))
    voxel_ave_feature = np.zeros((5, 10, Height, Width))

    # 将点云按照z从高到低排序
    # indices = np.lexsort((-PointCloud_[:, 2], PointCloud_[:, 1], PointCloud_[:, 0]))
    indices = np.argsort(-PointCloud_[:, 2])
    PointCloud_ = PointCloud_[indices]  
    range_list = list(range(CLOUD_R)) 
    random_shuffle_list = range_list

    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]
        z_ori = z + cnf.boundary["minZ"]
        i = PointCloud_[i, 3] / 255.0 # hesai40p的反射强度0~255

        row = Height * (x - cnf.boundary["minX"]) / (cnf.boundary["maxX"] - cnf.boundary["minX"])
        col = Width * (y - cnf.boundary["minY"]) / (cnf.boundary["maxY"] - cnf.boundary["minY"])
        deep = Deep * (z_ori - cnf.boundary["minZ"]) / (cnf.boundary["maxZ"] - cnf.boundary["minZ"])

        r = int(math.floor(row))
        c = int(math.floor(col))
        d = int(math.floor(deep))

        if r >= Height or c >= Width or d >= Deep:
            continue
        voxel_num[d, r, c] += 1
        voxel_all_feature[0, d, r, c] += x
        voxel_all_feature[1, d, r, c] += y
        voxel_all_feature[2, d, r, c] += z
        voxel_all_feature[3, d, r, c] += i

        voxel_ave_feature[0, d, r, c] = voxel_all_feature[0, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[1, d, r, c] = voxel_all_feature[1, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[2, d, r, c] = voxel_all_feature[2, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[3, d, r, c] = voxel_all_feature[3, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[4, d, r, c] = np.minimum(1.0, np.log(voxel_num[d, r, c] + 1) / np.log(64))


        # per_index = bev_map_per_index[r, c]
        # bev_map_per_index[r, c] += 1
        # if per_index > 9:
        #     continue
        # bev_map[0, per_index, r, c] = x
        # bev_map[1, per_index, r, c] = y
        # bev_map[2, per_index, r, c] = z
        # bev_map[3, per_index, r, c] = i

    # 压缩成有效的点
    # RGB_Map = np.zeros((4, 10, bev_map_per_index.nonzero()[0].shape[0]))
    # RGB_Map = bev_map[:, :, bev_map_per_index.nonzero()[0], bev_map_per_index.nonzero()[1]]

    # 保证可以还原回去
    # bev_map_1 = np.zeros((4, 10, Height, Width))
    # bev_map_1[:, :, bev_map_per_index.nonzero()[0], bev_map_per_index.nonzero()[1]] = RGB_Map
    # return RGB_Map, bev_map_per_index
    
    # 三个维度，可以翻转
    bev_map = voxel_ave_feature.reshape(-1, Height, Width)

    # 最原始的，占的显存比较大，空间换时间
    # bev_map = bev_map.reshape(4, 10, Height*Width)

    return bev_map, bev_map_per_index

def make_bev_voxel_no_i(PointCloud_, boundary, rgb=None):
    Height = cnf.BEV_HEIGHT
    Width = cnf.BEV_WIDTH
    Deep = 10
    CLOUD_R = PointCloud_.shape[0]
    bev_map = np.zeros((4, 10, Height, Width))
    # bev_map_per_voxel_x = np.zeros((4, 10, Height, Width))
    # bev_map_per_voxel_y = np.zeros((10, 10, Height, Width))
    # bev_map_per_voxel_z = np.zeros((10, 10, Height, Width))
    
    bev_map_per_index = np.zeros((Height, Width), dtype=int)
    # bev_map_x_voxel = np.zeros((Height, Width), dtype=int)

    # Adaptive Voxel height
    # bev_map_per_max_height = np.zeros((Height, Width), dtype=float)

    # voxel
    voxel_num = np.zeros((10, Height, Width), dtype=int)
    voxel_all_feature = np.zeros((3, 10, Height, Width))
    voxel_ave_feature = np.zeros((4, 10, Height, Width))

    # 将点云按照z从高到低排序
    # indices = np.lexsort((-PointCloud_[:, 2], PointCloud_[:, 1], PointCloud_[:, 0]))
    indices = np.argsort(-PointCloud_[:, 2])
    PointCloud_ = PointCloud_[indices]  
    range_list = list(range(CLOUD_R)) 
    random_shuffle_list = range_list

    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]
        z_ori = z + cnf.boundary["minZ"]

        row = Height * (x - cnf.boundary["minX"]) / (cnf.boundary["maxX"] - cnf.boundary["minX"])
        col = Width * (y - cnf.boundary["minY"]) / (cnf.boundary["maxY"] - cnf.boundary["minY"])
        r = int(math.floor(row))
        c = int(math.floor(col))

        deep = Deep * (z_ori - cnf.boundary["minZ"]) / (cnf.boundary["maxZ"] - cnf.boundary["minZ"])
        d = int(math.floor(deep))

        if r >= Height or c >= Width or d >= Deep:
            continue
        bev_map_per_index[r, c] = 1

        voxel_num[d, r, c] += 1
        voxel_all_feature[0, d, r, c] += x
        voxel_all_feature[1, d, r, c] += y
        voxel_all_feature[2, d, r, c] += z

        voxel_ave_feature[0, d, r, c] = voxel_all_feature[0, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[1, d, r, c] = voxel_all_feature[1, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[2, d, r, c] = voxel_all_feature[2, d, r, c] / voxel_num[d, r, c]

        # add density feature
        voxel_ave_feature[3, d, r, c] = np.minimum(1.0, np.log(voxel_num[d, r, c] + 1) / np.log(64))

    # 压缩成有效的点
    # bev_map_compress = np.zeros((4, 10, bev_map_per_index.nonzero()[0].shape[0]))
    # bev_map_compress = voxel_ave_feature[:, :, bev_map_per_index.nonzero()[0], bev_map_per_index.nonzero()[1]]

    # 保证可以还原回去
    # bev_map_1 = np.zeros((4, 10, Height, Width))
    # bev_map_1[:, :, bev_map_per_index.nonzero()[0], bev_map_per_index.nonzero()[1]] = RGB_Map
    # return RGB_Map, bev_map_per_index
    
    # 三个维度，可以翻转
    bev_map = voxel_ave_feature.reshape(-1, Height, Width)

    # 最原始的，占的显存比较大，空间换时间
    # bev_map = voxel_ave_feature.reshape(4, 10, Height*Width)

    # return bev_map_compress, bev_map_per_index
    return bev_map, bev_map_per_index #, bev_map_x_voxel
    # return bev_map

def make_bev_voxel_no_i_local_density(PointCloud_, boundary, rgb=None):
    Height = cnf.BEV_HEIGHT
    Width = cnf.BEV_WIDTH
    Deep = 10
    CLOUD_R = PointCloud_.shape[0]
    bev_map = np.zeros((5, Deep, Height, Width))
    
    bev_map_per_index = np.zeros((Height, Width), dtype=int)

    # voxel
    voxel_num_per_pixel = np.zeros((Height, Width), dtype=int)
    voxel_num_per_voxel = np.zeros((Deep, Height, Width), dtype=int)
    voxel_all_feature = np.zeros((3, Deep, Height, Width))
    voxel_ave_feature = np.zeros((5, Deep, Height, Width))

    # 将点云按照z从高到低排序
    # indices = np.lexsort((-PointCloud_[:, 2], PointCloud_[:, 1], PointCloud_[:, 0]))
    indices = np.argsort(-PointCloud_[:, 2])
    PointCloud_ = PointCloud_[indices]  
    range_list = list(range(CLOUD_R)) 
    random_shuffle_list = range_list

    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]
        z_ori = z + cnf.boundary["minZ"]

        row = Height * (x - cnf.boundary["minX"]) / (cnf.boundary["maxX"] - cnf.boundary["minX"])
        col = Width * (y - cnf.boundary["minY"]) / (cnf.boundary["maxY"] - cnf.boundary["minY"])
        r = int(math.floor(row))
        c = int(math.floor(col))

        # Adaptive Voxel height
        # if bev_map_per_max_height[r, c] == 0:
        #     bev_map_per_max_height[r, c] = z_ori
        # maxZ = bev_map_per_max_height[r, c]
        # deep = Deep * (z_ori - cnf.boundary["minZ"]) / (maxZ - cnf.boundary["minZ"])

        deep = Deep * (z_ori - cnf.boundary["minZ"]) / (cnf.boundary["maxZ"] - cnf.boundary["minZ"])
        d = int(math.floor(deep))

        if r >= Height or c >= Width or d >= Deep:
            continue
        bev_map_per_index[r, c] = 1

        voxel_num_per_pixel[r, c] += 1
        voxel_num_per_voxel[d, r, c] += 1
        voxel_all_feature[0, d, r, c] += x
        voxel_all_feature[1, d, r, c] += y
        voxel_all_feature[2, d, r, c] += z

        voxel_ave_feature[0, d, r, c] = voxel_all_feature[0, d, r, c] / voxel_num_per_voxel[d, r, c]
        voxel_ave_feature[1, d, r, c] = voxel_all_feature[1, d, r, c] / voxel_num_per_voxel[d, r, c]
        voxel_ave_feature[2, d, r, c] = voxel_all_feature[2, d, r, c] / voxel_num_per_voxel[d, r, c]


        # add local density feature
        voxel_ave_feature[3, d, r, c] = np.minimum(1.0, np.log(voxel_num_per_voxel[d, r, c] + 1) / (np.log(voxel_num_per_pixel[r, c] + 1) + 0.01))
        # add density feature
        voxel_ave_feature[4, d, r, c] = np.minimum(1.0, np.log(voxel_num_per_voxel[d, r, c] + 1) / np.log(64))


    # 压缩成有效的点
    # bev_map_compress = np.zeros((4, 10, bev_map_per_index.nonzero()[0].shape[0]))
    # bev_map_compress = voxel_ave_feature[:, :, bev_map_per_index.nonzero()[0], bev_map_per_index.nonzero()[1]]

    # 保证可以还原回去
    # bev_map_1 = np.zeros((4, 10, Height, Width))
    # bev_map_1[:, :, bev_map_per_index.nonzero()[0], bev_map_per_index.nonzero()[1]] = RGB_Map
    # return RGB_Map, bev_map_per_index
    
    # 三个维度，可以翻转
    bev_map = voxel_ave_feature.reshape(-1, Height, Width)

    # 最原始的，占的显存比较大，空间换时间
    # bev_map = voxel_ave_feature.reshape(4, 10, Height*Width)

    # return bev_map_compress, bev_map_per_index
    return bev_map, bev_map_per_index #, bev_map_x_voxel
    # return bev_map

def make_bev_voxel_no_i_add_adaptive_and_var(PointCloud_, boundary, rgb=None):
    config_voxel_num = 10
    # config_voxel_points_num = 20

    Height = cnf.BEV_HEIGHT
    Width = cnf.BEV_WIDTH
    Deep = 10
    CLOUD_R = PointCloud_.shape[0]
    # bev_map = np.zeros((7, config_voxel_num, Height, Width))
    # bev_map_per_voxel_x = np.zeros((config_voxel_points_num, config_voxel_num, Height, Width))
    # bev_map_per_voxel_y = np.zeros((config_voxel_points_num, config_voxel_num, Height, Width))
    # bev_map_per_voxel_z = np.zeros((config_voxel_points_num, config_voxel_num, Height, Width))
    
    bev_map_per_index = np.zeros((Height, Width), dtype=int)

    # Adaptive Voxel height
    bev_map_per_max_height = np.zeros((Height, Width), dtype=float)

    # voxel
    voxel_num = np.zeros((config_voxel_num, Height, Width), dtype=int)
    voxel_all_feature = np.zeros((3, config_voxel_num, Height, Width))
    voxel_ave_feature = np.zeros((4, config_voxel_num, Height, Width))

    # 将点云按照z从高到低排序
    # indices = np.lexsort((-PointCloud_[:, 2], PointCloud_[:, 1], PointCloud_[:, 0]))
    indices = np.argsort(-PointCloud_[:, 2])
    PointCloud_ = PointCloud_[indices]  
    range_list = list(range(CLOUD_R)) 
    random_shuffle_list = range_list

    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]
        z_ori = z + cnf.boundary["minZ"]

        row = Height * (x - cnf.boundary["minX"]) / (cnf.boundary["maxX"] - cnf.boundary["minX"])
        col = Width * (y - cnf.boundary["minY"]) / (cnf.boundary["maxY"] - cnf.boundary["minY"])
        r = int(math.floor(row))
        c = int(math.floor(col))

        # Adaptive Voxel height
        if r >= Height or c >= Width:
            continue        
        if bev_map_per_max_height[r, c] == 0:
            bev_map_per_max_height[r, c] = z_ori
            if z_ori <= cnf.boundary["minZ"]:
                bev_map_per_max_height[r, c] = cnf.boundary["minZ"] + 0.5
        maxZ = bev_map_per_max_height[r, c]
        deep = Deep * (z_ori - cnf.boundary["minZ"]) / (maxZ - cnf.boundary["minZ"])
        # print('r:', r)
        # print('c:', c)
        # deep = Deep * (z_ori - cnf.boundary["minZ"]) / (cnf.boundary["maxZ"] - cnf.boundary["minZ"])
        d = int(math.floor(deep))

        if r >= Height or c >= Width or d >= Deep:
            continue
        bev_map_per_index[r, c] = 1
        voxel_num[d, r, c] += 1
        voxel_all_feature[0, d, r, c] += x
        voxel_all_feature[1, d, r, c] += y
        voxel_all_feature[2, d, r, c] += z

        voxel_ave_feature[0, d, r, c] = voxel_all_feature[0, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[1, d, r, c] = voxel_all_feature[1, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[2, d, r, c] = voxel_all_feature[2, d, r, c] / voxel_num[d, r, c]

        # num = voxel_num[d, r, c]
        # if num <= config_voxel_points_num - 1:
        #     bev_map_per_voxel_x[num-1, d, r, c] = x
        #     bev_map_per_voxel_y[num-1, d, r, c] = y
        #     bev_map_per_voxel_z[num-1, d, r, c] = z
        
        # add variance feature
        # voxel_ave_feature[3, d, r, c] = np.var(bev_map_per_voxel_x[:num+1, d, r, c])
        # voxel_ave_feature[4, d, r, c] = np.var(bev_map_per_voxel_y[:num+1, d, r, c])
        # voxel_ave_feature[5, d, r, c] = np.var(bev_map_per_voxel_z[:num+1, d, r, c])

        # add density feature
        voxel_ave_feature[3, d, r, c] = np.minimum(1.0, np.log(voxel_num[d, r, c] + 1) / np.log(64))

        # per_index = bev_map_per_index[r, c]
        # bev_map_per_index[r, c] += 1
        # if per_index > 9:
        #     continue
        # bev_map[0, per_index, r, c] = x
        # bev_map[1, per_index, r, c] = y
        # bev_map[2, per_index, r, c] = z
        # bev_map[3, per_index, r, c] = i
    # voxel_num_nonzero = voxel_num.nonzero()
    # for i in range(voxel_num_nonzero[0].shape[0]):
    #     d = voxel_num_nonzero[0][i]
    #     r = voxel_num_nonzero[1][i]
    #     c = voxel_num_nonzero[2][i]
    #     num = voxel_num[d, r, c]
    #     if num != 1:
    #         voxel_ave_feature[1, d, r, c] = np.var(bev_map_per_voxel_x[:num, d, r, c]) 
    #         voxel_ave_feature[3, d, r, c] = np.var(bev_map_per_voxel_y[:num, d, r, c])
    #         voxel_ave_feature[5, d, r, c] = np.var(bev_map_per_voxel_z[:num, d, r, c])        


    # 压缩成有效的点
    # bev_map_compress = np.zeros((4, 10, bev_map_per_index.nonzero()[0].shape[0]))
    # bev_map_compress = voxel_ave_feature[:, :, bev_map_per_index.nonzero()[0], bev_map_per_index.nonzero()[1]]

    # 保证可以还原回去
    # bev_map_1 = np.zeros((4, 10, Height, Width))
    # bev_map_1[:, :, bev_map_per_index.nonzero()[0], bev_map_per_index.nonzero()[1]] = RGB_Map
    # return RGB_Map, bev_map_per_index
    
    # 三个维度，可以翻转
    bev_map = voxel_ave_feature.reshape(-1, Height, Width)

    # 最原始的，占的显存比较大，空间换时间
    # bev_map = voxel_ave_feature.reshape(7, 10, Height*Width)

    # return bev_map_compress, bev_map_per_index
    # return bev_map, bev_map_per_index
    return bev_map

def make_bev_ma_small(PointCloud_, boundary, rgb=None):
    Height = cnf.BEV_HEIGHT / 2
    Width = cnf.BEV_WIDTH / 2
    CLOUD_R = PointCloud_.shape[0]
    bev_map = np.zeros((4, 10, Height, Width))
    bev_map_per_index = np.zeros((Height, Width), dtype=int)

    # 点云随机选取10个点
    random_shuffle_list = list(range(CLOUD_R))
    random.shuffle(random_shuffle_list)

    # 将点云按照z从高到低排序
    # indices = np.lexsort((-PointCloud_[:, 2], PointCloud_[:, 1], PointCloud_[:, 0]))
    # PointCloud_ = PointCloud_[indices]  
    # range_list = list(range(CLOUD_R)) 
    # random_shuffle_list = range_list

    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]
        i = PointCloud_[i, 3]

        row = Height * (x - cnf.boundary["minX"]) / (cnf.boundary["maxX"] - cnf.boundary["minX"])
        col = Width * (y - cnf.boundary["minY"]) / (cnf.boundary["maxY"] - cnf.boundary["minY"])
        h = (z - cnf.boundary["minZ"]) / (cnf.boundary["maxZ"] - cnf.boundary["minZ"])
        r = int(math.floor(row))
        c = int(math.floor(col))

        if r >= Height or c >= Width:
            continue
        
        per_index = bev_map_per_index[r, c]
        bev_map_per_index[r, c] += 1
        if per_index > 9:
            # continue
            per_index = 9
        bev_map[0, per_index, r, c] = x
        bev_map[1, per_index, r, c] = y
        bev_map[2, per_index, r, c] = z
        bev_map[3, per_index, r, c] = i

    # 压缩成有效的点
    # RGB_Map = np.zeros((4, 10, bev_map_per_index.nonzero()[0].shape[0]))
    # RGB_Map = bev_map[:, :, bev_map_per_index.nonzero()[0], bev_map_per_index.nonzero()[1]]

    # 保证可以还原回去
    # bev_map_1 = np.zeros((4, 10, Height, Width))
    # bev_map_1[:, :, bev_map_per_index.nonzero()[0], bev_map_per_index.nonzero()[1]] = RGB_Map
    # return RGB_Map, bev_map_per_index
    
    # 三个维度，可以翻转
    # bev_map = bev_map.reshape(-1, Height, Width)

    # 最原始的，占的显存比较大，空间换时间
    bev_map = bev_map.reshape(4, 10, Height*Width)

    return bev_map, bev_map_per_index

# bev image coordinates format
def get_corners(x, y, w, l, yaw):
    bev_corners = np.zeros((4, 2), dtype=np.float32)
    cos_yaw = np.cos(yaw)
    sin_yaw = np.sin(yaw)
    # front left
    bev_corners[0, 0] = x - w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[0, 1] = y - w / 2 * sin_yaw + l / 2 * cos_yaw

    # rear left
    bev_corners[1, 0] = x - w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[1, 1] = y - w / 2 * sin_yaw - l / 2 * cos_yaw

    # rear right
    bev_corners[2, 0] = x + w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[2, 1] = y + w / 2 * sin_yaw - l / 2 * cos_yaw

    # front right
    bev_corners[3, 0] = x + w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[3, 1] = y + w / 2 * sin_yaw + l / 2 * cos_yaw

    return bev_corners

# bev image coordinates format
def get_pts(x, y, w, l, yaw):
    cos_yaw = np.cos(yaw)
    sin_yaw = np.sin(yaw)
    # front left
    x1 = x - w / 2 * cos_yaw - l / 2 * sin_yaw
    y1 = y - w / 2 * sin_yaw + l / 2 * cos_yaw

    # rear left
    x4 = x - w / 2 * cos_yaw + l / 2 * sin_yaw
    y4 = y - w / 2 * sin_yaw - l / 2 * cos_yaw

    # rear right
    x3 = x + w / 2 * cos_yaw + l / 2 * sin_yaw
    y3 = y + w / 2 * sin_yaw - l / 2 * cos_yaw

    # front right
    x2 = x + w / 2 * cos_yaw - l / 2 * sin_yaw
    y2 = y + w / 2 * sin_yaw + l / 2 * cos_yaw

    return [[x1,y1], [x2,y2], [x3,y3], [x4,y4]]

def mergeImg(inputImg, maskImg, contourData, drawPosition):
    '''
    :param inputImg: 输入的图像
    :param maskImg: 输入的模板图像
    :param contourData: 输入的模板中轮廓数据 numpy 形式如[(x1,y1),(x2,y2),...,]
    :param drawPosition: （x,y） 大图中要绘制模板的位置,以maskImg左上角为起始点
    :return: outPutImg：输出融合后的图像
             outContourData: 输出轮廓在inputImg的坐标数据
             outRectData: 输出轮廓的矩形框在inputImg的坐标数据
    '''

    if (inputImg.shape[2] != maskImg.shape[2]):
        print("inputImg shape != maskImg shape")
        return
    inputImg_h = inputImg.shape[0]
    inputImg_w = inputImg.shape[1]
    maskImg_h = maskImg.shape[0]
    maskImg_w = maskImg.shape[1]

    if(inputImg_h < maskImg_h or inputImg_w < maskImg_w):
        print("inputImg size < maskImg size")
        return

    if(((drawPosition[0] + maskImg_w) > inputImg_w) or ((drawPosition[1] + maskImg_h) > inputImg_h)):
        print("drawPosition + maskImg > inputImg range")
        return
    outPutImg = inputImg.copy()
    input_roi = outPutImg[drawPosition[1]:drawPosition[1]+maskImg_h,drawPosition[0]:drawPosition[0]+maskImg_w]
    imgMask_array = np.zeros((maskImg_h,maskImg_w,maskImg.shape[2]),dtype=np.uint8)
    #triangles_list = [np.zeros((len(contourData), 2), int)]
    triangles_list=[contourData]
    cv2.fillPoly(imgMask_array, triangles_list, color=(1,1,1))
    cv2.fillPoly(input_roi, triangles_list, color=(0, 0, 0))
    #cv2.imshow('imgMask_array', imgMask_array)
    imgMask_array = imgMask_array * maskImg
    output_ori = input_roi + imgMask_array
    outPutImg[drawPosition[1]:drawPosition[1] + maskImg_h, drawPosition[0]:drawPosition[0] + maskImg_w]=output_ori
    triangles_list[0][:, 0] = contourData[:, 0] + drawPosition[0]
    triangles_list[0][:, 1] = contourData[:, 1] + drawPosition[1]
    outContourData = triangles_list[0]
    return outPutImg, outContourData

def if_label(img, corners, cls_id):
    img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY).reshape(1216, 608, -1)
    mask_img_gray = np.zeros_like(img_gray)
    corners = corners.reshape(-1, 2)
    outPutImg, outContourData = mergeImg(mask_img_gray, img_gray, corners, (0,0))
    point_num = len(outPutImg.nonzero()[0])
    if cls_id == 1:
        if point_num <= 10:
            return False
        else:
            return True
    else:
        if point_num <= 2:
            return False
        else:
            return True


def filter_label(img, labels):
    labels_filter_list = []
    for box_idx, (cls_id, x, y, z, h, w, l, yaw) in enumerate(labels):
        # Draw rotated box
        yaw = -yaw
        y1 = int((x - cnf.boundary['minX']) / cnf.DISCRETIZATION_Y)
        x1 = int((y - cnf.boundary['minY']) / cnf.DISCRETIZATION_X)
        w1 = int(w / cnf.DISCRETIZATION_X)
        l1 = int(l / cnf.DISCRETIZATION_Y)

        bev_corners = get_corners(x1, y1, w1, l1, yaw)
        corners_int = bev_corners.reshape(-1, 1, 2).astype(int)
        flag_label_exist = if_label(img, corners_int, cls_id)
        flag_label_dis = math.sqrt(x*x + y*y) > 5
        if not flag_label_dis:
            break
        if flag_label_exist and flag_label_dis:
        # if 1:
            labels_filter_list.append(labels[box_idx])
    return flag_label_dis, labels_filter_list

def drawRotatedBox(img, x, y, w, l, yaw, color, c, rect=None, score=1.0):
    font = cv2.FONT_HERSHEY_SIMPLEX
    bev_corners = get_corners(x, y, w, l, yaw)

    corners_int = bev_corners.reshape(-1, 1, 2).astype(int)
    # flag_label = if_label(img, corners_int)
    # if flag_label == False:
    #     return
    # if c != 4:
    #     return
    cv2.polylines(img, [corners_int], True, color, 1)
    corners_int = bev_corners.reshape(-1, 2).astype(int)
    cv2.line(img, (corners_int[0, 0], corners_int[0, 1]), (corners_int[3, 0], corners_int[3, 1]), (255, 255, 0), 1)
    center = (int((corners_int[0, 0]+corners_int[2, 0])/2), int((corners_int[0, 1]+corners_int[2, 1])/2))
    # cv2.putText(img, str(c)[0:-2], (center[0], center[1]), font, 1, (255, 255, 0), 1)
    # cv2.putText(img, str(score)[0:4], (corners_int[0, 0], corners_int[0, 1]), font, 1, (0, 255, 0), 1)
    cv2.putText(img, str(int(c))[0:1], (corners_int[3, 0], corners_int[3, 1]), font, 1, (0, 0, 255), 1)
    if rect is not None:
        pt1_x, pt1_y = int(rect[0]), int(rect[1])
        pt2_x, pt2_y = int(rect[2]), int(rect[3])
        cv2.rectangle(img, (pt1_x, pt1_y), (pt2_x, pt2_y), (255, 255, 0), 1)

