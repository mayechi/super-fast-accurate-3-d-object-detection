import os
import numpy as np
import pickle as pkl
import tfrecord
from waymo_open_dataset.utils import box_utils
from multiprocessing import Process, Queue
from lidar_bbox_tools_c import extract_points, overlap, polygon_overlap

class MultiProcesser(object):
    def __init__(self, flist, process_func, num_workers):
        assert type(flist) in (list, tuple)
        self.flist = flist
        self.process_func = process_func
        self.data_queue = Queue()
        self.result_queue = Queue(maxsize=1000)
        self.put_list()
        self.start_worker(num_workers)

    def put_list(self):
        for index in range(len(self.flist)):
            self.data_queue.put(self.flist[index])

    def get_result(self):
        while True:
            try:
                yield self.result_queue.get(timeout=30)
            except:
                return

    def start_worker(self, num_workers):
        workers = []
        def eval_worker(data_queue, result_queue):
            while True:
                output_dict = data_queue.get()
                for data in self.process_func(output_dict):
                    result_queue.put(data)
        for _ in range(num_workers):
            workers.append(Process(target=eval_worker, args=(self.data_queue, self.result_queue)))
        for w in workers:
            w.daemon = True
            w.start()

    def __len__(self):
        return len(self.flist)

    def run(self):
        print('Start processing!')
        for data in self.get_result():
            yield data
    
def match_find_max(ori):
    temp = []
    temp_score = []
    for i in range(len(ori)):
        col_argmax = np.argmax(ori[i])
        col_max = np.max(ori[i])
        row_argmax = np.argmax(ori[:, col_argmax])
        row_max = np.max(ori[:, col_argmax])
        if i == row_argmax and col_max != 0 and row_max != 0:
            temp.append(col_argmax)
            temp_score.append(col_max)
        else:
            temp.append(-1)
            temp_score.append(-1)
    return temp, temp_score

def get_matching_by_iou_ma(pred_corners, gt_corners, valid_gt):
    # matching_lst 能匹配到的检测框
    matching_lst, cls_label, flag_list = [], [], []
    matching_gt_bbox = []
    iou_mat = np.zeros((pred_corners.shape[0], gt_corners.shape[0]), dtype=np.float32)
    polygon_overlap(pred_corners.reshape(-1, 8), gt_corners.reshape(-1, 8), iou_mat)
    # My method for match
    result_list = []                
    while (iou_mat == 0).all() == False:
        temp, temp_score = match_find_max(iou_mat)
        for i in range(len(temp)):
            if temp[i] != -1:
                result_list.append([i, temp[i], temp_score[i]])
                iou_mat[i, :] = 0
                iou_mat[:, temp[i]] = 0    
    num = 0
    for i in range(pred_corners.shape[0]):
        if num < len(result_list):
            pred_index, gt_index, iou_score = result_list[num]
        else:
            pred_index = -1
        if i == pred_index:
            num = num + 1
            # if iou_score > 0.3:
            if iou_score > 0.1 and valid_gt[gt_index, -1] == 3:
                matching_lst.append(gt_index)
                cls_label.append(valid_gt[gt_index, -1])
                matching_gt_bbox.append(valid_gt[gt_index]) 
                flag_list.append(1)
            else:
                matching_lst.append(-1)
                cls_label.append(0)
                matching_gt_bbox.append(np.zeros((1, 8)))
                flag_list.append(1)
        else:
            matching_lst.append(-1)
            cls_label.append(0)
            matching_gt_bbox.append(np.zeros((1, 8)))
            flag_list.append(0)            

    matching_gt_bbox = np.vstack(matching_gt_bbox)
    cls_label = np.array(cls_label)
    flag_array = np.array(flag_list)
    # matching_lst = np.argmax(iou_mat, axis=-1)
    # cls_label = valid_gt[matching_lst, -1]
    return matching_gt_bbox, matching_lst, cls_label, flag_array

def get_matching_by_iou(pred_corners, gt_corners, valid_gt):
    # matching_lst 能匹配到的检测框
    iou_mat = np.zeros((pred_corners.shape[0], gt_corners.shape[0]), dtype=np.float32)
    polygon_overlap(pred_corners.reshape(-1, 8), gt_corners.reshape(-1, 8), iou_mat)
    matching_lst = np.argmax(iou_mat, axis=-1)
    cls_label = valid_gt[matching_lst, -1]
    return matching_lst, cls_label

def get_matching_gt(label, result):
    if result['boxes'].shape[0] == 0:
        return False, None, None, None
    if len(label['boxes']) == 0:
        return False, np.ones((result['boxes'].shape[0], 9)), np.zeros(result['boxes'].shape[0]), np.zeros(result['boxes'].shape[0])  
    
    gt_bbox = np.hstack([label['boxes'][:, 0, :], np.array(label['types'])[:, None]])
    
    gt_corners = box_utils.get_upright_3d_box_corners(gt_bbox[:, :-1]).numpy().astype(np.float32)
    # try:
    # pred_corners = box_utils.get_upright_3d_box_corners(result['boxes'][:, 0, :]).numpy().astype(np.float32)
    pred_corners = box_utils.get_upright_3d_box_corners(result['boxes'][:, 0, :-2]).numpy().astype(np.float32)
    # except:
    #     import pdb
    #     pdb.set_trace()
    # only use BEV for simple
    gt_corners_2d = gt_corners[:, :4, :2]
    pred_corners_2d = pred_corners[:, :4, :2]

    # matching_gt_bbox, matching_lst, cls_label, flag_array = get_matching_by_iou_ma(pred_corners_2d, gt_corners_2d, gt_bbox)

    matching_lst, cls_label = get_matching_by_iou(pred_corners_2d, gt_corners_2d, gt_bbox)
    matching_gt_bbox = gt_bbox[matching_lst, :].astype(np.float32)
    return True, matching_gt_bbox, cls_label, None 
    # return True, matching_gt_bbox, cls_label, flag_array

def save_as_pcd(scan, path):
    if os.path.exists(path):
        os.remove(path)

    handle = open(path, 'a')
    header = ['# .PCD v0.7 - Point Cloud Data file format\n', 'VERSION 0.7\n', 'FIELDS x y z i\n', 'SIZE 4 4 4 4\n', 'TYPE F F F F\n', 'COUNT 1 1 1 1\n']
    handle.writelines(header)
    handle.write('WIDTH ' + str(scan.shape[0]) + '\n')
    handle.write('HEIGHT 1\n')
    handle.write('VIEWPOINT 0 0 0 1 0 0 0\n')
    handle.write('POINTS ' + str(scan.shape[0]) + '\n')
    handle.write('DATA ascii\n')

    for i in range(scan.shape[0]):
        pt_str = str(scan[i, 0]) + ' ' + str(scan[i, 1]) + ' ' + str(scan[i, 2]) + ' ' + str(scan[i, 3] / 255.0) + '\n'
        handle.write(pt_str)

    handle.close()

# pc_dir = '/home/mayechi/Work/Research/code/Super-Fast-Accurate-3D-Object-Detection/dataset/apollo/lidar_rcnn/data_processer_val/pc/'
# label_dir = '/home/mayechi/Work/Research/code/Super-Fast-Accurate-3D-Object-Detection/dataset/apollo/lidar_rcnn/data_processer_val/gt/'
# result_dir = '/home/mayechi/Work/Research/code/Super-Fast-Accurate-3D-Object-Detection/dataset/apollo/lidar_rcnn/data_processer_val/rs/'
# target_file = '/home/mayechi/Work/Research/code/Super-Fast-Accurate-3D-Object-Detection/dataset/apollo/lidar_rcnn/data_processer_val/target/val_person.pkl'
pc_dir = '/media/myc/Data2/apollo/lidar_rcnn/data_processer/pc/'
label_dir = '/media/myc/Data2/apollo/lidar_rcnn/data_processer/gt/'
result_dir = '/media/myc/Data2/apollo/lidar_rcnn/data_processer/rs/'
target_file = '/media/myc/Data2/apollo/lidar_rcnn/data_processer/target/train_person.pkl'
# pc_fore_dir = '/home/mayechi/Work/Research/code/Super-Fast-Accurate-3D-Object-Detection/dataset/apollo/lidar_rcnn/data_processer/pc_fore/'
# pc_back_dir = '/home/mayechi/Work/Research/code/Super-Fast-Accurate-3D-Object-Detection/dataset/apollo/lidar_rcnn/data_processer/pc_back/'
pc_fore_dir = '/media/myc/Data21/zhijiangyihao/training2/lidar_rcnn/pc_fore_pcd/'
pc_back_dir = '/media/myc/Data21/zhijiangyihao/training2/lidar_rcnn/pc_back_pcd/'
pc_list = os.listdir(pc_dir)
label_list = os.listdir(label_dir)
result_list = os.listdir(result_dir)

expand_proposal_meter = 3
with open(target_file, 'wb') as fo: 
    target_list = list()
    for i in range(len(pc_list)):
        # if i == 10:
        #     break
        frame_name = pc_list[i].split('.npz')[0]

        pcds = np.load(pc_dir+pc_list[i])['pc']
        label = np.load(label_dir+label_list[i])
        result = np.load(result_dir+result_list[i])
        flag, matching_gt_bbox, cls_label, flag_array = get_matching_gt(label, result)
        if flag == False:
            continue
        
        invalid_mask_cache = np.ones(pcds.shape[0]).astype(bool)
        # extract points
        pcds_in_box_all = []
        invalid_mask_cache = np.ones(pcds.shape[0]).astype(bool)
        for j in range(result['boxes'].shape[0]):
            cls = int(cls_label[j])
            score = result['boxes'][j, 0, -1]
            # if flag_array[j] == 0:
            #     continue 
            if score < 0.1:
                continue
            # if cls != 0:
            #     continue
            # get centerx, centery, length, width, heading
            pred_bbox_bev = result['boxes'][:, 0, :][j, [0, 1, 3, 4, 6]].astype(np.float32)
            # enlarge the length and width by 3 meterxin
            valid_mask = extract_points(pcds, pred_bbox_bev, expand_proposal_meter, False).reshape(-1) 
            pcds_in_box = pcds[valid_mask]
            invalid_mask_cache = (~valid_mask)&invalid_mask_cache

            pcds_out_box = pcds[~valid_mask]
            pcds_in_box_all.append(pcds_in_box)
            if len(pcds_in_box) != 0:
                name = frame_name + '_' + str(j)
                data = [frame_name, pcds_in_box.astype(np.float16), result['boxes'][:, 0, :].astype(np.float16)[j], matching_gt_bbox[j].astype(np.float16), cls_label.astype(np.float16)[j]]
                target_list.append(data)
        pcds_in_box_all = np.concatenate(pcds_in_box_all)
        pcds_out_box_all = pcds[invalid_mask_cache]
        
        save_as_pcd(pcds_in_box_all, pc_fore_dir+frame_name+".pcd")
        save_as_pcd(pcds_out_box_all, pc_back_dir+frame_name+".pcd")
        # pcds_in_box_all.tofile(pc_fore_dir+frame_name+".bin")
        # pcds_out_box_all.tofile(pc_back_dir+frame_name+".bin")
    # pkl.dump(target_list, fo)

# with open(target_file, 'rb') as fo:     # 读取pkl文件数据
#     dict_data = pkl.load(fo, encoding='bytes')