import shutil
import os

'''拷贝apollo_bin数据到一个文件夹'''
# bin_apollo_path = './dataset/apollo/ImageSets/val.txt'
# bin_apollo_path_list = [x[0:-1] for x in open(bin_apollo_path).readlines()]
# wirte_bin_dir = '/home/mayechi/Data/apollo/kitti_format/val/bin/'
# for bin_path in bin_apollo_path_list:
#     bin_name = bin_path.split('/')[-2] + '_' + bin_path.split('/')[-1]
#     shutil.copyfile(bin_path, wirte_bin_dir+bin_name)

'''拷贝apollo_label数据到一个文件夹'''  
bin_apollo_path = './dataset/apollo/ImageSets/val_label.txt'
bin_apollo_path_list = [x[0:-1] for x in open(bin_apollo_path).readlines()]
wirte_bin_dir = '/home/mayechi/Data/apollo/kitti_format/val/label/'
for bin_path in bin_apollo_path_list:
    bin_name = 'result_' + bin_path.split('/')[-2] + '_frame_' + bin_path.split('/')[-1]
    shutil.copyfile(bin_path, wirte_bin_dir+bin_name)    

'''apollo的标注格式转换成kitti格式用验证map'''
# apollo_kitti_dict = {1:'Car', 2:'Car', 3:'Pedestrian', 4:'Pedestrian', 5:'Cyclist', 6:'Cyclist'}
# label_apollo_dir = '/home/mayechi/Data/apollo/kitti_format/train/label/'
# wirte_kitti_format_dir = '/home/mayechi/Data/apollo/kitti_format/train/label_kitti_format/' 
# label_apollo_files = os.listdir(label_apollo_dir)
# for label_name in label_apollo_files:
#     label_path = label_apollo_dir + label_name
#     with open(wirte_kitti_format_dir+label_name, 'w') as f:
#         for line in open(label_path, 'r'):
#             line = line.rstrip()
#             line_parts = line.split(' ')
#             cat_id = int(line_parts[0]) 
#             cat = apollo_kitti_dict[cat_id]
#             x, y, z = line_parts[1], line_parts[2], line_parts[3]
#             l, w, h = line_parts[4], line_parts[5], line_parts[6]
#             ry = line_parts[7]
#             f.write(cat+' 0 0 0 '+'0'+' '+'0'+' '+'0'+' '+'0'+' '+h+' '+w+' '+l+' '+x+' '+y+' '+z+' '+ry+'\n')
#     f.close()

