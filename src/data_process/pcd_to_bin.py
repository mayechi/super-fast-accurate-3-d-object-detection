from ftplib import error_perm
import os
import re
import struct
import warnings

import lzf
import numpy as np
import shutil

numpy_pcd_type_mappings = [(np.dtype('float32'), ('F', 4)),
                           (np.dtype('float64'), ('F', 8)),
                           (np.dtype('uint8'), ('U', 1)),
                           (np.dtype('uint16'), ('U', 2)),
                           (np.dtype('uint32'), ('U', 4)),
                           (np.dtype('uint64'), ('U', 8)),
                           (np.dtype('int16'), ('I', 2)),
                           (np.dtype('int32'), ('I', 4)),
                           (np.dtype('int64'), ('I', 8))]
numpy_type_to_pcd_type = dict(numpy_pcd_type_mappings)
pcd_type_to_numpy_type = dict((q, p) for (p, q) in numpy_pcd_type_mappings)


def parse_header(lines):
    metadata = {}
    for ln in lines:
        if ln.startswith('#') or len(ln) < 2:
            continue
        match = re.match(r'(\w+)\s+([\w\s\.]+)', ln)
        if not match:
            warnings.warn("warning: can't understand line: %s" % ln)
            continue
        key, value = match.group(1).lower(), match.group(2)
        if key == 'version':
            metadata[key] = value
        elif key in ('fields', 'type'):
            metadata[key] = value.split()
        elif key in ('size', 'count'):
            metadata[key] = [int(i) for i in value.split()]
        elif key in ('width', 'height', 'points'):
            metadata[key] = int(value)
        elif key == 'viewpoint':
            metadata[key] = [float(i) for i in value.split()]
        elif key == 'data':
            metadata[key] = value.strip().lower()
        # TODO apparently count is not required?
    # add some reasonable defaults
    if 'count' not in metadata:
        metadata['count'] = [1] * len(metadata['fields'])
    if 'viewpoint' not in metadata:
        metadata['viewpoint'] = [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0]
    if 'version' not in metadata:
        metadata['version'] = '.7'
    return metadata


def build_dtype(metadata):
    """ build numpy structured array dtype from pcl metadata.
    note that fields with count > 1 are 'flattened' by creating multiple
    single-count fields.
    TODO: allow 'proper' multi-count fields.
    """
    fieldnames = []
    typenames = []
    for f, c, t, s in zip(metadata['fields'], metadata['count'],
                          metadata['type'], metadata['size']):
        np_type = pcd_type_to_numpy_type[(t, s)]
        if c == 1:
            fieldnames.append(f)
            typenames.append(np_type)
        else:
            fieldnames.extend(['%s_%04d' % (f, i) for i in range(c)])
            typenames.extend([np_type] * c)

    dtype = np.dtype(list(zip(fieldnames, typenames)))
    return dtype


def read_pcd(filename):
    """ Reads and pcd file and return the elements as pandas Dataframes.
    Parameters
    ----------
    filename: str
        Path to the pcd file.
    Returns
    -------
    pandas Dataframe.
    """
    data = {}
    with open(filename, 'rb') as f:
        header = []
        index = 0
        while True:
            ln = f.readline().strip().decode()
            header.append(ln)
            index += 1
            if ln.startswith('DATA'):
                metadata = parse_header(header)
                dtype = build_dtype(metadata)
                break
            if index > 20:
                return None

        if metadata['data'] == 'ascii':
            pc_data = np.loadtxt(f, dtype=dtype, delimiter=' ')

        elif metadata['data'] == 'binary':
            rowstep = metadata['points'] * dtype.itemsize
            # for some reason pcl adds empty space at the end of files
            buf = f.read(rowstep)

            pc_data = np.fromstring(buf, dtype=dtype)

        elif metadata['data'] == 'binary_compressed':
            fmt = 'II'
            compressed_size, uncompressed_size =\
                struct.unpack(fmt, f.read(struct.calcsize(fmt)))
            compressed_data = f.read(compressed_size)
            # TODO what to use as second argument? if buf is None
            # (compressed > uncompressed)
            # should we read buf as raw binary?
            buf = lzf.decompress(compressed_data, uncompressed_size)
            if len(buf) != uncompressed_size:
                raise IOError('Error decompressing data')
            # the data is stored field-by-field
            pc_data = np.zeros(metadata['width'], dtype=dtype)
            ix = 0
            for dti in range(len(dtype)):
                dt = dtype[dti]
                bytes = dt.itemsize * metadata['width']
                column = np.frombuffer(buf[ix:(ix + bytes)], dt)
                pc_data[dtype.names[dti]] = column
                ix += bytes
    return pc_data


def write_header(metadata, rename_padding=False):
    """ Given metadata as dictionary, return a string header.
    """
    template = """\
VERSION {version}
FIELDS {fields}
SIZE {size}
TYPE {type}
COUNT {count}
WIDTH {width}
HEIGHT {height}
VIEWPOINT {viewpoint}
POINTS {points}
DATA {data}
"""
    str_metadata = metadata.copy()

    if not rename_padding:
        str_metadata['fields'] = ' '.join(metadata['fields'])
    else:
        new_fields = []
        for f in metadata['fields']:
            if f == '_':
                new_fields.append('padding')
            else:
                new_fields.append(f)
        str_metadata['fields'] = ' '.join(new_fields)
    str_metadata['size'] = ' '.join(map(str, metadata['size']))
    str_metadata['type'] = ' '.join(metadata['type'])
    str_metadata['count'] = ' '.join(map(str, metadata['count']))
    str_metadata['width'] = str(metadata['width'])
    str_metadata['height'] = str(metadata['height'])
    str_metadata['viewpoint'] = ' '.join(map(str, metadata['viewpoint']))
    str_metadata['points'] = str(metadata['points'])
    tmpl = template.format(**str_metadata)
    return tmpl


def point_cloud_to_pcd(pointcloud, save_path, compressed=True):
    """ Write pointcloud as .pcd to fileobj.
    If data_compression is not None it overrides pc.data.
    """
    points = pointcloud.point
    size = len(points)
    header = {
        'version': .7,
        'fields': ['x', 'y', 'z', 'intensity'],
        'count': [1, 1, 1, 1],
        'width': size,
        'height': 1,
        'viewpoint': [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
        'points': size,
        'type': ['F', 'F', 'F', 'U'],
        'size': [4, 4, 4, 4],
        'data': 'binary_compressed'
    }
    if not compressed:
        header['data'] = 'binary'
    tmpl = write_header(header)

    pcd_file = os.path.join(
        save_path,
        str(int(pointcloud.measurement_time * 1e6)) + '.pcd')
    typenames = []
    for pcd_type, numpy_type in zip(header['type'], header['size']):
        numpy_type = pcd_type_to_numpy_type[(pcd_type, numpy_type)]
        typenames.append(numpy_type)
    np_dtype = np.dtype(list(zip(header['fields'], typenames)))
    arr = np.zeros(len(points), dtype=np_dtype)
    for i, point in enumerate(points):
        # change timestamp to timestamp_sec
        arr[i] = (
            point.x,
            point.y,
            point.z,
            point.intensity,
        )
    file_handle = open(pcd_file, 'wb')
    file_handle.write(tmpl.encode("utf-8"))
    if compressed:
        uncompressed_lst = []
        for fieldname in arr.dtype.names:
            column = np.ascontiguousarray(arr[fieldname]).tobytes('C')
            uncompressed_lst.append(column)
        uncompressed = b''.join(uncompressed_lst)
        uncompressed_size = len(uncompressed)
        # print("uncompressed_size = %r"%(uncompressed_size))
        buf = lzf.compress(uncompressed)
        if buf is None:
            # compression didn't shrink the file
            # TODO what do to do in this case when reading?
            buf = uncompressed
            compressed_size = uncompressed_size
        else:
            compressed_size = len(buf)
        fmt = 'II'
        file_handle.write(struct.pack(fmt, compressed_size, uncompressed_size))
        file_handle.write(buf)
    else:
        file_handle.write(arr.tobytes('C'))
    file_handle.close()


def save_pointcloud_bin(pointcloud, save_path, measurement_time):
    # points = pointcloud.point
    points = pointcloud
    header = {
        'version': .7,
        'fields': ['x', 'y', 'z', 'intensity'],
        'count': [1, 1, 1, 1],
        'width': len(points),
        'height': 1,
        'viewpoint': [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
        'points': len(points),
        'type': ['F', 'F', 'F', 'U'],
        'size': [4, 4, 4, 4],
        'data': 'binary'
    }
    typenames = []
    for pcd_type, numpy_type in zip(header['type'], header['size']):
        numpy_type = pcd_type_to_numpy_type[(pcd_type, numpy_type)]
        typenames.append(numpy_type)
    np_dtype = np.dtype(list(zip(header['fields'], typenames)))
    arr = np.zeros(len(points), dtype=np_dtype)
    for i, point in enumerate(points):
        # change timestamp to timestamp_sec
        arr[i] = (
            point[0],# point.x,
            point[1],# point.y,
            point[2],# point.z,
            point[3],# point.intensity,
        )
    bin_file = os.path.join(
        save_path,
        measurement_time + '.bin')
    arr.tofile(bin_file)


if __name__ == "__main__":
    pcd_dir_path = '/home/myc/桌面/temp/clean_pcd/'
    bin_dir_path = '/home/myc/桌面/temp/clean_bin/'
    label_dir_path = '/home/myc/桌面/temp/clean_label/'
    label_output_path = '/home/myc/桌面/temp/clean_label_output/'

    pcd_error_dir = '/home/myc/桌面/a/pcd_error/'
    label_error_dir = '/home/myc/桌面/a/label_error/'

    pcd_path_list = os.listdir(pcd_dir_path)
    for i in range(len(pcd_path_list)):
        # print(pcd_path_list[i])
        pc_data = read_pcd(pcd_dir_path+pcd_path_list[i])
        if pc_data == None:
            print(pcd_path_list[i])
            # os.remove(pcd_dir_path+pcd_path_list[i])
            shutil.move(pcd_dir_path+pcd_path_list[i], pcd_error_dir+pcd_path_list[i])
            # os.remove(label_dir_path+pcd_path_list[i].split('.pcd')[0]+'.txt')
            shutil.move(label_dir_path+pcd_path_list[i].split('.pcd')[0]+'.txt', label_error_dir+pcd_path_list[i].split('.pcd')[0]+'.txt')
            continue
        measurement_time_long = pcd_path_list[i].split('.pcd')[0]
        # measurement_time = measurement_time_long[0:-3]
        measurement_time = measurement_time_long
        save_pointcloud_bin(pc_data, bin_dir_path, measurement_time)
        shutil.copy(label_dir_path+pcd_path_list[i].split('.pcd')[0]+'.txt', label_output_path+pcd_path_list[i].split('.pcd')[0]+'.txt')


