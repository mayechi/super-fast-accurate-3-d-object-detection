"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.08.17
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: This script for the KITTI dataset
"""

import sys
import os
import time
import math
import json
from builtins import int
from glob import glob

import numpy as np
from torch.utils.data import Dataset
import cv2
import torch

sys.path.append('../')

from data_process.kitti_data_utils import get_filtered_lidar
from data_process.kitti_bev_utils import makeBEVMap
import config.kitti_config as cnf

from autolab_core import RigidTransform
from .pandaset import DataSet as Dataset_panda
from .pandaset import utils

class Demo_KittiDataset(Dataset):
    def __init__(self, configs, mode='test', lidar_aug=None, hflip_prob=None, num_samples=None):
        self.dataset_dir = configs.data_path
        self.input_size = configs.input_size
        self.hm_size = configs.hm_size

        self.num_classes = configs.num_classes
        self.max_objects = configs.max_objects

        assert mode in ['train', 'val', 'test'], 'Invalid mode: {}'.format(mode)
        self.mode = mode
        self.is_test = (self.mode == 'test')
        sub_folder = 'testing' if self.is_test else 'training'

        self.lidar_aug = lidar_aug
        self.hflip_prob = hflip_prob

        self.image_dir = os.path.join(self.dataset_dir, sub_folder, "image_2")
        self.lidar_dir = os.path.join(self.dataset_dir, sub_folder, "velodyne")
        self.calib_dir = os.path.join(self.dataset_dir, sub_folder, "calib")
        self.label_dir = os.path.join(self.dataset_dir, sub_folder, "label_2")
        split_txt_path = os.path.join(self.dataset_dir, 'ImageSets', '{}.txt'.format(mode))
        self.sample_id_list = [int(x.strip()) for x in open(split_txt_path).readlines()]

        if num_samples is not None:
            self.sample_id_list = self.sample_id_list[:num_samples]
        self.num_samples = len(self.sample_id_list)

    def __len__(self):
        return len(self.sample_id_list)

    def __getitem__(self, index):
        if self.is_test:
            return self.load_img_only(index)
        else:
            return self.load_img_with_targets(index)

    def load_img_only(self, index):
        """Load only image for the testing phase"""
        sample_id = int(self.sample_id_list[index])
        img_path, img_rgb = self.get_image(sample_id)
        lidarData = self.get_lidar(sample_id)
        lidarData = get_filtered_lidar(lidarData, cnf.boundary)
        bev_map = makeBEVMap(lidarData, cnf.boundary)
        bev_map = torch.from_numpy(bev_map)

        metadatas = {
            'img_path': img_path,
        }
        return bev_map
        # return metadatas, bev_map, img_rgb

    def load_img_with_targets(self, index):
        """Load images and targets for the training and validation phase"""
        sample_id = int(self.sample_id_list[index])
        img_path = os.path.join(self.image_dir, '{:06d}.png'.format(sample_id))
        lidarData = self.get_lidar(sample_id)
        calib = self.get_calib(sample_id)
        labels, has_labels = self.get_label(sample_id)
        if has_labels:
            labels[:, 1:] = transformation.camera_to_lidar_box(labels[:, 1:], calib.V2C, calib.R0, calib.P2)

        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])

        lidarData, labels = get_filtered_lidar(lidarData, cnf.boundary, labels)

        bev_map = makeBEVMap(lidarData, cnf.boundary)
        bev_map = torch.from_numpy(bev_map)

        hflipped = False
        if np.random.random() < self.hflip_prob:
            hflipped = True
            # C, H, W
            bev_map = torch.flip(bev_map, [-1])

        targets = self.build_targets(labels, hflipped)

        metadatas = {
            'img_path': img_path,
            'hflipped': hflipped
        }

        return metadatas, bev_map, targets

    def get_image(self, idx):
        # pdb.set_trace()
        img_path = os.path.join(self.image_dir, '{:06d}.png'.format(idx))
        img = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)

        return img_path, img

    def get_calib(self, idx):
        calib_file = os.path.join(self.calib_dir, '{:06d}.txt'.format(idx))
        # assert os.path.isfile(calib_file)
        return Calibration(calib_file)

    def get_lidar(self, idx):
        lidar_file = os.path.join(self.lidar_dir, '{:06d}.bin'.format(idx))
        # assert os.path.isfile(lidar_file)
        return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)

    def get_label(self, idx):
        labels = []
        label_path = os.path.join(self.label_dir, '{:06d}.txt'.format(idx))
        for line in open(label_path, 'r'):
            line = line.rstrip()
            line_parts = line.split(' ')
            obj_name = line_parts[0]  # 'Car', 'Pedestrian', ...
            cat_id = int(cnf.CLASS_NAME_TO_ID[obj_name])
            if cat_id <= -99:  # ignore Tram and Misc
                continue
            truncated = int(float(line_parts[1]))  # truncated pixel ratio [0..1]
            occluded = int(line_parts[2])  # 0=visible, 1=partly occluded, 2=fully occluded, 3=unknown
            alpha = float(line_parts[3])  # object observation angle [-pi..pi]
            # xmin, ymin, xmax, ymax
            bbox = np.array([float(line_parts[4]), float(line_parts[5]), float(line_parts[6]), float(line_parts[7])])
            # height, width, length (h, w, l)
            h, w, l = float(line_parts[8]), float(line_parts[9]), float(line_parts[10])
            # location (x,y,z) in camera coord.
            x, y, z = float(line_parts[11]), float(line_parts[12]), float(line_parts[13])
            ry = float(line_parts[14])  # yaw angle (around Y-axis in camera coordinates) [-pi..pi]

            object_label = [cat_id, x, y, z, h, w, l, ry]
            # object_label = [cat_id, x, y, z, l, w, h, ry]
            labels.append(object_label)

        if len(labels) == 0:
            labels = np.zeros((1, 8), dtype=np.float32)
            has_labels = False
        else:
            labels = np.array(labels, dtype=np.float32)
            has_labels = True

        return labels, has_labels

    def build_targets(self, labels, hflipped):
        minX = cnf.boundary['minX']
        maxX = cnf.boundary['maxX']
        minY = cnf.boundary['minY']
        maxY = cnf.boundary['maxY']
        minZ = cnf.boundary['minZ']
        maxZ = cnf.boundary['maxZ']

        num_objects = min(len(labels), self.max_objects)
        hm_l, hm_w = self.hm_size

        hm_main_center = np.zeros((self.num_classes, hm_l, hm_w), dtype=np.float32)
        cen_offset = np.zeros((self.max_objects, 2), dtype=np.float32)
        direction = np.zeros((self.max_objects, 2), dtype=np.float32)
        z_coor = np.zeros((self.max_objects, 1), dtype=np.float32)
        dimension = np.zeros((self.max_objects, 3), dtype=np.float32)

        indices_center = np.zeros((self.max_objects), dtype=np.int64)
        obj_mask = np.zeros((self.max_objects), dtype=np.uint8)

        for k in range(num_objects):
            cls_id, x, y, z, h, w, l, yaw = labels[k]
            cls_id = int(cls_id)
            # Invert yaw angle
            yaw = -yaw
            if not ((minX <= x <= maxX) and (minY <= y <= maxY) and (minZ <= z <= maxZ)):
                continue
            if (h <= 0) or (w <= 0) or (l <= 0):
                continue

            bbox_l = l / cnf.bound_size_x * hm_l
            bbox_w = w / cnf.bound_size_y * hm_w
            radius = compute_radius((math.ceil(bbox_l), math.ceil(bbox_w)))
            radius = max(0, int(radius))

            center_y = (x - minX) / cnf.bound_size_x * hm_l  # x --> y (invert to 2D image space)
            center_x = (y - minY) / cnf.bound_size_y * hm_w  # y --> x
            center = np.array([center_x, center_y], dtype=np.float32)

            if hflipped:
                center[0] = hm_w - center[0] - 1

            center_int = center.astype(np.int32)
            if cls_id < 0:
                ignore_ids = [_ for _ in range(self.num_classes)] if cls_id == - 1 else [- cls_id - 2]
                # Consider to make mask ignore
                for cls_ig in ignore_ids:
                    gen_hm_radius(hm_main_center[cls_ig], center_int, radius)
                hm_main_center[ignore_ids, center_int[1], center_int[0]] = 0.9999
                continue

            # Generate heatmaps for main center
            gen_hm_radius(hm_main_center[cls_id], center, radius)
            # Index of the center
            indices_center[k] = center_int[1] * hm_w + center_int[0]

            # targets for center offset
            cen_offset[k] = center - center_int

            # targets for dimension
            dimension[k, 0] = h
            dimension[k, 1] = w
            dimension[k, 2] = l

            # targets for direction
            direction[k, 0] = math.sin(float(yaw))  # im
            direction[k, 1] = math.cos(float(yaw))  # re
            # im -->> -im
            if hflipped:
                direction[k, 0] = - direction[k, 0]

            # targets for depth
            z_coor[k] = z - minZ

            # Generate object masks
            obj_mask[k] = 1

        targets = {
            'hm_cen': hm_main_center,
            'cen_offset': cen_offset,
            'direction': direction,
            'z_coor': z_coor,
            'dim': dimension,
            'indices_center': indices_center,
            'obj_mask': obj_mask,
        }

        return targets

    def draw_img_with_label(self, index):
        sample_id = int(self.sample_id_list[index])
        img_path, img_rgb = self.get_image(sample_id)
        lidarData = self.get_lidar(sample_id)
        calib = self.get_calib(sample_id)
        labels, has_labels = self.get_label(sample_id)
        if has_labels:
            labels[:, 1:] = transformation.camera_to_lidar_box(labels[:, 1:], calib.V2C, calib.R0, calib.P2)

        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])

        lidarData, labels = get_filtered_lidar(lidarData, cnf.boundary, labels)
        # V.draw_scenes(
        #     points=lidarData, ref_boxes=labels[:, 1:]
        # )
        # mlab.show(stop=True)

        bev_map = makeBEVMap(lidarData, cnf.boundary)

        return bev_map, labels, img_rgb, img_path


class DemoDataset_panda(Dataset):
    def __init__(self, data_path=None, ext='.bin'):
        # self.data_path = data_path
        # self.ext = ext
        # data_file_list = glob(str(data_path / f'*{self.ext}')) if self.data_path.is_dir() else [self.data_path]

        # data_file_list.sort()
        # self.sample_file_list = data_file_list
        self.data_path = data_path
        mode = 'test'
        self.dataset = Dataset_panda(self.data_path)
        seq_total_list = self.dataset.sequences()
        if mode == 'train':
            self.seq_list = seq_total_list[:-5]
        if mode == 'val':
            seq_num = len(seq_total_list)
            self.seq_list = seq_total_list[seq_num-5:]
        elif mode == 'test':
            seq_num = len(seq_total_list)
            self.seq_list = seq_total_list[seq_num-5:]
        self.seq_frame_list = []
        self.seq_obj = None
        self.seq_last = None
        self.frame_num = 80
        for seq in self.seq_list:
            for i in range(self.frame_num):
                self.seq_frame_list.append(seq+'_'+str(i))

    def __len__(self):
        return len(self.sample_file_list)

    def __getitem__(self, index):
        # sample_file = self.sample_file_list[index]
        # lidarData = self.get_lidar(sample_file)
        # return lidarData
        # bevmap = self.load_bevmap_front(index)
        bevmap = self.load_img_panda_only(index)
        return bevmap

    def load_img_panda_only(self, index):
        self.seq = self.seq_frame_list[index].split('_')[0]
        frame = int(self.seq_frame_list[index].split('_')[1])
        if self.seq != self.seq_last:
            if self.seq_obj and self.dataset:
                del self.seq_obj, self.dataset
                gc.collect()
            self.seq_last = self.seq
            self.dataset = Dataset_panda(self.data_path)
            self.seq_obj = self.dataset[self.seq]
            self.seq_obj.load()
            self.seq_obj.lidar.set_sensor(0)
        lidarData = self.trans_lidar(frame)

        # self.seq = self.seq_frame_list[index].split('_')[0]
        # frame = self.seq_frame_list[index].split('_')[1]
        # seq_obj = self.dataset[seq]
        # seq_obj.load()
        # seq_obj.lidar.set_sensor(0)
        # xyz = np.dot(seq_obj.lidar[frame].values[:, :3], cnf.R_z_90_)
        # f = seq_obj.lidar[frame].values[:, 3].reshape(-1, 1)
        # lidarData = np.hstack((xyz, f))
        t0 = time.time()
        lidarData = get_filtered_lidar(lidarData, cnf.boundary)
        bev_map = makeBEVMap(lidarData, cnf.boundary)
        bev_map = torch.from_numpy(bev_map)
        t1 = time.time()
        print("Pre cost time", t1 - t0)

        return bev_map

    def trans_lidar(self, frame):
        # print(self.data_path+'/' + self.seq + '/lidar/poses.json')
        with open(self.data_path+'/' + self.seq + '/lidar/poses.json') as f:
            pose = json.load(f)[frame]
            x = float(pose['position']['x'])
            y = float(pose['position']['y'])
            z = float(pose['position']['z'])

            q0 = float(pose['heading']['w'])
            q1 = float(pose['heading']['x'])
            q2 = float(pose['heading']['y'])
            q3 = float(pose['heading']['z'])
            self.R = math.atan2(2*(q0*q1+q2*q3),1-2*(q1*q1+q2*q2))
            self.P = math.asin(2*(q0*q2-q3*q1))
            self.Y = math.atan2(2*(q0*q3+q1*q2),1-2*(q3*q3+q2*q2))
            rotation_quaternion = np.asarray([q0, q1, q2, q3])
            translation = np.asarray([x, y, z])
            T_qua2rota = RigidTransform(rotation_quaternion, translation)
            self.RT = np.hstack((T_qua2rota.rotation, T_qua2rota.translation.reshape(3, 1)))
            self.RT = np.vstack((self.RT, np.array([0, 0, 0, 1])))

            # R = Rotation.from_quat([q0, q1, q2, q3]).as_matrix()
            # T = np.array([x, y, z]).reshape(3, 1)
            # RT = np.hstack((R, T))
            # RT = np.vstack((RT, np.array([0, 0, 0, 1])))
        
        xyz = self.seq_obj.lidar[frame].values[:, :3]

        xyz_num = xyz.shape[0]
        xyz = np.vstack((xyz.T, np.ones((1, xyz_num))))
        xyz = np.dot(np.linalg.inv(self.RT), xyz)

        xyz = xyz[0:-1, :].T
        xyz = np.dot(xyz, cnf.R_z_90)
        f = self.seq_obj.lidar[frame].values[:, 3].reshape(-1, 1)
        lidarData = np.hstack((xyz, f))   
        return  lidarData  


    def get_lidar(self, lidar_file):
        print('lidar_file:', lidar_file)
        points = np.fromfile(lidar_file).reshape(-1, 4).astype(np.float32)
        points[:, -1] = 0
        return points

    def load_bevmap_front(self, index):
        """Load only image for the testing phase"""
        sample_file = self.sample_file_list[index]
        lidarData = self.get_lidar(sample_file)
        t0 = time.time()
        front_lidar = get_filtered_lidar(lidarData, cnf.boundary)
        front_bevmap = makeBEVMap(front_lidar, cnf.boundary)
        front_bevmap = torch.from_numpy(front_bevmap)
        t1 = time.time()
        print('load data:', t1-t0)
        return front_bevmap

class DemoDataset_apollo(Dataset):
    def __init__(self, ):
        mode = 'test'
        if mode == 'train':
            self.lidar_path = '/home/mayechi/Data/apollo/ImageSets/train.txt'
            self.label_path = '/home/mayechi/Data/apollo/ImageSets/train_label.txt'
            # self.lidar_path = '/media/xrc/d/Data/apollo/ImageSets/train.txt'
            # self.label_path = '/media/xrc/d/Data/apollo/ImageSets/train_label.txt'            
            self.sample_label_list = [x[0:-1] for x in open(self.label_path).readlines()]
        if mode == 'val':
            self.lidar_path = '/home/mayechi/Data/apollo/ImageSets/val.txt'
            self.label_path = '/home/mayechi/Data/apollo/ImageSets/val_label.txt'
            # self.lidar_path = '/media/xrc/d/Data/apollo/ImageSets/val.txt'
            # self.label_path = '/media/xrc/d/Data/apollo/ImageSets/val_label.txt'           
            self.sample_label_list = [x[0:-1] for x in open(self.label_path).readlines()]
        elif mode == 'test':
            # self.lidar_path = '/home/mayechi/Data/apollo/ImageSets/test.txt'
            self.lidar_path = '/home/mayechi/Data/apollo/ImageSets/test.txt'
            self.label_list = ''
            self.sample_label_list = []


        self.sample_path_list = [x[0:-1] for x in open(self.lidar_path).readlines()]
        self.num_samples = len(self.sample_path_list)

    def __len__(self):
        return self.num_samples

    def __getitem__(self, index):
        # sample_file = self.sample_file_list[index]
        # lidarData = self.get_lidar(sample_file)
        # return lidarData
        bevmap = self.load_bevmap_front(index)
        # bevmap = self.load_img_panda_only(index)
        return bevmap

    def get_lidar(self, lidar_file):
        return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)

    def load_bevmap_front(self, index):
        """Load only image for the testing phase"""
        sample_file = self.sample_path_list[index]
        lidarData = self.get_lidar(sample_file)
        t0 = time.time()
        front_lidar = get_filtered_lidar(lidarData, cnf.boundary)
        front_bevmap = makeBEVMap(front_lidar, cnf.boundary)
        front_bevmap = torch.from_numpy(front_bevmap)
        t1 = time.time()
        print('load data:', t1-t0)
        return front_bevmap