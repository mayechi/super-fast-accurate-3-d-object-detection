import os
import shutil

from matplotlib.pyplot import flag

# dir_path = '/home/myc/桌面/temp/label22_no_cut/'
# out_path = '/home/myc/桌面/temp/label22/'
# file_list = os.listdir(dir_path)

# for file in file_list:
#   name_tag = file.split('.txt')[0] + '_S2_1.txt'
#   shutil.copy(dir_path+file, out_path+name_tag)

# for file in file_list:
#   name_tag = file.split('.txt')[0][:16]+'.txt'
#   shutil.copy(dir_path+file, out_path+name_tag)


# Delete the wrong files
# bin_path = '/media/myc/Data21/zhijiangyihao/training2/pointcloud/bin/'
# delete_path = '/media/myc/Data21/zhijiangyihao/training2/pointcloud/bin16_S2/'

# bin_files_list = os.listdir(bin_path)
# delete_files_list = os.listdir(delete_path)

# for bin_file in bin_files_list:
#   for delete_file in delete_files_list:
#     if bin_file == delete_file:
#       os.remove(bin_path+bin_file)

# 更改名字（加上视觉时间戳）
# label_9_1_dir = '/media/myc/Data21/zhijiangyihao/training_no_train/label/label9/'
# label_9_2_dir = '/media/myc/Data21/zhijiangyihao/training_fusion/label9/'

# label_9_1_list = os.listdir(label_9_1_dir)
# label_9_2_list = os.listdir(label_9_2_dir)

# for label_9_1 in label_9_1_list:
#   for label_9_2 in label_9_2_list:
#     if label_9_1.split('.txt')[0] in label_9_2.split('.txt')[0]:
#       shutil.copy(label_9_1_dir+label_9_1, label_9_2_dir+label_9_2)

# input_dir = '/media/myc/Data21/zhijiangyihao/training_no_train/label17/'
# out_dir = '/media/myc/Data21/zhijiangyihao/training_no_train/label17_S2/'
# input_list = os.listdir(input_dir)
# out_list = os.listdir(out_dir)

# for input in input_list:
#   if 'S2' in input:
#     shutil.move(input_dir+input, out_dir+input.split('_S2_2')[0]+'_S2_1.txt')
 

dir_path = '/media/myc/Data21/zhijiangyihao/training2/pointcloud/label/'
val_list_path = '/media/myc/Data21/zhijiangyihao/training2/pointcloud/ImageSets/val_id.txt'

train_list_path = '/media/myc/Data21/zhijiangyihao/training2/pointcloud/ImageSets/train_id.txt'


with open(val_list_path, 'r') as f:
  val_list = f.readlines()

file_list = os.listdir(dir_path)
file_list.sort()
with open(train_list_path, 'w') as f:
  for file_path in file_list:
    file_id = file_path.split('.txt')[0]
    flag = 1
    for val_id in val_list:
      if file_id == val_id[0:-1]:
        flag = 0
        break
    if flag == 1:
      f.write(file_id)
      f.write('\n')
f.close()










