#!/usr/bin/env python3
import os
from typing import List
import numpy as np
import torch
# from torchvision.ops.boxes import nms

import config.kitti_config as cnf


def subdirectories(directory: str) -> List[str]:
    """List all subdirectories of a directory.

    Args:
        directory: Relative or absolute path

    Returns:
        List of path strings for every subdirectory in `directory`.
    """
    return [d.path for d in os.scandir(directory) if d.is_dir()]

def nms_filter(labels):
    res = []
    for i in range(labels.shape[0]-1, -1, -1):
        repeat_flag = 0
        for j in range(i-1, -1, -1):
            cls_id_i, x_i, y_i, z_i, h_i, w_i, l_i, yaw_i = labels[i]
            cls_id_j, x_j, y_j, z_j, h_j, w_j, l_j, yaw_j = labels[j]
            diff_xyz = abs(x_i - x_j) + abs(y_i - y_j) + abs(z_i - z_j)
            diff_whl = abs(h_i - h_j) + abs(w_i - w_j) + abs(l_i - l_j) 
            diff_yaw = abs(yaw_i - yaw_j)
            if diff_xyz < 1.5 and diff_whl < 1 and diff_yaw < 0.1:
                repeat_flag = 1
                break
        if repeat_flag == 0:
            res.append(labels[i].reshape(1, -1))
    if res == []:
        return labels
    else:
        res = np.concatenate(res)
        return res
    # for box_idx, (cls_id, x, y, z, h, w, l, yaw) in enumerate(labels):

    #     a = 0
    #     yaw = -yaw
    #     y_p = int((x - cnf.boundary['minX']) / cnf.DISCRETIZATION)
    #     x_p = int((y - cnf.boundary['minY']) / cnf.DISCRETIZATION)
    #     w_p = int(w / cnf.DISCRETIZATION)
    #     l_p = int(l / cnf.DISCRETIZATION) 

    #     # torch.zeros((4, 2), dtype=np.float32)
    #     cos_yaw = np.cos(yaw)
    #     sin_yaw = np.sin(yaw)
    #     # front left
    #     x1 = x_p - w_p / 2 * cos_yaw - l_p / 2 * sin_yaw
    #     y1 = y_p - w / 2 * sin_yaw + l_p / 2 * cos_yaw

    #     # rear right
    #     x2 = x_p + w_p / 2 * cos_yaw + l_p / 2 * sin_yaw
    #     y2 = y_p + w_p / 2 * sin_yaw - l_p / 2 * cos_yaw
    #     bev_box2d.append(torch.tensor((x1, y1, x2, y2)).reshape(1, 4))
    #     bev_score.append(torch.tensor((1.0)).reshape(1, 1))
    # bev_box2d.append(torch.tensor((x1, y1, x2, y2)).reshape(1, 4))
    # bev_box2d.append(torch.tensor((5.0, 10.0, 15.0, 20.0)).reshape(1, 4))
    # bev_box2d.append(torch.tensor((5.0, 10.0, 15.0, 20.0)).reshape(1, 4))
    # bev_score.append(torch.tensor((0.9)).reshape(1, 1))
    # bev_score.append(torch.tensor((1.0)).reshape(1, 1))
    # bev_score.append(torch.tensor((0.9)).reshape(1, 1))
    # bev_box2d = torch.cat(bev_box2d)
    # bev_score = torch.cat(bev_score)
    # keep = nms(bev_box2d, bev_score, 1.0)
    # return bev_corners
if __name__ == '__main__':
    pass

# def iou(box1, box2):
# 　　  assert box1.size()==4 and box2.size()==4,"bounding box coordinate size must be 4"
#       bxmin = np.max(box1[0], box2[0])
#       bymin = np.max(box1[1], box2[1])
#       bxmax = np.min(box1[2], box2[2])
#       bymax = np.min(box1[3], box2[3])
#       bwidth = bxmax - bxmin
#       bhight = bymax - bxmin
#       inter = bwidth * bhight
#       union = (box1[2] - box1[0]) * (box1[3] - box1[1]) + (box2[2] - box2[0]) * (box2[3] - box2[1]) - inter
#       return inter / union

