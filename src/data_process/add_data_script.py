import os
import random
import shutil

train_id_file = '/home/mayechi/Data2/zhijiangyihao/training2/pointcloud/ImageSets/train_id_add_7.txt'
val_id_file = '/home/mayechi/Data2/zhijiangyihao/training2/pointcloud/ImageSets/val_id_add_7.txt'

label_dir = '/home/mayechi/Data2/zhijiangyihao/training2/pointcloud/label7'
label_file_list = os.listdir(label_dir)
file_id_list = []
for file in label_file_list:
    file_id = file.split('/')[-1].split('.txt')[0]
    file_id_list.append(file_id)

random.shuffle(file_id_list)
train_file_id_list = file_id_list[0:int(0.8*len(file_id_list))]
val_file_id_list = file_id_list[int(0.8*len(file_id_list)):]

with open(train_id_file, 'a') as f_train:
    for file_id in train_file_id_list:
        f_train.write(file_id)
        f_train.write('\n')
f_train.close()

with open(val_id_file, 'a') as f_val:
    for file_id in val_file_id_list:
        f_val.write('\n')
        f_val.write(file_id)
f_val.close()


# dir = '/media/myc/Data21/zhijiangyihao/test/BIN/temp/'
# out_dir = '/media/myc/Data21/zhijiangyihao/test/BIN/r/' 
# files_list = os.listdir(dir)
# files_list = sorted(files_list)
# for i, file in enumerate(files_list):
#     shutil.copy(dir+file, out_dir+str(i)+'.jpg')
#     a = 0