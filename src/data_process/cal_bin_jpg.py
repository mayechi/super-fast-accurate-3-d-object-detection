import os
import shutil


bin_dir = '/media/myc/Data21/zhijiangyihao/training2/fusion/3_0218/bin/'
out_bin_dir = '/media/myc/Data21/zhijiangyihao/training2/fusion/3_0218/bin_filter/'
jpg_dir = '/media/myc/Data21/zhijiangyihao/training2/fusion/3_0218/jpg/'
out_jpg_dir = '/media/myc/Data21/zhijiangyihao/training2/fusion/3_0218/jpg_filter/'


bin_list = os.listdir(bin_dir)
jpg_list = os.listdir(jpg_dir)

for bin_frame_id in bin_list:
  bin_frame_id = int(bin_frame_id.split('.')[0])
  diff_min = bin_frame_id
  for jpg_frame_id in jpg_list:
    jpg_frame_id = int(jpg_frame_id.split('.')[0])
    diff = abs(bin_frame_id - jpg_frame_id)
    if diff < diff_min:
      diff_min = diff
      jpg_frame_id_min = jpg_frame_id
  if diff_min < 50*1000:
    shutil.copy(bin_dir+str(bin_frame_id)+'.bin', out_bin_dir+str(bin_frame_id)+'.bin')
    shutil.copy(jpg_dir+str(jpg_frame_id_min)+'.jpg', out_jpg_dir+str(jpg_frame_id_min)+'.jpg')
      