"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.08.17
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: This script for creating the dataloader for training/validation/test phase
"""

import sys

import torch
from torch.utils.data import DataLoader
import numpy as np
import collections
from collections import defaultdict

sys.path.append('../')

from data_process.kitti_dataset import KittiDataset
from data_process.our_dataset import OurDataset
from data_process.apollo_dataset import ApolloDataset
from data_process.apollo_dataset_voxel import ApolloDatasetVoxel
from data_process.apollo_fusion_dataset import ApolloDatasetFusion
# from data_process.apollo_dataset_process import ApolloProcessDataset
# from data_process.apollo_centernet2_dataset import ApolloDatasetCenterNet2
from data_process.transformation import OneOf, Random_Rotation, Random_Scaling


def create_train_dataloader(configs):
    """Create dataloader for training"""
    # train_lidar_aug = OneOf([
    #     Random_Rotation(limit_angle=np.pi / 4, p=1.0),
    #     Random_Scaling(scaling_range=(0.95, 1.05), p=1.0),
    # ], p=0.66)
    train_lidar_aug = None
    # train_dataset = KittiDataset(configs, mode='train', lidar_aug=train_lidar_aug, hflip_prob=configs.hflip_prob,
    #                              num_samples=configs.num_samples)
    train_dataset = ApolloDataset(configs, mode='train', lidar_aug=train_lidar_aug, hflip_prob=configs.hflip_prob,
                                 num_samples=configs.num_samples)
    train_sampler = None
    # train_sampler = 1
    if configs.distributed:
        train_sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)
    train_dataloader = DataLoader(train_dataset, batch_size=configs.batch_size, shuffle=(train_sampler is None), #collate_fn=collate_dataset,
                                  pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=train_sampler)

    return train_dataloader, train_sampler

def create_voxel_train_dataloader(configs):
    """Create dataloader for training"""
    # train_lidar_aug = OneOf([
    #     Random_Rotation(limit_angle=np.pi / 4, p=1.0),
    #     Random_Scaling(scaling_range=(0.95, 1.05), p=1.0),
    # ], p=0.66)
    train_lidar_aug = None
    # train_dataset = KittiDataset(configs, mode='train', lidar_aug=train_lidar_aug, hflip_prob=configs.hflip_prob,
    #                              num_samples=configs.num_samples)
    train_dataset = ApolloDatasetVoxel(configs, mode='train', lidar_aug=train_lidar_aug, hflip_prob=configs.hflip_prob,
                                 num_samples=configs.num_samples)
    train_sampler = None
    # train_sampler = 1
    if configs.distributed:
        train_sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)
    train_dataloader = DataLoader(train_dataset, batch_size=configs.batch_size, shuffle=(train_sampler is None), collate_fn=collate_dataset,
                                  pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=train_sampler)

    return train_dataloader, train_sampler

def create_fusion_train_dataloader(configs):
    """Create dataloader for training"""
    # train_lidar_aug = OneOf([
    #     Random_Rotation(limit_angle=np.pi / 4, p=1.0),
    #     Random_Scaling(scaling_range=(0.95, 1.05), p=1.0),
    # ], p=0.66)
    train_lidar_aug = None
    train_dataset = ApolloDatasetFusion(configs, mode='train_fusion', lidar_aug=train_lidar_aug, hflip_prob=configs.hflip_prob,
                                        num_samples=configs.num_samples)

    if configs.distributed:
        train_sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)
    train_dataloader = DataLoader(train_dataset, batch_size=1, shuffle=False,
                                  pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=None)

    return train_dataloader

def create_fusion_val_dataloader(configs):
    """Create dataloader for training"""
    # train_lidar_aug = OneOf([
    #     Random_Rotation(limit_angle=np.pi / 4, p=1.0),
    #     Random_Scaling(scaling_range=(0.95, 1.05), p=1.0),
    # ], p=0.66)
    val_lidar_aug = None
    val_dataset = ApolloDatasetFusion(configs, mode='val_fusion', lidar_aug=val_lidar_aug, hflip_prob=configs.hflip_prob,
                                        num_samples=configs.num_samples)

    if configs.distributed:
        val_sampler = torch.utils.data.distributed.DistributedSampler(val_dataset)
    val_dataloader = DataLoader(val_dataset, batch_size=1, shuffle=False,
                                  pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=None)

    return val_dataloader

def create_fusion_no_nn_test_dataloader(configs):
    """Create dataloader for training"""
  
    train_lidar_aug = None
    train_dataset = ApolloDatasetFusion(configs, mode='test_fusion', lidar_aug=train_lidar_aug, hflip_prob=configs.hflip_prob,
                                        num_samples=configs.num_samples)
    train_dataloader = DataLoader(train_dataset, batch_size=1, shuffle=False,
                                  pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=None)

    return train_dataloader

def create_train_dataloader_lidar_rcnn(configs):
    """Create dataloader for training"""
    train_lidar_aug = OneOf([
        Random_Rotation(limit_angle=np.pi / 4, p=1.0),
        Random_Scaling(scaling_range=(0.95, 1.05), p=1.0),
    ], p=0.66)
    # train_dataset = KittiDataset(configs, mode='train', lidar_aug=train_lidar_aug, hflip_prob=configs.hflip_prob,
    #                              num_samples=configs.num_samples)
    train_dataset = ApolloProcessDataset(configs, mode='train', lidar_aug=train_lidar_aug, hflip_prob=configs.hflip_prob,
                                         num_samples=configs.num_samples)
    train_sampler = None
    if configs.distributed:
        train_sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)
    train_dataloader = DataLoader(train_dataset, batch_size=configs.batch_size, shuffle=False,#(train_sampler is None),
                                  pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=train_sampler)

    return train_dataloader, train_sampler

def create_val_dataloader(configs):
    """Create dataloader for validation"""
    val_sampler = None
    val_dataset = ApolloDataset(configs, mode='val', lidar_aug=None, hflip_prob=0., num_samples=configs.num_samples)
    if configs.distributed:
        val_sampler = torch.utils.data.distributed.DistributedSampler(val_dataset, shuffle=False)
    val_dataloader = DataLoader(val_dataset, batch_size=configs.batch_size, shuffle=False,
                                pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=val_sampler)

    return val_dataloader

def create_test_dataloader(configs):
    """Create dataloader for testing phase"""
    # test_dataset = KittiDataset(configs, mode='test', lidar_aug=None, hflip_prob=0., num_samples=configs.num_samples)
    test_dataset = ApolloDataset(configs, mode='test', lidar_aug=None, hflip_prob=0., num_samples=configs.num_samples)
    # test_dataset = ApolloDataset(configs, mode='test', lidar_aug=None, hflip_prob=0., num_samples=configs.num_samples)
    test_sampler = None
    if configs.distributed:
        test_sampler = torch.utils.data.distributed.DistributedSampler(test_dataset)
    test_dataloader = DataLoader(test_dataset, batch_size=1, shuffle=False,
                                 pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=test_sampler)
                                #  pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=test_sampler, collate_fn=collate_dataset)


    return test_dataloader

def create_test_voxel_dataloader(configs):
    """Create dataloader for testing phase"""
    # test_dataset = KittiDataset(configs, mode='test', lidar_aug=None, hflip_prob=0., num_samples=configs.num_samples)
    test_dataset = ApolloDatasetVoxel(configs, mode='test', lidar_aug=None, hflip_prob=0., num_samples=configs.num_samples)
    # test_dataset = ApolloDataset(configs, mode='test', lidar_aug=None, hflip_prob=0., num_samples=configs.num_samples)
    test_sampler = None
    # if configs.distributed:
    #     test_sampler = torch.utils.data.distributed.DistributedSampler(test_dataset)
    test_dataloader = DataLoader(test_dataset, batch_size=1, shuffle=False,
                                 pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=test_sampler, collate_fn=collate_dataset)


    return test_dataloader

def create_test_dataloader_lidar_rcnn(configs):
    """Create dataloader for testing phase"""
    # test_dataset = KittiDataset(configs, mode='test', lidar_aug=None, hflip_prob=0., num_samples=configs.num_samples)
    test_dataset = ApolloProcessDataset(configs, mode='test', lidar_aug=None, hflip_prob=0., num_samples=configs.num_samples)
    # test_dataset = ApolloDataset(configs, mode='test', lidar_aug=None, hflip_prob=0., num_samples=configs.num_samples)
    test_sampler = None
    if configs.distributed:
        test_sampler = torch.utils.data.distributed.DistributedSampler(test_dataset)
    test_dataloader = DataLoader(test_dataset, batch_size=256, shuffle=False,
                                 pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=test_sampler)

    return test_dataloader

def create_train_dataloader_centernet2(configs):
    """Create dataloader for training"""
    train_lidar_aug = None
    train_dataset = ApolloDatasetCenterNet2(configs, mode='train', lidar_aug=train_lidar_aug, hflip_prob=configs.hflip_prob,
                                            num_samples=configs.num_samples)
    train_sampler = None
    if configs.distributed:
        train_sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)
    train_dataloader = DataLoader(train_dataset, batch_size=configs.batch_size, shuffle=(train_sampler is None),
                                  pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=train_sampler)

    return train_dataloader, train_sampler

def collate_dataset(batch_list, samples_per_gpu=1):
    example_merged = collections.defaultdict(list)
    for example in batch_list:
        for k, v in example.items():
            example_merged[k].append(v)
    batch_size = len(batch_list)
    ret = {}

    for key, elems in example_merged.items():
        # if key in ['voxels', 'num_points_per_voxel']:
        if key in ['voxels', 'num_points_per_voxel', 'input_features']:            
            ret[key] = torch.tensor(np.concatenate(elems, axis=0))
        elif key in ["metadatas"]:
            ret[key] = elems
        elif key in ["coordinates", "points"]:
            coors = []
            for i, coor in enumerate(elems):
                coor_pad = np.pad(
                    coor, ((0, 0), (1, 0)), mode="constant", constant_values=i
                )
                coors.append(coor_pad)
            ret[key] = torch.tensor(np.concatenate(coors, axis=0))        
        elif key in ['targets']:
            ret[key] = defaultdict(list)
            res = []
            for elem in elems:
                for idx, ele in enumerate(elem):
                    ret[key][ele].append(torch.tensor(elem[ele]))
                    
            for kk, vv in ret[key].items():
                ret[key][kk] = torch.stack(vv)          
        elif key in ['gt_boxes', 'gt_boxes_no3daug']:
            max_gt = max([len(x) for x in elems])
            batch_gt_boxes3d = np.zeros(
                (batch_size, max_gt, elems[0].shape[-1]), dtype=np.float32)
            for k in range(batch_size):
                batch_gt_boxes3d[k, :elems[k].__len__(), :] = elems[k]
            ret[key] = batch_gt_boxes3d
        elif key in ['gt_names', 'gt_truncated', 'gt_occluded', 'gt_difficulty', 'gt_index']:
            ret[key] = [np.array(x) for x in elems]
        else:
            try:
                ret[key] = np.stack(elems, axis=0)
            except Exception:
                # print("key")
                # print(key)
                import ipdb; ipdb.set_trace()

    return ret
    # for key, elems in example_merged.items():
    #     if key in 
