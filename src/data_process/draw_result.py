boundary = {
    "minX": -50,
    "maxX": 50,
    "minY": -25, 
    "maxY": 25,
    "minZ": -2.4,
    "maxZ": 0.6
}

data_from = 'apollo'
if data_from == 'kitti':
    import config.kitti_config as cnf
elif data_from == 'apollo':
    import config.apollo_config as cnf
else:
    import config.simulation_config as cnf

def makeBEVMap(PointCloud_, boundary, rgb=None):
    Height = cnf.BEV_HEIGHT + 1
    Width = cnf.BEV_WIDTH + 1

    # Discretize Feature Map
    PointCloud = np.copy(PointCloud_)
    # PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / cnf.DISCRETIZATION))
    if data_from == 'kitti':
        # 针对Kitti数据集，只检测正前方
        PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / cnf.DISCRETIZATION))
        PointCloud[:, 1] = np.int_(np.floor(PointCloud[:, 1] / cnf.DISCRETIZATION) + Width / 2)
    else:
        # 针对Apollo数据集，检测360°
        PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / cnf.DISCRETIZATION_Y) + Height / 2)
        PointCloud[:, 1] = np.int_(np.floor(PointCloud[:, 1] / cnf.DISCRETIZATION_X) + Width / 2)


    # sort-3times
    indices = np.lexsort((-PointCloud[:, 2], PointCloud[:, 1], PointCloud[:, 0]))
    PointCloud = PointCloud[indices]
    if rgb is not None:
        rgb = rgb[indices]

    # Height Map
    heightMap = np.zeros((Height, Width))

    _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
    PointCloud_frac = PointCloud[indices]
    # some important problem is image coordinate is (y,x), not (x,y)
    max_height = float(np.abs(boundary['maxZ'] - boundary['minZ']))

    heightMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = PointCloud_frac[:, 2] / max_height

    # Intensity Map & DensityMap
    intensityMap = np.zeros((Height, Width))
    densityMap = np.zeros((Height, Width))

    _, indices, counts = np.unique(PointCloud[:, 0:2], axis=0, return_index=True, return_counts=True)
    PointCloud_top = PointCloud[indices]

    normalizedCounts = np.minimum(1.0, np.log(counts + 1) / np.log(64))

    intensityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = PointCloud_top[:, 3] / 255.0 # hesai40p的反射强度0~255
    densityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = normalizedCounts

    if rgb is not None:
        # Height Map
        # ImgMap = np.zeros((Height, Width, 1))
        ImgMap = np.zeros((Height, Width, 3))
        _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
        PointCloud_frac = PointCloud[indices]        
        # rgb_frac = rgb[indices].reshape(-1, 1)
        rgb_frac = rgb[indices].reshape(-1, 3)
        ImgMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = rgb_frac / 255.0
        ImgMap = np.transpose(ImgMap, (2, 0, 1))
        # RGB_Map = np.zeros((3, Height - 1, Width - 1))
        RGB_Map = np.zeros((3, Height - 1, Width - 1))
        
        RGB_Map[0:3, :, :] = ImgMap[:, :cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]
        # RGB_Map[1, :, :] = ImgMap[:, :cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]

        # RGB_Map[4, :, :] = densityMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # r_map
        # RGB_Map[3, :, :] = heightMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # g_map
        # RGB_Map[0, :, :] = intensityMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # b_map     
    else:
        if data_from == 'kitti':        
            RGB_Map = np.zeros((3, Height - 1, Width - 1))
            RGB_Map[2, :, :] = densityMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # r_map
            RGB_Map[1, :, :] = heightMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # g_map
            RGB_Map[0, :, :] = intensityMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # b_map
        else:
            RGB_Map = np.zeros((3, Height - 1, Width - 1))
            RGB_Map[2, :, :] = densityMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # r_map
            RGB_Map[1, :, :] = heightMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # g_map
            # RGB_Map[0, :, :] = intensityMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # b_map            

    return RGB_Map


def get_lidar(self, lidar_file):
    return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)

def get_label(self, idx):
    labels = []
    label_path = self.sample_label_list[idx]
    # print("label_path:", label_path)
    for line in open(label_path, 'r'):
        line = line.rstrip()
        line_parts = line.split(' ')
        cat_id = int(line_parts[0])  # 1 for small vehicles, 2 for big vehicles, 3 for pedestrian, 4 for motorcyclist and bicyclist, 5 for traffic cones and 6 for others
        # print("cat_id:", cat_id)
        if cat_id <= -99:  # ignore Tram and Misc
            continue
        x, y, z = float(line_parts[1]), float(line_parts[2]), float(line_parts[3])
        l, w, h = float(line_parts[4]), float(line_parts[5]), float(line_parts[6])
        # w, l, h = float(line_parts[4]), float(line_parts[5]), float(line_parts[6])
        ry = float(line_parts[7])
        object_label = [cat_id, x, y, z, h, w, l, ry]
        labels.append(object_label)

    if len(labels) == 0:
        labels = np.zeros((1, 8), dtype=np.float32)
        has_labels = False
    else:
        labels = np.array(labels, dtype=np.float32)
        has_labels = True

    return labels, has_labels

def get_filtered_lidar(lidar, boundary, labels=None, rgb=None):
    minX = boundary['minX']
    maxX = boundary['maxX']
    minY = boundary['minY']
    maxY = boundary['maxY']
    minZ = boundary['minZ']
    maxZ = boundary['maxZ']

    # Remove the point out of range x,y,z
    mask = np.where((lidar[:, 0] >= minX) & (lidar[:, 0] <= maxX) &
                    (lidar[:, 1] >= minY) & (lidar[:, 1] <= maxY) &
                    (lidar[:, 2] >= minZ) & (lidar[:, 2] <= maxZ))
    lidar = lidar[mask]
    lidar[:, 2] = lidar[:, 2] - minZ

    
    if labels is not None:
        label_x = (labels[:, 1] >= minX) & (labels[:, 1] < maxX)
        label_y = (labels[:, 2] >= minY) & (labels[:, 2] < maxY)
        label_z = (labels[:, 3] >= minZ) & (labels[:, 3] < maxZ)
        mask_label = label_x & label_y & label_z
        labels = labels[mask_label]
        if rgb is not None:
            rgb = rgb[mask]        
            return lidar, labels, rgb
        else:
            return lidar, labels, None
    else:
        if rgb is not None:
            rgb = rgb[mask] 
            return lidar, rgb     
        else:
            return lidar, None

def draw_lidar_with_label(self, index):
    sample_path = self.sample_path_list[index]
    lidarData = self.get_lidar(sample_path)
    labels, has_labels = self.get_label(index)

    lidarData, labels, _ = get_filtered_lidar(lidarData, cnf.boundary, labels)
    # lidarData, labels = get_filtered_lidar_simulation(lidarData, cnf.boundary, labels)
    bev_map = makeBEVMap(lidarData, cnf.boundary)

    return bev_map, labels

def drawRotatedBox(img, x, y, w, l, yaw, color, c, rect=None, score=1.0):
    font = cv2.FONT_HERSHEY_SIMPLEX
    bev_corners = get_corners(x, y, w, l, yaw)
    corners_int = bev_corners.reshape(-1, 1, 2).astype(int)
    # flag_label = if_label(img, corners_int)
    # if flag_label == False:
    #     return
    cv2.polylines(img, [corners_int], True, color, 1)
    corners_int = bev_corners.reshape(-1, 2).astype(int)
    cv2.line(img, (corners_int[0, 0], corners_int[0, 1]), (corners_int[3, 0], corners_int[3, 1]), (255, 255, 0), 1)
    center = (int((corners_int[0, 0]+corners_int[2, 0])/2), int((corners_int[0, 1]+corners_int[2, 1])/2))
    # cv2.putText(img, str(c)[0:-2], (center[0], center[1]), font, 1, (255, 255, 0), 1)
    # cv2.putText(img, str(score)[0:4], (corners_int[0, 0], corners_int[0, 1]), font, 1, (0, 255, 0), 1)
    cv2.putText(img, str(int(c))[0:2], (corners_int[3, 0], corners_int[3, 1]), font, 1, (0, 0, 255), 1)
    if rect is not None:
        pt1_x, pt1_y = int(rect[0]), int(rect[1])
        pt2_x, pt2_y = int(rect[2]), int(rect[3])
        cv2.rectangle(img, (pt1_x, pt1_y), (pt2_x, pt2_y), (255, 255, 0), 1)

# bev image coordinates format
def get_corners(x, y, w, l, yaw):
    bev_corners = np.zeros((4, 2), dtype=np.float32)
    cos_yaw = np.cos(yaw)
    sin_yaw = np.sin(yaw)
    # front left
    bev_corners[0, 0] = x - w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[0, 1] = y - w / 2 * sin_yaw + l / 2 * cos_yaw

    # rear left
    bev_corners[1, 0] = x - w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[1, 1] = y - w / 2 * sin_yaw - l / 2 * cos_yaw

    # rear right
    bev_corners[2, 0] = x + w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[2, 1] = y + w / 2 * sin_yaw - l / 2 * cos_yaw

    # front right
    bev_corners[3, 0] = x + w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[3, 1] = y + w / 2 * sin_yaw + l / 2 * cos_yaw

    return bev_corners

for idx in range(len(dataset)): 
    bev_map, labels = dataset.draw_lidar_with_label(idx) 
    bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
    bev_map = cv2.resize(bev_map, (cnf.BEV_WIDTH, cnf.BEV_HEIGHT))
    bev_map_ori = bev_map.copy()

    for box_idx, (cls_id, x, y, z, h, w, l, yaw) in enumerate(labels):
        if (h <= 0) or (w <= 0) or (l <= 0):
            continue
        # Draw rotated box
        yaw = -yaw
        y1 = int((x - cnf.boundary['minX']) / cnf.DISCRETIZATION_Y)
        x1 = int((y - cnf.boundary['minY']) / cnf.DISCRETIZATION_X)
        w1 = int(w / cnf.DISCRETIZATION_X)
        l1 = int(l / cnf.DISCRETIZATION_Y)

        drawRotatedBox(bev_map, x1, y1, w1, l1, yaw, cnf.colors[int(cls_id)], cls_id)
    
    # Rotate the bev_map 
    bev_map_ori = cv2.rotate(bev_map_ori, cv2.ROTATE_180)
    bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
    bev_map_ori = cv2.resize(bev_map_ori, (500, 1000))
    bev_map = cv2.resize(bev_map, (500, 1000))
    # bev_map = cv2.resize(bev_map, (int(cnf.BEV_WIDTH/1.5), int(cnf.BEV_HEIGHT/1.5)))
    cv2.imshow('bev_map_ori', bev_map_ori)
    cv2.imshow('bev_map', bev_map)