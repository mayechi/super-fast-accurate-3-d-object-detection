"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Ma Yechi
# DoC: 2020.09.17
# email: mayechi@zhejianglab.com
-----------------------------------------------------------------------------------
# Description: This script for the Panda dataset
"""

import sys
import os
import gc
import math
import json
from builtins import int
from autolab_core import RigidTransform

import numpy as np
from torch.utils.data import Dataset
# import mayavi.mlab as mlab
import cv2
import torch
# from torchvision.ops.boxes import nms

sys.path.append('./src/')

from data_process.kitti_data_utils import gen_hm_radius, compute_radius, Calibration, get_filtered_lidar
from data_process.kitti_bev_utils import makeBEVMap, drawRotatedBox, get_corners
from data_process import transformation
from data_process.universal_data_utils import voxel_filter
import config.kitti_config as cnf

import pdb
import time

from pandaset import DataSet as Dataset_panda
from pandaset import utils
# from visual_utils import visualize_utils as V

class PandaDadaset(Dataset):
    def __init__(self, configs, mode='train', lidar_aug=None, hflip_prob=None, num_samples=None):
        self.data_path = configs.data_path
        self.input_size = configs.input_size
        self.hm_size = configs.hm_size

        self.num_classes = configs.num_classes
        self.max_objects = configs.max_objects

        self.cat_list = configs.cat_list
        self.cat_match_id = configs.cat_match_id


        assert mode in ['train', 'val', 'test'], 'Invalid mode: {}'.format(mode)
        self.mode = mode
        self.is_test = (self.mode == 'test')

        self.lidar_aug = lidar_aug
        self.hflip_prob = hflip_prob

        self.dataset = Dataset_panda(self.data_path)
        seq_total_list = self.dataset.sequences()
        if mode == 'train':
            self.seq_list = seq_total_list[:-5]
        if mode == 'val':
            seq_num = len(seq_total_list)
            self.seq_list = seq_total_list[seq_num-5:]
        elif mode == 'test':
            seq_num = len(seq_total_list)
            self.seq_list = seq_total_list[seq_num-5:]
        self.seq_frame_list = []
        self.seq_obj = None
        self.seq_last = None
        self.frame_num = 80
        for seq in self.seq_list:
            for i in range(self.frame_num):
                self.seq_frame_list.append(seq+'_'+str(i))

    def __len__(self):
        return len(self.seq_frame_list)

    def __getitem__(self, index):
        if self.is_test:
            return self.load_img_only(index)
        else:
            return self.load_img_with_targets(index)

    def trans_lidar(self, frame):
        # print(self.data_path+'/' + self.seq + '/lidar/poses.json')
        with open(self.data_path+'/' + self.seq + '/lidar/poses.json') as f:
            pose = json.load(f)[frame]
            x = float(pose['position']['x'])
            y = float(pose['position']['y'])
            z = float(pose['position']['z'])

            q0 = float(pose['heading']['w'])
            q1 = float(pose['heading']['x'])
            q2 = float(pose['heading']['y'])
            q3 = float(pose['heading']['z'])
            self.R = math.atan2(2*(q0*q1+q2*q3),1-2*(q1*q1+q2*q2))
            self.P = math.asin(2*(q0*q2-q3*q1))
            self.Y = math.atan2(2*(q0*q3+q1*q2),1-2*(q3*q3+q2*q2))
            rotation_quaternion = np.asarray([q0, q1, q2, q3])
            translation = np.asarray([x, y, z])
            T_qua2rota = RigidTransform(rotation_quaternion, translation)
            self.RT = np.hstack((T_qua2rota.rotation, T_qua2rota.translation.reshape(3, 1)))
            self.RT = np.vstack((self.RT, np.array([0, 0, 0, 1])))

            # R = Rotation.from_quat([q0, q1, q2, q3]).as_matrix()
            # T = np.array([x, y, z]).reshape(3, 1)
            # RT = np.hstack((R, T))
            # RT = np.vstack((RT, np.array([0, 0, 0, 1])))
        
        xyz = self.seq_obj.lidar[frame].values[:, :3]

        xyz_num = xyz.shape[0]
        xyz = np.vstack((xyz.T, np.ones((1, xyz_num))))
        xyz = np.dot(np.linalg.inv(self.RT), xyz)

        xyz = xyz[0:-1, :].T
        xyz = np.dot(xyz, cnf.R_z_90)
        f = self.seq_obj.lidar[frame].values[:, 3].reshape(-1, 1) / 255
        lidarData = np.hstack((xyz, f))   
        return  lidarData  

    def trans_label(self, frame):
        labels = []
        for j in range(len(self.seq_obj.cuboids[frame])):
            x = self.seq_obj.cuboids[frame]['position.x'][j]
            y = self.seq_obj.cuboids[frame]['position.y'][j]
            z = self.seq_obj.cuboids[frame]['position.z'][j]
            xyz = np.asarray([x, y, z, 1]).reshape(4, 1)
            xyz = np.dot(np.linalg.inv(self.RT), xyz)
            xyz = xyz[0:-1, :].T
            xyz = np.dot(xyz, cnf.R_z_90)
            x, y, z = xyz[0][0], xyz[0][1], xyz[0][2]

            w = self.seq_obj.cuboids[frame]['dimensions.x'][j]
            h = self.seq_obj.cuboids[frame]['dimensions.z'][j]
            l = self.seq_obj.cuboids[frame]['dimensions.y'][j]

            ry = self.seq_obj.cuboids[frame]['yaw'][j] - self.Y

            # cat_id = self.cat_list.index(self.seq_obj.cuboids[frame]['label'][j])
            cat_id = self.cat_match_id[self.seq_obj.cuboids[frame]['label'][j]]
            object_label = [cat_id, x, y, z, h, w, l, ry]
            labels.append(object_label) 
        return  labels  

    def load_img_only(self, index):
        self.seq = self.seq_frame_list[index].split('_')[0]
        frame = int(self.seq_frame_list[index].split('_')[1])
        if self.seq != self.seq_last:
            if self.seq_obj and self.dataset:
                del self.seq_obj, self.dataset
                gc.collect()
            self.seq_last = self.seq
            self.dataset = Dataset_panda(self.data_path)
            self.seq_obj = self.dataset[self.seq]
            self.seq_obj.load()
            self.seq_obj.lidar.set_sensor(0)
        lidarData = self.trans_lidar(frame)

        # self.seq = self.seq_frame_list[index].split('_')[0]
        # frame = self.seq_frame_list[index].split('_')[1]
        # seq_obj = self.dataset[seq]
        # seq_obj.load()
        # seq_obj.lidar.set_sensor(0)
        # xyz = np.dot(seq_obj.lidar[frame].values[:, :3], cnf.R_z_90_)
        # f = seq_obj.lidar[frame].values[:, 3].reshape(-1, 1)
        # lidarData = np.hstack((xyz, f))
        t0 = time.time()
        lidarData = get_filtered_lidar(lidarData, cnf.boundary)
        bev_map = makeBEVMap(lidarData, cnf.boundary)
        bev_map = torch.from_numpy(bev_map)
        t1 = time.time()
        print("Pre cost time", t1 - t0)

        return bev_map

    def load_img_with_targets(self, index):
        self.seq = self.seq_frame_list[index].split('_')[0]
        frame = int(self.seq_frame_list[index].split('_')[1])
        if self.seq != self.seq_last:
            if self.seq_obj and self.dataset:
                del self.seq_obj, self.dataset
                gc.collect()
            self.seq_last = self.seq
            self.dataset = Dataset_panda(self.data_path)
            self.seq_obj = self.dataset[self.seq]
            self.seq_obj.load()
            self.seq_obj.lidar.set_sensor(0)
        lidarData = self.trans_lidar(frame)
        labels = self.trans_label(frame)
        # xyz = np.dot(self.seq_obj.lidar[frame].values[:, :3], cnf.R_z_90_)
        # f = self.seq_obj.lidar[frame].values[:, 3].reshape(-1, 1)
        # lidarData = np.hstack((xyz, f))
        # labels = []
        # for j in range(len(self.seq_obj.cuboids[frame])):
        #     y = self.seq_obj.cuboids[frame]['position.x'][j]
        #     x = -self.seq_obj.cuboids[frame]['position.y'][j]
        #     z = self.seq_obj.cuboids[frame]['position.z'][j]

        #     w = self.seq_obj.cuboids[frame]['dimensions.x'][j]
        #     h = self.seq_obj.cuboids[frame]['dimensions.z'][j]
        #     l = self.seq_obj.cuboids[frame]['dimensions.y'][j]

        #     ry = np.pi + self.seq_obj.cuboids[frame]['yaw'][j]

        #     cat_id = self.cat_list.index(self.seq_obj.cuboids[frame]['label'][j])
        #     object_label = [cat_id, x, y, z, h, w, l, ry]
        #     labels.append(object_label)
        if len(labels) == 0:
            labels = np.zeros((1, 8), dtype=np.float32)
        else:
            labels = np.array(labels, dtype=np.float32)              

        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])

        t0 = time.time()
        lidarData, labels = get_filtered_lidar(lidarData, cnf.boundary, labels)
        bev_map = makeBEVMap(lidarData, cnf.boundary)
        bev_map = torch.from_numpy(bev_map)
        t1 = time.time()
        # print("Pre cost time", t1 - t0)

        hflipped = False
        if np.random.random() < self.hflip_prob:
            hflipped = True
            # C, H, W
            bev_map = torch.flip(bev_map, [-1])

        targets = self.build_targets(labels, hflipped)

        metadatas = {
            'hflipped': hflipped
        }
        return metadatas, bev_map, targets

    def build_targets(self, labels, hflipped):
        minX = cnf.boundary['minX']
        maxX = cnf.boundary['maxX']
        minY = cnf.boundary['minY']
        maxY = cnf.boundary['maxY']
        minZ = cnf.boundary['minZ']
        maxZ = cnf.boundary['maxZ']

        num_objects = min(len(labels), self.max_objects)
        hm_l, hm_w = self.hm_size

        hm_main_center = np.zeros((self.num_classes, hm_l, hm_w), dtype=np.float32)
        cen_offset = np.zeros((self.max_objects, 2), dtype=np.float32)
        direction = np.zeros((self.max_objects, 2), dtype=np.float32)
        z_coor = np.zeros((self.max_objects, 1), dtype=np.float32)
        dimension = np.zeros((self.max_objects, 3), dtype=np.float32)

        indices_center = np.zeros((self.max_objects), dtype=np.int64)
        obj_mask = np.zeros((self.max_objects), dtype=np.uint8)

        for k in range(num_objects):
            cls_id, x, y, z, h, w, l, yaw = labels[k]
            cls_id = int(cls_id)
            # Invert yaw angle
            yaw = -yaw
            if not ((minX <= x <= maxX) and (minY <= y <= maxY) and (minZ <= z <= maxZ)):
                continue
            if (h <= 0) or (w <= 0) or (l <= 0):
                continue

            bbox_l = l / cnf.bound_size_x * hm_l
            bbox_w = w / cnf.bound_size_y * hm_w
            radius = compute_radius((math.ceil(bbox_l), math.ceil(bbox_w)))
            radius = max(0, int(radius))

            center_y = (x - minX) / cnf.bound_size_x * hm_l  # x --> y (invert to 2D image space)
            center_x = (y - minY) / cnf.bound_size_y * hm_w  # y --> x
            center = np.array([center_x, center_y], dtype=np.float32)

            if hflipped:
                center[0] = hm_w - center[0] - 1

            center_int = center.astype(np.int32)
            if cls_id < 0:
                ignore_ids = [_ for _ in range(self.num_classes)] if cls_id == - 1 else [- cls_id - 2]
                # Consider to make mask ignore
                for cls_ig in ignore_ids:
                    gen_hm_radius(hm_main_center[cls_ig], center_int, radius)
                hm_main_center[ignore_ids, center_int[1], center_int[0]] = 0.9999
                continue

            # Generate heatmaps for main center
            gen_hm_radius(hm_main_center[cls_id], center, radius)
            # Index of the center
            indices_center[k] = center_int[1] * hm_w + center_int[0]

            # targets for center offset
            cen_offset[k] = center - center_int

            # targets for dimension
            dimension[k, 0] = h
            dimension[k, 1] = w
            dimension[k, 2] = l

            # targets for direction
            direction[k, 0] = math.sin(float(yaw))  # im
            direction[k, 1] = math.cos(float(yaw))  # re
            # im -->> -im
            if hflipped:
                direction[k, 0] = - direction[k, 0]

            # targets for depth
            z_coor[k] = z - minZ

            # Generate object masks
            obj_mask[k] = 1

        targets = {
            'hm_cen': hm_main_center,
            'cen_offset': cen_offset,
            'direction': direction,
            'z_coor': z_coor,
            'dim': dimension,
            'indices_center': indices_center,
            'obj_mask': obj_mask,
        }

        return targets

    def draw_lidar_with_label(self, index):
        self.seq = self.seq_frame_list[index].split('_')[0]
        frame = int(self.seq_frame_list[index].split('_')[1])
        print('seq:', self.seq)
        print('frame:', frame)
        dataset_panda = Dataset_panda(self.data_path)
        if self.seq != self.seq_last:
            if self.seq_obj and self.dataset:
                del self.seq_obj, self.dataset
                gc.collect()
            self.seq_last = self.seq
            self.dataset = Dataset_panda(self.data_path)
            self.seq_obj = self.dataset[self.seq]
            self.seq_obj.load()
            self.seq_obj.lidar.set_sensor(0)
        lidarData = self.trans_lidar(frame)
        labels = self.trans_label(frame)
        if len(labels) == 0:
            labels = np.zeros((1, 8), dtype=np.float32)
        else:
            labels = np.array(labels, dtype=np.float32)    
        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])

        lidarData, labels = get_filtered_lidar(lidarData, cnf.boundary, labels)
        bev_map = makeBEVMap(lidarData, cnf.boundary)

        return lidarData, bev_map, labels

if __name__ == '__main__':
    from easydict import EasyDict as edict
    from data_process.transformation import OneOf, Random_Scaling, Random_Rotation, lidar_to_camera_box
    from utils.visualization_utils import merge_rgb_to_bev, show_rgb_image_with_boxes

    configs = edict()
    configs.distributed = False  # For testing
    configs.pin_memory = False
    configs.num_samples = None
    configs.input_size = (608, 608)
    configs.hm_size = (152, 152)
    configs.max_objects = 50
    configs.num_classes = 3
    configs.output_width = 608
    configs.num_classes = 28
    configs.data_path = './dataset/pandaset'
    configs.cat_list = ['Car', 'Pickup Truck', 'Medium-sized Truck', 'Semi-truck', 'Towed Object', 'Motorcycle', 
                        'Other Vehicle - Construction Vehicle', 'Other Vehicle - Uncommon',
                        'Other Vehicle - Pedicab', 'Emergency Vehicle', 'Bus', 'Personal Mobility Device', 'Motorized Scooter', 
                        'Bicycle', 'Train', 'Trolley', 'Tram / Subway', 'Pedestrian', 'Pedestrian with Object', 'Animals - Bird', 
                        'Animals - Other', 'Pylons', 'Road Barriers', 'Signs', 'Cones', 'Construction Signs', 'Temporary Construction Barriers',
                        'Rolling Containers']
    configs.cat_match_id = {'Car':0, 'Pickup Truck':1, 'Medium-sized Truck':1, 'Semi-truck':1, 'Towed Object':5, 'Motorcycle':3, 
                            'Other Vehicle - Construction Vehicle':1, 'Other Vehicle - Uncommon':0,
                            'Other Vehicle - Pedicab':3, 'Emergency Vehicle':0, 'Bus':1, 'Personal Mobility Device':2, 'Motorized Scooter':2, 
                            'Bicycle':3, 'Train':5, 'Trolley':1, 'Tram / Subway':1, 'Pedestrian':2, 'Pedestrian with Object':2, 'Animals - Bird':5, 
                            'Animals - Other':5, 'Pylons':5, 'Road Barriers':5, 'Signs':5, 'Cones':4, 'Construction Signs':5, 'Temporary Construction Barriers':5,
                            'Rolling Containers':5}
    configs.cat_new_list = []

    # lidar_aug = OneOf([
    #     Random_Rotation(limit_angle=np.pi / 4, p=1.),
    #     Random_Scaling(scaling_range=(0.95, 1.05), p=1.),
    # ], p=1.)
    lidar_aug = None

    dataset = PandaDadaset(configs, mode='train', lidar_aug=lidar_aug, hflip_prob=0., num_samples=configs.num_samples)

    print('\n\nPress n to see the next sample >>> Press Esc to quit...')
    # for idx in range(len(dataset)):
    for idx in range(7840):
        print('idx:', idx)
        lidarData, bev_map, labels = dataset.draw_lidar_with_label(idx)
        bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
        bev_map = cv2.resize(bev_map, (cnf.BEV_HEIGHT, cnf.BEV_WIDTH))
        # V.draw_scenes(
        #     points=lidarData
        # )
        # mlab.show(stop=True)
        labels = utils.nms_filter(labels)
        for box_idx, (cls_id, x, y, z, h, w, l, yaw) in enumerate(labels):
            # Draw rotated box
            yaw = -yaw
            y1 = int((x - cnf.boundary['minX']) / cnf.DISCRETIZATION)
            x1 = int((y - cnf.boundary['minY']) / cnf.DISCRETIZATION)
            w1 = int(w / cnf.DISCRETIZATION)
            l1 = int(l / cnf.DISCRETIZATION)
            drawRotatedBox(bev_map, x1, y1, w1, l1, yaw, cnf.colors[int(cls_id)], cls_id)
        # Rotate the bev_map
        bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
        cv2.imshow('bev_map', bev_map)

        if cv2.waitKey(1) & 0xff == 27:
            break