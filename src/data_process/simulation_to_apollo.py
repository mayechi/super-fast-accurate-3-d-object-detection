import os
import csv
import math
import cv2
import numpy as np
import shutil
import argparse
# from autolab_core import RigidTransform

# boundary = {
#     "minX": -20,
#     "maxX": 20,
#     "minY": -10,
#     "maxY": 10,
#     "minZ": -1,
#     "maxZ": 3
# }
boundary = {
    "minX": -50,
    "maxX": 50,
    "minY": -25,
    "maxY": 25,
    "minZ": -2.28,
    "maxZ": 0.72
}
BEV_WIDTH = 608  # across y axis -25m ~ 25m
BEV_HEIGHT = 1216  # across x axis -50m ~ 50m

DISCRETIZATION = (boundary["maxX"] - boundary["minX"]) / BEV_HEIGHT

DISCRETIZATION_Y = (boundary["maxX"] - boundary["minX"]) / BEV_HEIGHT
DISCRETIZATION_X = (boundary["maxY"] - boundary["minY"]) / BEV_WIDTH

# class_dict = {"2":1, "4":2, "6":3}
class_dict = {"1":1, "2":2, "3":3, "4":4, "5":5, "6":6}

lidar_imu_x, lidar_imu_y, lidar_imu_z = 0.029804419726133347+0.10726959, -0.001680449931882321+0.01277985, -0.01585540734231472+1.75511627
imu_lidar_x, imu_lidar_y, imu_lidar_z = -lidar_imu_x, -lidar_imu_y, -lidar_imu_z
# imu_lidar_x, imu_lidar_z = 0, 0

def get_lidar(lidar_file):
    return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)

def get_label(label_path):
    # print("label_path:", label_path)
    labels = []
    for line in open(label_path, 'r'):
        line = line.rstrip()
        line_parts = line.split(' ')
        cat_id = int(line_parts[0])  # 1 for small vehicles, 2 for big vehicles, 3 for pedestrian, 4 for motorcyclist and bicyclist, 5 for traffic cones and 6 for others
        # print("cat_id:", cat_id)
        if cat_id <= -99:  # ignore Tram and Misc
            continue
        x, y, z = float(line_parts[1]), float(line_parts[2]), float(line_parts[3])
        l, w, h = float(line_parts[4]), float(line_parts[5]), float(line_parts[6])
        ry = float(line_parts[7])

        object_label = [cat_id, x, y, z, h, w, l, ry]
        labels.append(object_label)

    if len(labels) == 0:
        labels = np.zeros((1, 8), dtype=np.float32)
        has_labels = False
    else:
        labels = np.array(labels, dtype=np.float32)
        has_labels = True

    return labels, has_labels

def get_filtered_lidar_simulation(lidar, boundary, labels=None):
    minX = boundary['minX']
    maxX = boundary['maxX']
    minY = boundary['minY']
    maxY = boundary['maxY']
    minZ = boundary['minZ']
    maxZ = boundary['maxZ']

    # Remove the point out of range x,y,z
    # 将仿真中的lidar坐标系转成底盘坐标系，高度相差1.5m，并且将反射强度归一化
    # lidar[:, 2] = lidar[:, 2] + 1.5
    mask = np.where((lidar[:, 0] >= minX) & (lidar[:, 0] <= maxX) &
                    (lidar[:, 1] >= minY) & (lidar[:, 1] <= maxY) &
                    (lidar[:, 2] >= minZ) & (lidar[:, 2] <= maxZ))
    lidar = lidar[mask]
    lidar[:, 2] = lidar[:, 2] - minZ
    lidar[:, 3] = lidar[:, 3] / 255

    if labels is not None:
        label_x = (labels[:, 1] >= minX) & (labels[:, 1] < maxX)
        label_y = (labels[:, 2] >= minY) & (labels[:, 2] < maxY)
        label_z = (labels[:, 3] >= minZ) & (labels[:, 3] < maxZ)
        mask_label = label_x & label_y & label_z
        labels = labels[mask_label]
        return lidar, labels
    else:
        return lidar

def makeBEVMap(PointCloud_, boundary, rgb=None):
    Height = BEV_HEIGHT + 1
    Width = BEV_WIDTH + 1

    # Discretize Feature Map
    PointCloud = np.copy(PointCloud_)
    # PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / DISCRETIZATION))
    # 针对Kitti数据集，只检测正前方
    # PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / DISCRETIZATION))
    # PointCloud[:, 1] = np.int_(np.floor(PointCloud[:, 1] / DISCRETIZATION) + Width / 2)
    # 针对Apollo数据集，检测360°
    PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / DISCRETIZATION) + Height / 2)
    PointCloud[:, 1] = np.int_(np.floor(PointCloud[:, 1] / DISCRETIZATION) + Width / 2)

    # sort-3times
    indices = np.lexsort((-PointCloud[:, 2], PointCloud[:, 1], PointCloud[:, 0]))
    PointCloud = PointCloud[indices]
    if rgb is not None:
        rgb = rgb[indices]

    # Height Map
    heightMap = np.zeros((Height, Width))

    _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
    PointCloud_frac = PointCloud[indices]
    # some important problem is image coordinate is (y,x), not (x,y)
    max_height = float(np.abs(boundary['maxZ'] - boundary['minZ']))

    heightMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = PointCloud_frac[:, 2] / max_height

    # Intensity Map & DensityMap
    intensityMap = np.zeros((Height, Width))
    densityMap = np.zeros((Height, Width))

    _, indices, counts = np.unique(PointCloud[:, 0:2], axis=0, return_index=True, return_counts=True)
    PointCloud_top = PointCloud[indices]

    normalizedCounts = np.minimum(1.0, np.log(counts + 1) / np.log(64))

    intensityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = PointCloud_top[:, 3]
    densityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = normalizedCounts

    if rgb is not None:
        # Height Map
        ImgMap = np.zeros((Height, Width, 3))
        _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
        PointCloud_frac = PointCloud[indices]        
        rgb_frac = rgb[indices]
        ImgMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = rgb_frac / 255.0
        ImgMap = np.transpose(ImgMap, (2, 0, 1))
        # RGB_Map = np.zeros((3, Height - 1, Width - 1))
        RGB_Map = np.zeros((6, Height - 1, Width - 1))
        
        RGB_Map[3:, :, :] = ImgMap[:, :BEV_HEIGHT, :BEV_WIDTH]
        # RGB_Map[1, :, :] = ImgMap[1, :BEV_HEIGHT, :BEV_WIDTH]

        RGB_Map[2, :, :] = densityMap[:BEV_HEIGHT, :BEV_WIDTH]  # r_map
        RGB_Map[1, :, :] = heightMap[:BEV_HEIGHT, :BEV_WIDTH]  # g_map
        RGB_Map[0, :, :] = intensityMap[:BEV_HEIGHT, :BEV_WIDTH]  # b_map     
    else:        
        RGB_Map = np.zeros((3, Height - 1, Width - 1))
        RGB_Map[2, :, :] = densityMap[:BEV_HEIGHT, :BEV_WIDTH]  # r_map
        RGB_Map[1, :, :] = heightMap[:BEV_HEIGHT, :BEV_WIDTH]  # g_map
        RGB_Map[0, :, :] = intensityMap[:BEV_HEIGHT, :BEV_WIDTH]  # b_map

    return RGB_Map

# bev image coordinates format
def get_corners(x, y, w, l, yaw):
    bev_corners = np.zeros((4, 2), dtype=np.float32)
    cos_yaw = np.cos(yaw)
    sin_yaw = np.sin(yaw)
    # front left
    bev_corners[0, 0] = x - w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[0, 1] = y - w / 2 * sin_yaw + l / 2 * cos_yaw

    # rear left
    bev_corners[1, 0] = x - w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[1, 1] = y - w / 2 * sin_yaw - l / 2 * cos_yaw

    # rear right
    bev_corners[2, 0] = x + w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[2, 1] = y + w / 2 * sin_yaw - l / 2 * cos_yaw

    # front right
    bev_corners[3, 0] = x + w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[3, 1] = y + w / 2 * sin_yaw + l / 2 * cos_yaw

    return bev_corners

def mergeImg(inputImg, maskImg, contourData, drawPosition):
    '''
    :param inputImg: 输入的图像
    :param maskImg: 输入的模板图像
    :param contourData: 输入的模板中轮廓数据 numpy 形式如[(x1,y1),(x2,y2),...,]
    :param drawPosition: （x,y） 大图中要绘制模板的位置,以maskImg左上角为起始点
    :return: outPutImg：输出融合后的图像
             outContourData: 输出轮廓在inputImg的坐标数据
             outRectData: 输出轮廓的矩形框在inputImg的坐标数据
    '''

    if (inputImg.shape[2] != maskImg.shape[2]):
        print("inputImg shape != maskImg shape")
        return
    inputImg_h = inputImg.shape[0]
    inputImg_w = inputImg.shape[1]
    maskImg_h = maskImg.shape[0]
    maskImg_w = maskImg.shape[1]

    if(inputImg_h < maskImg_h or inputImg_w < maskImg_w):
        print("inputImg size < maskImg size")
        return

    if(((drawPosition[0] + maskImg_w) > inputImg_w) or ((drawPosition[1] + maskImg_h) > inputImg_h)):
        print("drawPosition + maskImg > inputImg range")
        return
    outPutImg = inputImg.copy()
    input_roi = outPutImg[drawPosition[1]:drawPosition[1]+maskImg_h,drawPosition[0]:drawPosition[0]+maskImg_w]
    imgMask_array = np.zeros((maskImg_h,maskImg_w,maskImg.shape[2]),dtype=np.uint8)
    #triangles_list = [np.zeros((len(contourData), 2), int)]
    triangles_list=[contourData]
    cv2.fillPoly(imgMask_array, triangles_list, color=(1,1,1))
    cv2.fillPoly(input_roi, triangles_list, color=(0, 0, 0))
    #cv2.imshow('imgMask_array', imgMask_array)
    imgMask_array = imgMask_array * maskImg
    output_ori = input_roi + imgMask_array
    outPutImg[drawPosition[1]:drawPosition[1] + maskImg_h, drawPosition[0]:drawPosition[0] + maskImg_w]=output_ori
    triangles_list[0][:, 0] = contourData[:, 0] + drawPosition[0]
    triangles_list[0][:, 1] = contourData[:, 1] + drawPosition[1]
    outContourData = triangles_list[0]
    return outPutImg, outContourData

def if_label(img, corners, cls_id):
    img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY).reshape(1216, 608, -1)
    mask_img_gray = np.zeros_like(img_gray)
    corners = corners.reshape(-1, 2)
    outPutImg, outContourData = mergeImg(mask_img_gray, img_gray, corners, (0,0))
    point_num = len(outPutImg.nonzero()[0])
    if cls_id == 1:
        if point_num <= 10:
            return False
        else:
            return True
    else:
        if point_num <= 2:
            return False
        else:
            return True

def filter_label(img, labels):
    labels_filter_list = []
    for box_idx, (cls_id, x, y, z, h, w, l, yaw) in enumerate(labels):
        # Draw rotated box
        yaw = -yaw
        y1 = int((x - boundary['minX']) / DISCRETIZATION_Y)
        x1 = int((y - boundary['minY']) / DISCRETIZATION_X)
        w1 = int(w / DISCRETIZATION_X)
        l1 = int(l / DISCRETIZATION_Y)

        bev_corners = get_corners(x1, y1, w1, l1, yaw)
        corners_int = bev_corners.reshape(-1, 1, 2).astype(int)
        flag_label = if_label(img, corners_int, cls_id)
        if flag_label:
        # if 1:
            labels_filter_list.append(labels[box_idx])
    return labels_filter_list

parser = argparse.ArgumentParser(description='Config the input or output')
parser.add_argument("-i", "--input", type=str, default='/media/myc/Data21/zhijiangyihao_si/Truck_data/20211222', required=False)
parser.add_argument("-o", "--output", type=str, default='/media/myc/Data21/zhijiangyihao_si/Truck_data/20211222/result',required=False)
# parser.add_argument("-i", "--input", type=str, required=True)
# parser.add_argument("-o", "--output", type=str, required=True)
args = parser.parse_args()
read_folder = args.input
write_folder = args.output

# pc_files_path = "/home/mayechi/Data/20201217164347/pcl/"
# npc_files_path = "/home/mayechi/Data/20201217164347/npc/"
# label_files_path = "/home/mayechi/Data/20201217164347/label/"

# pc_files_use_path = "/home/mayechi/Data/20201217164347/pcl_use/"
# label_files_use_path = "/home/mayechi/Data/20201217164347/label_use/"

pc_files_path = os.path.join(read_folder, "pcl")
npc_files_path = os.path.join(read_folder, "npc")
label_files_path = os.path.join(read_folder, "label")

pc_files_use_path = os.path.join(write_folder, "pcl_use")
label_files_use_path = os.path.join(write_folder, "label_use")

if not os.path.exists(label_files_path):
    os.makedirs(label_files_path)
if not os.path.exists(pc_files_use_path):
    os.makedirs(pc_files_use_path)
if not os.path.exists(label_files_use_path):
    os.makedirs(label_files_use_path)

pc_files = os.listdir(pc_files_path)
npc_files = os.listdir(npc_files_path)
pc_files.sort()
npc_files.sort()

# for i, pc_file in enumerate(pc_files):
for pc_file, npc_file in zip(pc_files, npc_files):
    time_stamp_pc = pc_file.split(".bin")[0].split("pcl_")[-1]
    time_stamp_npc = npc_file.split(".csv")[0].split("npc_")[-1]
    if time_stamp_pc == "1640052046903":
        a = 0
    pc_file_name = pc_file
    # npc_file_name = "npc_" + time_stamp_npc + ".csv"
    # label_file_name = "label_" + time_stamp_npc + ".txt" 
    npc_file_name = time_stamp_npc + ".csv"
    label_file_name = time_stamp_npc + ".txt" 
    # npc_file_path = npc_files_path + npc_file_name
    # label_file_path = label_files_path + label_file_name
    npc_file_path = os.path.join(npc_files_path, npc_file_name)
    label_file_path = os.path.join(label_files_path, label_file_name)
    with open(label_file_path, "w") as lable_file:    
        csv_file = open(npc_file_path, "r")
        reader = csv.reader(csv_file)
        npc_label = ""       
        for item in reader:
            if reader.line_num == 1:
                continue        
            # 针对被测车
            if reader.line_num == 2:
                continue
                # x_a, y_a, z_a = float(item[2]) + imu_lidar_x, float(item[3]) + imu_lidar_y, float(item[4]) + imu_lidar_z
                x_a, y_a, z_a = float(item[2]), float(item[3]), float(item[4])
                qw_a, qx_a, qy_a, qz_a = float(item[8]), float(item[9]), float(item[10]), float(item[11])
                # x_a, y_a, z_a = float(item[3]), float(item[4]), float(item[5])
                # qw_a, qx_a, qy_a, qz_a = float(item[9]), float(item[10]), float(item[11]), float(item[12])

                rotation_quaternion = np.asarray([qw_a, qx_a, qy_a, qz_a])
                translation = np.asarray([x_a, y_a, z_a])
                T_qua2rota = RigidTransform(rotation_quaternion, translation)
                RT = np.hstack((T_qua2rota.rotation, T_qua2rota.translation.reshape(3, 1)))
                RT = np.vstack((RT, np.array([0, 0, 0, 1])))            
                roll_a = math.atan2(2*(qw_a*qx_a+qy_a*qz_a),1-2*(qx_a*qx_a+qy_a*qy_a))
                pitch_a = math.asin(2*(qw_a*qy_a-qz_a*qx_a))
                yaw_a = math.atan2(2*(qw_a*qz_a+qx_a*qy_a),1-2*(qz_a*qz_a+qy_a*qy_a))            
            else:
                # cls = int(item[1])-1
                # cls = class_dict[item[1]]
                cls = int(item[1])
                label_x, label_y, label_z = item[2], item[3], item[4]
                label_l, label_w, label_h = item[5], item[6], item[7]
                label_r_y = item[13]
                # qw, qx, qy, qz = float(item[8]), float(item[9]), float(item[10]), float(item[11])

                # cls = class_dict[item[1]]
                # label_cls_sub = item[2]
                # x, y, z = float(item[3]), float(item[4]), float(item[5])
                # l, w, h = float(item[6]), float(item[7]), float(item[8])
                # qw, qx, qy, qz = float(item[9]), float(item[10]), float(item[11]), float(item[12])


                # roll = math.atan2(2*(qw*qx+qy*qz),1-2*(qx*qx+qy*qy))
                # pitch = math.asin(2*(qw*qy-qz*qx))
                # yaw = math.atan2(2*(qw*qz+qx*qy),1-2*(qz*qz+qy*qy))

                # xyz = np.array([x, y, z, 1])
                # xyz = np.dot(np.linalg.inv(RT), xyz)
                # r_y = yaw - yaw_a
                label_cls = str(cls)
                # label_cls = label_cls_sub

                # label_x, label_y, label_z = str(xyz[0]-imu_lidar_x), str(xyz[1]), str(xyz[2]-imu_lidar_z)
                # label_x, label_y, label_z = str(xyz[0]+imu_lidar_x), str(xyz[1]+imu_lidar_y), str(xyz[2]+imu_lidar_z)
                # label_x, label_y, label_z = str(xyz[0]), str(xyz[1]), str(xyz[2])
                # label_w, label_h, label_l = str(w), str(h), str(l)
                npc_label = label_cls+' '+label_x+' '+label_y+' '+label_z+' '+label_l+' '+label_w+' '+label_h+' '+label_r_y+'\n'
                lable_file.write(npc_label)
    lable_file.close()
    csv_file.close()
    if npc_label != "":
        lidar_file = os.path.join(pc_files_path, pc_file_name)
        lidarData = get_lidar(lidar_file)
        label_file = os.path.join(label_files_path, label_file_name)
        labels, has_labels = get_label(label_file)
        lidarData, labels = get_filtered_lidar_simulation(lidarData, boundary, labels)
        bev_map = makeBEVMap(lidarData, boundary)
        bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
        labels = filter_label(bev_map, labels)
        if len(labels) > 0:
            ori_path = os.path.join(pc_files_path, pc_file_name)
            dst_path = os.path.join(pc_files_use_path, pc_file_name)
            shutil.copy(ori_path, dst_path)
            ori_path = os.path.join(label_files_path, label_file_name)
            dst_path = os.path.join(label_files_use_path, label_file_name)            
            shutil.copy(ori_path, dst_path)

        # shutil.copyfile(os.path.join(pc_files_path+pc_file_name), os.path.join(pc_files_use_path+pc_file_name))
        # shutil.copyfile(os.path.join(label_files_path+label_file_name), os.path.join(label_files_use_path+label_file_name))

