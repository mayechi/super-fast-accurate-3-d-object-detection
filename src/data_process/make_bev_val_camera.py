import os
import numpy as np

import sys
sys.path.append('/media/myc/Work/Research/code/Super-Fast-Accurate-3D-Object-Detection/src')
from utils.lidar_to_camera import whether_in_image

# bev image coordinates format
def get_corners(x, y, w, l, yaw):
    bev_corners = np.zeros((4, 2), dtype=np.float32)
    cos_yaw = np.cos(yaw)
    sin_yaw = np.sin(yaw)
    # front left
    bev_corners[0, 0] = x - w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[0, 1] = y - w / 2 * sin_yaw + l / 2 * cos_yaw

    # rear left
    bev_corners[1, 0] = x - w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[1, 1] = y - w / 2 * sin_yaw - l / 2 * cos_yaw

    # rear right
    bev_corners[2, 0] = x + w / 2 * cos_yaw + l / 2 * sin_yaw
    bev_corners[2, 1] = y + w / 2 * sin_yaw - l / 2 * cos_yaw

    # front right
    bev_corners[3, 0] = x + w / 2 * cos_yaw - l / 2 * sin_yaw
    bev_corners[3, 1] = y + w / 2 * sin_yaw + l / 2 * cos_yaw

    return bev_corners

def unitQ2RotateM_L(unitQ):
    [w,x,y,z] = unitQ
    rotateM = np.zeros((3,3))
    rotateM[0,0] = 1 - 2*y**2 - 2*z**2
    rotateM[0,1] = 2*x*y + 2*w*z
    rotateM[0,2] = 2*x*z - 2*w*y
    rotateM[1,0] = 2*x*y - 2*w*z
    rotateM[1,1] = 1 - 2*x**2 - 2*z**2
    rotateM[1,2] = 2*y*z + 2*w*x
    rotateM[2,0] = 2*x*z + 2*w*y
    rotateM[2,1] = 2*y*z - 2*w*x
    rotateM[2,2] = 1 - 2*x**2 - 2*y**2
    return rotateM.T

def lidar_to_camera(points):
    R_lidar_camera = unitQ2RotateM_L([0.50700055, 0.49858351, -0.49288942,  0.50142277]).reshape((3,3))
    T_lidar_camera = np.array([2.98358574e-01,  -4.76430543e-01, -3.26496075e-01]).reshape((3,1))
    velo2cam = np.hstack((R_lidar_camera, T_lidar_camera))

    points = points[:, 0:-1] 
    points_shape = list(points.shape[:-1])
    if points.shape[-1] == 3:
        points = np.concatenate([points, np.ones(points_shape + [1])], axis=-1)
    camera_points = np.matmul(points, velo2cam.T)
    # camera_points = np.matmul(velo2cam, points) 
    return camera_points[..., :3].astype(np.float32)

def cart_to_hom(pts):
    """
    :param pts: (N, 3 or 2)
    :return pts_hom: (N, 4 or 3)
    """
    pts_hom = np.hstack(
        (pts, np.ones((pts.shape[0], 1), dtype=np.float32)))
    return pts_hom

def rect_to_lidar_pseudo(pts_rect):
    pts_rect_hom = cart_to_hom(pts_rect)
    T = np.array([[0, 0, 1, 0],
                    [-1, 0, 0, 0],
                    [0, -1, 0, 0],
                    [0, 0, 0, 1]], dtype=np.float32)
    pts_lidar = np.dot(pts_rect_hom, np.linalg.inv(T))
    return pts_lidar[:, 0:3]

# input_dir = "/home/mayechi/Data/Sensor/testing/velodyne/"
# input_names = os.listdir(input_dir)
# for input_name in input_names:
#     if len(input_name) == 5:
#         os.rename(input_dir+input_name, input_dir+"00000"+input_name)
#     if len(input_name) == 6:
#         os.rename(input_dir+input_name, input_dir+"0000"+input_name)
#     if len(input_name) == 7:
#         os.rename(input_dir+input_name, input_dir+"000"+input_name)



# input_dir = "/home/mayechi/Data/apollo/detection_test_bin/"
# write_file = "/home/mayechi/Data/apollo/ImageSets/test.txt"
# with open(write_file, 'w') as f:
#     for root, dirs, files in os.walk(input_dir):
#         for dir in dirs:
#             for file in os.listdir(root+dir,):
#                 file_path = root + dir + '/' + file + '\n'
#                 f.write(file_path)
# f.close()

cnf_dict = {}
# cnf_dict['minX'] = -50
# cnf_dict['maxX'] = 50
# cnf_dict['minY'] = -25
# cnf_dict['maxY'] = 25
# cnf_dict['minZ'] = -2.28
# cnf_dict['maxZ'] = 0.72   
# cnf_dict['BEV_HEIGHT'] = 1216
# cnf_dict['BEV_WIDTH'] = 608

cnf_dict['minX'] = -7
cnf_dict['maxX'] = 70.6
cnf_dict['minY'] = -30.4
cnf_dict['maxY'] = 30.4
cnf_dict['minZ'] = -3
cnf_dict['maxZ'] = 1   
cnf_dict['BEV_HEIGHT'] = 776
cnf_dict['BEV_WIDTH'] = 608
cnf_dict['DISCRETIZATION'] = (cnf_dict['maxX'] - cnf_dict['minX']) / (cnf_dict['BEV_HEIGHT'] * 1.0)
cnf_dict['peak_thresh'] = 0.2
cnf_dict['bound_size_x'] = cnf_dict['maxX'] - cnf_dict['minX']
cnf_dict['bound_size_y'] = cnf_dict['maxY'] - cnf_dict['minY']
cnf_dict['bound_size_z'] = cnf_dict['maxZ'] - cnf_dict['minZ']

# input_dir = "/media/myc/Data2/apollo/kitti_format/val/label/"
# write_dir = "/media/myc/Data2/apollo/kitti_format/val/label_for_val/"
input_dir = "/media/myc/Data21/zhijiangyihao/test/mayechi/gt_0916/"
write_dir = "/media/myc/Data21/zhijiangyihao/test/mayechi/gt_map_0916/"
id_path = "/media/myc/Data21/zhijiangyihao/test/mayechi/test_map_0916.txt"
# input_dir = "/media/myc/Data21/zhijiangyihao/training_fusion/label9/"
# write_dir = "/media/myc/Data21/zhijiangyihao/training_fusion/test_map9_image/"
# input_dir = "/media/myc/Data21/zhijiangyihao/training_fusion/label10/"
# write_dir = "/media/myc/Data21/zhijiangyihao/training_fusion/test_map10/"

# For test camera
# input_dir = "/media/myc/Data21/zhijiangyihao/test/0809_r/"
# write_dir = "/media/myc/Data21/zhijiangyihao/test/0809_test_map/"
files = [x[0:-1]+'.txt' for x in open(id_path).readlines()]
# files = os.listdir(input_dir)
for file in files:
    with open(input_dir+file, 'r') as f:
        f_w = open(write_dir+file, 'w')
        lines = f.readlines()
        for line in lines:
            split_txt = line[0:-1].split(' ')
            cls_id = int(split_txt[0])
            x, y, z = float(split_txt[1]), float(split_txt[2]), float(split_txt[3])

            # xyz_lidar = np.array([x, y, z, 1.0]).reshape(-1, 4)
            # pc_camera = lidar_to_camera(xyz_lidar)
            # pc_lidar_pseudo = rect_to_lidar_pseudo(pc_camera)
            # x, y, z = pc_lidar_pseudo[0][0],  pc_lidar_pseudo[0][1],  pc_lidar_pseudo[0][2]

            if x < cnf_dict['minX'] or x > cnf_dict['maxX']:
                continue
            if y < cnf_dict['minY'] or y > cnf_dict['maxY']:
                continue
            if z < cnf_dict['minZ'] or z > cnf_dict['maxZ']:
                continue
            # occu = float(split_txt[8])
            # if occu > 0.5:
            #     continue
            # if x < -20 or x > 20:
            #     continue
            # if y < -10 or y > 10:
            #     continue
            # if z < cnf_dict['minZ'] or z > cnf_dict['maxZ']:
            #     continue
            l, w, h = float(split_txt[4]), float(split_txt[5]), float(split_txt[6])
            # h, w, l = float(split_txt[4]), float(split_txt[5]), float(split_txt[6])
            yaw = float(split_txt[7])


            # 针对是否投影到图像做box过滤
            # flag_in_image = whether_in_image([x, y, z, l, w, h, yaw])
            # if flag_in_image == False:
            #     continue


            l = l / cnf_dict['bound_size_x'] * cnf_dict['BEV_HEIGHT']
            w = w / cnf_dict['bound_size_y'] * cnf_dict['BEV_WIDTH']
            center_input_y = (x - cnf_dict['minX']) / cnf_dict['bound_size_x'] * cnf_dict['BEV_HEIGHT']  # x --> y (invert to 2D image space)
            center_input_x = (y - cnf_dict['minY']) / cnf_dict['bound_size_y'] * cnf_dict['BEV_WIDTH']  # y --> x
            yaw = -yaw
            
            bev_corners = get_corners(center_input_x, center_input_y, w, l, yaw)
            x1, y1, x2, y2, x3, y3, x4, y4 = \
            bev_corners[0, 0], bev_corners[0, 1], bev_corners[1, 0], bev_corners[1, 1], \
            bev_corners[2, 0], bev_corners[2, 1], bev_corners[3, 0], bev_corners[3, 1]
            f_w.write(str(x1)+' '+str(y1)+' '+str(x2)+' '+str(y2)+' '+str(x3)+' '+str(y3)+' '+str(x4)+' '+str(y4)+' '+str(cls_id)+' 0'+'\n')
    f.close()
    f_w.close()
