"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.08.17
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: This script for the KITTI dataset
"""

import sys
import os
import math
import random
import pickle
import logging
from builtins import int
from pathlib import Path

import numpy as np
from torch.utils.data import Dataset
import cv2
import torch

sys.path.append('./src/')

from data_process.kitti_data_utils import gen_hm_radius, compute_radius, Calibration, get_filtered_lidar, get_filtered_lidar_distillation
from data_process.kitti_bev_utils import makeBEVMap, make_bev_ma, make_bev_voxel, make_bev_voxel_no_i, make_bev_voxel_no_i_local_density, drawRotatedBox, get_corners, filter_label
from data_process import transformation
from data_process.universal_data_utils import voxel_filter, wirte_label
from det3d.core.sampler import preprocess as prep
from det3d.core.sampler.sample_ops_v2 import DataBaseSamplerV2
from det3d.core.bbox import box_np_ops
from det3d.datasets.utils import sa_da_v2
from det3d.builder import build_db_preprocess, DataBasePreprocessor
# import config.kitti_config as cnf
import config.apollo_config_distillation as cnf
# import config.simulation_config as cnf

from input.voxel_generator import VoxelGenerator
import pdb
import time

ID_TYPE_CONVERSION = {
    1: 'Car',
    2: 'Van',
    3: 'Pedestrian',
    4: 'Cyclist',
    5: 'Trafficcone',
    6: 'Others'
}

class ApolloDatasetVoxel(Dataset):
    def __init__(self, configs, mode='train', lidar_aug=None, hflip_prob=None, num_samples=None):
        
        # Voxel Generator
        # voxel_size = [0.05, 0.05, 0.1]
        voxel_size = [(77.6/776), (60.8/608), (4/20)]
        point_cloud_range = np.array([-7, -30.4, -3, 70.6, 30.4, 1])
        max_num_points = 50
        self.generator = VoxelGenerator(voxel_size, point_cloud_range, max_num_points, max_voxels=100000)   

        self.input_size = configs.input_size
        self.hm_size = configs.hm_size

        self.class_names = configs.class_names
        self.num_classes = configs.num_classes
        self.max_objects = configs.max_objects

        assert mode in ['train', 'val', 'test'], 'Invalid mode: {}'.format(mode)
        self.mode = mode
        self.is_test = (self.mode == 'test')

        # For data_aug
        if mode == 'train':
            self.lidar_aug = lidar_aug
            self.hflip_prob = hflip_prob
            self.gt_loc_noise_std = [1.0, 1.0, 0.5]
            self.gt_rotation_noise = [-0.785, 0.785]
            self.global_rotation_noise = [-0.785, 0.785]
            self.global_scaling_noise = [0.95, 1.05]
            self.global_random_rot_range = [0, 0]
            self.global_translate_noise_std = [0.0, 0.0, 0.0]

            self.db_sampler = self.build_data_aug_sample()  # GT-AUG
            self.remove_points_after_sample = True



        self.data_dir = '/home/mayechi/Data/Sensor/20210120100517/testing'
        # self.lidar_dir = './dataset/zhijiangyihao/training/bin/'
        # self.label_dir = './dataset/zhijiangyihao/training/label/'
        self.lidar_dir = './dataset/zhijiangyihao/training2/pointcloud/bin/'
        self.label_dir = './dataset/zhijiangyihao/training2/pointcloud/label/'
        # self.data_dir = '/lirong/mayechi/data/simulation/20210120100517/testing'
        if mode == 'train':
            self.lidar_apollo_path = './dataset/apollo/ImageSets/train.txt' 
            self.label_apollo_path = './dataset/apollo/ImageSets/train_label.txt'
            # self.lidar_zhijiangyihao_path = './dataset/zhijiangyihao/first_stage/training/ImageSets/train.txt'
            # self.label_zhijiangyihao_path = './dataset/zhijiangyihao/first_stage/training/ImageSets/train_label.txt' 
            # self.id_zhijiangyihao_path = './dataset/zhijiangyihao/training/ImageSets/train_id.txt'
            # self.id_zhijiangyihao_path = './dataset/zhijiangyihao/training2/pointcloud/ImageSets/train_id.txt'
            self.id_zhijiangyihao_path = './dataset/zhijiangyihao/training2/pointcloud/ImageSets/train_id_sample.txt'
            # self.lidar_path = '/lirong/mayechi/data/apollo/ImageSets_v100/train.txt'
            # self.label_path = '/lirong/mayechi/data/apollo/ImageSets_v100/train_label.txt' 
            # self.lidar_path = '/home/apollo/mayechi/data/apollo/ImageSets_1080ti/train.txt'
            # self.label_path = '/home/apollo/mayechi/data/apollo/ImageSets_1080ti/train_label.txt'             #   
            # self.lidar_path = '/home/mayechi/Data/Sensor/20201217164347/t.txt'
            # self.label_path = '/home/mayechi/Data/Sensor/20201217164347/t_l.txt' 
            # self.lidar_path = '/home/mayechi/Data/Sensor/20201217164347/train_use2.txt'
            # self.label_path = '/home/mayechi/Data/Sensor/20201217164347/train_label_use2.txt'
            # self.lidar_path = '/home/mayechi/Data/Sensor/20210120091343/testing/t.txt'
            # self.label_path = '/home/mayechi/Data/Sensor/20210120091343/testing/t_label.txt'

            # self.data_txt = os.path.join(self.data_dir, 'shuffle_train.txt')
            # self.sample_id_list = [x[0:-1] for x in open(self.data_txt).readlines()]
            # self.lidar_path_list = [os.path.join(self.data_dir, 'pcl_use', 'pcl_'+idx+'.bin'.format(idx)) for idx in self.sample_id_list]
            # self.label_path_list = [os.path.join(self.data_dir, 'label_use', 'label_'+idx+'.txt'.format(idx)) for idx in self.sample_id_list]

        if mode == 'val':
            self.lidar_apollo_path = './dataset/apollo/ImageSets/val.txt'
            self.label_apollo_path = './dataset/apollo/ImageSets/val_label.txt'
            # self.id_zhijiangyihao_path = './dataset/zhijiangyihao/training/ImageSets/val_id.txt'
            self.id_zhijiangyihao_path = './dataset/zhijiangyihao/training2/pointcloud/ImageSets/val_id.txt'
            # self.lidar_zhijiangyihao_path = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val.txt'
            # self.label_zhijiangyihao_path = './dataset/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val_label.txt' 
            # self.lidar_path = '/lirong/mayechi/data/apollo/ImageSets_v100/val.txt'
            # self.label_path = '/lirong/mayechi/data/apollo/ImageSets_v100/val_label.txt'           
            # self.lidar_path = '/home/apollo/mayechi/data/apollo/ImageSets_1080ti/val.txt'
            # self.label_path = '/home/apollo/mayechi/data/apollo/ImageSets_1080ti/val_label.txt' 
            # self.lidar_path = '/home/mayechi/Data/Sensor/20201217164347/train_use2.txt'
            # self.label_path = '/home/mayechi/Data/Sensor/20201217164347/train_label_use2.txt'
            # self.lidar_path = '/home/mayechi/Data/Sensor/20201217164347/v.txt'
            # self.label_path = '/home/mayechi/Data/Sensor/20201217164347/v_label.txt'             
            # self.data_txt = os.path.join(self.data_dir, 'shuffle_val.txt')
            # self.sample_id_list = [x[0:-1] for x in open(self.data_txt).readlines()]
            # self.lidar_path_list = [os.path.join(self.data_dir, 'pcl_use', 'pcl_'+idx+'.bin'.format(idx)) for idx in self.sample_id_list]
            # self.label_path_list = [os.path.join(self.data_dir, 'label_use', 'label_'+idx+'.txt'.format(idx)) for idx in self.sample_id_list]
        elif mode == 'test':
            self.lidar_apollo_path = './dataset/apollo/ImageSets/val.txt'
            # self.lidar_apollo_path = './dataset/apollo/ImageSets/train.txt' 
            # self.lidar_zhijiangyihao_path = '/home/mayechi/Data/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val.txt'
            # self.label_zhijiangyihao_path = '/home/mayechi/Data/zhijiangyihao/f_s_stage/pointcloud/training/ImageSets/val.txt'
            # self.id_zhijiangyihao_path = './dataset/zhijiangyihao/training/ImageSets/val_id.txt'
            self.id_zhijiangyihao_path = './dataset/zhijiangyihao/training2/pointcloud/ImageSets/val_id.txt'
            # self.id_zhijiangyihao_path = './dataset/zhijiangyihao/training/ImageSets/train_id.txt'
            # self.lidar_path = '/lirong/mayechi/data/apollo/ImageSets_v100/val.txt'
            # self.lidar_path = '/lirong/mayechi/data/apollo/ImageSets/test.txt'
            # self.label_list = ''
            # self.sample_label_list = []

        if mode == 'train' or mode == 'val':
            self.sample_apollo_path_list = [x[0:-1] for x in open(self.lidar_apollo_path).readlines()]
            self.sample_apollo_label_list = [x[0:-1] for x in open(self.label_apollo_path).readlines()]
            # self.sample_zhijiangyihao_path_list = [x[0:-1] for x in open(self.lidar_zhijiangyihao_path).readlines()]
            # self.sample_zhijiangyihao_label_list = [x[0:-1] for x in open(self.label_zhijiangyihao_path).readlines()]
            self.sample_zhijiangyihao_path_list = [self.lidar_dir+x[0:-1]+'.bin' for x in open(self.id_zhijiangyihao_path).readlines()]
            self.sample_zhijiangyihao_label_list = [self.label_dir+x[0:-1]+'.txt' for x in open(self.id_zhijiangyihao_path).readlines()]
            # self.sample_path_list = self.sample_apollo_path_list + self.sample_zhijiangyihao_path_list
            # self.sample_label_list = self.sample_apollo_label_list + self.sample_zhijiangyihao_label_list
            self.sample_path_list =  self.sample_zhijiangyihao_path_list
            self.sample_label_list = self.sample_zhijiangyihao_label_list
        elif mode == 'test':
            self.sample_apollo_path_list = [x[0:-1] for x in open(self.lidar_apollo_path).readlines()]    
            # self.sample_apollo_path_list = []
            # self.sample_zhijiangyihao_path_list = [x[0:-1] for x in open(self.lidar_zhijiangyihao_path).readlines()]  
            self.sample_zhijiangyihao_path_list = [self.lidar_dir+x[0:-1]+'.bin' for x in open(self.id_zhijiangyihao_path).readlines()]
            # self.sample_zhijiangyihao_path_list = []
            self.sample_path_list = self.sample_apollo_path_list + self.sample_zhijiangyihao_path_list     
            # self.sample_path_list = self.sample_zhijiangyihao_path_list   

            # test path
            test_lidar_9_dir = '/media/myc/Data21/zhijiangyihao/training_fusion/bin9/'
            test_lidar_10_dir = '/media/myc/Data21/zhijiangyihao/training_fusion/bin10_filter/'
            self.test_9_path = '/media/myc/Data21/zhijiangyihao/training_fusion/test_9_shuffle.list'
            self.test_10_path = '/media/myc/Data21/zhijiangyihao/training_fusion/test_10_shuffle.list'
            # self.id_zhijiangyihao_path = './dataset/apollo/lidar_rcnn/data_processer/ImageSets/id.txt'
            # self.sample_path_list = [self.lidar_dir+x[0:-1]+'.bin' for x in open(self.id_zhijiangyihao_path).readlines()][:100]
            self.sample_9_path_list = [test_lidar_9_dir+x[0:-1]+'.bin' for x in open(self.test_9_path).readlines()]
            self.sample_10_path_list = [test_lidar_10_dir+x[0:-1]+'.bin' for x in open(self.test_10_path).readlines()]
            self.sample_path_list = self.sample_9_path_list + self.sample_10_path_list 
        # random.shuffle(self.sample_path_list)
        
        id_path = '/media/myc/Data21/zhijiangyihao/test/wsq/test_map_sample.txt'
        test_lidar_dir = '/media/myc/Data21/zhijiangyihao/test/wsq/pc/'
        test_label_dir = '/media/myc/Data21/zhijiangyihao/test/wsq/gt/'
        self.sample_path_list = [test_lidar_dir+x[0:-1]+'.bin' for x in open(id_path).readlines()]
        self.sample_label_list = [test_label_dir+x[0:-1]+'.txt' for x in open(id_path).readlines()] 

        # num_samples = 100
        if num_samples is not None:
            self.sample_path_list = self.sample_path_list[:num_samples]
            # self.sample_path_list = self.sample_path_list[8:8+num_samples]
            # self.sample_path_list = self.sample_path_list[len(self.sample_path_list)-num_samples:]
        self.num_samples = len(self.sample_path_list)

    def __len__(self):
        return len(self.sample_path_list)

    def __getitem__(self, index):
        # if self.is_test:
        if self.mode == 'test':
            return self.load_img_only(index)
        else:
            return self.load_img_with_targets(index)

    def load_img_only(self, index):
        sample_path = self.sample_path_list[index]
        lidarData = self.get_lidar(sample_path)
        # t0 = time.time()
        # lidarData, _ = get_filtered_lidar(lidarData, cnf.boundary)
        lidarData, _ = get_filtered_lidar_distillation(sample_path, lidarData, cnf.boundary)
        # lidarData = get_filtered_lidar_simulation(lidarData, cnf.boundary)
        # bev_map_ori = makeBEVMap(lidarData, cnf.boundary)
        # bev_map, bev_map_per_index = make_bev_ma(lidarData, cnf.boundary)
        # bev_map, bev_map_per_index = make_bev_voxel(lidarData, cnf.boundary)
        # bev_map, bev_map_per_index = make_bev_voxel_no_i(lidarData, cnf.boundary)
        # bev_map = np.zeros((4, 10, cnf.BEV_HEIGHT*cnf.BEV_WIDTH))
        # bev_map, bev_map_per_index = make_bev_voxel_no_i_local_density(lidarData, cnf.boundary)
        # bev_map, bev_map_per_index, bev_map_x_voxel = make_bev_voxel_no_i(lidarData, cnf.boundary)
        # bev_map = make_bev_voxel_no_i(lidarData, cnf.boundary)
        # bev_map = make_bev_voxel_no_i(lidarData, cnf.boundary)
        # bev_map = make_bev_voxel_no_i_add_adaptive_and_var(lidarData, cnf.boundary)

        # bev_map = torch.from_numpy(bev_map)

        # bev_map = bev_map.view(4, 10, cnf.BEV_HEIGHT*cnf.BEV_WIDTH)
        # bev_map = bev_map.view(5, 10, cnf.BEV_HEIGHT*cnf.BEV_WIDTH)
        # bev_map_per_index = bev_map_per_index.reshape(-1)     
        
        # t1 = time.time()
        # print("Pre cost time", t1 - t0)
        if 'apollo' in sample_path:
            frame_id = sample_path.split('/')[-2] + '_' + sample_path.split('/')[-1].split('.bin')[0]
        else:
            frame_id = sample_path.split('/')[-1].split('.bin')[0]
        metadatas = {
            # 'img_path': img_path,
            'frame_id': [frame_id]
        }
        # Voxel forward
        points = lidarData[:, :3]
        voxels, coors, num_points_per_voxel = self.generator.generate(points)
        
        input_density = np.minimum(1.0, np.log(num_points_per_voxel + 1) / np.log(64)).reshape(-1, 1) 
        input_xyz = np.sum(voxels, axis=1) / num_points_per_voxel.astype(voxels.dtype).reshape(-1, 1) 
        input_features = np.hstack([input_xyz, input_density])
        # z特征需要转换
        input_features[:, 2] = input_features[:, 2] - cnf.boundary['minZ']
        data = {
            'metadatas': metadatas,
            'points': points,
            'voxels': voxels,
            'coordinates': coors,
            'num_points_per_voxel': num_points_per_voxel,
            'input_features': input_features,
            'metadatas': metadatas,
        }           
        return data

    def keep_arrays_by_name(self, gt_names, used_classes):
        inds = [i for i, x in enumerate(gt_names) if x in used_classes]
        inds = np.array(inds, dtype=np.int64)
        return inds

    def load_img_with_targets(self, index):
        sample_path = self.sample_path_list[index]
        # print(sample_path)
        lidarData = self.get_lidar(sample_path)
        labels, gt_boxes, gt_names, has_labels = self.get_label(index)

        # For lidarData to data aug
        if np.random.random() <= -1:
            lidarData, labels = self.do_data_aug_sample(lidarData, labels)            
            lidarData, labels = self.do_data_aug_global(lidarData, labels)
            lidarData = self.do_data_aug_pyramid(lidarData, labels)
        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])

        # if use voxel idea, annotate 'lidar[:, 2] = lidar[:, 2] - minZ'
        lidarData, labels, mask_label, _ = get_filtered_lidar_distillation(sample_path, lidarData, cnf.boundary, labels)
        gt_boxes = gt_boxes[mask_label]
        gt_names = gt_names[mask_label]

        hflipped = False
        targets = self.build_targets(labels, hflipped)

        metadatas = {
            'hflipped': hflipped
        }

        # Voxel forward
        points = lidarData[:, :3]
        voxels, coors, num_points_per_voxel = self.generator.generate(points)
        
        input_density = np.minimum(1.0, np.log(num_points_per_voxel + 1) / np.log(64)).reshape(-1, 1) 
        input_xyz = np.sum(voxels, axis=1) / num_points_per_voxel.astype(voxels.dtype).reshape(-1, 1) 
        input_features = np.hstack([input_xyz, input_density])
        # z特征需要转换
        input_features[:, 2] = input_features[:, 2] - cnf.boundary['minZ']

        gt_truncated = np.zeros_like(gt_names, dtype=np.float)
        gt_occluded = np.zeros_like(gt_names, dtype=np.float)
        gt_difficulty = np.ones_like(gt_names, dtype=np.int32)
        gt_index =  np.arange(0, gt_names.shape[0], 1).astype(np.int32)

        data = {
            'metadatas': metadatas,
            'points': points,
            'voxels': voxels,
            'coordinates': coors,
            'num_points_per_voxel': num_points_per_voxel,
            'input_features': input_features,
            'metadatas': metadatas,
            'targets': targets,

            'gt_names': gt_names,
            'gt_boxes': gt_boxes,
            'gt_truncated': gt_truncated,
            'gt_occluded': gt_occluded,
            'gt_difficulty': gt_difficulty,
            'gt_index': gt_index,

        }

        if data.get('gt_boxes', None) is not None:
            if 'gt_boxes_no3daug' not in data:
                data['gt_boxes_no3daug'] = data['gt_boxes'].copy()

            selected = self.keep_arrays_by_name(
                data['gt_names'], self.class_names)
            if len(selected) != len(data['gt_names']):
                for key in ['gt_names', 'gt_boxes', 'gt_truncated', 'gt_occluded', 'gt_difficulty', 'gt_index', 'gt_boxes_no3daug']:
                    data[key] = data[key][selected]
            gt_classes = np.array([self.class_names.index(
                n) + 1 for n in data['gt_names']], dtype=np.int32)
            data['gt_boxes'] = np.concatenate(
                (data['gt_boxes'], gt_classes.reshape(-1, 1).astype(np.float32)), axis=1)
            data['gt_boxes_no3daug'] = np.concatenate(
                (data['gt_boxes_no3daug'], gt_classes.reshape(-1, 1).astype(np.float32)), axis=1)

        return data

    def do_data_aug_global(self, lidarData, labels):
        # gt_boxes_mask = np.ones((labels.shape[0]), dtype=np.bool_)
        # From h w l to w l h, ry to -ry - pi/2
        gt_boxes = labels[:, 1:][:, [0, 1, 2, 4, 5, 3, 6]].copy()
        gt_boxes[:, -1] = -gt_boxes[:, -1] - math.pi / 2

        gt_names = labels[:, 0].astype('int').astype('str')

        # per-object augmentation, bad effect
        # prep.noise_per_object_v4_(
        #     gt_boxes,
        #     lidarData,
        #     gt_boxes_mask,
        #     rotation_perturb=self.gt_rotation_noise,
        #     center_noise_std=self.gt_loc_noise_std,
        #     global_random_rot_range=self.global_random_rot_range,
        #     group_ids=None,
        #     num_try=100,
        # )

        # with global augmentation
        gt_boxes, lidarData, flipped = prep.random_flip_v2(gt_boxes, lidarData)
        gt_boxes, lidarData, noise_rotation = prep.global_rotation_v3(gt_boxes, lidarData, self.global_rotation_noise)
        gt_boxes, lidarData, noise_scale = prep.global_scaling_v3(gt_boxes, lidarData, *self.global_scaling_noise)

        # From w l h to h w l, ry to -ry - pi/2
        gt_boxes = gt_boxes[:, [0, 1, 2, 5, 3, 4, 6]]
        gt_boxes[:, -1] = -gt_boxes[:, -1] - math.pi / 2

        labels = np.concatenate([gt_names.reshape(-1, 1).astype(float), gt_boxes], axis=1)        

        return lidarData, labels

    def build_data_aug_sample(self, ):
        logger = logging.getLogger("build_dbsampler")        
        # data_root_prefix = './dataset/zhijiangyihao'
        # data_root_prefix = './dataset/apollo'
        data_root_prefix = './dataset/db_sample'
        db_sampler_cfg = dict(
            type="GT-AUG",
            enable=True,
            db_info_zhijiangyihao_path=data_root_prefix + "/dbinfos_train_zhijiangyihao.pkl",
            db_info_apollo_path=data_root_prefix + "/dbinfos_train_apollo.pkl",
            sample_groups=[{'1':10}, {'2':0}, {'3':10}, {'4':5}, {'5':5}, {'6':0}],
            # sample_groups=[{'1':20}, {'2':0}, {'3':0}, {'4':0}, {'5':0}, {'6':0}],
            db_prep_steps=[
                dict(filter_by_min_num_points={'1':5, '2':5, '3':5, '4':5, '5':5, '6':5}),
                dict(filter_by_difficulty=[-1, -1, -1, -1, -1, -1],),    # todo: need to check carefully
            ],
            global_random_rotation_range_per_object=[0, 0],
            rate=1.0,
            gt_random_drop=-1,
            gt_aug_with_context=-1,
            gt_aug_similar_type=False,
        )
    
        prepors = [build_db_preprocess(c, logger=logger) for c in db_sampler_cfg['db_prep_steps']]
        db_prepor = DataBasePreprocessor(prepors)
        rate = db_sampler_cfg['rate']                                                 # 1.0
        grot_range = list(db_sampler_cfg['global_random_rotation_range_per_object'])  # [0, 0]
        groups = db_sampler_cfg['sample_groups']                                      # [dict(Car=15,),],
        info_zhijiangyihao_path = db_sampler_cfg['db_info_zhijiangyihao_path']         # object/dbinfos_train.pickle
        info_apollo_path = db_sampler_cfg['db_info_apollo_path']                       # object/dbinfos_train.pickle
        gt_random_drop = db_sampler_cfg['gt_random_drop']

        gt_aug_with_context = db_sampler_cfg['gt_aug_with_context']
        gt_aug_similar_type = db_sampler_cfg['gt_aug_similar_type']

        with open(info_zhijiangyihao_path, "rb") as f:
            db_zhijiangyihao_infos = pickle.load(f)
        with open(info_apollo_path, "rb") as f:
            db_apollo_infos = pickle.load(f)
        db_infos = {}
        # db_infos = db_apollo_infos
        for key in db_zhijiangyihao_infos:
            db_zhijiangyihao_infos[key] = []
            db_infos[key] = db_zhijiangyihao_infos[key] + db_apollo_infos[key]
        
        sampler = DataBaseSamplerV2(db_infos, groups, db_prepor, rate, grot_range, logger=logger, gt_random_drop=gt_random_drop,\
                                    gt_aug_with_context=gt_aug_with_context, gt_aug_similar_type=gt_aug_similar_type)

        return sampler

    def do_data_aug_sample(self, lidarData, labels):
        # data_root_prefix = './dataset/zhijiangyihao'
        data_root_prefix = './dataset/db_sample'
        # data_root_prefix = './dataset/apollo'
        data_root_prefix = Path(data_root_prefix)
        gt_boxes_mask = np.ones((labels.shape[0]), dtype=np.bool_)
        labels_ori = labels
        # From h w l to w l h, ry to -ry - pi/2
        gt_boxes = labels[:, 1:][:, [0, 1, 2, 4, 5, 3, 6]].copy()
        gt_boxes[:, -1] = -gt_boxes[:, -1] - math.pi / 2

        gt_names = labels[:, 0].astype('int').astype('str')
        if self.db_sampler:
            sampled_dict = self.db_sampler.sample_all(
                data_root_prefix,
                gt_boxes,
                gt_names,
                4,
                False,  # False
                gt_group_ids=None,
                calib=None,
                targeted_class_names=None,
            )        

            if sampled_dict is not None:
                sampled_gt_names = sampled_dict["gt_names"]
                sampled_gt_boxes = sampled_dict["gt_boxes"]
                sampled_points = sampled_dict["points"]
                sampled_gt_masks = sampled_dict["gt_masks"]  # all 1.

                gt_names = np.concatenate([gt_names, sampled_gt_names], axis=0)
                gt_boxes = np.concatenate([gt_boxes, sampled_gt_boxes])
                # gt_names = sampled_gt_names
                # gt_boxes = sampled_gt_boxes             
                gt_boxes_mask = np.concatenate([gt_boxes_mask, sampled_gt_masks], axis=0)

                # True, remove points in original scene with location occupied by auged gt boxes.
                if self.remove_points_after_sample:
                    masks = box_np_ops.points_in_rbbox(lidarData, sampled_gt_boxes)
                    # masks = box_np_ops.points_in_rbbox(lidarData, gt_boxes)
                    lidarData = lidarData[np.logical_not(masks.any(-1))]
                lidarData = np.concatenate([sampled_points, lidarData], axis=0)  # concat existed points and points in gt-aug boxes

            # From w l h to h w l, ry to -ry - pi/2
            gt_boxes = gt_boxes[:, [0, 1, 2, 5, 3, 4, 6]]
            gt_boxes[:, -1] = -gt_boxes[:, -1] - math.pi / 2

            labels = np.concatenate([gt_names.reshape(-1, 1).astype(float), gt_boxes], axis=1)
        return lidarData, labels

    def do_data_aug_pyramid(self, lidarData, labels):
        # From h w l to w l h, ry to -ry - pi/2
        gt_boxes = labels[:, 1:][:, [0, 1, 2, 4, 5, 3, 6]].copy()
        # gt_boxes = labels[:, 1:][:, [0, 1, 2, 3, 4, 5, 6]].copy()
        gt_boxes[:, -1] = -gt_boxes[:, -1] - math.pi / 2
        gt_boxes_car = gt_boxes[labels[:, 0] == 1].copy()
        gt_boxes_person = gt_boxes[labels[:, 0] == 3].copy()
        # for car
        lidarData = sa_da_v2.pyramid_augment_v0(gt_boxes_car, lidarData,
                                                enable_sa_dropout=0.25,
                                                enable_sa_sparsity=[0.05, 50],
                                                enable_sa_swap=[0.1, 50],
                                                )
        # for person
        lidarData = sa_da_v2.pyramid_augment_v0(gt_boxes_person, lidarData,
                                                enable_sa_dropout=0.2,
                                                enable_sa_sparsity=[0.1, 25],
                                                enable_sa_swap=[0.1, 10],
                                                )
        return lidarData



    def get_image(self, idx):
        img_path = os.path.join(self.image_dir, '{:06d}.png'.format(idx))
        img = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)

        return img_path, img

    def cart_to_hom(self, pts):
        """
        :param pts: (N, 3 or 2)
        :return pts_hom: (N, 4 or 3)
        """
        pts_hom = np.hstack(
            (pts, np.ones((pts.shape[0], 1), dtype=np.float32)))
        return pts_hom

    def rect_to_lidar_pseudo(self, pts_rect):
        pts_rect_hom = self.cart_to_hom(pts_rect)
        T = np.array([[0, 0, 1, 0],
                        [-1, 0, 0, 0],
                        [0, -1, 0, 0],
                        [0, 0, 0, 1]], dtype=np.float32)
        pts_lidar = np.dot(pts_rect_hom, np.linalg.inv(T))
        return pts_lidar[:, 0:3]

    def unitQ2RotateM_L(self, unitQ):
        [w,x,y,z] = unitQ
        rotateM = np.zeros((3,3))
        rotateM[0,0] = 1 - 2*y**2 - 2*z**2
        rotateM[0,1] = 2*x*y + 2*w*z
        rotateM[0,2] = 2*x*z - 2*w*y
        rotateM[1,0] = 2*x*y - 2*w*z
        rotateM[1,1] = 1 - 2*x**2 - 2*z**2
        rotateM[1,2] = 2*y*z + 2*w*x
        rotateM[2,0] = 2*x*z + 2*w*y
        rotateM[2,1] = 2*y*z - 2*w*x
        rotateM[2,2] = 1 - 2*x**2 - 2*y**2
        return rotateM.T

    def lidar_to_camera(self, points):
        R_lidar_camera = self.unitQ2RotateM_L([-0.50752564, -0.46122194,  0.50232435, -0.52671198]).reshape((3,3))
        T_lidar_camera = np.array([0.28262414, -0.38069493, -0.02679241]).reshape((3,1))
        velo2cam = np.hstack((R_lidar_camera, T_lidar_camera))

        points = points[:, 0:-1] 
        points_shape = list(points.shape[:-1])
        if points.shape[-1] == 3:
            points = np.concatenate([points, np.ones(points_shape + [1])], axis=-1)
        camera_points = np.matmul(points, velo2cam.T)
        # camera_points = np.matmul(velo2cam, points) 
        return camera_points[..., :3].astype(np.float32)

    def get_lidar(self, lidar_file):
        pc_lidar = np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)
        intensity = pc_lidar[:, -1].reshape(-1, 1)
        pc_camera = self.lidar_to_camera(pc_lidar)
        pc_lidar_pseudo = self.rect_to_lidar_pseudo(pc_camera)
        pc_lidar_pseudo = np.concatenate([pc_lidar_pseudo, intensity], axis=-1)
        return pc_lidar_pseudo

    # def get_lidar(self, lidar_file):
    #     return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)

    def draw_lidar_with_label(self, index):
        sample_path = self.sample_path_list[index]
        print('sample_path:', sample_path)
        lidarData = self.get_lidar(sample_path)
        labels, gt_boxes, gt_names, has_labels = self.get_label(index)

        # For data aug
        # lidarData, labels = self.do_data_aug_sample(lidarData, labels)        
        # lidarData, labels = self.do_data_aug_global(lidarData, labels)
        # lidarData = self.do_data_aug_pyramid(lidarData, labels)

        # if has_labels:
        #     labels[:, -1] = -labels[:, -1] - np.pi / 2

        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])

        lidarData, labels, mask_label, _ = get_filtered_lidar_distillation(sample_path, lidarData, cnf.boundary, labels)
        # lidarData, labels = get_filtered_lidar_simulation(lidarData, cnf.boundary, labels)
        bev_map = self.makeBEVMap(lidarData, cnf.boundary)

        return bev_map, labels

    def get_label(self, idx):
        labels = []
        gt_boxes , gt_names = [], []
        label_path = self.sample_label_list[idx]
        # print("label_path:", label_path)
        for line in open(label_path, 'r'):
            line = line.rstrip()
            line_parts = line.split(' ')
            cat_id = int(line_parts[0])  # 1 for small vehicles, 2 for big vehicles, 3 for pedestrian, 4 for motorcyclist and bicyclist, 5 for traffic cones and 6 for others
            # print("cat_id:", cat_id)
            if cat_id <= -99:  # ignore Tram and Misc
                continue

            # lidar coordinate to pseudo coordinate
            x, y, z = float(line_parts[1]), float(line_parts[2]), float(line_parts[3])
            xyz_lidar = np.array([x, y, z, 1.0]).reshape(-1, 4)
            pc_camera = self.lidar_to_camera(xyz_lidar)
            pc_lidar_pseudo = self.rect_to_lidar_pseudo(pc_camera)
            x, y, z = pc_lidar_pseudo[0][0], pc_lidar_pseudo[0][1],  pc_lidar_pseudo[0][2]

            h, w, l = float(line_parts[4]), float(line_parts[5]), float(line_parts[6])
            # w, l, h = float(line_parts[4]), float(line_parts[5]), float(line_parts[6])
            ry = float(line_parts[7])

            object_label = [cat_id, x, y, z, h, w, l, ry]
            object_boxes = [x, y, z, l, w, h, ry]
            object_names = [ID_TYPE_CONVERSION[cat_id]]
            labels.append(object_label)
            gt_boxes.append(object_boxes)
            gt_names.append(object_names)

        if len(labels) == 0:
            labels = np.zeros((1, 8), dtype=np.float32)
            gt_boxes = np.zeros((1, 7), dtype=np.float32)
            gt_names = np.zeros((1), dtype='U10')
            has_labels = False
        else:
            labels = np.array(labels, dtype=np.float32)
            gt_boxes = np.array(gt_boxes, dtype=np.float32)
            gt_names = np.array(gt_names, dtype='U10').reshape(-1)
            has_labels = True

        return labels, gt_boxes, gt_names, has_labels

    def build_targets(self, labels, hflipped):
        minX = cnf.boundary['minX']
        maxX = cnf.boundary['maxX']
        minY = cnf.boundary['minY']
        maxY = cnf.boundary['maxY']
        minZ = cnf.boundary['minZ']
        maxZ = cnf.boundary['maxZ']

        num_objects = min(len(labels), self.max_objects)
        hm_l, hm_w = self.hm_size

        hm_main_center = np.zeros((self.num_classes, hm_l, hm_w), dtype=np.float32)
        cen_offset = np.zeros((self.max_objects, 2), dtype=np.float32)
        direction = np.zeros((self.max_objects, 2), dtype=np.float32)
        z_coor = np.zeros((self.max_objects, 1), dtype=np.float32)
        dimension = np.zeros((self.max_objects, 3), dtype=np.float32)

        indices_center = np.zeros((self.max_objects), dtype=np.int64)
        obj_mask = np.zeros((self.max_objects), dtype=np.uint8)

        # gt_boxes = np.zeros((self.max_objects, 7), dtype=np.float32)
        for k in range(num_objects):
            cls_id, x, y, z, h, w, l, yaw = labels[k]
            # gt_boxes[k, :] = np.array([x, y, z, w, l, h, yaw])
            cls_id = int(cls_id)-1
            # Invert yaw angle
            yaw = -yaw
            if not ((minX <= x <= maxX) and (minY <= y <= maxY) and (minZ <= z <= maxZ)):
                continue
            if (h <= 0) or (w <= 0) or (l <= 0):
                continue

            bbox_l = l / cnf.bound_size_x * hm_l
            bbox_w = w / cnf.bound_size_y * hm_w
            radius = compute_radius((math.ceil(bbox_l), math.ceil(bbox_w)))
            radius = max(0, int(radius))

            center_y = (x - minX) / cnf.bound_size_x * hm_l  # x --> y (invert to 2D image space)
            center_x = (y - minY) / cnf.bound_size_y * hm_w  # y --> x
            center = np.array([center_x, center_y], dtype=np.float32)

            if hflipped:
                center[0] = hm_w - center[0] - 1

            center_int = center.astype(np.int32)
            if cls_id < 0:
                ignore_ids = [_ for _ in range(self.num_classes)] if cls_id == - 1 else [- cls_id - 2]
                # Consider to make mask ignore
                for cls_ig in ignore_ids:
                    gen_hm_radius(hm_main_center[cls_ig], center_int, radius)
                hm_main_center[ignore_ids, center_int[1], center_int[0]] = 0.9999
                continue
            
            try:
                # Generate heatmaps for main center
                gen_hm_radius(hm_main_center[cls_id], center, radius)
                # Index of the center
                indices_center[k] = center_int[1] * hm_w + center_int[0]
            except:
                a = 0

            # targets for center offset
            cen_offset[k] = center - center_int

            # targets for dimension
            dimension[k, 0] = h
            dimension[k, 1] = w
            dimension[k, 2] = l

            # targets for direction
            direction[k, 0] = math.sin(float(yaw))  # im
            direction[k, 1] = math.cos(float(yaw))  # re
            # im -->> -im
            if hflipped:
                direction[k, 0] = - direction[k, 0]

            # targets for depth
            z_coor[k] = z - minZ

            # Generate object masks
            obj_mask[k] = 1

        targets = {
            'hm_cen': hm_main_center,
            'cen_offset': cen_offset,
            'direction': direction,
            'z_coor': z_coor,
            'dim': dimension,
            'indices_center': indices_center,
            'obj_mask': obj_mask,
            # 'gt_boxes': gt_boxes
        }

        return targets

    def makeBEVMap(self, PointCloud_, boundary, rgb=None):
        BEV_WIDTH = 608
        BEV_HEIGHT = 776
        Height = BEV_HEIGHT + 1
        Width = BEV_WIDTH + 1
        DISCRETIZATION_Y = (boundary["maxX"] - boundary["minX"]) / BEV_HEIGHT
        DISCRETIZATION_X = (boundary["maxY"] - boundary["minY"]) / BEV_WIDTH

        # Discretize Feature Map
        PointCloud = np.copy(PointCloud_)
        PointCloud[:, 0] = np.int_(np.floor((PointCloud[:, 0] - boundary['minX']) / DISCRETIZATION_Y))
        PointCloud[:, 1] = np.int_(np.floor((PointCloud[:, 1] -boundary['minY']) / DISCRETIZATION_X))

        # sort-3times
        indices = np.lexsort((-PointCloud[:, 2], PointCloud[:, 1], PointCloud[:, 0]))
        PointCloud = PointCloud[indices]
        if rgb is not None:
            rgb = rgb[indices]

        # Height Map
        heightMap = np.zeros((Height, Width))

        _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
        PointCloud_frac = PointCloud[indices]
        # some important problem is image coordinate is (y,x), not (x,y)
        max_height = float(np.abs(boundary['maxZ'] - boundary['minZ']))

        heightMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = PointCloud_frac[:, 2] / max_height

        # Intensity Map & DensityMap
        intensityMap = np.zeros((Height, Width))
        densityMap = np.zeros((Height, Width))

        _, indices, counts = np.unique(PointCloud[:, 0:2], axis=0, return_index=True, return_counts=True)
        PointCloud_top = PointCloud[indices]

        normalizedCounts = np.minimum(1.0, np.log(counts + 1) / np.log(64))

        intensityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = PointCloud_top[:, 3] / 255.0 # hesai40p的反射强度0~255
        densityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = normalizedCounts

        if rgb is not None:
            # Height Map
            # ImgMap = np.zeros((Height, Width, 1))
            ImgMap = np.zeros((Height, Width, 3))
            _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
            PointCloud_frac = PointCloud[indices]        
            # rgb_frac = rgb[indices].reshape(-1, 1)
            rgb_frac = rgb[indices].reshape(-1, 3)
            ImgMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = rgb_frac / 255.0
            ImgMap = np.transpose(ImgMap, (2, 0, 1))
            # RGB_Map = np.zeros((3, Height - 1, Width - 1))
            RGB_Map = np.zeros((3, Height - 1, Width - 1))
            
            RGB_Map[0:3, :, :] = ImgMap[:, :BEV_HEIGHT, :BEV_WIDTH]
            # RGB_Map[1, :, :] = ImgMap[:, :BEV_HEIGHT, :BEV_WIDTH]

            # RGB_Map[4, :, :] = densityMap[:BEV_HEIGHT, :BEV_WIDTH]  # r_map
            # RGB_Map[3, :, :] = heightMap[:BEV_HEIGHT, :BEV_WIDTH]  # g_map
            # RGB_Map[0, :, :] = intensityMap[:BEV_HEIGHT, :BEV_WIDTH]  # b_map     
        else:
            RGB_Map = np.zeros((3, Height - 1, Width - 1))
            RGB_Map[2, :, :] = densityMap[:BEV_HEIGHT, :BEV_WIDTH]  # r_map
            RGB_Map[1, :, :] = heightMap[:BEV_HEIGHT, :BEV_WIDTH]  # g_map
            # RGB_Map[0, :, :] = intensityMap[:BEV_HEIGHT, :BEV_WIDTH]  # b_map            

        return RGB_Map    

if __name__ == '__main__':
    from easydict import EasyDict as edict
    from data_process.transformation import OneOf, Random_Scaling, Random_Rotation, lidar_to_camera_box
    from utils.visualization_utils import merge_rgb_to_bev, show_rgb_image_with_boxes

    configs = edict()
    configs.distributed = False  # For testing
    configs.pin_memory = False
    configs.num_samples = None
    # configs.input_size = (608, 608)
    configs.input_size = (1216, 608)
    # configs.hm_size = (152, 152)
    configs.hm_size = (304, 152)
    configs.max_objects = 50
    configs.num_classes = 3
    configs.output_width = 608
    configs.class_names = ['Car', 'Van', 'Pedestrian', 'Cyclist', 'Trafficcone', 'Others']
    # lidar_aug = OneOf([
    #     Random_Rotation(limit_angle=np.pi / 4, p=1.),
    #     Random_Scaling(scaling_range=(0.95, 1.05), p=1.),
    # ], p=1.)
    lidar_aug = None

    dataset = ApolloDatasetVoxel(configs, mode='train', lidar_aug=lidar_aug, hflip_prob=0., num_samples=configs.num_samples)

    print('\n\nPress n to see the next sample >>> Press Esc to quit...')

    # write label for val
    # for idx in range(len(dataset)): 
    #     print(idx)
    #     dataset.load_img_with_targets(idx)   
        
    # base_path = '/home/mayechi/Data/Sensor/20210119170143/testing/'
    # f_train = open(base_path + 't.txt', 'w')                

    for idx in range(len(dataset)): 
        # if idx < 15               
        # if idx < 200:
        #     continue
        print('idx:', idx+1)
        bev_map, labels = dataset.draw_lidar_with_label(idx) 
        bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
        # labels = filter_label(bev_map, labels)
        # print('labels num:', len(labels))
        if len(labels) == 0:
            continue
        # bev_map = cv2.resize(bev_map, (cnf.BEV_HEIGHT, cnf.BEV_WIDTH))
        bev_map = cv2.resize(bev_map, (cnf.BEV_WIDTH, cnf.BEV_HEIGHT))
        bev_map_ori = bev_map.copy()

        for box_idx, (cls_id, x, y, z, h, w, l, yaw) in enumerate(labels):
            # Draw rotated box
            yaw = -yaw
            y1 = int((x - cnf.boundary['minX']) / cnf.DISCRETIZATION_Y)
            x1 = int((y - cnf.boundary['minY']) / cnf.DISCRETIZATION_X)
            w1 = int(w / cnf.DISCRETIZATION_X)
            l1 = int(l / cnf.DISCRETIZATION_Y)
            # # bev 的四个顶点写下
            # corners = get_corners(x1, y1, w1, l1, yaw)

            drawRotatedBox(bev_map, x1, y1, w1, l1, yaw, cnf.colors[int(cls_id)], cls_id)
        # Rotate the bev_map 
        bev_map_ori = cv2.rotate(bev_map_ori, cv2.ROTATE_180)
        bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
        bev_map_ori = cv2.resize(bev_map_ori, (500, 1000))
        # bev_map = cv2.resize(bev_map, (int(cnf.BEV_WIDTH/1.5), int(cnf.BEV_HEIGHT/1.5)))
        cv2.imshow('bev_map_ori', bev_map_ori)
        cv2.imshow('bev_map', bev_map)

        keyboard = cv2.waitKey(0)
        # if keyboard == ord('a'):
        #     # with open(base_path + 'train_use.txt', 'w') as f_train:
        #     f_train.write(dataset.sample_path_list[idx])
        #     f_train.write('\n') 
        #     # with open(base_path + 'train_label_use.txt', 'w') as f_train_label:
        #     f_train_label.write(dataset.sample_label_list[idx])
        #     f_train_label.write('\n')
        # else:
        #     continue
        if cv2.waitKey(0) & 0xff == 27:
            break
        elif cv2.waitKey(0) & 0xff == ord('2'):
            cv2.imwrite('/home/mayechi/桌面/write/2/'+str(idx)+'.bmp', bev_map_ori)
    # f_train.close()
    # f_train_label.close()
       
