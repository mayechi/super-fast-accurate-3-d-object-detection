import os
import numpy as np

def voxel_filter(point_cloud, leaf_size, random=False):
    filtered_points = []
    # 计算边界点
    x_min, y_min, z_min, _ = np.amin(point_cloud, axis=0) #计算x y z 三个维度的最值
    x_max, y_max, z_max, _ = np.amax(point_cloud, axis=0)

    # 计算 voxel grid维度
    Dx = (x_max - x_min)//leaf_size[0] + 1
    Dy = (y_max - y_min)//leaf_size[1] + 1
    Dz = (z_max - z_min)//leaf_size[2] + 1
    print("Dx x Dy x Dz is {} x {} x {}".format(Dx, Dy, Dz))

    # 计算每个点的voxel索引
    h = list()  #h 为保存索引的列表
    for i in range(len(point_cloud)):
        hx = (point_cloud[i][0] - x_min)//leaf_size[0]
        hy = (point_cloud[i][1] - y_min)//leaf_size[1]
        hz = (point_cloud[i][2] - z_min)//leaf_size[2]
        h.append(hx + hy*Dx + hz*Dx*Dy)
    h = np.array(h)

    # 筛选点
    h_indice = np.argsort(h) # 返回h里面的元素按从小到大排序的索引
    h_sorted = h[h_indice]
    begin = 0
    for i in range(len(h_sorted)-1):   # 0~9999
        if h_sorted[i] == h_sorted[i + 1]:
            continue
        else:
            point_idx = h_indice[begin : i + 1]
            filtered_points.append(np.mean(point_cloud[point_idx], axis=0))
            begin = i

    # 把点云格式改成array，并对外返回
    filtered_points = np.array(filtered_points, dtype=np.float64)
    return filtered_points
 
def wirte_label(labels, label_path):

    # output_dir = "/home/mayechi/Data/apollo/result/3d_15m_label_oritype/"
    # output_dir = "/home/mayechi/Data/apollo/result/3d_output_oritype/"
    # name = label_path.split('/')[-2]
    # output_dir = output_dir + name
    # if not os.path.exists(output_dir):
    #     os.makedirs(output_dir)
    # output_path = output_dir + '/' + label_path.split('/')[-1]

    output_dir = "/home/mayechi/Data/apollo/result/3d_50m_label/"
    name = label_path.split('/')[-2]+'_'+label_path.split('/')[-1].split('.txt')[0]
    output_path = output_dir + str(name) + '_label.txt'

    with open(output_path, 'w') as f:
        for i in range(labels.shape[0]):
            cls_id = int(labels[i][0])
            x, y, z = labels[i][1], labels[i][2], labels[i][3]
            _h, w, l = labels[i][4], labels[i][5], labels[i][6]
            _yaw = labels[i][7]
            f.write(str(cls_id)+' '+str(x)+' '+str(y)+' '+str(z)+' '+str(_h)+' '+str(w)+' '+str(l)+' '+str(_yaw)+' 1.0'+'\n')
        f.close()