import os
import shutil

camera0_1_dir = '/home/mayechi/Data2/zhijiangyihao/train_no_label/camera/jpg6/'

# camera1_0_dir = '/home/mayechi/Work/Research/code/apollo/data/bag/parsed/camera1_0/'
# camera1_1_dir = '/home/mayechi/Work/Research/code/apollo/data/bag/parsed/camera1_1/'
# camera1_2_dir = '/home/mayechi/Work/Research/code/apollo/data/bag/parsed/camera1_2/'
# camera1_3_dir = '/home/mayechi/Work/Research/code/apollo/data/bag/parsed/camera1_3/'
# hesai40_PointCloud2_dir = '/home/mayechi/Data2/zhijiangyihao/train_no_label/lidar/bin6/'
lidar_fusion_PointCloud2_dir = '/home/mayechi/Data2/zhijiangyihao/train_no_label/lidar/bin6/'

camera0_1_list = os.listdir(camera0_1_dir)
# camera1_0_list = os.listdir(camera1_0_dir)
# camera1_1_list = os.listdir(camera1_1_dir)
# camera1_2_list = os.listdir(camera1_2_dir)
# camera1_3_list = os.listdir(camera1_3_dir)
camera1_0_list = []
camera1_1_list = []
camera1_2_list = []
camera1_3_list = []
# hesai40_PointCloud2_list = os.listdir(hesai40_PointCloud2_dir)
hesai40_PointCloud2_list = []
lidar_fusion_PointCloud2_list = os.listdir(lidar_fusion_PointCloud2_dir)

for lidar_fusion_PointCloud2 in lidar_fusion_PointCloud2_list:
    flag = 0
    lidar_fusion_PointCloud2_time = float(lidar_fusion_PointCloud2.split('.bin')[0]) / 1000000.0

    # hesai40
    if hesai40_PointCloud2_list == []:
        flag = 1
    time_diff_0_min = 1000
    for hesai40_PointCloud2 in hesai40_PointCloud2_list:
        hesai40_PointCloud2_time = float(hesai40_PointCloud2.split('.bin')[0]) / 1000000.0
        time_diff_0 = abs(lidar_fusion_PointCloud2_time - hesai40_PointCloud2_time)
        if time_diff_0 < time_diff_0_min:
            time_diff_0_min = time_diff_0
            hesai40_PointCloud2_name = hesai40_PointCloud2
        if time_diff_0_min == 0.0:
            flag = 1
            break
        if time_diff_0_min < 0.05:
            flag = 1
    if flag == 0:
        continue

    # camera0_1
    if camera0_1_list == []:
        flag = 2    
    time_diff_1_min = 1000
    # time_diff_1_min_temp = 1000
    for camera0_1 in camera0_1_list:
        camera0_1_time = float(camera0_1.split('.jpg')[0]) / 1000000.0
        # time_diff_1_temp = lidar_fusion_PointCloud2_time - camera0_1_time
        time_diff_1 = abs(lidar_fusion_PointCloud2_time - camera0_1_time)
        if time_diff_1 < time_diff_1_min:
            time_diff_1_min = time_diff_1
            # time_diff_1_min_temp = time_diff_1_temp
            camera0_1_name = camera0_1
        if time_diff_1_min == 0.0:
            flag = 2
            break
        if time_diff_1_min < 0.05:
            flag = 2
            # if time_diff_1_min < 0.02:
            #     a = 0
    if flag == 1:
        continue        

    # camera1_0
    if camera1_0_list == []:
        flag = 3      
    time_diff_2_min = 1000
    for camera1_0 in camera1_0_list:
        camera1_0_time = float(camera1_0.split('.jpg')[0]) / 1000000.0
        time_diff_2 = abs(lidar_fusion_PointCloud2_time - camera1_0_time)
        if time_diff_2 < time_diff_2_min:
            time_diff_2_min = time_diff_2
            camera1_0_name = camera1_0
        if time_diff_2_min == 0.0:
            flag = 3
            break
        if time_diff_2_min < 0.05:
            flag = 3
    if flag == 2:
        continue   

    # camera1_1
    if camera1_1_list == []:
        flag = 4      
    time_diff_3_min = 1000
    for camera1_1 in camera1_1_list:
        camera1_1_time = float(camera1_1.split('.jpg')[0]) / 1000000.0
        time_diff_3 = abs(lidar_fusion_PointCloud2_time - camera1_1_time)
        if time_diff_3 < time_diff_3_min:
            time_diff_3_min = time_diff_3
            camera1_1_name = camera1_1
        if time_diff_3_min == 0.0:
            flag = 4
            break
        if time_diff_3_min < 0.05:
            flag = 4
    if flag == 3:
        continue 

    # camera1_2
    if camera1_2_list == []:
        flag = 5    
    time_diff_4_min = 1000
    for camera1_2 in camera1_2_list:
        camera1_2_time = float(camera1_2.split('.jpg')[0]) / 1000000.0
        time_diff_4 = abs(lidar_fusion_PointCloud2_time - camera1_2_time)
        if time_diff_4 < time_diff_4_min:
            time_diff_4_min = time_diff_4
            camera1_2_name = camera1_2
        if time_diff_4_min == 0.0:
            flag = 5
            break
        if time_diff_4_min < 0.05:
            flag = 5
    if flag == 4:
        continue 

    # camera1_3
    if camera1_3_list == []:
        flag = 6
    time_diff_5_min = 1000
    for camera1_3 in camera1_3_list:
        camera1_3_time = float(camera1_3.split('.jpg')[0]) / 1000000.0
        time_diff_5 = abs(lidar_fusion_PointCloud2_time - camera1_3_time)
        if time_diff_5 < time_diff_5_min:
            time_diff_5_min = time_diff_5
            camera1_3_name = camera1_3
        if time_diff_5_min == 0.0:
            flag = 6
            break
        if time_diff_5_min < 0.05:
            flag = 6
    if flag == 5:
        continue
    lidar_fusion_PointCloud2_name = lidar_fusion_PointCloud2

    camera0_1_used_dir = camera0_1_dir.replace('jpg6', 'jpg6used') 
    if not os.path.exists(camera0_1_dir.replace('jpg6', 'jpg6used')):         
        os.makedirs(camera0_1_used_dir)
    
    # camera1_0_used_dir = camera1_0_dir.replace('parsed', 'used')
    # if not os.path.exists(camera1_0_dir.replace('parsed', 'used')):
        # os.makedirs(camera1_0_used_dir)

    # camera1_1_used_dir = camera1_1_dir.replace('parsed', 'used')    
    # if not os.path.exists(camera1_1_dir.replace('parsed', 'used')):
    #     os.makedirs(camera1_1_used_dir)    

    # camera1_2_used_dir = camera1_2_dir.replace('parsed', 'used')    
    # if not os.path.exists(camera1_2_dir.replace('parsed', 'used')):
    #     os.makedirs(camera1_2_used_dir)   

    # camera1_3_used_dir = camera1_3_dir.replace('parsed', 'used')
    # if not os.path.exists(camera1_3_dir.replace('parsed', 'used')):
    #     os.makedirs(camera1_3_used_dir) 

    # hesai40_PointCloud2_used_dir = hesai40_PointCloud2_dir.replace('parsed', 'used')
    # if not os.path.exists(hesai40_PointCloud2_dir.replace('parsed', 'used')):
    #     os.makedirs(hesai40_PointCloud2_used_dir) 

    lidar_fusion_PointCloud2_used_dir = lidar_fusion_PointCloud2_dir.replace('bin6', 'bin6used')
    if not os.path.exists(lidar_fusion_PointCloud2_dir.replace('bin6', 'bin6used')):        
        os.makedirs(lidar_fusion_PointCloud2_used_dir) 
    
    shutil.copy(camera0_1_dir+camera0_1_name, camera0_1_used_dir+camera0_1_name)
    # shutil.copy(camera1_0_dir+camera1_0_name, camera1_0_used_dir+camera1_0_name)
    # shutil.copy(camera1_1_dir+camera1_1_name, camera1_1_used_dir+camera1_1_name)
    # shutil.copy(camera1_2_dir+camera1_2_name, camera1_2_used_dir+camera1_2_name)    
    # shutil.copy(camera1_3_dir+camera1_3_name, camera1_3_used_dir+camera1_3_name)    
    # shutil.copy(hesai40_PointCloud2_dir+hesai40_PointCloud2_name, hesai40_PointCloud2_used_dir+hesai40_PointCloud2_name)
    shutil.copy(lidar_fusion_PointCloud2_dir+lidar_fusion_PointCloud2_name, lidar_fusion_PointCloud2_used_dir+lidar_fusion_PointCloud2_name)    