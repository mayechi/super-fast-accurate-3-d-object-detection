"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.08.17
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: This script for the KITTI dataset
"""

import sys
import os
import math
from builtins import int

import numpy as np
from torch.utils.data import Dataset
import cv2
import torch

sys.path.append('./src/')

from data_process.kitti_data_utils import gen_hm_radius, compute_radius, Calibration, get_filtered_lidar
from data_process.kitti_bev_utils import makeBEVMap, make_bev_ma, drawRotatedBox, get_corners
from data_process import transformation
import config.kitti_config as cnf
# from ..utils import calibration_kitti

# import mayavi.mlab as mlab
# from visual_utils import visualize_utils as V
import pdb

h_ave = {0:1.74, 1:1.58, 2:1.74}
w_ave = {0:0.66, 1:1.65, 2:0.60}
l_ave = {0:0.84, 1:3.99, 2:1.76}

class KittiDataset(Dataset):
    def __init__(self, configs, mode='train', lidar_aug=None, hflip_prob=None, num_samples=None):
        self.dataset_dir = configs.dataset_dir
        self.input_size = configs.input_size
        self.hm_size = configs.hm_size

        self.num_classes = configs.num_classes
        self.max_objects = configs.max_objects

        assert mode in ['train', 'val', 'test', 'fusion_train', 'fusion_val'], 'Invalid mode: {}'.format(mode)
        self.mode = mode
        # self.is_test = (self.mode == 'test' or self.mode == 'val')
        self.is_test = (self.mode == 'test' or self.mode == 'fusion_train' or self.mode == 'fusion_val')
        
        # sub_folder = 'testing' if mode=='test' else 'training'
        sub_folder = 'training'

        self.lidar_aug = lidar_aug
        self.hflip_prob = hflip_prob

        self.image_2_dir = os.path.join(self.dataset_dir, sub_folder, "image_2")
        # self.image_3_dir = os.path.join(self.dataset_dir, sub_folder, "image_3")
        self.image_3_dir = None
        self.lidar_dir = os.path.join(self.dataset_dir, sub_folder, "velodyne")
        # self.lidar_dir = os.path.join(self.dataset_dir, sub_folder, "aanet_predict_velodyne_rgb")
        # self.lidar_dir = os.path.join(self.dataset_dir, sub_folder, "disparity_velodyne")
        self.calib_dir = os.path.join(self.dataset_dir, sub_folder, "calib")
        self.label_dir = os.path.join(self.dataset_dir, sub_folder, "label_2")
        split_txt_path = os.path.join(self.dataset_dir, 'ImageSets', '{}.txt'.format(mode))
        self.sample_id_list = [int(x.strip()) for x in open(split_txt_path).readlines()]

        if num_samples is not None:
            self.sample_id_list = self.sample_id_list[:num_samples]
        self.num_samples = len(self.sample_id_list)

    def __len__(self):
        return len(self.sample_id_list)

    def __getitem__(self, index):
        # if self.is_test:
        if self.mode == 'test':
        # if 1:    
            return self.load_img_only(index)
        elif self.mode == 'fusion_train' or self.mode == 'fusion_val':
            return self.load_img_with_targets_fusion(index)
        else:
            return self.load_img_with_targets(index)

    def load_img_only(self, index):
        """Load only image for the testing phase"""
        sample_id = int(self.sample_id_list[index])
        img_path, img_2_rgb, img_3_rgb = self.get_image(sample_id)
        # 3通道压成1通道
        # img_2_gray = cv2.cvtColor(img_2_rgb, cv2.COLOR_BGR2GRAY)
        # img_3_gray = cv2.cvtColor(img_3_rgb, cv2.COLOR_BGR2GRAY)
        # 3通道直接使用
        img_2_gray = img_2_rgb
        img_3_gray = img_3_rgb
        calib = self.get_calib(sample_id)
        lidarData = self.get_lidar(sample_id)
        
        # img_2_gray = None
        # lidarData, uvs, rgb = self.velo2img(calib, lidarData, img_2_gray, img_3_gray)

        # base方案，点云投影到图像坐标系下，不用图像坐标系的 gray 
        rgb = None

        # 方案1，点云投影到图像坐标系下，用图像坐标系的 gray 
        # rgb = rgb

        lidarData, rgb = get_filtered_lidar(lidarData, cnf.boundary, None, rgb)

        bev_map = makeBEVMap(lidarData, cnf.boundary, rgb)
        # bev_map, bev_map_per_index = make_bev_ma(lidarData, cnf.boundary)
        bev_map = torch.from_numpy(bev_map)

        metadatas = {
            'img_path': img_path,
        }

        return metadatas, bev_map, img_2_rgb
        # return metadatas, bev_map, bev_map_per_index, img_2_rgb

    def load_img_with_targets(self, index):
        """Load images and targets for the training and validation phase"""
        sample_id = int(self.sample_id_list[index])
        img_path = os.path.join(self.image_2_dir, '{:06d}.png'.format(sample_id))
        lidarData = self.get_lidar(sample_id)
        img_path, img_2_rgb, img_3_rgb = self.get_image(sample_id)
        # 3通道压成1通道
        # img_2_gray = cv2.cvtColor(img_2_rgb, cv2.COLOR_BGR2GRAY)
        # img_3_gray = cv2.cvtColor(img_3_rgb, cv2.COLOR_BGR2GRAY)
        # 3通道直接使用
        img_2_gray = img_2_rgb
        img_3_gray = img_3_rgb

        calib = self.get_calib(sample_id)
        labels, has_labels = self.get_label(sample_id)
        if has_labels:
            labels[:, 1:] = transformation.camera_to_lidar_box(labels[:, 1:], calib.V2C, calib.R0, calib.P2)
        
        # self.wirte_label(sample_id, labels)

        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])

        # rgb = lidarData[:, 3:]
        # lidarData = lidarData[:, 0:4]
        # img_2_gray = None
        # lidarData, uvs, rgb = self.velo2img(calib, lidarData, img_2_gray, img_3_gray)

        # base方案，点云投影到图像坐标系下，不用图像坐标系的 gray 
        rgb = None

        # 方案1，点云投影到图像坐标系下，用图像坐标系的 gray 
        # rgb = rgb

        lidarData, labels, rgb = get_filtered_lidar(lidarData, cnf.boundary, labels, rgb)

        bev_map = makeBEVMap(lidarData, cnf.boundary, rgb)
        # bev_map, bev_map_per_index = make_bev_ma(lidarData, cnf.boundary)

        # bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
        # cv2.imshow('bev_map', bev_map)
        # cv2.waitKey(0)      
        bev_map = torch.from_numpy(bev_map)


        hflipped = False
        if np.random.random() < self.hflip_prob:
            hflipped = True
            # C, H, W
            bev_map = torch.flip(bev_map, [-1])

        targets = self.build_targets(labels, hflipped)

        metadatas = {
            'img_path': img_path,
            'hflipped': hflipped
        }
        # print('bev_map shape:', bev_map.shape)
        # print('metadatas:', metadatas)
        # print('targets:', targets)

        return metadatas, bev_map, targets
        # return metadatas, bev_map, bev_map_per_index, targets

    def load_img_with_targets_fusion(self, index):
        """Load images and targets for the training and validation phase"""
        sample_id = int(self.sample_id_list[index])
        img_path = os.path.join(self.image_2_dir, '{:06d}.png'.format(sample_id))
        lidarData = self.get_lidar(sample_id)
        img_path, img_2_rgb, img_3_rgb = self.get_image(sample_id)
        # 3通道压成1通道
        # img_2_gray = cv2.cvtColor(img_2_rgb, cv2.COLOR_BGR2GRAY)
        # img_3_gray = cv2.cvtColor(img_3_rgb, cv2.COLOR_BGR2GRAY)
        # 3通道直接使用
        img_2_gray = img_2_rgb
        img_3_gray = img_3_rgb

        calib = self.get_calib(sample_id)
        labels, has_labels = self.get_label(sample_id)
        if has_labels:
            labels[:, 1:] = transformation.camera_to_lidar_box(labels[:, 1:], calib.V2C, calib.R0, calib.P2)

        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])

        # rgb = lidarData[:, 3:]
        # lidarData = lidarData[:, 0:4]
        # img_2_gray = None
        # lidarData, uvs, rgb = self.velo2img(calib, lidarData, img_2_gray, img_3_gray)

        # base方案，点云投影到图像坐标系下，不用图像坐标系的 gray 
        rgb = None

        # 方案1，点云投影到图像坐标系下，用图像坐标系的 gray 
        # rgb = rgb

        lidarData, labels, rgb = get_filtered_lidar(lidarData, cnf.boundary, labels, rgb)

        bev_map = makeBEVMap(lidarData, cnf.boundary, rgb)
        # bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
        # cv2.imshow('bev_map', bev_map)
        # cv2.waitKey(0)      
        bev_map = torch.from_numpy(bev_map)


        hflipped = False
        if np.random.random() < self.hflip_prob:
            hflipped = True
            # C, H, W
            bev_map = torch.flip(bev_map, [-1])

        # targets = self.build_targets(labels, hflipped)

        metadatas = {
            'img_idx': img_path.split('/')[-1].split('.png')[0],
            'img_path': img_path,
            'hflipped': hflipped
        }

        return metadatas, bev_map, img_2_rgb, labels

    def get_image(self, idx):
        # pdb.set_trace()
        img_path = os.path.join(self.image_2_dir, '{:06d}.png'.format(idx))
        img = cv2.imread(img_path)        
        if self.image_3_dir is not None:
            img_3_path = os.path.join(self.image_3_dir, '{:06d}.png'.format(idx))
            img_3 = cv2.imread(img_3_path)
            return img_path, img, img_3
        else:
            return img_path, img, None
        # img = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)


    def get_calib(self, idx):
        calib_file = os.path.join(self.calib_dir, '{:06d}.txt'.format(idx))
        # assert os.path.isfile(calib_file)
        return Calibration(calib_file)

    # def get_calib_by_mm(self, idx):
    #     calib_file = self.root_split_path / 'calib' / ('%s.txt' % idx)
    #     assert calib_file.exists()
    #     return calibration_kitti.Calibration(calib_file)

    def get_lidar(self, idx):
        lidar_file = os.path.join(self.lidar_dir, '{:06d}.bin'.format(idx))
        # assert os.path.isfile(lidar_file)
        return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)
        # return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 6)

    def get_label(self, idx):
        labels = []
        label_path = os.path.join(self.label_dir, '{:06d}.txt'.format(idx))
        for line in open(label_path, 'r'):
            line = line.rstrip()
            line_parts = line.split(' ')
            obj_name = line_parts[0]  # 'Car', 'Pedestrian', ...
            cat_id = int(cnf.CLASS_NAME_TO_ID[obj_name])
            if cat_id <= -99:  # ignore Tram and Misc
                continue
            truncated = int(float(line_parts[1]))  # truncated pixel ratio [0..1]
            occluded = int(line_parts[2])  # 0=visible, 1=partly occluded, 2=fully occluded, 3=unknown
            alpha = float(line_parts[3])  # object observation angle [-pi..pi]
            # xmin, ymin, xmax, ymax
            bbox = np.array([float(line_parts[4]), float(line_parts[5]), float(line_parts[6]), float(line_parts[7])])
            # height, width, length (h, w, l)
            h, w, l = float(line_parts[8]), float(line_parts[9]), float(line_parts[10])
            # location (x,y,z) in camera coord.
            x, y, z = float(line_parts[11]), float(line_parts[12]), float(line_parts[13])
            ry = float(line_parts[14])  # yaw angle (around Y-axis in camera coordinates) [-pi..pi]

            object_label = [cat_id, x, y, z, h, w, l, ry]
            # object_label = [cat_id, x, y, z, l, w, h, ry]
            labels.append(object_label)

        if len(labels) == 0:
            labels = np.zeros((1, 8), dtype=np.float32)
            has_labels = False
        else:
            labels = np.array(labels, dtype=np.float32)
            has_labels = True

        return labels, has_labels

    def wirte_label(self, sample_id, labels):
        
        self.wirte_dir = '/home/mayechi/Data/kitti/training/label_kitti_axis_2_val/'
        write_path = self.wirte_dir + '{:06d}.txt'.format(sample_id)
        with open(write_path, 'w') as f:
            for i in range(labels.shape[0]):
                cat_id = int(labels[i, 0])
                if cat_id not in cnf.CLASS_ID_TO_NAME:
                    continue
                cat = cnf.CLASS_ID_TO_NAME[cat_id]
                z, x, y = str(labels[i, 1]), str(-labels[i, 2]), str(-labels[i, 3])
                h, w, l = str(labels[i, 4]), str(labels[i, 5]), str(labels[i, 6])
                ry = labels[i, 7]
                ry = -ry - np.pi / 2
                ry = str(labels[i, 7])
                f.write(cat+' 0 0 0 '+'0'+' '+'0'+' '+'0'+' '+'0'+' '+h+' '+w+' '+l+' '+x+' '+y+' '+z+' '+ry+'\n')
        f.close()

    def build_targets(self, labels, hflipped):
        minX = cnf.boundary['minX']
        maxX = cnf.boundary['maxX']
        minY = cnf.boundary['minY']
        maxY = cnf.boundary['maxY']
        minZ = cnf.boundary['minZ']
        maxZ = cnf.boundary['maxZ']

        num_objects = min(len(labels), self.max_objects)
        hm_l, hm_w = self.hm_size

        hm_main_center = np.zeros((self.num_classes, hm_l, hm_w), dtype=np.float32)
        cen_offset = np.zeros((self.max_objects, 2), dtype=np.float32)
        direction = np.zeros((self.max_objects, 2), dtype=np.float32)
        z_coor = np.zeros((self.max_objects, 1), dtype=np.float32)
        dimension = np.zeros((self.max_objects, 3), dtype=np.float32)
        img_dim = np.zeros((self.max_objects, 2), dtype=np.float32)
        wl = np.zeros((self.max_objects, 2), dtype=np.float32)
        hh = np.zeros((self.max_objects, 1), dtype=np.float32)
        hwl_scale = np.zeros((self.max_objects, 3), dtype=np.float32)
        # wl_scale = np.zeros((self.max_objects, 2), dtype=np.float32)

        indices_center = np.zeros((self.max_objects), dtype=np.int64)
        obj_mask = np.zeros((self.max_objects), dtype=np.uint8)

        for k in range(num_objects):
            cls_id, x, y, z, h, w, l, yaw = labels[k]
            cls_id = int(cls_id)
            # Invert yaw angle
            yaw = -yaw
            if not ((minX <= x <= maxX) and (minY <= y <= maxY) and (minZ <= z <= maxZ)):
                continue
            if (h <= 0) or (w <= 0) or (l <= 0):
                continue

            bbox_l = l / cnf.bound_size_x * hm_l
            bbox_w = w / cnf.bound_size_y * hm_w
            radius = compute_radius((math.ceil(bbox_l), math.ceil(bbox_w)))
            radius = max(0, int(radius))

            center_y = (x - minX) / cnf.bound_size_x * hm_l  # x --> y (invert to 2D image space)
            center_x = (y - minY) / cnf.bound_size_y * hm_w  # y --> x
            center = np.array([center_x, center_y], dtype=np.float32)

            if hflipped:
                center[0] = hm_w - center[0] - 1

            center_int = center.astype(np.int32)
            if cls_id < 0:
                ignore_ids = [_ for _ in range(self.num_classes)] if cls_id == - 1 else [- cls_id - 2]
                # Consider to make mask ignore
                for cls_ig in ignore_ids:
                    gen_hm_radius(hm_main_center[cls_ig], center_int, radius)
                hm_main_center[ignore_ids, center_int[1], center_int[0]] = 0.9999
                continue

            # Generate heatmaps for main center
            gen_hm_radius(hm_main_center[cls_id], center, radius)
            # Index of the center
            indices_center[k] = center_int[1] * hm_w + center_int[0]

            # targets for center offset
            cen_offset[k] = center - center_int

            # targets for dimension
            dimension[k, 0] = h
            dimension[k, 1] = w
            dimension[k, 2] = l

            # dimension[k, 0] = h - h_ave[cls_id]
            # dimension[k, 1] = w - w_ave[cls_id]
            # dimension[k, 2] = l - l_ave[cls_id]

            hwl_scale[k, 0] = 1 / h
            hwl_scale[k, 1] = 1 / w * 2
            hwl_scale[k, 2] = 1 / l * 2

            # targets for wl scale
            # wl_scale[k, 0] = 1 / w
            # wl_scale[k, 1] = 1 / l


            # targets for wl in image
            # hh[k, 0] = h
            # targets for wl in image
            # wl[k, 0] = w / cnf.bound_size_y * cnf.BEV_WIDTH
            # wl[k, 1] = l / cnf.bound_size_y * cnf.BEV_WIDTH

            # targets for direction
            direction[k, 0] = math.sin(float(yaw))  # im
            direction[k, 1] = math.cos(float(yaw))  # re
            # im -->> -im
            if hflipped:
                direction[k, 0] = - direction[k, 0]

            # targets for depth
            z_coor[k] = z - minZ

            # Generate object masks
            obj_mask[k] = 1

        targets = {
            'hm_cen': hm_main_center,
            'cen_offset': cen_offset,
            'direction': direction,
            'z_coor': z_coor,
            'hwl_scale': hwl_scale,
            # 'wl_scale': wl_scale,
            # 'wl': wl,
            # 'h': hh,
            'dim': dimension,
            'indices_center': indices_center,
            'obj_mask': obj_mask,
        }

        return targets

    def draw_img_with_label(self, index):
        sample_id = int(self.sample_id_list[index])
        img_path, img_rgb, _ = self.get_image(sample_id)
        lidarData = self.get_lidar(sample_id)
        calib = self.get_calib(sample_id)
        labels, has_labels = self.get_label(sample_id)
        if has_labels:
            labels[:, 1:] = transformation.camera_to_lidar_box(labels[:, 1:], calib.V2C, calib.R0, calib.P2)

        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])

        lidarData, labels, _ = get_filtered_lidar(lidarData, cnf.boundary, labels)
        # V.draw_scenes(
        #     points=lidarData, ref_boxes=labels[:, 1:]
        # )
        # mlab.show(stop=True)

        bev_map = makeBEVMap(lidarData, cnf.boundary)

        return bev_map, labels, img_rgb, img_path

    def draw_lidar_with_label(self, index):
        print('index:', str(index))
        sample_id = int(self.sample_id_list[index])
        lidarData = self.get_lidar(sample_id)
        # lidarData[:, 3] = 1.0
        img_path, img_2_rgb, img_3_rgb = self.get_image(sample_id)
        # 3通道压成1通道
        # img_2_gray = cv2.cvtColor(img_2_rgb, cv2.COLOR_BGR2GRAY)
        # img_3_gray = cv2.cvtColor(img_3_rgb, cv2.COLOR_BGR2GRAY)
        # 3通道直接使用
        img_2_gray = img_2_rgb
        img_3_gray = img_3_rgb

        labels, has_labels = self.get_label(sample_id)
        calib = self.get_calib(sample_id)
        if has_labels:
            labels[:, 1:] = transformation.camera_to_lidar_box(labels[:, 1:], calib.V2C, calib.R0, calib.P2)

        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])
        
        # rgb = lidarData[:, 3:]
        # lidarData = lidarData[:, 0:4]

        # img_2_gray = None
        # lidarData, uvs, rgb = self.velo2img(calib, lidarData, img_2_gray, img_3_gray)

        # base方案，点云投影到图像坐标系下，不用图像坐标系的 gray 
        rgb = None

        # 方案1，点云投影到图像坐标系下，用图像坐标系的 gray 
        # rgb = rgb

        # # 方案2，针对类似虚拟点云投影的点，用bgr的特征
        # rgb = lidarData[:, 3:]
        # lidarData = lidarData[:, 0:4]

        lidarData, labels, rgb = get_filtered_lidar(lidarData, cnf.boundary, labels, rgb)

        bev_map = makeBEVMap(lidarData, cnf.boundary, rgb)
        # bev_map = make_bev_ma(lidarData, cnf.boundary)

        return bev_map, labels

    def velo2img(self, calib, cloud_array_ori, img_2=None, img_3=None):
        # img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        # img = self.gamma_transform(img, 0.3)
        if img_2 is not None:
            temp = np.array([0, 0, 0, 1])
            zeros = np.zeros([3, 1])
            #velo_to_camera cal parameter#
            # T_velo_to_camera = np.array([[7.533745e-03, -9.999714e-01, -6.166020e-04, -4.069766e-03],
            #                                 [1.480249e-02, 7.280733e-04, -9.998902e-01, -7.631618e-02],
            #                                 [9.998621e-01, 7.523790e-03, 1.480755e-02, -2.717806e-01],
            #                                 [0, 0, 0, 1]], np.float64)
            T_velo_to_camera = calib.V2C
            T_velo_to_camera = np.vstack((T_velo_to_camera, temp))
            #cam0 parameter#
            # R_rect_00 = np.array([[9.999239e-01, 9.837760e-03, -7.445048e-03, 0],
            #                         [-9.869795e-03, 9.999421e-01, -4.278459e-03, 0],
            #                         [7.402527e-03, 4.351614e-03, 9.999631e-01, 0],
            #                         [0, 0, 0, 1]], np.float64)
            R_rect_00 = calib.R0
            R_rect_00 = np.hstack((R_rect_00, zeros))
            R_rect_00 = np.vstack((R_rect_00, temp))
            #cam0 to cam2 parameter#
            # P_rect_02 = np.array([[7.215377e+02, 0.000000e+00, 6.095593e+02, 4.485728e+01],
            #                         [0.000000e+00, 7.215377e+02, 1.728540e+02, 2.163791e-01],
            #                         [0.000000e+00, 0.000000e+00, 1.000000e+00, 2.745884e-03]], np.float64)      
            P_rect_02 = calib.P2
            P_rect_03 = calib.P3


            P2_velo_to_img = np.dot(np.dot(P_rect_02, R_rect_00), T_velo_to_camera)
            P3_velo_to_img = np.dot(np.dot(P_rect_03, R_rect_00), T_velo_to_camera)

            cloud_array_3d = cloud_array_ori.copy()
            cloud_array_3d[:, -1] = 1
            cloud_array_3d = cloud_array_3d.T
            cloud_array_3d = cloud_array_3d[:, cloud_array_3d[0]>0]
            # cloud_array = cloud_array[:, cloud_array[:, 0]>0]
            # cloud_mask = cloud_array_ori[:, 0] > 0
            cloud_array = cloud_array_ori[cloud_array_ori[:, 0]>0, :]
            # cloud_array = cloud_array_ori

            img_2_2d = np.dot(P2_velo_to_img, cloud_array_3d)
            img_2_2d_1 = (img_2_2d/img_2_2d[-1]).T
            # img_2_2d_1_to_0 = np.dot(P_rect_20, img_2_2d_1)

            img_3_2d = np.dot(P3_velo_to_img, cloud_array_3d)
            img_3_2d_1 = (img_3_2d/img_3_2d[-1]).T

            uvs_2 = img_2_2d_1[:, 0:-1]
            uvs_3 = img_3_2d_1[:, 0:-1]

            m_2 = (uvs_2[:, 0] < img_2.shape[1]) & (uvs_2[:, 0] >= 0) & (uvs_2[:, 1] < img_2.shape[0]) & (uvs_2[:, 1] >= 0)
            m_3 = (uvs_3[:, 0] < img_3.shape[1]) & (uvs_3[:, 0] >= 0) & (uvs_3[:, 1] < img_3.shape[0]) & (uvs_3[:, 1] >= 0)
            m_2_3 = m_2 & m_3
            m_3_single = np.bitwise_xor(m_3, m_2_3)
            m_2_3_u = m_2 | m_3_single
            # cloud_array = cloud_array[np.where(m_2_3_u)]

            cloud_array_2 = cloud_array[np.where(m_2)]
            cloud_array_3_single = cloud_array[np.where(m_3_single)]
            cloud_array = np.vstack((cloud_array_2, cloud_array_3_single))

            uvs_2 = uvs_2[np.where(m_2)].astype(np.int)
            # rgb_2 = img_2[list(uvs_2[:, 1]), list(uvs_2[:, 0])].reshape(-1, 1)
            rgb_2 = img_2[list(uvs_2[:, 1]), list(uvs_2[:, 0])].reshape(-1, 3)

            uvs_3 = uvs_3[np.where(m_3_single)].astype(np.int)
            # uvs_3 = uvs_3[np.where(m_3)].astype(np.int)
            # rgb_3 = img_3[list(uvs_3[:, 1]), list(uvs_3[:, 0])].reshape(-1, 1)
            rgb_3 = img_3[list(uvs_3[:, 1]), list(uvs_3[:, 0])].reshape(-1, 3)

            # uvs = np.vstack((uvs_2, uvs_3))
            rgb = np.vstack((rgb_2, rgb_3))
            # rgb = rgb[m_2]
            # rgb = rgb_2


            # mask_2 = np.where((uvs_2[:, 0] < img.shape[1]) & (uvs_2[:, 0] >= 0) &
            #                  (uvs_2[:, 1] < img.shape[0]) & (uvs_2[:, 1] >= 0))
            # mask_3 = np.where((uvs_3[:, 0] < img.shape[1]) & (uvs_3[:, 0] >= 0) &
            #                  (uvs_3[:, 1] < img.shape[0]) & (uvs_3[:, 1] >= 0))
            # mask_ = np.where((uvs_3[:, 0] < img.shape[1]) & (uvs_3[:, 0] >= 0) &
            #                  (uvs_3[:, 1] < img.shape[0]) & (uvs_3[:, 1] >= 0))            
            # cloud_array = cloud_array[mask_2]
            # uvs_2 = uvs_2[mask_2].astype(np.int)
            # rgb_2 = img[list(uvs_2[:, 1]), list(uvs_2[:, 0])]

            # uvs_3 = uvs_3[mask_3].astype(np.int)
            # rgb_3 = img_3[list(uvs_3[:, 1]), list(uvs_3[:, 0])]


            # show点云渲染到图片的效果
            # black = np.zeros_like(img)
            # black_3 = np.zeros((375, 1300))
            # black_3[list(uvs_3[:, 1]), list(uvs_3[:, 0])] = rgb_3.reshape(-1)
            # black_2 = np.zeros((375, 1300))
            # black_2[list(uvs_2[:, 1]), list(uvs_2[:, 0])] = rgb_2.reshape(-1)
            # black = np.zeros((375, 1300))
            # black[list(uvs[:, 1]), list(uvs[:, 0])] = rgb.reshape(-1)
            # cv2.imshow('black_3', black_3)
            # cv2.imshow('black_2', black_2)
            # cv2.imshow('black', black)
            # cv2.imshow('img', img)
            # cv2.waitKey(0)   

            # uvs = uvs_2
            # # img = img_2
            # for i in range(uvs.shape[0]):
            #     x = uvs[i, 0]
            #     y = uvs[i, 1]
            #     cv2.circle(img, (x, y), 1, (0, 0, 255), -1)
            # cv2.imshow('k', img)
            # cv2.waitKey(0)

            return cloud_array, uvs_2, rgb
            # return cloud_array_3d.T, img2d_1[:, 0:-1]
        else:
            return cloud_array_ori, None, None            

    def gamma_transform(self, img, gamma):
        is_gray = img.ndim == 2 or img.shape[1] == 1
        if is_gray:
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        illum = hsv[..., 2] / 255.
        illum = np.power(illum, gamma)
        v = illum * 255.
        v[v > 255] = 255
        v[v < 0] = 0
        hsv[..., 2] = v.astype(np.uint8)
        img = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
        if is_gray:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        return img

if __name__ == '__main__':
    from easydict import EasyDict as edict
    from data_process.transformation import OneOf, Random_Scaling, Random_Rotation, lidar_to_camera_box
    from utils.visualization_utils import merge_rgb_to_bev, show_rgb_image_with_boxes

    configs = edict()
    configs.distributed = False  # For testing
    configs.pin_memory = False
    configs.num_samples = None
    configs.input_size = (608, 608)
    configs.hm_size = (152, 152)
    configs.max_objects = 50
    configs.num_classes = 3
    configs.output_width = 608

    configs.dataset_dir = os.path.join('./', 'dataset', 'kitti')
    # lidar_aug = OneOf([
    #     Random_Rotation(limit_angle=np.pi / 4, p=1.),
    #     Random_Scaling(scaling_range=(0.95, 1.05), p=1.),
    # ], p=1.)
    lidar_aug = None

    dataset = KittiDataset(configs, mode='train', lidar_aug=lidar_aug, hflip_prob=0., num_samples=configs.num_samples)

    print('\n\nPress n to see the next sample >>> Press Esc to quit...')
    # for idx in range(len(dataset)):
    #     # if idx <= 9:
    #     #     continue
    #     bev_map, labels, img_rgb, img_path = dataset.draw_img_with_label(idx)
    #     print("img_path", img_path)
    #     calib = Calibration(img_path.replace(".png", ".txt").replace("image_2", "calib"))
    #     bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
    #     bev_map = cv2.resize(bev_map, (cnf.BEV_HEIGHT, cnf.BEV_WIDTH))

    #     for box_idx, (cls_id, x, y, z, h, w, l, yaw) in enumerate(labels):
    #         # Draw rotated box
    #         yaw = -yaw
    #         y1 = int((x - cnf.boundary['minX']) / cnf.DISCRETIZATION)
    #         x1 = int((y - cnf.boundary['minY']) / cnf.DISCRETIZATION)
    #         w1 = int(w / cnf.DISCRETIZATION)
    #         l1 = int(l / cnf.DISCRETIZATION)

    #         drawRotatedBox(bev_map, x1, y1, w1, l1, yaw, cnf.colors[int(cls_id)], int(cls_id))
    #     # Rotate the bev_map
    #     bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)

    #     _, labels[:, 1:] = lidar_to_camera_box(labels[:, 1:], calib.V2C, calib.R0, calib.P2)
    #     # img_rgb = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2BGR)
    #     img_rgb = show_rgb_image_with_boxes(img_rgb, labels, calib)

    #     out_img = merge_rgb_to_bev(img_rgb, bev_map, output_width=configs.output_width)
    #     cv2.imshow('bev_map', out_img)

    #     # if cv2.waitKey(0) & 0xff == 27:
    #     #     break
    #     if cv2.waitKey(0) & 0xff == 27:
    #         break
    #     elif cv2.waitKey(0) & 0xff == ord('1'):
    #         cv2.imwrite('/home/mayechi/桌面/write/1/'+str(idx)+'.bmp', out_img)

    # h_0_total, h_1_total, h_2_total = 0, 0, 0
    # w_0_total, w_1_total, w_2_total = 0, 0, 0
    # l_0_total, l_1_total, l_2_total = 0, 0, 0
    # object_num_0, object_num_1, object_num_2 = 0, 0, 0
    for idx in range(len(dataset)): 
        # if idx > 1000:
        #     break
        bev_map, labels = dataset.draw_lidar_with_label(idx)
        bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
        # bev_map = cv2.resize(bev_map, (cnf.BEV_HEIGHT, cnf.BEV_WIDTH))
        bev_map = cv2.resize(bev_map, (cnf.BEV_WIDTH, cnf.BEV_HEIGHT))
        bev_map_ori = bev_map.copy()

        for box_idx, (cls_id, x, y, z, h, w, l, yaw) in enumerate(labels):
            # if cls_id == 0.0:
            #     h_0_total += h
            #     w_0_total += w
            #     l_0_total += l
            #     object_num_0 += 1
            # elif cls_id == 1.0:
            #     h_1_total += h
            #     w_1_total += w
            #     l_1_total += l
            #     object_num_1 += 1
            # elif cls_id == 2.0:
            #     h_2_total += h
            #     w_2_total += w
            #     l_2_total += l
            #     object_num_2 += 1
            # continue
            # Draw rotated box
            yaw = -yaw
            y1 = int((x - cnf.boundary['minX']) / cnf.DISCRETIZATION_Y)
            x1 = int((y - cnf.boundary['minY']) / cnf.DISCRETIZATION_X)
            w1 = int(w / cnf.DISCRETIZATION_X)
            l1 = int(l / cnf.DISCRETIZATION_Y)

            drawRotatedBox(bev_map, x1, y1, w1, l1, yaw, cnf.colors[int(cls_id)], cls_id)
        # Rotate the bev_map
        bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
        bev_map_ori = cv2.rotate(bev_map_ori, cv2.ROTATE_180)
        cv2.imshow('bev_map', bev_map)
        cv2.imshow('bev_map_ori', bev_map_ori)
 
        if cv2.waitKey(0) & 0xff == 27:
            break
        elif cv2.waitKey(0) & 0xff == ord('1'):
            cv2.imwrite('/home/mayechi/桌面/write/1/'+str(idx)+'.bmp', bev_map_ori)
    # h_0_ave = h_0_total / object_num_0
    # h_1_ave = h_1_total / object_num_1
    # h_2_ave = h_2_total / object_num_2

    # w_0_ave = w_0_total / object_num_0
    # w_1_ave = w_1_total / object_num_1
    # w_2_ave = w_2_total / object_num_2

    # l_0_ave = l_0_total / object_num_0
    # l_1_ave = l_1_total / object_num_1
    # l_2_ave = l_2_total / object_num_2
    # a = 0