import numpy as np
import os
import random, string

# output_dir = './dataset/apollo/lidar_rcnn/data_processer_val/'
output_dir = './dataset/apollo/lidar_rcnn/data_processer/'
label_output_dir = os.path.join(output_dir, 'gt')
lidar_output_dir = os.path.join(output_dir, 'pc')
if not os.path.exists(label_output_dir):
    os.makedirs(label_output_dir)
if not os.path.exists(lidar_output_dir):
    os.makedirs(lidar_output_dir)

# label_apollo_path = './dataset/apollo/ImageSets/val_label.txt'
train_id_path = './dataset/zhijiangyihao/training2/pointcloud/ImageSets/train_id.txt'
lidar_dir = './dataset/zhijiangyihao/training2/pointcloud/bin/'
label_dir = './dataset/zhijiangyihao/training2/pointcloud/label/'
# label_apollo_path = './dataset/apollo/ImageSets/train_label.txt'
# label_apollo_path_list = [x[0:-1] for x in open(label_apollo_path).readlines()]
label_apollo_path_list = [label_dir+x[0:-1]+'.txt' for x in open(train_id_path).readlines()][:100]
lidar_apollo_path_list = [lidar_dir+x[0:-1]+'.bin' for x in open(train_id_path).readlines()][:100]

# lidar_apollo_path = './dataset/apollo/ImageSets/val.txt'
# lidar_apollo_path = './dataset/apollo/ImageSets/train.txt'
# lidar_apollo_path_list = [x[0:-1] for x in open(lidar_apollo_path).readlines()]

for i in range(len(label_apollo_path_list)):
    label_path = label_apollo_path_list[i]
    lidar_path = lidar_apollo_path_list[i]
    lidar = np.fromfile(lidar_path, dtype=np.float32).reshape(-1, 4)
    # Apollo Dataset
    # frame_name = lidar_path.split('/')[-2] + '_' + lidar_path.split('/')[-1].split('.bin')[0]
    # Zhijiangyihao Dataset
    frame_name = lidar_path.split('/')[-1].split('.bin')[0]
    frame_boxes = list()
    frame_types = list()
    frame_ids = list()
    for line in open(label_path, 'r'):
        line = line.rstrip()
        line_parts = line.split(' ')
        cat_id = int(line_parts[0])
        # if cat_id != 3:
        #     continue
        x, y, z = float(line_parts[1]), float(line_parts[2]), float(line_parts[3])
        l, w, h = float(line_parts[4]), float(line_parts[5]), float(line_parts[6])
        ry = float(line_parts[7])
        box_list = [x, y, z, l, w, h, ry]
        box_array = np.array(box_list)
        frame_boxes.append(box_array[np.newaxis, :])
        frame_types.append(cat_id)
        id = ''.join(random.sample(string.ascii_letters + string.digits, 8))
        frame_ids.append(id)
    gt_info = {'boxes': frame_boxes, 'ids': frame_ids, 'types': frame_types}
    label_output_path = os.path.join(label_output_dir, frame_name)
    lidar_output_path = os.path.join(lidar_output_dir, frame_name)
    np.savez_compressed(label_output_path, **gt_info)
    np.savez_compressed(lidar_output_path, pc=lidar)

