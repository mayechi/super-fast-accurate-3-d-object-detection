import torch
import yaml
from torch.utils.data import DataLoader
from torch.utils.data import DistributedSampler as _DistributedSampler
from easydict import EasyDict

from .utils import common_utils

#from .dataset import DatasetTemplate
#from .kitti.kitti_dataset import KittiDataset
from .nuscenes_dataset import NuScenesDataset

__all__ = {
    #'DatasetTemplate': DatasetTemplate,
    #'KittiDataset': KittiDataset,
    'NuScenesDataset': NuScenesDataset
}


class DistributedSampler(_DistributedSampler):

    def __init__(self, dataset, num_replicas=None, rank=None, shuffle=True):
        super().__init__(dataset, num_replicas=num_replicas, rank=rank)
        self.shuffle = shuffle

    def __iter__(self):
        if self.shuffle:
            g = torch.Generator()
            g.manual_seed(self.epoch)
            indices = torch.randperm(len(self.dataset), generator=g).tolist()
        else:
            indices = torch.arange(len(self.dataset)).tolist()

        indices += indices[:(self.total_size - len(indices))]
        assert len(indices) == self.total_size

        indices = indices[self.rank:self.total_size:self.num_replicas]
        assert len(indices) == self.num_samples

        return iter(indices)


def build_dataloader(configs, dataset_cfg_path, dist, root_path=None, logger=None, 
                     training=True, merge_all_iters_to_one_epoch=False, total_epochs=0):
    with open(dataset_cfg_path, 'r') as f:
        try:
            dataset_cfg = yaml.load(f, Loader=yaml.FullLoader)
        except:
            dataset_cfg = yaml.load(f)   
    dataset_cfg = EasyDict(dataset_cfg)
    class_names = dataset_cfg.CLASS_NAMES

    dataset = __all__[dataset_cfg.DATASET](
        configs=configs,
        dataset_cfg=dataset_cfg,
        class_names=class_names,
        root_path=root_path,
        training=training,
        logger=logger,
    )

    # if merge_all_iters_to_one_epoch:
    #     assert hasattr(dataset, 'merge_all_iters_to_one_epoch')
    #     dataset.merge_all_iters_to_one_epoch(merge=True, epochs=total_epochs)

    # if dist:
    #     if training:
    #         sampler = torch.utils.data.distributed.DistributedSampler(dataset)
    #     else:
    #         rank, world_size = common_utils.get_dist_info()
    #         sampler = DistributedSampler(dataset, world_size, rank, shuffle=False)
    # else:
    #     sampler = None
    sampler = None
    dataloader = DataLoader(dataset, batch_size=configs.batch_size, shuffle=(sampler is None),
                            pin_memory=configs.pin_memory, num_workers=configs.num_workers, sampler=sampler)
    # dataloader = DataLoader(
    #     dataset, batch_size=configs.batch_size, pin_memory=True, num_workers=workers,
    #     shuffle=(sampler is None) and training, collate_fn=dataset.collate_batch,
    #     drop_last=False, sampler=sampler, timeout=0
    # )

    return dataset, dataloader, sampler

