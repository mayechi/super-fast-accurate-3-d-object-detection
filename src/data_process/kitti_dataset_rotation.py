"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.08.17
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: This script for the KITTI dataset
"""

import sys
import os
import math
from builtins import int

import numpy as np
from torch.utils.data import Dataset
import cv2
import torch
import torch.nn.functional as F

sys.path.append('./src/')

from data_process.kitti_data_utils import gen_hm_radius, compute_radius, Calibration, get_filtered_lidar
from data_process.kitti_bev_utils import makeBEVMap, make_bev_ma, drawRotatedBox, get_corners, get_pts
from data_process import transformation
import config.kitti_config as cnf
# from ..utils import calibration_kitti

# import mayavi.mlab as mlab
# from visual_utils import visualize_utils as V
import pdb

class KittiRotationDataset(Dataset):
    def __init__(self, configs, mode='train', lidar_aug=None, hflip_prob=None, num_samples=None):
        self.dataset_dir = configs.dataset_dir
        self.input_size = configs.input_size
        self.hm_size = configs.hm_size

        self.num_classes = configs.num_classes
        self.max_objects = configs.max_objects

        assert mode in ['train', 'val', 'test', 'fusion_train', 'fusion_val'], 'Invalid mode: {}'.format(mode)
        self.mode = mode
        # self.is_test = (self.mode == 'test' or self.mode == 'val')
        self.is_test = (self.mode == 'test' or self.mode == 'fusion_train' or self.mode == 'fusion_val')
        
        # sub_folder = 'testing' if mode=='test' else 'training'
        sub_folder = 'training'

        self.lidar_aug = lidar_aug
        self.hflip_prob = hflip_prob

        self.image_2_dir = os.path.join(self.dataset_dir, sub_folder, "image_2")
        self.image_3_dir = os.path.join(self.dataset_dir, sub_folder, "image_3")
        self.lidar_dir = os.path.join(self.dataset_dir, sub_folder, "velodyne")
        # self.lidar_dir = os.path.join(self.dataset_dir, sub_folder, "aanet_predict_velodyne_rgb")
        # self.lidar_dir = os.path.join(self.dataset_dir, sub_folder, "disparity_velodyne")
        self.calib_dir = os.path.join(self.dataset_dir, sub_folder, "calib")
        self.label_dir = os.path.join(self.dataset_dir, sub_folder, "label_2")
        split_txt_path = os.path.join(self.dataset_dir, 'ImageSets', '{}.txt'.format(mode))
        self.sample_id_list = [int(x.strip()) for x in open(split_txt_path).readlines()]

        if num_samples is not None:
            self.sample_id_list = self.sample_id_list[:num_samples]
        self.num_samples = len(self.sample_id_list)
        self.bbox_w_standard = 5
        self.bbox_l_standard = 5

    def __len__(self):
        return len(self.sample_id_list)

    def __getitem__(self, index):
        # if self.is_test:
        if self.mode == 'test':
        # if 1:    
            return self.load_img_only(index)
        elif self.mode == 'fusion_train' or self.mode == 'fusion_val':
            return self.load_img_with_targets_fusion(index)
        else:
            return self.load_img_with_targets(index)

    def load_img_only(self, index):
        """Load only image for the testing phase"""
        sample_id = int(self.sample_id_list[index])
        img_path, img_2_rgb, img_3_rgb = self.get_image(sample_id)
        # 3通道压成1通道
        # img_2_gray = cv2.cvtColor(img_2_rgb, cv2.COLOR_BGR2GRAY)
        # img_3_gray = cv2.cvtColor(img_3_rgb, cv2.COLOR_BGR2GRAY)
        # 3通道直接使用
        img_2_gray = img_2_rgb
        img_3_gray = img_3_rgb
        calib = self.get_calib(sample_id)
        lidarData = self.get_lidar(sample_id)
        
        # img_2_gray = None
        # lidarData, uvs, rgb = self.velo2img(calib, lidarData, img_2_gray, img_3_gray)

        # base方案，点云投影到图像坐标系下，不用图像坐标系的 gray 
        rgb = None

        # 方案1，点云投影到图像坐标系下，用图像坐标系的 gray 
        # rgb = rgb

        lidarData, rgb = get_filtered_lidar(lidarData, cnf.boundary, None, rgb)

        bev_map = makeBEVMap(lidarData, cnf.boundary, rgb)
        # bev_map, bev_map_per_index = make_bev_ma(lidarData, cnf.boundary)
        bev_map = torch.from_numpy(bev_map)

        metadatas = {
            'img_path': img_path,
        }

        return metadatas, bev_map, img_2_rgb
        # return metadatas, bev_map, bev_map_per_index, img_2_rgb

    def load_img_with_targets(self, index):
        # print('index:', index)
        """Load images and targets for the training and validation phase"""
        sample_id = int(self.sample_id_list[index])
        img_path = os.path.join(self.image_2_dir, '{:06d}.png'.format(sample_id))
        lidarData = self.get_lidar(sample_id)
        img_path, img_2_rgb, img_3_rgb = self.get_image(sample_id)
        # 3通道压成1通道
        # img_2_gray = cv2.cvtColor(img_2_rgb, cv2.COLOR_BGR2GRAY)
        # img_3_gray = cv2.cvtColor(img_3_rgb, cv2.COLOR_BGR2GRAY)
        # 3通道直接使用
        img_2_gray = img_2_rgb
        img_3_gray = img_3_rgb

        calib = self.get_calib(sample_id)
        labels, has_labels = self.get_label(sample_id)
        if has_labels:
            labels[:, 1:] = transformation.camera_to_lidar_box(labels[:, 1:], calib.V2C, calib.R0, calib.P2)

        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])

        # rgb = lidarData[:, 3:]
        # lidarData = lidarData[:, 0:4]
        # img_2_gray = None
        # lidarData, uvs, rgb = self.velo2img(calib, lidarData, img_2_gray, img_3_gray)

        # base方案，点云投影到图像坐标系下，不用图像坐标系的 gray 
        rgb = None

        # 方案1，点云投影到图像坐标系下，用图像坐标系的 gray 
        # rgb = rgb

        lidarData, labels, rgb = get_filtered_lidar(lidarData, cnf.boundary, labels, rgb)

        bev_map = makeBEVMap(lidarData, cnf.boundary, rgb)
        # bev_map, bev_map_per_index = make_bev_ma(lidarData, cnf.boundary)

        # bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
        # cv2.imshow('bev_map', bev_map)
        # cv2.waitKey(0)      
        self.bev_map = bev_map.copy()
        self.bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
        self.bev_map = cv2.resize(self.bev_map, (608, 608))              
        bev_map = torch.from_numpy(bev_map)


        hflipped = False
        if np.random.random() < self.hflip_prob:
            hflipped = True
            # self.bev_map = cv2.flip(self.bev_map, 1)
            # C, H, W
            bev_map = torch.flip(bev_map, [-1])

        targets = self.build_targets(labels, hflipped)

        metadatas = {
            'img_path': img_path,
            'hflipped': hflipped
        }
        # print('bev_map shape:', bev_map.shape)
        # print('metadatas:', metadatas)
        # print('targets:', targets)

        return metadatas, bev_map, targets
        # return metadatas, bev_map, bev_map_per_index, targets

    def load_img_with_targets_fusion(self, index):
        """Load images and targets for the training and validation phase"""
        sample_id = int(self.sample_id_list[index])
        img_path = os.path.join(self.image_2_dir, '{:06d}.png'.format(sample_id))
        lidarData = self.get_lidar(sample_id)
        img_path, img_2_rgb, img_3_rgb = self.get_image(sample_id)
        # 3通道压成1通道
        # img_2_gray = cv2.cvtColor(img_2_rgb, cv2.COLOR_BGR2GRAY)
        # img_3_gray = cv2.cvtColor(img_3_rgb, cv2.COLOR_BGR2GRAY)
        # 3通道直接使用
        img_2_gray = img_2_rgb
        img_3_gray = img_3_rgb

        calib = self.get_calib(sample_id)
        labels, has_labels = self.get_label(sample_id)
        if has_labels:
            labels[:, 1:] = transformation.camera_to_lidar_box(labels[:, 1:], calib.V2C, calib.R0, calib.P2)

        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])

        # rgb = lidarData[:, 3:]
        # lidarData = lidarData[:, 0:4]
        # img_2_gray = None
        # lidarData, uvs, rgb = self.velo2img(calib, lidarData, img_2_gray, img_3_gray)

        # base方案，点云投影到图像坐标系下，不用图像坐标系的 gray 
        rgb = None

        # 方案1，点云投影到图像坐标系下，用图像坐标系的 gray 
        # rgb = rgb

        lidarData, labels, rgb = get_filtered_lidar(lidarData, cnf.boundary, labels, rgb)

        bev_map = makeBEVMap(lidarData, cnf.boundary, rgb)
        # bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
        # cv2.imshow('bev_map', bev_map)
        # cv2.waitKey(0)      
        bev_map = torch.from_numpy(bev_map)


        hflipped = False
        if np.random.random() < self.hflip_prob:
            hflipped = True
            # C, H, W
            bev_map = torch.flip(bev_map, [-1])

        # targets = self.build_targets(labels, hflipped)

        metadatas = {
            'img_idx': img_path.split('/')[-1].split('.png')[0],
            'img_path': img_path,
            'hflipped': hflipped
        }

        return metadatas, bev_map, img_2_rgb, labels

    def get_image(self, idx):
        # pdb.set_trace()
        img_path = os.path.join(self.image_2_dir, '{:06d}.png'.format(idx))
        img = cv2.imread(img_path)        
        if self.image_3_dir is not None:
            img_3_path = os.path.join(self.image_3_dir, '{:06d}.png'.format(idx))
            img_3 = cv2.imread(img_3_path)
            return img_path, img, img_3
        else:
            return img_path, img, None
        # img = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)


    def get_calib(self, idx):
        calib_file = os.path.join(self.calib_dir, '{:06d}.txt'.format(idx))
        # assert os.path.isfile(calib_file)
        return Calibration(calib_file)

    # def get_calib_by_mm(self, idx):
    #     calib_file = self.root_split_path / 'calib' / ('%s.txt' % idx)
    #     assert calib_file.exists()
    #     return calibration_kitti.Calibration(calib_file)

    def get_lidar(self, idx):
        lidar_file = os.path.join(self.lidar_dir, '{:06d}.bin'.format(idx))
        # assert os.path.isfile(lidar_file)
        return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)
        # return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 6)

    def get_label(self, idx):
        labels = []
        label_path = os.path.join(self.label_dir, '{:06d}.txt'.format(idx))
        for line in open(label_path, 'r'):
            line = line.rstrip()
            line_parts = line.split(' ')
            obj_name = line_parts[0]  # 'Car', 'Pedestrian', ...
            cat_id = int(cnf.CLASS_NAME_TO_ID[obj_name])
            if cat_id <= -99:  # ignore Tram and Misc
                continue
            truncated = int(float(line_parts[1]))  # truncated pixel ratio [0..1]
            occluded = int(line_parts[2])  # 0=visible, 1=partly occluded, 2=fully occluded, 3=unknown
            alpha = float(line_parts[3])  # object observation angle [-pi..pi]
            # xmin, ymin, xmax, ymax
            bbox = np.array([float(line_parts[4]), float(line_parts[5]), float(line_parts[6]), float(line_parts[7])])
            # height, width, length (h, w, l)
            h, w, l = float(line_parts[8]), float(line_parts[9]), float(line_parts[10])
            # location (x,y,z) in camera coord.
            x, y, z = float(line_parts[11]), float(line_parts[12]), float(line_parts[13])
            ry = float(line_parts[14])  # yaw angle (around Y-axis in camera coordinates) [-pi..pi]

            object_label = [cat_id, x, y, z, h, w, l, ry]
            # object_label = [cat_id, x, y, z, l, w, h, ry]
            labels.append(object_label)

        if len(labels) == 0:
            labels = np.zeros((1, 8), dtype=np.float32)
            has_labels = False
        else:
            labels = np.array(labels, dtype=np.float32)
            has_labels = True

        return labels, has_labels

    def build_targets(self, labels, hflipped):
        minX = cnf.boundary['minX']
        maxX = cnf.boundary['maxX']
        minY = cnf.boundary['minY']
        maxY = cnf.boundary['maxY']
        minZ = cnf.boundary['minZ']
        maxZ = cnf.boundary['maxZ']

        num_objects = min(len(labels), self.max_objects)
        hm_l, hm_w = self.hm_size
        hm_l_input, hm_w_input = self.input_size
        # hm_l, hm_w = hm_l*4, hm_w*4

        hm_main_center = np.zeros((self.num_classes, hm_l, hm_w), dtype=np.float32)
        cen_offset = np.zeros((self.max_objects, 2), dtype=np.float32)
        direction = np.zeros((self.max_objects, 2), dtype=np.float32)
        z_coor = np.zeros((self.max_objects, 1), dtype=np.float32)
        dimension = np.zeros((self.max_objects, 1), dtype=np.float32)
        img_dim = np.zeros((self.max_objects, 2), dtype=np.float32)
        wl = np.zeros((self.max_objects, 4), dtype=np.float32)
        wl_scale = np.zeros((self.max_objects, 4), dtype=np.float32)
        ## add
        # cls_theta = np.zeros((self.max_objects, 1), dtype=np.float32)

        indices_center = np.zeros((self.max_objects), dtype=np.int64)
        obj_mask = np.zeros((self.max_objects), dtype=np.uint8)

        for k in range(num_objects):
            cls_id, x, y, z, h, w, l, yaw = labels[k]
            cls_id = int(cls_id)
            # Invert yaw angle
            # print('yaw:', yaw)
            yaw = -yaw

            if not ((minX <= x <= maxX) and (minY <= y <= maxY) and (minZ <= z <= maxZ)):
                continue
            if (h <= 0) or (w <= 0) or (l <= 0):
                continue

            bbox_l = l / cnf.bound_size_x * hm_l
            bbox_w = w / cnf.bound_size_y * hm_w
            bbox_input_l = l / cnf.bound_size_x * hm_l_input
            bbox_input_w = w / cnf.bound_size_y * hm_w_input
            # l_scale = bbox_l / self.bbox_l_standard
            # w_scale = bbox_w / self.bbox_w_standard
            wl_scale[k, 0] = 10 / bbox_l
            wl_scale[k, 1] = 10 / bbox_l
            wl_scale[k, 2] = 10 / bbox_w
            wl_scale[k, 3] = 10 / bbox_w

            # bbox_input_l = int(bbox_input_l)
            # bbox_input_w = int(bbox_input_w)
            radius = compute_radius((math.ceil(bbox_l), math.ceil(bbox_w)))
            radius = max(0, int(radius))

            center_y = (x - minX) / cnf.bound_size_x * hm_l  # x --> y (invert to 2D image space)
            center_x = (y - minY) / cnf.bound_size_y * hm_w  # y --> x
            center = np.array([center_x, center_y], dtype=np.float32)

            center_input_y = (x - minX) / cnf.bound_size_x * hm_l_input  # x --> y (invert to 2D image space)
            center_input_x = (y - minY) / cnf.bound_size_y * hm_w_input  # y --> x
            center_input = np.array([center_input_x, center_input_y], dtype=np.float32)
            # center_input = center_input.astype(np.int32)

            if hflipped:
                center[0] = hm_w - center[0] - 1

            center_int = center.astype(np.int32)
            if cls_id < 0:
                ignore_ids = [_ for _ in range(self.num_classes)] if cls_id == - 1 else [- cls_id - 2]
                # Consider to make mask ignore
                for cls_ig in ignore_ids:
                    gen_hm_radius(hm_main_center[cls_ig], center_int, radius)
                hm_main_center[ignore_ids, center_int[1], center_int[0]] = 0.9999
                continue

            # Generate heatmaps for main center
            gen_hm_radius(hm_main_center[cls_id], center, radius)
            # Index of the center
            indices_center[k] = center_int[1] * hm_w + center_int[0]

            # targets for center offset
            cen_offset[k] = center - center_int

            # targets for dimension
            dimension[k, 0] = h
            # dimension[k, 1] = w
            # dimension[k, 2] = l

            # 旋转检测，将wh和yaw融合在一起
            pts = get_pts(center_input[0], center_input[1], bbox_input_w, bbox_input_l, yaw)
            pts = np.asarray(pts, np.float32)
            tl = pts[0, :]
            tr = pts[1, :]
            br = pts[2, :]
            bl = pts[3, :]
            # 通过顶点坐标得到目标在相机坐标系的角度
            # yaw_eval = math.atan2((tl[0] - bl[0]), (tl[1] - bl[1]))
            # print('yaw_eval:', yaw_eval) 
            tt = (np.asarray(tl, np.float32) + np.asarray(tr, np.float32))/2
            rr = (np.asarray(tr, np.float32) + np.asarray(br, np.float32))/2                      
            bb = 2*center_input - tt
            ll = 2*center_input - rr
            # yaw_eval = math.atan2((tt[0] - bb[0]), (tt[1] - bb[1]))
            # print('yaw_eval:', yaw_eval) 
            # 在已知中心点坐标的情况下，只计算2个顶点的坐标就可以
            # bb_ori = (np.asarray(bl, np.float32) + np.asarray(br, np.float32))/2
            # ll_ori = (np.asarray(tl, np.float32) + np.asarray(bl, np.float32))/2            
            # print('bb-bb_ori', bb-bb_ori)
            # print('ll-ll_ori', ll-ll_ori)
            # rotational channel
            wl[k, 0:2] = (tt - center_input) / 4
            wl[k, 2:4] = (rr - center_input) / 4
            # wl[k, 0:2] = (tt - center_input) * cnf.DISCRETIZATION
            # wl[k, 2:4] = (rr - center_input) * cnf.DISCRETIZATION
            # horizontal channel
            # w_hbbox, h_hbbox = cal_bbox_wh(pts)
            # wl[k, 4:6] = 1. * w_hbbox / 4, 1. * h_hbbox / 4
            # jaccard_score = self.ex_box_jaccard(pts.copy(), self.cal_bbox_pts(pts).copy())
            # if jaccard_score < 0.95:
            #     cls_theta[k, 0] = 1    

            # tt_x = center_x+wl[..., 0:1]
            # tt_y = center_y+wl[..., 1:2]
            # rr_x = center_x+wl[..., 2:3]
            # rr_y = center_y+wl[..., 3:4]
            # bb_x = 2*center_x - tt_x
            # bb_y = 2*center_y - tt_y
            # ll_x = 2*center_x - rr_x
            # ll_y = 2*center_y - rr_y
            # tt = torch.cat((torch.from_numpy(tt_x), torch.from_numpy(tt_y)), dim=-1)
            # rr = torch.cat((torch.from_numpy(rr_x), torch.from_numpy(rr_y)), dim=-1)
            # bb = torch.cat((torch.from_numpy(bb_x), torch.from_numpy(bb_y)), dim=-1)
            # ll = torch.cat((torch.from_numpy(ll_x), torch.from_numpy(ll_y)), dim=-1)
            # w = F.pairwise_distance(rr.view(-1, 2), ll.view(-1, 2))
            # w = w.view(batch_size, K, 1)
            # l = F.pairwise_distance(tt.view(-1, 2), bb.view(-1, 2))
            # l = l.view(batch_size, K, 1)            
            # rr = torch.from_numpy(rr)
            # ll = torch.from_numpy(ll)
            # tt = torch.from_numpy(tt)
            # bb = torch.from_numpy(bb)
            # w = F.pairwise_distance(rr.view(-1, 2), ll.view(-1, 2))
            # w = w.view(1, 50, 1)
            # l = F.pairwise_distance(tt.view(-1, 2), bb.view(-1, 2))
            # l = l.view(1, 50, 1)                        
            # print('center_int:', center_int)
            # print('tt:', tt)
            # print('rr:', rr)
            # print('bb:', bb)
            # print('ll:', ll)
            # cv2.line(self.bev_map, (center_int[0], center_int[1]), (int(tt[0]), int(tt[1])), (0, 0, 255), 1, 1)
            # cv2.line(self.bev_map, (center_int[0], center_int[1]), (int(rr[0]), int(rr[1])), (255, 0, 255), 1, 1)
            # cv2.line(self.bev_map, (center_int[0], center_int[1]), (int(bb[0]), int(bb[1])), (0, 255, 255), 1, 1)
            # cv2.line(self.bev_map, (center_int[0], center_int[1]), (int(ll[0]), int(ll[1])), (255, 0, 0), 1, 1)
         
            # targets for direction
            # direction[k, 0] = math.sin(float(yaw))  # im
            # direction[k, 1] = math.cos(float(yaw))  # re
            # im -->> -im
            # if hflipped:
            #     direction[k, 0] = - direction[k, 0]

            # targets for depth
            z_coor[k] = z - minZ

            # Generate object masks
            obj_mask[k] = 1
        # self.bev_map = cv2.rotate(self.bev_map, cv2.ROTATE_180)              
        # cv2.imshow('bev_map', self.bev_map)
        # cv2.waitKey(0)   
        targets = {
            'hm_cen': hm_main_center,
            'cen_offset': cen_offset,
            # 'direction': direction,
            'z_coor': z_coor,
            'wl': wl,
            'wl_scale': wl_scale,
            # 'h': h,
            'dim': dimension,
            # 'cls_theta':cls_theta,
            'indices_center': indices_center,
            'obj_mask': obj_mask,
        }

        return targets

    def draw_img_with_label(self, index):
        sample_id = int(self.sample_id_list[index])
        img_path, img_rgb, _ = self.get_image(sample_id)
        lidarData = self.get_lidar(sample_id)
        calib = self.get_calib(sample_id)
        labels, has_labels = self.get_label(sample_id)
        if has_labels:
            labels[:, 1:] = transformation.camera_to_lidar_box(labels[:, 1:], calib.V2C, calib.R0, calib.P2)

        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])

        lidarData, labels, _ = get_filtered_lidar(lidarData, cnf.boundary, labels)
        # V.draw_scenes(
        #     points=lidarData, ref_boxes=labels[:, 1:]
        # )
        # mlab.show(stop=True)

        bev_map = makeBEVMap(lidarData, cnf.boundary)

        return bev_map, labels, img_rgb, img_path

    def draw_lidar_with_label(self, index):
        print('index:', str(index))
        sample_id = int(self.sample_id_list[index])
        lidarData = self.get_lidar(sample_id)
        # lidarData[:, 3] = 1.0
        img_path, img_2_rgb, img_3_rgb = self.get_image(sample_id)
        # 3通道压成1通道
        # img_2_gray = cv2.cvtColor(img_2_rgb, cv2.COLOR_BGR2GRAY)
        # img_3_gray = cv2.cvtColor(img_3_rgb, cv2.COLOR_BGR2GRAY)
        # 3通道直接使用
        img_2_gray = img_2_rgb
        img_3_gray = img_3_rgb

        labels, has_labels = self.get_label(sample_id)
        calib = self.get_calib(sample_id)
        if has_labels:
            labels[:, 1:] = transformation.camera_to_lidar_box(labels[:, 1:], calib.V2C, calib.R0, calib.P2)

        if self.lidar_aug:
            lidarData, labels[:, 1:] = self.lidar_aug(lidarData, labels[:, 1:])
        
        # rgb = lidarData[:, 3:]
        # lidarData = lidarData[:, 0:4]

        # img_2_gray = None
        # lidarData, uvs, rgb = self.velo2img(calib, lidarData, img_2_gray, img_3_gray)

        # base方案，点云投影到图像坐标系下，不用图像坐标系的 gray 
        rgb = None

        # 方案1，点云投影到图像坐标系下，用图像坐标系的 gray 
        # rgb = rgb

        # # 方案2，针对类似虚拟点云投影的点，用bgr的特征
        # rgb = lidarData[:, 3:]
        # lidarData = lidarData[:, 0:4]

        lidarData, labels, rgb = get_filtered_lidar(lidarData, cnf.boundary, labels, rgb)

        bev_map = makeBEVMap(lidarData, cnf.boundary, rgb)
        # bev_map = make_bev_ma(lidarData, cnf.boundary)

        return bev_map, labels

    def velo2img(self, calib, cloud_array_ori, img_2=None, img_3=None):
        # img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        # img = self.gamma_transform(img, 0.3)
        if img_2 is not None:
            temp = np.array([0, 0, 0, 1])
            zeros = np.zeros([3, 1])
            #velo_to_camera cal parameter#
            # T_velo_to_camera = np.array([[7.533745e-03, -9.999714e-01, -6.166020e-04, -4.069766e-03],
            #                                 [1.480249e-02, 7.280733e-04, -9.998902e-01, -7.631618e-02],
            #                                 [9.998621e-01, 7.523790e-03, 1.480755e-02, -2.717806e-01],
            #                                 [0, 0, 0, 1]], np.float64)
            T_velo_to_camera = calib.V2C
            T_velo_to_camera = np.vstack((T_velo_to_camera, temp))
            #cam0 parameter#
            # R_rect_00 = np.array([[9.999239e-01, 9.837760e-03, -7.445048e-03, 0],
            #                         [-9.869795e-03, 9.999421e-01, -4.278459e-03, 0],
            #                         [7.402527e-03, 4.351614e-03, 9.999631e-01, 0],
            #                         [0, 0, 0, 1]], np.float64)
            R_rect_00 = calib.R0
            R_rect_00 = np.hstack((R_rect_00, zeros))
            R_rect_00 = np.vstack((R_rect_00, temp))
            #cam0 to cam2 parameter#
            # P_rect_02 = np.array([[7.215377e+02, 0.000000e+00, 6.095593e+02, 4.485728e+01],
            #                         [0.000000e+00, 7.215377e+02, 1.728540e+02, 2.163791e-01],
            #                         [0.000000e+00, 0.000000e+00, 1.000000e+00, 2.745884e-03]], np.float64)      
            P_rect_02 = calib.P2
            P_rect_03 = calib.P3


            P2_velo_to_img = np.dot(np.dot(P_rect_02, R_rect_00), T_velo_to_camera)
            P3_velo_to_img = np.dot(np.dot(P_rect_03, R_rect_00), T_velo_to_camera)

            cloud_array_3d = cloud_array_ori.copy()
            cloud_array_3d[:, -1] = 1
            cloud_array_3d = cloud_array_3d.T
            cloud_array_3d = cloud_array_3d[:, cloud_array_3d[0]>0]
            # cloud_array = cloud_array[:, cloud_array[:, 0]>0]
            # cloud_mask = cloud_array_ori[:, 0] > 0
            cloud_array = cloud_array_ori[cloud_array_ori[:, 0]>0, :]
            # cloud_array = cloud_array_ori

            img_2_2d = np.dot(P2_velo_to_img, cloud_array_3d)
            img_2_2d_1 = (img_2_2d/img_2_2d[-1]).T
            # img_2_2d_1_to_0 = np.dot(P_rect_20, img_2_2d_1)

            img_3_2d = np.dot(P3_velo_to_img, cloud_array_3d)
            img_3_2d_1 = (img_3_2d/img_3_2d[-1]).T

            uvs_2 = img_2_2d_1[:, 0:-1]
            uvs_3 = img_3_2d_1[:, 0:-1]

            m_2 = (uvs_2[:, 0] < img_2.shape[1]) & (uvs_2[:, 0] >= 0) & (uvs_2[:, 1] < img_2.shape[0]) & (uvs_2[:, 1] >= 0)
            m_3 = (uvs_3[:, 0] < img_3.shape[1]) & (uvs_3[:, 0] >= 0) & (uvs_3[:, 1] < img_3.shape[0]) & (uvs_3[:, 1] >= 0)
            m_2_3 = m_2 & m_3
            m_3_single = np.bitwise_xor(m_3, m_2_3)
            m_2_3_u = m_2 | m_3_single
            # cloud_array = cloud_array[np.where(m_2_3_u)]

            cloud_array_2 = cloud_array[np.where(m_2)]
            cloud_array_3_single = cloud_array[np.where(m_3_single)]
            cloud_array = np.vstack((cloud_array_2, cloud_array_3_single))

            uvs_2 = uvs_2[np.where(m_2)].astype(np.int)
            # rgb_2 = img_2[list(uvs_2[:, 1]), list(uvs_2[:, 0])].reshape(-1, 1)
            rgb_2 = img_2[list(uvs_2[:, 1]), list(uvs_2[:, 0])].reshape(-1, 3)

            uvs_3 = uvs_3[np.where(m_3_single)].astype(np.int)
            # uvs_3 = uvs_3[np.where(m_3)].astype(np.int)
            # rgb_3 = img_3[list(uvs_3[:, 1]), list(uvs_3[:, 0])].reshape(-1, 1)
            rgb_3 = img_3[list(uvs_3[:, 1]), list(uvs_3[:, 0])].reshape(-1, 3)

            # uvs = np.vstack((uvs_2, uvs_3))
            rgb = np.vstack((rgb_2, rgb_3))
            # rgb = rgb[m_2]
            # rgb = rgb_2


            # mask_2 = np.where((uvs_2[:, 0] < img.shape[1]) & (uvs_2[:, 0] >= 0) &
            #                  (uvs_2[:, 1] < img.shape[0]) & (uvs_2[:, 1] >= 0))
            # mask_3 = np.where((uvs_3[:, 0] < img.shape[1]) & (uvs_3[:, 0] >= 0) &
            #                  (uvs_3[:, 1] < img.shape[0]) & (uvs_3[:, 1] >= 0))
            # mask_ = np.where((uvs_3[:, 0] < img.shape[1]) & (uvs_3[:, 0] >= 0) &
            #                  (uvs_3[:, 1] < img.shape[0]) & (uvs_3[:, 1] >= 0))            
            # cloud_array = cloud_array[mask_2]
            # uvs_2 = uvs_2[mask_2].astype(np.int)
            # rgb_2 = img[list(uvs_2[:, 1]), list(uvs_2[:, 0])]

            # uvs_3 = uvs_3[mask_3].astype(np.int)
            # rgb_3 = img_3[list(uvs_3[:, 1]), list(uvs_3[:, 0])]


            # show点云渲染到图片的效果
            # black = np.zeros_like(img)
            # black_3 = np.zeros((375, 1300))
            # black_3[list(uvs_3[:, 1]), list(uvs_3[:, 0])] = rgb_3.reshape(-1)
            # black_2 = np.zeros((375, 1300))
            # black_2[list(uvs_2[:, 1]), list(uvs_2[:, 0])] = rgb_2.reshape(-1)
            # black = np.zeros((375, 1300))
            # black[list(uvs[:, 1]), list(uvs[:, 0])] = rgb.reshape(-1)
            # cv2.imshow('black_3', black_3)
            # cv2.imshow('black_2', black_2)
            # cv2.imshow('black', black)
            # cv2.imshow('img', img)
            # cv2.waitKey(0)   

            # uvs = uvs_2
            # # img = img_2
            # for i in range(uvs.shape[0]):
            #     x = uvs[i, 0]
            #     y = uvs[i, 1]
            #     cv2.circle(img, (x, y), 1, (0, 0, 255), -1)
            # cv2.imshow('k', img)
            # cv2.waitKey(0)

            return cloud_array, uvs_2, rgb
            # return cloud_array_3d.T, img2d_1[:, 0:-1]
        else:
            return cloud_array_ori, None, None            

    def gamma_transform(self, img, gamma):
        is_gray = img.ndim == 2 or img.shape[1] == 1
        if is_gray:
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        illum = hsv[..., 2] / 255.
        illum = np.power(illum, gamma)
        v = illum * 255.
        v[v > 255] = 255
        v[v < 0] = 0
        hsv[..., 2] = v.astype(np.uint8)
        img = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
        if is_gray:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        return img

    def ex_box_jaccard(self, a, b):
        a = np.asarray(a, np.float32)
        b = np.asarray(b, np.float32)
        inter_x1 = np.maximum(np.min(a[:,0]), np.min(b[:,0]))
        inter_x2 = np.minimum(np.max(a[:,0]), np.max(b[:,0]))
        inter_y1 = np.maximum(np.min(a[:,1]), np.min(b[:,1]))
        inter_y2 = np.minimum(np.max(a[:,1]), np.max(b[:,1]))
        if inter_x1>=inter_x2 or inter_y1>=inter_y2:
            return 0.
        x1 = np.minimum(np.min(a[:,0]), np.min(b[:,0]))
        x2 = np.maximum(np.max(a[:,0]), np.max(b[:,0]))
        y1 = np.minimum(np.min(a[:,1]), np.min(b[:,1]))
        y2 = np.maximum(np.max(a[:,1]), np.max(b[:,1]))
        mask_w = np.int(np.ceil(x2-x1))
        mask_h = np.int(np.ceil(y2-y1))
        mask_a = np.zeros(shape=(mask_h, mask_w), dtype=np.uint8)
        mask_b = np.zeros(shape=(mask_h, mask_w), dtype=np.uint8)
        a[:,0] -= x1
        a[:,1] -= y1
        b[:,0] -= x1
        b[:,1] -= y1
        mask_a = cv2.fillPoly(mask_a, pts=np.asarray([a], 'int32'), color=1)
        mask_b = cv2.fillPoly(mask_b, pts=np.asarray([b], 'int32'), color=1)
        inter = np.logical_and(mask_a, mask_b).sum()
        union = np.logical_or(mask_a, mask_b).sum()
        iou = float(inter)/(float(union)+1e-12)
        # cv2.imshow('img1', np.uint8(mask_a*255))
        # cv2.imshow('img2', np.uint8(mask_b*255))
        # k = cv2.waitKey(0)
        # if k==ord('q'):
        #     cv2.destroyAllWindows()
        #     exit()
        return iou
    def cal_bbox_pts(self, pts_4):
        x1 = np.min(pts_4[:,0])
        x2 = np.max(pts_4[:,0])
        y1 = np.min(pts_4[:,1])
        y2 = np.max(pts_4[:,1])
        bl = [x1, y2]
        tl = [x1, y1]
        tr = [x2, y1]
        br = [x2, y2]
        return np.asarray([bl, tl, tr, br], np.float32)
def decode_yaw(yaw):
    if yaw > 0:
        if yaw < math.pi/2:
            return math.pi/2 - yaw
        else:
            return yaw - math.pi/2
    else:
        if yaw < -math.pi/2:
            return math.pi/2 + yaw
        else:
            return yaw

def reorder_pts(tt, rr, bb, ll):
    pts = np.asarray([tt,rr,bb,ll],np.float32)
    l_ind = np.argmin(pts[:,0])
    r_ind = np.argmax(pts[:,0])
    t_ind = np.argmin(pts[:,1])
    b_ind = np.argmax(pts[:,1])
    tt_new = pts[t_ind,:]
    rr_new = pts[r_ind,:]
    bb_new = pts[b_ind,:]
    ll_new = pts[l_ind,:]
    return tt_new,rr_new,bb_new,ll_new

def cal_bbox_wh(pts_4):
    x1 = np.min(pts_4[:,0])
    x2 = np.max(pts_4[:,0])
    y1 = np.min(pts_4[:,1])
    y2 = np.max(pts_4[:,1])
    return x2-x1, y2-y1

if __name__ == '__main__':
    from easydict import EasyDict as edict
    from data_process.transformation import OneOf, Random_Scaling, Random_Rotation, lidar_to_camera_box
    from utils.visualization_utils import merge_rgb_to_bev, show_rgb_image_with_boxes

    configs = edict()
    configs.distributed = False  # For testing
    configs.pin_memory = False
    configs.num_samples = None
    configs.input_size = (608, 608)
    configs.hm_size = (152, 152)
    configs.max_objects = 50
    configs.num_classes = 3
    configs.output_width = 608

    configs.dataset_dir = os.path.join('./', 'dataset', 'kitti')
    # lidar_aug = OneOf([
    #     Random_Rotation(limit_angle=np.pi / 4, p=1.),
    #     Random_Scaling(scaling_range=(0.95, 1.05), p=1.),
    # ], p=1.)
    lidar_aug = None

    dataset = KittiRotationDataset(configs, mode='train', lidar_aug=lidar_aug, hflip_prob=0., num_samples=configs.num_samples)

    print('\n\nPress n to see the next sample >>> Press Esc to quit...')
    for idx in range(len(dataset)):
        bev_map, labels, img_rgb, img_path = dataset.draw_img_with_label(idx)
        print("img_path", img_path)
        calib = Calibration(img_path.replace(".png", ".txt").replace("image_2", "calib"))
        bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
        bev_map = cv2.resize(bev_map, (cnf.BEV_HEIGHT, cnf.BEV_WIDTH))
        bev_map_ori = bev_map.copy()
        valid_pts = []
        wl = torch.zeros((1, 50, 6))
        cls_theta = torch.zeros((1, 50, 1))
        for box_idx, (cls_id, x, y, z, h, w, l, yaw) in enumerate(labels):
            # Draw rotated box
            print('yaw:', yaw)
            yaw = -yaw
            # 将-pi-pi的角度转换成-p1/2-0
            # yaw = decode_yaw(yaw)          
            y1 = int((x - cnf.boundary['minX']) / cnf.DISCRETIZATION)
            x1 = int((y - cnf.boundary['minY']) / cnf.DISCRETIZATION)
            w1 = int(w / cnf.DISCRETIZATION)
            l1 = int(l / cnf.DISCRETIZATION)
            ct = torch.tensor([x1, y1])
            cen_x_int, cen_y_int = int(x1), int(y1)
            
            pts = get_pts(x1, y1, w1, l1, yaw)
            pts = torch.tensor(pts)
            tl = pts[0, :]
            tr = pts[1, :]
            br = pts[2, :]
            bl = pts[3, :]

            # 通过顶点坐标得到目标在相机坐标系的角度
            # yaw_eval = math.atan2((tl[0] - bl[0]), (tl[1] - bl[1]))
            # print('yaw_eval:', yaw_eval)                        
            tt = (torch.tensor(tl) + torch.tensor(tr))/2
            rr = (torch.tensor(tr) + torch.tensor(br))/2
            bb = 2*ct - tt
            ll = 2*ct - rr
            bb_ori = (torch.tensor(bl) + torch.tensor(br))/2
            ll_ori = (torch.tensor(tl) + torch.tensor(bl))/2
            # tt_tensor, bb_tensor = torch.from_numpy(tt).view(1, -1, 2), torch.from_numpy(bb).view(1, -1, 2)
            # rr_tensor, ll_tensor = torch.from_numpy(rr).view(1, -1, 2), torch.from_numpy(ll).view(1, -1, 2)
            # w = F.pairwise_distance(rr_tensor.view(-1, 2), ll_tensor.view(-1, 2))
            # l = F.pairwise_distance(tt_tensor.view(-1, 2), bb_tensor.view(-1, 2))
            # print('w:', w)
            # print('w1:', w1)
            # print('l:', l)
            # print('l1:', l1)
            # print('w1-w:', w1-w)
            # print('l1-l:', l1-l)
            # yaw_eval = torch.atan2((tt_tensor[:, :, 0] - bb_tensor[:, :, 0]), (tt_tensor[:, :, 1] - bb_tensor[:, :, 1]))
            # print('yaw_eval:', yaw_eval)     
                    
            # 在已知中心点坐标的情况下，只计算2个顶点的坐标就可以
            # print('bb-bb_ori', bb-bb_ori)
            # print('ll-ll_ori', ll-ll_ori)
            # rotational channel
            wl[0, box_idx, 0:2] = tt - ct            
            wl[0, box_idx, 2:4] = rr - ct
            # horizontal channel
            w_hbbox, h_hbbox = cal_bbox_wh(pts.numpy())
            wl[0, box_idx, 4:6] = torch.tensor([1. * w_hbbox, 1. * h_hbbox])
            jaccard_score = dataset.ex_box_jaccard(pts.numpy().copy(), dataset.cal_bbox_pts(pts.numpy()).copy())
            if jaccard_score < 0.95:
                cls_theta[0, box_idx, 0] = 1  
            


            mask = (cls_theta>0.8).float().view(1, 50, 1)
            tt_x = (x1+wl[..., 0:1])*mask + (x1)*(1.-mask)
            tt_y = (y1+wl[..., 1:2])*mask + (y1-wl[..., 5:6]/2)*(1.-mask)
            rr_x = (x1+wl[..., 2:3])*mask + (x1+wl[..., 4:5]/2)*(1.-mask)
            rr_y = (y1+wl[..., 3:4])*mask + (y1)*(1.-mask)
            bb_x = 2*x1 - tt_x
            bb_y = 2*y1 - tt_y
            ll_x = 2*x1 - rr_x
            ll_y = 2*y1 - rr_y
            tt = torch.cat((tt_x, tt_y), dim=2)
            rr = torch.cat((rr_x, rr_y), dim=2)
            bb = torch.cat((bb_x, bb_y), dim=2)
            ll = torch.cat((ll_x, ll_y), dim=2)

            yaw = torch.atan2((tt[:, :, 0] - bb[:, :, 0]), (tt[:, :, 1] - bb[:, :, 1]))
            yaw = yaw.view(1, 50, 1)

            w = F.pairwise_distance(rr.view(-1, 2), ll.view(-1, 2))
            w = w.view(1, 50, 1)
            l = F.pairwise_distance(tt.view(-1, 2), bb.view(-1, 2))
            l = l.view(1, 50, 1)
            # draw
            cv2.line(bev_map, (cen_x_int, cen_y_int), (int(tt[0]), int(tt[1])), (0, 0, 255), 1, 1)
            cv2.line(bev_map, (cen_x_int, cen_y_int), (int(rr[0]), int(rr[1])), (255, 0, 255), 1, 1)
            cv2.line(bev_map, (cen_x_int, cen_y_int), (int(bb[0]), int(bb[1])), (0, 255, 255), 1, 1)
            cv2.line(bev_map, (cen_x_int, cen_y_int), (int(ll[0]), int(ll[1])), (255, 0, 0), 1, 1)
            

        
            # rect = cv2.minAreaRect(pts)
            # cen_x, cen_y, bbox_w, bbox_h, theta = rect[0][0], rect[0][1], rect[1][0], rect[1][1], rect[2]
            # ct = np.asarray([cen_x, cen_y], dtype=np.float32)
            # cen_x_int, cen_y_int = int(cen_x), int(cen_y)
            # pts_new = cv2.boxPoints(((cen_x, cen_y), (bbox_w, bbox_h), theta))

            # bl = pts_new[0, :]
            # tl = pts_new[1, :]
            # tr = pts_new[2, :]
            # br = pts_new[3, :]

            # br_new = pts_new[0, :]
            # tr_new = pts_new[1, :]
            # tl_new = pts_new[2, :]
            # bl_new = pts_new[3, :]




            # if theta in [-90.0, -0.0, 0.0]:  # (-90, 0]
            #     tt,rr,bb,ll = reorder_pts(tt,rr,bb,ll)
            # rotational channel
            # wh[box_idx, 0:2] = tt - ct
            # wh[box_idx, 2:4] = rr - ct
            # wh[box_idx, 4:6] = bb - ct
            # wh[box_idx, 6:8] = ll - ct
            # horizontal channel
            # w_hbbox, h_hbbox = cal_bbox_wh(pts)
            # wh[box_idx, 8:10] = 1. * w_hbbox, 1. * h_hbbox

            # draw
            # cv2.line(bev_map, (cen_x_int, cen_y_int), (int(tt[0]), int(tt[1])), (0, 0, 255), 1, 1)
            # cv2.line(bev_map, (cen_x_int, cen_y_int), (int(rr[0]), int(rr[1])), (255, 0, 255), 1, 1)
            # cv2.line(bev_map, (cen_x_int, cen_y_int), (int(bb[0]), int(bb[1])), (0, 255, 255), 1, 1)
            # cv2.line(bev_map, (cen_x_int, cen_y_int), (int(ll[0]), int(ll[1])), (255, 0, 0), 1, 1)

            # cv2.line(bev_map, (cen_x_int, cen_y_int), (int(cen_x), int(cen_y-wh[box_idx, 9]/2)), (0, 0, 255), 1, 1)
            # cv2.line(bev_map, (cen_x_int, cen_y_int), (int(cen_x+wh[box_idx, 8]/2), int(cen_y)), (255, 0, 255), 1, 1)
            # cv2.line(bev_map, (cen_x_int, cen_y_int), (int(cen_x), int(cen_y+wh[box_idx, 9]/2)), (0, 255, 255), 1, 1)
            # cv2.line(bev_map, (cen_x_int, cen_y_int), (int(cen_x-wh[box_idx, 8]/2), int(cen_y)), (255, 0, 0), 1, 1)

            # cv2.line(bev_map, (int(tl[0]), int(tl[1])), (int(tr[0]), int(tr[1])), (0, 0, 255), 1, 1)
            # cv2.line(bev_map, (int(tr[0]), int(tr[1])), (int(br[0]), int(br[1])), (255, 0, 255), 1, 1)
            # cv2.line(bev_map, (int(br[0]), int(br[1])), (int(bl[0]), int(bl[1])), (0, 255, 255), 1, 1)
            # cv2.line(bev_map, (int(bl[0]), int(bl[1])), (int(tl[0]), int(tl[1])), (255, 0, 0), 1, 1)
            # cv2.putText(bev_map, '{}:{}'.format(valid_dif[i], self.category[valid_cat[i]]), (int(tl[0]), int(tl[1])), cv2.FONT_HERSHEY_TRIPLEX, 0.6,
            #             (0, 0, 255), 1, 1)

            drawRotatedBox(bev_map, x1, y1, w1, l1, yaw, cnf.colors[int(cls_id)], int(cls_id))
        # Rotate the bev_map
        bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
        bev_map_ori = cv2.rotate(bev_map_ori, cv2.ROTATE_180)
        cv2.imshow('bev_map', bev_map)
        cv2.imshow('bev_map_ori', bev_map_ori)
        cv2.waitKey(0)
