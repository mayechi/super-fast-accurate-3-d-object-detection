import det3d.core.sampler.preprocess as prep



def build_db_preprocess(db_prep_config, logger=None):
    cfg = db_prep_config
    if "filter_by_difficulty" in cfg:
        v = cfg["filter_by_difficulty"]
        return prep.DBFilterByDifficulty(v, logger=logger)
    elif "filter_by_min_num_points" in cfg:
        v = cfg["filter_by_min_num_points"]
        return prep.DBFilterByMinNumPoint(v, logger=logger)
    else:
        raise ValueError("unknown database prep type")