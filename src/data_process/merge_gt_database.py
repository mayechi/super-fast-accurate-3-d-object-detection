import pickle

db_all_path = '/media/myc/Data21/db_sample/dbinfos_train_zhijiangyihao.pkl'
db_add_path = '/media/myc/Data21/db_sample/dbinfos_train_zhijiangyihao_add.pkl'
db_out_path = '/media/myc/Data21/db_sample/dbinfos_train_zhijiangyihao_out.pkl'

with open(db_all_path, "rb") as f:
    db_all = pickle.load(f)

with open(db_add_path, "rb") as f:
    db_add = pickle.load(f) 

for key in db_all:
    db_all[key] = db_all[key] + db_add[key]

with open(db_out_path, "wb") as f:
    pickle.dump(db_all, f)