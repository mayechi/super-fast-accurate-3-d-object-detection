import numba
import math
import numpy as np

minX = -50
maxX = 50
minY = -25
maxY = 25
minZ = -2.28
maxZ = 0.72

Height = 1216
Width = 608
Deep = 10

@numba.jit(nopython=True)
def make_bev_voxel_no_i(PointCloud_, rgb=None):
    CLOUD_R = PointCloud_.shape[0]
    bev_map = np.zeros((4, 10, Height, Width))
    
    bev_map_per_index = np.zeros((Height, Width), dtype=np.int32)

    # voxel
    voxel_num = np.zeros((10, Height, Width), dtype=np.int32)
    voxel_all_feature = np.zeros((3, 10, Height, Width))
    voxel_ave_feature = np.zeros((4, 10, Height, Width))

    # 将点云按照z从高到低排序
    indices = np.argsort(-PointCloud_[:, 2])
    PointCloud_ = PointCloud_[indices]  
    range_list = list(range(CLOUD_R)) 
    random_shuffle_list = range_list

    for i in random_shuffle_list:
        x = PointCloud_[i, 0]
        y = PointCloud_[i, 1]
        z = PointCloud_[i, 2]
        z_ori = z + minZ

        row = Height * (x - minX) / (maxX - minX)
        col = Width * (y - minY) / (maxY - minY)
        r = int(math.floor(row))
        c = int(math.floor(col))

        deep = Deep * (z_ori - minZ) / (maxZ - minZ)
        d = int(math.floor(deep))

        if r >= Height or c >= Width or d >= Deep:
            continue
        bev_map_per_index[r, c] = 1

        voxel_num[d, r, c] += 1
        voxel_all_feature[0, d, r, c] += x
        voxel_all_feature[1, d, r, c] += y
        voxel_all_feature[2, d, r, c] += z

        voxel_ave_feature[0, d, r, c] = voxel_all_feature[0, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[1, d, r, c] = voxel_all_feature[1, d, r, c] / voxel_num[d, r, c]
        voxel_ave_feature[2, d, r, c] = voxel_all_feature[2, d, r, c] / voxel_num[d, r, c]

        # add density feature
        voxel_ave_feature[3, d, r, c] = np.minimum(1.0, np.log(voxel_num[d, r, c] + 1) / np.log(64))
    
    # 三个维度，可以翻转
    bev_map = voxel_ave_feature.reshape(-1, Height, Width)

    # 最原始的，占的显存比较大，空间换时间
    # bev_map = voxel_ave_feature.reshape(4, 10, Height*Width)

    # return bev_map_compress, bev_map_per_index
    return bev_map, bev_map_per_index #, bev_map_x_voxel
    # return bev_map