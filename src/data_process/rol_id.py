import os
import shutil
import sys
import numpy as np
sys.path.append('./src/')
import config.apollo_config as cnf

numpy_pcd_type_mappings = [(np.dtype('float32'), ('F', 4)),
                           (np.dtype('float64'), ('F', 8)),
                           (np.dtype('uint8'), ('U', 1)),
                           (np.dtype('uint16'), ('U', 2)),
                           (np.dtype('uint32'), ('U', 4)),
                           (np.dtype('uint64'), ('U', 8)),
                           (np.dtype('int16'), ('I', 2)),
                           (np.dtype('int32'), ('I', 4)),
                           (np.dtype('int64'), ('I', 8))]
numpy_type_to_pcd_type = dict(numpy_pcd_type_mappings)
pcd_type_to_numpy_type = dict((q, p) for (p, q) in numpy_pcd_type_mappings)

id_40_list = [
    78, 62, 72, 12, 53, 70, 79, 30, 68, 60, 
    21, 75,  4, 18, 42, 24, 15, 80, 11, 69,
    29, 56, 16,  5, 43, 3, 50, 10, 47, 59,
    20, 17, 41, 73, 54, 65,  1, 32, 14, 25,
    -1,
]

def get_filtered_lidar(lidar, boundary):
    minX = boundary['minX']
    maxX = boundary['maxX']
    minY = boundary['minY']
    maxY = boundary['maxY']
    minZ = boundary['minZ']
    maxZ = boundary['maxZ']

    # Remove the point out of range x,y,z
    mask = np.where((lidar[:, 0] >= minX) & (lidar[:, 0] <= maxX) &
                    (lidar[:, 1] >= minY) & (lidar[:, 1] <= maxY) &
                    (lidar[:, 2] >= minZ) & (lidar[:, 2] <= maxZ))
    lidar = lidar[mask]
    lidar[:, 2] = lidar[:, 2] - minZ

    return lidar

def makeBEVMap(PointCloud_, boundary, rgb=None):
    Height = cnf.BEV_HEIGHT + 1
    Width = cnf.BEV_WIDTH + 1

    # Discretize Feature Map
    PointCloud = np.copy(PointCloud_)
    # 针对Apollo数据集，检测360°
    PointCloud[:, 0] = np.int_(np.floor(PointCloud[:, 0] / cnf.DISCRETIZATION_Y) + Height / 2)
    PointCloud[:, 1] = np.int_(np.floor(PointCloud[:, 1] / cnf.DISCRETIZATION_X) + Width / 2)


    # sort-3times
    indices = np.lexsort((-PointCloud[:, 2], PointCloud[:, 1], PointCloud[:, 0]))
    PointCloud = PointCloud[indices]
    if rgb is not None:
        rgb = rgb[indices]

    # Height Map
    heightMap = np.zeros((Height, Width))

    _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
    PointCloud_frac = PointCloud[indices]
    # some important problem is image coordinate is (y,x), not (x,y)
    max_height = float(np.abs(boundary['maxZ'] - boundary['minZ']))

    heightMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = PointCloud_frac[:, 2] / max_height

    # Intensity Map & DensityMap
    intensityMap = np.zeros((Height, Width))
    densityMap = np.zeros((Height, Width))

    _, indices, counts = np.unique(PointCloud[:, 0:2], axis=0, return_index=True, return_counts=True)
    PointCloud_top = PointCloud[indices]

    normalizedCounts = np.minimum(1.0, np.log(counts + 1) / np.log(64))

    intensityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = PointCloud_top[:, 3] / 255.0 # hesai40p的反射强度0~255
    densityMap[np.int_(PointCloud_top[:, 0]), np.int_(PointCloud_top[:, 1])] = normalizedCounts

    if rgb is not None:
        # Height Map
        # ImgMap = np.zeros((Height, Width, 1))
        ImgMap = np.zeros((Height, Width, 3))
        _, indices = np.unique(PointCloud[:, 0:2], axis=0, return_index=True)
        PointCloud_frac = PointCloud[indices]        
        # rgb_frac = rgb[indices].reshape(-1, 1)
        rgb_frac = rgb[indices].reshape(-1, 3)
        ImgMap[np.int_(PointCloud_frac[:, 0]), np.int_(PointCloud_frac[:, 1])] = rgb_frac / 255.0
        ImgMap = np.transpose(ImgMap, (2, 0, 1))
        # RGB_Map = np.zeros((3, Height - 1, Width - 1))
        RGB_Map = np.zeros((3, Height - 1, Width - 1))
        
        RGB_Map[0:3, :, :] = ImgMap[:, :cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]    
    else:
        RGB_Map = np.zeros((3, Height - 1, Width - 1))
        RGB_Map[2, :, :] = densityMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # r_map
        RGB_Map[1, :, :] = heightMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # g_map
        # RGB_Map[0, :, :] = intensityMap[:cnf.BEV_HEIGHT, :cnf.BEV_WIDTH]  # b_map            

    return RGB_Map

def get_lidar(lidar_file):
    return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)

def cal_rol_id(pc):
    pc_new_list = []
    for i in range(pc.shape[0]):
        id = int(pc[i][-1] / 1000) + 1
        if id in id_40_list or id > 99:
            pc_new_list.append(pc[i])
    pc_new = np.stack(pc_new_list)
    return pc_new

def save_pointcloud_bin(pointcloud, save_path, measurement_time):
    # points = pointcloud.point
    points = pointcloud
    header = {
        'version': .7,
        'fields': ['x', 'y', 'z', 'intensity'],
        'count': [1, 1, 1, 1],
        'width': len(points),
        'height': 1,
        'viewpoint': [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
        'points': len(points),
        'type': ['F', 'F', 'F', 'U'],
        'size': [4, 4, 4, 4],
        'data': 'binary'
    }
    typenames = []
    for pcd_type, numpy_type in zip(header['type'], header['size']):
        numpy_type = pcd_type_to_numpy_type[(pcd_type, numpy_type)]
        typenames.append(numpy_type)
    np_dtype = np.dtype(list(zip(header['fields'], typenames)))
    arr = np.zeros(len(points), dtype=np_dtype)
    for i, point in enumerate(points):
        # change timestamp to timestamp_sec
        arr[i] = (
            point[0],# point.x,
            point[1],# point.y,
            point[2],# point.z,
            point[3],# point.intensity,
        )
    bin_file = os.path.join(
        save_path,
        measurement_time + '.bin')
    arr.tofile(bin_file)

# root_dir = '/media/myc/Data21/zhijiangyihao/training2/pointcloud/'
# lidar_dir_name = '5_8_truck_bin'
# lidar_dir = '/media/myc/Data21/zhijiangyihao/training2/pointcloud/' + lidar_dir_name + '/'
# save_path = root_dir + lidar_dir_name + '_sparse_40/'
# if not os.path.exists(save_path):         
#     os.makedirs(save_path)
# lidar_list = os.listdir(lidar_dir)
# lidar_list = sorted(lidar_list)
# for i in range(0, len(lidar_list)):
#     lidar_name = lidar_list[i]
#     pc = get_lidar(lidar_dir+lidar_name)
#     pc_new = cal_rol_id(pc)
#     lidar_name = lidar_name.split('.bin')[0] + '_sparse_40'
#     save_pointcloud_bin(pc_new, save_path, lidar_name)

    # 可视化点云
    # pc = get_filtered_lidar(pc, cnf.boundary)
    # bev_map_ori = makeBEVMap(pc, cnf.boundary)
    # bev_map_ori = (bev_map_ori.transpose(1, 2, 0) * 255).astype(np.uint8)
    # bev_map = cv2.resize(bev_map_ori, (cnf.BEV_WIDTH, cnf.BEV_HEIGHT))
    # bev_map_ori = cv2.rotate(bev_map_ori, cv2.ROTATE_180)
    # # bev_map_ori = cv2.resize(bev_map_ori, (500, 1000))

    # pc_new = get_filtered_lidar(pc_new, cnf.boundary)
    # bev_map = makeBEVMap(pc_new, cnf.boundary)
    # bev_map = (bev_map.transpose(1, 2, 0) * 255).astype(np.uint8)
    # bev_map = cv2.resize(bev_map, (cnf.BEV_WIDTH, cnf.BEV_HEIGHT))
    # bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
    # # bev_map = cv2.resize(bev_map, (500, 1000))
    # cv2.imshow('bev_map', bev_map)
    # cv2.imshow('bev_map_ori', bev_map_ori)
    # cv2.waitKey(0)

#处理label
root_dir = '/media/myc/Data21/zhijiangyihao/training2/pointcloud/'
label_dir_name = '5_0_bike_label'
label_dir = '/media/myc/Data21/zhijiangyihao/training2/pointcloud/' + label_dir_name + '/'
save_path = root_dir + label_dir_name + '_sparse_40/'
if not os.path.exists(save_path):         
    os.makedirs(save_path)
label_list = os.listdir(label_dir)
label_list = sorted(label_list)
for i in range(0, len(label_list)):
    label_name = label_list[i]
    label_name_new = label_name.split('.txt')[0] + '_sparse_40' + '.txt'
    shutil.copy(label_dir+label_name, save_path+label_name_new)
