"""
# -*- coding: utf-8 -*-
-----------------------------------------------------------------------------------
# Author: Nguyen Mau Dung
# DoC: 2020.08.17
# email: nguyenmaudung93.kstn@gmail.com
-----------------------------------------------------------------------------------
# Description: Demonstration script for the front view only
"""

import argparse
import sys
import os
import time
import warnings
import zipfile

warnings.filterwarnings("ignore", category=UserWarning)

import cv2
import torch
import numpy as np
from pathlib import Path

sys.path.append('./')

from data_process.demo_dataset import Demo_KittiDataset, DemoDataset_panda, DemoDataset_apollo
from models.model_utils import create_model
from utils.evaluation_utils import draw_predictions, convert_det_to_real_values
import config.kitti_config as cnf
from data_process.transformation import lidar_to_camera_box
from utils.visualization_utils import merge_rgb_to_bev, show_rgb_image_with_boxes
from data_process.kitti_data_utils import Calibration
from utils.demo_utils import parse_demo_configs, do_detect, download_and_unzip, write_credit

if __name__ == '__main__':
    configs = parse_demo_configs()

    model = create_model(configs)
    print('\n\n' + '-*=' * 30 + '\n\n')
    assert os.path.isfile(configs.pretrained_path), "No file at {}".format(configs.pretrained_path)
    model.load_state_dict(torch.load(configs.pretrained_path, map_location=lambda storage, loc: storage.cuda(0)))
    print('Loaded weights from {}\n'.format(configs.pretrained_path))

    configs.device = torch.device('cpu' if configs.no_cuda else 'cuda:{}'.format(configs.gpu_idx))
    model = model.to(device=configs.device)
    model.eval()

    out_cap = None
    demo_dataset = Demo_KittiDataset(configs)
    # demo_dataset = DemoDataset_apollo()
    # demo_dataset = DemoDataset_panda(data_path=configs.data_path)
    with torch.no_grad():
        for sample_idx, bev_map in enumerate(demo_dataset):
            # model(bev_map)
            t0 = time.time()
            detections, bev_map, fps = do_detect(configs, model, bev_map, is_front=True)
            t1 = time.time()
            print('inference time:', t1-t0)
    
            # Draw prediction in the image
            bev_map = (bev_map.permute(1, 2, 0).numpy() * 255).astype(np.uint8)
            bev_map = cv2.resize(bev_map, (cnf.BEV_WIDTH, cnf.BEV_HEIGHT))
            bev_map_copy = bev_map.copy()
            bev_map_copy = cv2.rotate(bev_map_copy, cv2.ROTATE_180)
            bev_map = draw_predictions(bev_map, detections, configs.num_classes)

            # Rotate the bev_map
            bev_map = cv2.rotate(bev_map, cv2.ROTATE_180)
            cv2.imshow('oir', bev_map_copy)
            cv2.imshow('result', bev_map)
            cv2.waitKey(0)

    cv2.destroyAllWindows()
